#include "pixel.h"
#include <stdlib.h>

pixel_t *createPixel(int x, int y)
{
    pixel_t *px = (pixel_t*)malloc(sizeof(pixel_t));
    if(px==NULL)return NULL;
    return px;
}

rgb_t* colorComputing(int x,int y, int size)
{
    rgb_t *rgb = (rgb_t*)malloc(sizeof(rgb_t));
    if(rgb==NULL) return NULL;
    rgb= createRgb(x%256, y%256, size%256);
    return rgb;
}
