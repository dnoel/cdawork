#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include "tree.h"
#include "node.h"
#include <math.h>
#include "pixel.h"
#include "data.h"
#include "../zPixel/screen.h"
#include "../zPixel/zPixel.h"


int computeZpixelSize(int pictureX, int pictureY)
{
    int res = 1; 
    int treshold = pictureX>=pictureY?pictureX:pictureY;
    while(res < treshold )
    {
        res = res*2;
    }

    return res;
}

gboolean getNodeByDegradation( GNode *node, gpointer data)
{
    printf("getNodebydegradation commence\n");
    printf("On lance getNode by degaradation\n");
    data_t *data2 = (data_t*)data;
    if(node==NULL) return FALSE;
    zpixel_t *zp= (zpixel_t *)(node->data);
    if(zp->degradation < data2->degradation) 
    {
        printf("On applique le zpixel au screen\n");
        printf("valeur rgb du zpixel : %d%d%d\n", zp->rgb->r, zp->rgb->g, zp->rgb->b);
        applyZpixel(data2->pictureTable, zp, data2->screen);
        printArray(data2->screen, data2->pictureTable);
        return FALSE;
    }
    return TRUE;
}

void parcourArbreDegradation(GNode *node, data_t *data)
{
    printf("parcoursArbreDegradation commence \n");
    if(node ==NULL)
    {
        printf("Arbre null \n"); 
        return;
    }
    
    g_node_traverse(node, G_POST_ORDER, G_TRAVERSE_ALL, -1, getNodeByDegradation, data);
    
    printArray(data->screen, data->pictureTable);
    printf("Fin du parcours de l'arbre\n");
}



double degradationComputing(int x,int y, int size)
{
    //return (size-1)*(1+sqrt((x-y)*(x-y)));
    return (size - 1) * (1 + abs(x-y));
}


int colorAndDegradationCompute(GNode *node, int x, int y, int size)
{
   // printf("adresse du noeud : %p\n", (void*)&(node));
    unsigned char r, g, b;
    int i=0;
    for(i=0; i<g_node_n_children(node); i++)
    {
        zpixel_t *zp = (zpixel_t*)(g_node_nth_child(node, i)->data);
        rgb_t *rgb = zp->rgb;
        r += rgb->r;
        g += rgb->g;
        b += rgb->b;
    }
        
  //  printf("Moyennage des valeurs rgb \n");
    if(i!=0)
    {
        r= (unsigned char)(((int)r)/i);
        g=(unsigned char)(((int)g)/i)/i;
        b=(unsigned char)(((int)b)/i);
    }
    
 //   printf("Création d'un nouveau rgb\n");
    rgb_t *rgb = createRgb(r,g,b);
    if(rgb==NULL) return 0;
    double degradation = degradationComputing(x,y,size);
    zpixel_t *zp = (zpixel_t*)(node->data);
    zp->rgb =rgb;
    zp->degradation=degradation;
}


GNode * createZpixelTree(int x,int y,int size, picture_t *pic)
{
    if(x>=pic->x || y>=pic->y) return NULL;

    zpixel_t *zp = createZPixelWithNoRgb(x,y,size);
    GNode *node = createNode(zp);

    if(size==1)
    {
        pixel_t *px = createPixel(x,y);
        px->rgb= colorComputing(zp->x, zp->y, zp->size);
        
        if(!initialiseZpixelNode(node, px))
        {
            free(zp);
            free(node);
            free(zp);
            return NULL;
        } 
        return node;
    }

    printf("Création du noeud haut gauche\n");
    GNode *upperLeftNode = createZpixelTree(x,y,size/2, pic);
    if(upperLeftNode != NULL)
    {
        if(!addSubNode(node, upperLeftNode))
        {
            printf("    !erreur fonction addSubNode() de upperLeftNodel!\n");
            free(upperLeftNode);
            free(node);
            free(zp);   
            return NULL;
        };
         printf("   noeud haut gauche crée\n");
    }
    else
    {
        printf("    Le noeud est en dehors de l'image \n");
    }
    
    printf("création du noeud haut droit\n");
    GNode *upperRightNode = createZpixelTree(x+size/2,y,size/2, pic);
    if(upperRightNode != NULL)
    {
        if(!addSubNode(node, upperRightNode))
        {
            printf("    !erreur fonction addSubNode() de upperRightNode!\n");
            free(upperRightNode);
            free(upperLeftNode);
            free(node);
            free(zp);
            return NULL;
        };
         printf("   noeud haut droit crée\n");
    }
    else
    {
        printf("    Le noeud est en dehors de l'image \n");
    }
        printf("Création du noeud bas gauche\n");
    GNode *downLeftNode = createZpixelTree(x,y+size/2,size/2, pic);
    if(downLeftNode != NULL)
    {
        if(!addSubNode(node, downLeftNode))
        {
            printf("    !erreur fonction addSubNode() de downLeftNode!\n");
            free(downLeftNode);
            free(upperRightNode);
            free(upperLeftNode);
            free(node);
            free(zp);   
            return NULL;
        };
         printf("   noeud bas gauche crée\n");
    }
    else
    {
        printf("    Le noeud est en dehors de l'image \n");
    }
    printf("Création du noeud bas droit\n");
    GNode *downRightNode = createZpixelTree(x+size/2,y+size/2,size/2, pic);
    if(downRightNode != NULL)
    {
        if(!addSubNode(node, downRightNode))
        {
            printf("    !erreur fonction addSubNode() de downRightNode!\n");
            free(downRightNode);
            free(downLeftNode);
            free(upperRightNode);
            free(upperLeftNode);
            free(node);
            free(zp);   
            return NULL;
        }
         printf("   noeud bas droit crée\n");
    }
    else
    {
        printf("    Le noeud est en dehors de l'image \n");
    }
    
    printf("On commence colorAndDegradationCompute\n");

    if(!colorAndDegradationCompute(node, x, y, size))
    {
        printf("    !erreur fonction colorAndDegradationCompute à la fin du remplissage de l'arbre\n");
        free(downRightNode);
        free(downLeftNode);
        free(upperRightNode);
        free(upperLeftNode);
        free(node);
        free(zp);
        exit(0); 
    }


printf("On a complété un noeud\n");
    return node;

}
