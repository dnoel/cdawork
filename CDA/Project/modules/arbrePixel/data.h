#ifndef DATA_H
#define DATA_H
#include "picture.h"
#include "../zPixel/screen.h"
typedef struct data
{
    unsigned char *pictureTable;
    screen_t *screen;
    int pictureX;
    int pictureY;
    double degradation;
    
} data_t;
data_t*  createData(unsigned char pictureTable[], double degradation, screen_t *screen);
#endif
