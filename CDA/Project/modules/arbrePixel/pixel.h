#ifndef PIXEL_H
#define PIXEL_H
#include "../zPixel/rgb.h"

typedef struct pixel
{
    int x,y;
    rgb_t *rgb;
} pixel_t;

pixel_t *createPixel(int x, int y);
rgb_t* colorComputing(int x,int y, int size);
#endif