#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include "node.h"
#include "pixel.h"


GNode * createNode(zpixel_t *zp)
{
    if(zp==NULL) return NULL;
    return (g_node_new(zp));
}

int addSubNode(GNode *sourceNode, GNode *newNode)
{
    if(sourceNode==NULL || newNode==NULL) return 0;
    g_node_append(sourceNode, newNode);
    return 1;
}

int initialiseZpixelNode(GNode *node, pixel_t *px)
{
    if(node==NULL || px==NULL) return 0;
    zpixel_t *zp = (zpixel_t *)(node->data);
    zp->rgb = px->rgb;
    return 1;
}
