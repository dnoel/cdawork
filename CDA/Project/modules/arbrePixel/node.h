#ifndef NODE_H
#define NODE_H
#include "../zPixel/zPixel.h"
#include "pixel.h"
#define SUBNODES_NB 4
#include <glib.h>

typedef struct node
{
    zpixel_t *zp;
    int nextNodeIndex;
    struct node *subNodes[SUBNODES_NB];
} node_t;

GNode * createNode(zpixel_t *zp);
void afficheNode(node_t *node);
int addSubNode(GNode *sourceNode, GNode *newNode);
int initialiseZpixelNode(GNode *node, pixel_t *px);
#endif
