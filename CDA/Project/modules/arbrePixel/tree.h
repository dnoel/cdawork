#ifndef ARBRE_H
#define ARBRE_H
#include "node.h"
#include "../zPixel/screen.h"
#include "picture.h"
#include "pixel.h"
#include <glib.h>
#include "data.h"
typedef struct tree
{
    node_t *rootNode;
    
} tree_t;
gboolean getNodeByDegradation( GNode *node, gpointer data);
void parcourArbreDegradation(GNode *node, data_t *data);
int computeZpixelSize(int pictureX, int pictureY);
tree_t* createTree();
GNode * createZpixelTree(int x,int y,int size, picture_t *pic);
rgb_t* colorComputing(int x,int y, int size);
double degradationComputing(int x,int y, int size);
int colorAndDegradationCompute(GNode *node, int x, int y, int size);
#endif
