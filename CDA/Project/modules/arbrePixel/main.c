#include <stdio.h>
#include <stdlib.h>
#include "node.h"
#include "../zPixel/zPixel.h"
#include "tree.h"
#include "picture.h"
#include "data.h"
#include "../zPixel/screen.h"

int main()
{
    
    printf("*****************************************\n");
    printf("TEST DE CREATION D'UN ARBRE DE ZPIXELS\n");
    printf("*****************************************\n");
    printf("\n");
    
    double degradation = 10;
   
    picture_t *pic  = createPicture(4,4);
    
    printf("Création d'une fausse image pour tester la création de l'arbre(x:%d/y:%d)\n", pic->x, pic->y); 
    screen_t *screen = createScreen(pic->x*3, pic->y*3);
    unsigned char *pictureTable = createArray(screen);
    data_t *data = createData(pictureTable, degradation, screen); 
    printf("On affiche l screen de data\n");
    printArray(data->screen, data->pictureTable);
    
    int zpixelSize = computeZpixelSize(pic->x, pic->y);
    printf("La largeur du premier zpixel est de %d\n", zpixelSize);

    printf("Création d'un arbre\n");
    GNode *rootNode = createZpixelTree(0,0, zpixelSize, pic);
    printf("arbre crée !\n"); 
    printf("affichage en fonction de la degradation de l'arbre\n");
    parcourArbreDegradation(rootNode, data);
}
