#include <stdio.h>
#include <stdlib.h>
#include "data.h"

data_t*  createData(unsigned char pictureTable[], double degradation, screen_t *screen)
{
    data_t *data = (data_t*)(malloc(sizeof(data_t)));
    data->pictureTable = pictureTable;
    data->screen = screen;
    data->pictureX;
    data->pictureY;
    data->degradation = degradation;
    return data;
}
