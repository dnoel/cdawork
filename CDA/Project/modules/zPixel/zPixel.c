#include <stdio.h>
#include <stdlib.h>
#include "zPixel.h"
#include <math.h>

void afficheZPixel(const zpixel_t *zp)
{
    
    if(zp==NULL) return;
    printf("{\n");
    printf("  size : %d,\n", zp->size);
    printf("  coordinate : %d, %d\n",  zp->x, zp->y);
    if(zp->rgb!=NULL) printf("  rgb : %d, %d, %d\n", zp->rgb->r, zp->rgb->g, zp->rgb->b);
    else
    {
        printf("  rgb : NULL\n");
    }
    printf("dégradation : %.2f\n", zp->degradation);

    
    printf("}\n");
}

zpixel_t* createZPixelWithNoRgb(int size, int x,int y)
{
    zpixel_t *zp = malloc(sizeof(zpixel_t)); 
    if(zp==NULL) return NULL;
    zp->size = size;
    zp->x=x;
    zp->y=y;
    return zp;
}

zpixel_t* createZPixel(int size, int x,int y, unsigned char r, unsigned char g, unsigned char b)
{
    zpixel_t *zp = malloc(sizeof(zpixel_t)); 
    if(zp==NULL) return NULL;
    zp->size = size;
    zp->x=x;
    zp->y=y;
    zp->rgb = createRgb(r,g,b);//rajouter etst null
    return zp;
}

zpixel_t* createBlackZPixel(int size, int x,int y)
{
    zpixel_t *zp = malloc(sizeof(zpixel_t)); 
    if(zp==NULL) return NULL;
    zp->size = size;
    zp->x=x;
    zp->y=y;
    zp->rgb = createBlackRgb();
    if(zp->rgb == NULL)
    {
        free(zp);
        return NULL;
    }
    return zp;
}

zpixel_t* createWhiteZPixel(int size, int x,int y)
{
    zpixel_t *zp = malloc(sizeof(zpixel_t)); 
    if(zp==NULL) return NULL;
    zp->size = size;
    zp->x=x;
    zp->y=y;
    zp->rgb = createWhiteRgb();
    if(zp->rgb == NULL)
    {
        free(zp);
        return NULL;
    }
    
    return zp;
}


int BrigthnessZPixel(const zpixel_t *zp)
{

    return ((maxRgb(zp->rgb) +minRgb(zp->rgb))/2);
}

int saturationZPixel(const zpixel_t *zp)
{
    double  max = (double)maxRgb(zp->rgb);
    double min = (double)minRgb(zp->rgb);
    double res;
    if(BrigthnessZPixel(zp) <128)
    {
        if(BrigthnessZPixel(zp) <=0) return 0;
        return 255*((max-min)/(max+min));
    }
    else
    { 
        return 255*((max-min)/(511-(max+min)));
    }
    return ((maxRgb(zp->rgb) +minRgb(zp->rgb))/2);
}

double distanceCouleurZPixel(const zpixel_t *zp1, const zpixel_t *zp2)
{
    double r = zp2->rgb->r - zp1->rgb->r;
    r = r*r;

    double g = zp2->rgb->g - zp1->rgb->g;
    g = g*g;

    double b = zp2->rgb->b - zp1->rgb->b;
    b = b*b;

    double res = sqrt(r+g+b);

    return res;

}

int moyenneRgbZPixel(const zpixel_t *zp)
{
    return (int)((zp->rgb->r + zp->rgb->g + zp->rgb->b)/3);

}