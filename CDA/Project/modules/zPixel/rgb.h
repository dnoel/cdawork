#ifndef RGB_H
#define RGB_H

struct rgb
{
    unsigned char r;
    unsigned char g;
    unsigned char b;

};

typedef struct rgb rgb_t;
rgb_t* createRgb(unsigned char r, unsigned char g, unsigned char b);
rgb_t* createBlackRgb();
rgb_t* createWhiteRgb();

int maxRgb(const rgb_t *rgb);
int minRgb(const rgb_t *rgb);
#endif