#include <stdio.h>
#include <stdlib.h>
#include "zPixel.h"
#include "screen.h"

int main()
{
    printf("***************************\n");
    printf("TEST DE CREATION DES PIXELS\n");
    printf("***************************\n");
    printf("\n");

    printf("Pixel de couleur quelconque\n");
    zpixel_t *zp= createZPixel(2,0,1,200,205,145);
    
    if(zp==NULL)
    {
        printf("erreur lors de la création du pixel\n");
        exit(0);
    }

    afficheZPixel(zp);
    printf("\n");

    printf("Pixel de couleur noire\n");
    zp = createBlackZPixel(2,0,1);
    if(zp==NULL)
    {
        printf("erreur lors de la création du pixel\n");
        exit(0);
    }
    afficheZPixel(zp);
    printf("\n");

    printf("Pixel de couleur blanche\n");
    zp = createWhiteZPixel(2,0,1);
    if(zp==NULL)
    {
        printf("erreur lors de la création du pixel\n");
        free(zp);
        exit(0);
    }
    afficheZPixel(zp);
    printf("\n");

    printf("*********************\n");
    printf("TEST DE LA LUMINOSITE\n");
    printf("*********************\n");
    printf("\n");

    printf("Pixel de couleur quelconque\n");
    zp= createZPixel(2,0,1,200,205,145);
    afficheZPixel(zp);
    printf("\n");

    printf("Test de la luminosité \n");
    if(BrigthnessZPixel(zp)!=175)
    {
        printf("Mauvais calcul de la luminosité\n");
        exit(0);
    }
    printf("Luminosité: %d\n", BrigthnessZPixel(zp));
    printf("\n");

    printf("*********************\n");
    printf("TEST DE LA SATURATION\n");
    printf("*********************\n");
    printf("\n");

    printf("Pixel de couleur noire zp\n");
    zp= createBlackZPixel(2,0,1);
    afficheZPixel(zp);
    printf("\n");

    printf("Luminosité de zp noir: %d\n", BrigthnessZPixel(zp));
    printf("Test de la saturation\n");
    if(saturationZPixel(zp)!=0)
    {
        printf("Mauvais calcul de la saturation\n");
        free(zp);
        exit(0);
    }
    printf("saturation de zp noir: %d\n", saturationZPixel(zp));

    printf("Pixel de couleur blanche zp\n");
    zp = createWhiteZPixel(3,5,1);
    afficheZPixel(zp);
    printf("\n");

    printf("Luminosité de zp blanc: %d\n", BrigthnessZPixel(zp));

    printf("Test de la saturation\n");
    if(saturationZPixel(zp)!=0)
    {
        printf("Mauvais calcul de la saturation\n");
        free(zp);
        exit(0);
    }
    printf("saturation de zp blanc: %d\n", saturationZPixel(zp));
    printf("\n");

    printf("Pixel de couleur quelconque zp\n");
    zp= createZPixel(2,0,1,200,205,145);
    afficheZPixel(zp);
    printf("\n");

    printf("Luminosité de zp quelconque: %d\n", BrigthnessZPixel(zp));
    printf("Test de la saturation\n");
    if(saturationZPixel(zp)!=95)
    {
        printf("Mauvais calcul de la saturation\n");
        free(zp);
        exit(0);
    }
    printf("saturation de zp quelconque: %d\n", saturationZPixel(zp));
    printf("\n");


    printf("********************************\n");
    printf("TEST DE LA DISTANCE DES COULEURS\n");
    printf("********************************\n");
    printf("\n");

    printf("Pixel de couleur noire zp\n");
    zp= createBlackZPixel(2,0,1);
    afficheZPixel(zp);

    printf("Pixel de couleur blanche zp2\n");
    zpixel_t *zp2 = createWhiteZPixel(3,5,1);
    afficheZPixel(zp2);

    if(441.67 >=distanceCouleurZPixel(zp, zp2) && distanceCouleurZPixel(zp, zp2)<=441.68)
    {
        printf("Mauvais calcul de la distance des couleurs\n");
        free(zp);
        free(zp2);
        exit(0);
    }
    printf("Distance entre les couleurs des deux points zp et zp2 : %f\n", distanceCouleurZPixel(zp, zp2));

    printf("\n");
    printf("*********************************\n");
    printf("TEST DE LA PROJECTION D'UN ZPIXEL\n");
    printf("*********************************\n");
    printf("\n");

    printf("Création d'un faux écran pour tester la projection du zPixel");    
    screen_t *screen  = createScreen(15,33);
    printf("screen : linestride : %d / rowstride : %d\n", screen->linestride, screen->rowstride);

    printf("Création d'un tableau représentatif de l'affichage de l'écran\n");
    unsigned char *array = createArray(screen);
    if(array==NULL)
    {
        printf("Echec lors de la création du tableau\n");
        free(screen);
        free(array);
        free(zp);
        free(zp2);
        exit(0);
    }
    printf("Affichage du tableau\n");
    printArray(screen, array);   
    printf("\n");
    printf("\n");

    printf("Création d'un zPIxel\n");
    zp = createZPixel(2, 0,0,1,2,3);
    afficheZPixel(zp);
    printf("\n");
    printf("Moyenne du zpixel : %d\n", moyenneRgbZPixel(zp));

    printf("Impression du zpixel sur le tableau (Atribution des valeurs rgb des pixels de l'écran, chaque cellule du tableau représente une cellule rgb)\n");
    applyZpixel(array, zp, screen);

    printf("Affichage du tableau modifié\n");
    printArray(screen, array);
    printf("\n");

    printf("Création d'un zPIxel\n");
    zp = createZPixel(3, 9,0,1,7,5);
    afficheZPixel(zp);
    printf("\n");
    
    printf("Impression du zpixel sur le tableau (Atribution des valeurs rgb des pixels de l'écran, chaque cellule du tableau représente une cellule rgb)\n");
    applyZpixel(array, zp, screen);

    printf("Affichage du tableau modifié\n");
    printArray(screen, array);

    printf("Création d'un zPIxel\n");
    zp = createZPixel(4, 3,5,7,5,2);
    afficheZPixel(zp);
    printf("\n");

    printf("Impression du zpixel sur le tableau (Atribution des valeurs rgb des pixels de l'écran, chaque cellule du tableau représente une cellule rgb)\n");
    applyZpixel(array, zp, screen);

    printf("Affichage du tableau modifié\n");
    printArray(screen, array);
    printf("\n");

    printf("Création d'un zPIxel\n");
    zp = createZPixel(1, 10,14,5,5,5);
    afficheZPixel(zp);
    printf("\n");

    printf("Impression du zpixel sur le tableau (Atribution des valeurs rgb des pixels de l'écran, chaque cellule du tableau représente une cellule rgb)\n");
    applyZpixel(array, zp, screen);

    printf("Affichage du tableau modifié\n");
    printArray(screen, array);
    printf("\n");

    printf("Création d'un zPIxel\n");
    zp = createZPixel(4, 9,6,7,7,7);
    afficheZPixel(zp);
    printf("\n");

    printf("Impression du zpixel sur le tableau (Atribution des valeurs rgb des pixels de l'écran, chaque cellule du tableau représente une cellule rgb)\n");
    applyZpixel(array, zp, screen);

    printf("Affichage du tableau modifié\n");
    printArray(screen, array);
    printf("\n");


}