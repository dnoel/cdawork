#include <stdlib.h>
#include <stdio.h>
#include "screen.h"


screen_t * createScreen(const int linestride, const int rowstride)
{
    screen_t * screen = (screen_t*)malloc(sizeof(screen_t*));
    if(screen==NULL)
    {
        return NULL;
    }

    screen->linestride = linestride;
    screen->rowstride = rowstride;

    return screen;
}

unsigned char* createArray(const screen_t* screen)
{
    unsigned char *array = (unsigned char *)malloc((screen->linestride*screen->rowstride)*sizeof(unsigned char));
    if(array==NULL) return NULL;
    return array;
}

// void printArray(const screen_t *screen , const unsigned char *array)
// {
//     printf("SCREEN : \n");

//      for(int i = 0; i<screen->rowstride; i++)
//      {
//          if(i>=screen->rowstride/2)
//          {
//              printf("%d",screen->rowstride);
//              break;
//          }
//          printf("   ");
//      }
//      printf("\n");

//     printf("<");
//     for(int i = 1; i<screen->rowstride-1; i++) printf("---");
//     printf("---->\n");

    
//     for(int i=0; i<(screen->linestride); i++)
//     {
       
        
//         for(int j=0; j<screen->rowstride;j++)
//         {
//             printf("[%d]", (int)*(array+(i*screen->rowstride)+j));

//         }

        
//         if(i==screen->linestride/2)
//         {
//             printf("  |%d",screen->linestride);
//         }
//         else
//         {
//             if(i==0) printf("  ^");
//             if(0<i)
//             {
//                 if(i<screen->linestride-1)  printf("  |");
//                 else
//                 {
//                     printf("  v");
//                 }
//             }
           
             
//             // if(i==screen->linestride);
            
//         }
//         printf("\n");
//     }

// }

void printArray(const screen_t *screen , const unsigned char *array)
{
    printf("width : %d/ height : %d\n", screen->rowstride, screen->linestride);
    for(int i=0; i<screen->linestride; i++)
    {
        for(int j=0; j<screen->rowstride;j++)
        {
            printf("[%d]", (int)*(array+(i*screen->rowstride)+j));
        }
        printf("\n");
    }
    
}

void applyZpixel(unsigned char *array, const zpixel_t *zpixel, const screen_t *screen)
{
    //if the zpixel is located outside of the screen we get out of the function
    if(zpixel->x*3 > screen->rowstride || zpixel->y > screen->linestride) return;
    //Pour i du premier pixel de zPixel au nombre de pixel total de l'écran 
    int i=0;
    if(zpixel->y!=0) i=i+(screen->rowstride)*(zpixel->y);
    if(zpixel->x!=0) i=i+zpixel->x*3;

    //creation of a limit that our loop shall not cross
    // limit is weather the zpixel's last pixel's adress or the screen's last pixel's adress
    //if @zpixelLastPixel  < @screenLastPixel -> limite = @zpixelLastPixel ELSE limite = @screenLastPixel

    int limite = i+(zpixel->size*3)+(screen->rowstride*(zpixel->size-1)); //zpîxel last pixel adress
    int nbPixel = screen->linestride*screen->rowstride; //screen lastpixel adress
    if(limite >nbPixel) limite = nbPixel;//we test which one is the shorter
    
    while(i<=limite)
    {
        for(int j=0;j<zpixel->size*3; j=j+3)
        {
            if(zpixel->x*3 + j+3 <= screen->rowstride)
            {
                *(array+i+j) = zpixel->rgb->r;
                *(array+i+j+1) = zpixel->rgb->g;
                *(array+i+j+2) = zpixel->rgb->b;
            }
            else
            {
                break;
            }  
        }
        i=i+screen->rowstride;
    }
}