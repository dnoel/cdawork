#include <stdlib.h>
#include <stdio.h>
#include "rgb.h"

rgb_t* createRgb(unsigned char r, unsigned char g, unsigned char b)
{
    rgb_t *rgb = malloc(sizeof(rgb_t));
    if(rgb==NULL)return NULL;
    rgb->r = r;
    rgb->g = g;
    rgb->b = b;
    return rgb;

}

rgb_t* createBlackRgb()
{
    rgb_t *rgb = malloc(sizeof(rgb_t));
    if(rgb==NULL)return NULL;
    rgb->r = 0;
    rgb->g = 0;
    rgb->b = 0;
    return rgb;

}

rgb_t* createWhiteRgb()
{
    rgb_t *rgb = malloc(sizeof(rgb_t));
    if(rgb==NULL)return NULL;

    rgb->r = 255;
    rgb->g = 255;
    rgb->b = 255;
    return rgb;

}

int maxRgb(const rgb_t *rgb)
{
    if(rgb->r >= rgb->g)
    {
        if(rgb->r >= rgb->b)return rgb->r;
        return rgb->b;
    }
    else
    {
        if(rgb->g >= rgb->b)return rgb->g;
        return rgb->b;
    }
    
}

int minRgb(const rgb_t *rgb)
{
    if(rgb->r <= rgb->g)
    {
        if(rgb->r <= rgb->b)return rgb->r;
        return rgb->b;
    }
    else
    {
        if(rgb->g <= rgb->b)return rgb->g;
        return rgb->b;
    }
    
}

