#ifndef SCREEN_H
#define SCREEN_H
#include "zPixel.h"

struct screen
{
    int linestride;
    int rowstride;
};
typedef struct screen screen_t;

screen_t * createScreen(const int linestride, const int rowstride);
unsigned char* createArray(const screen_t* screen);
void printArray(const screen_t *screen , const unsigned char *array);
void applyZpixel(unsigned char *array, const zpixel_t *zpixel, const screen_t *screen);

#endif