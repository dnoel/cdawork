#ifndef ZPIXEL_H
#define ZPIXEL_H
#include "rgb.h"
struct zpixel
{
    int size;
    int x;
    int y;
    rgb_t *rgb;
    double degradation;
};

typedef struct zpixel zpixel_t;


zpixel_t* createZPixelWithNoRgb(int size, int x,int y);
zpixel_t* createZPixel(int size, int x,int y, unsigned char r, unsigned char g, unsigned char b);
zpixel_t* createBlackZPixel(int size, int x,int y);
zpixel_t* createWhiteZPixel(int size, int x,int y);

void afficheZPixel(const zpixel_t *zp);
int BrigthnessZPixel(const zpixel_t *zp);
int saturationZPixel(const zpixel_t *zp);
double distanceCouleurZPixel(const zpixel_t *zp1, const zpixel_t *zp2);
int moyenneRgbZPixel(const zpixel_t *zp);
#endif