#include <stdio.h>
#include <stdlib.h>
#include "node.h"
#include "pixel.h"
#include "tree.h"

void afficheNode(node_t *node)
{
    printf("NOde d'adresse %p", (void*)&node);
    printf("{\n");
    printf("    zpixel :\n");
    afficheZPixel(node->zp);
    printf("nextNodeIndex : %d\n", node->nextNodeIndex);
    printf(" nodeTable : ");

    int i=0;
    while(i<node->nextNodeIndex)
    {
        printf("[%p]", (void*)&*(node->subNodes+i));
        i++;
    }
    if(node->nextNodeIndex<SUBNODES_NB)
    {
        i=node->nextNodeIndex;
        while(i<SUBNODES_NB)
        {
            printf("[NULL]");
            i++;
        }
        printf("\n");
    }
    printf("\n");
    printf("}\n");
}

node_t* createNode(zpixel_t *zp)
{
    node_t *node = (node_t*)malloc(sizeof(node_t*));
    if(node==NULL) return NULL;
    node->zp = zp;
    node->nextNodeIndex=0;
    return node;
}

// zpixel_t *zp createNodeZPixelWithPixel(pixel_t *px)
// {
    
// }

int addSubNode(node_t *sourceNode, node_t *newNode)
{
    if(sourceNode->nextNodeIndex<SUBNODES_NB)
    {
        *(sourceNode->subNodes+sourceNode->nextNodeIndex++) = newNode;
        return 1;
    }
    return 0;
}

int initialiseZpixelNode(node_t *node, pixel_t *px)
{       
    if(node==NULL || px==NULL) return 0;
    node->zp->rgb = px->rgb;
    node->zp = createZPixel(1,px->x, px->y, px->rgb->r, px->rgb->g, px->rgb->b);
    node->zp->degradation = 0;
    return 1;
}

