#ifndef PICTURE_H
#define PICTURE_H

typedef struct picture
{
    int width,height;
} picture_t;

picture_t * createPicture(int width,int height);
#endif
