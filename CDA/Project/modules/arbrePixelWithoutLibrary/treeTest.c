#include <stdio.h>
#include <stdlib.h>
#include "node.h"
#include "../zPixel/zPixel.h"
#include "tree.h"
#include "picture.h"
#include "data.h"
#include "../zPixel/screen.h"


int main()
{
    // printf("***************************\n");
    // printf("TEST DE CREATION DES NOEUDS\n");
    // printf("***************************\n");
    // printf("\n");

    // printf("Création d'un zpixel zp\n");

    // zpixel_t *zp = createZPixelWithNoRgb(1,2,3);
    // if(zp==NULL)
    // {
    //     printf("erreur lors de la création du pixel\n");
    //     exit(0);
    // }
    // printf("zpixel crée : ");
    // afficheZPixel(zp);
    // printf("\n");

    // printf("Création d'un noeud node  avec comme zpixel zp \n");
    // node_t *node = createNode(zp);
    // if(node==NULL)
    // {
    //     printf("erreur lors de la création du noeud\n");
    //     free(zp);
    //     exit(0);
    // }
    // printf("node crée : \n");
    // afficheNode(node);
    // printf("\n");

    // printf("*****************************************\n");
    // printf("TEST D'AJOUT DE SOUS-NOEUDS DANS UN NOEUD\n");
    // printf("*****************************************\n");
    // printf("\n");    // printf("Création d'un nouveau rgb\n");


    // printf("Création d'un zpixel zp2\n");
    // zpixel_t *zp2 = createZPixelWithNoRgb(3,4,5);
    // if(zp2==NULL)
    // {
    //     printf("erreur lors de la création du pixel\n");
    //     free(zp);
    //     exit(0);
    // }
    // printf("zPixel zp2 crée : ");    
    // afficheZPixel(zp2);
    // printf("\n");

    // printf("Création d'un noeud node  avec comme zpixel zp \n");
    // node = createNode(zp);
    // if(node==NULL)
    // {
    //     printf("erreur lors de la création du noeuf\n");
    //     free(zp);
    //     free(zp2);
    //     exit(0);
    // }
    // printf("node crée : \n");
    // afficheNode(node);
    // printf("\n");

    // printf("Création d'un noeud node2  avec comme zpixel zp2 \n");
    // node_t *node2 = createNode(zp2);
    // if(node2==NULL)
    // {
    //     printf("erreur lors de la création du node\n");
    //     free(zp);
    //     free(zp2);
    //     free(node);
    //     exit(0);
    // }
    // printf("node crée : \n");
    // afficheNode(node2);
    // printf("\n");

    // printf("Test de l'ajouter de node2 en tant que subNode de node \n");

    // if(!addSubNode(node, node2)) 
    // {
    //     printf("Impossible d'ajouter le node ");
    // }
    // else
    // {
    //     printf("node ajouté ");
    // }
    

    // printf("affichage de node : \n");
    // afficheNode(node);
    // printf("\n");

    // printf("Ajout de node2 4 fois supplémentaires dans node\n");
    // for(int i=0; i<SUBNODES_NB; i++)
    // {
    //     printf("    Ajout %d : ", i+1);
    //     if(!addSubNode(node, node2)) 
    //     {
    //         printf("Impossible d'ajouter le node\n");
    //     }
    //     else
    //     {
    //         printf("node ajouté\n");
    //     }
    // }

    // printf("affichage de node : \n");
    // afficheNode(node);
    // printf("\n");

    // free(zp);
    // free(zp2);
    // free(node);

    printf("*****************************************\n");
    printf("TEST DE CREATION D'UN ARBRE DE ZPIXELS\n");
    printf("*****************************************\n");
    printf("\n");
    
    double degradation = 2;
   
    picture_t *pic  = createPicture(4,4);
    
    printf("Création d'une fausse image pour tester la création de l'arbre(x:%d/y:%d)\n", pic->width, pic->height); 
    screen_t *screen = createScreen(pic->height, pic->width*3);
    unsigned char *pictureTable = createArray(screen);
    data_t *data = createData(pictureTable, degradation, screen); 
    // printf("On affiche l screen de data\n");
    // printArray(data->screen, data->pictureTable);
    
    int zpixelSize = computeZpixelSize(pic->width, pic->height);
    printf("La largeur du premier zpixel est de %d\n", zpixelSize);

    printf("Création d'un arbre\n");
    node_t *rootNode = createZpixelTree(0,0, zpixelSize, pic);
    printf("arbre crée !\n"); 
    // applyZpixel(data->pictureTable, rootNode->zp, data->screen);
    // printArray(data->screen, data->pictureTable);
    printf("affichage en fonction de la degradation de l'arbre\n");
    treeBrowsingDegradation(rootNode, data);
    

    printf("On affcihe le tableau\n");
    printArray(data->screen, data->pictureTable);
}