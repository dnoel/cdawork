#ifndef ARBRE_H
#define ARBRE_H
#include "node.h"
#include "../zPixel/screen.h"
#include "picture.h"
#include "pixel.h"
#include "data.h"
typedef struct tree
{
    node_t *rootNode;
    
} tree_t;

int computeZpixelSize(int pictureWidth, int pictureHeight);
node_t* createZpixelTree(int x,int y,int size, picture_t *pic);
rgb_t* colorComputing(int x,int y, int size);
double degradationComputing(int x,int y, int size);
void treeBrowsingDegradation(node_t *node, data_t *data);
int colorAndDegradationCompute(node_t *node, int x, int y, int size);
#endif