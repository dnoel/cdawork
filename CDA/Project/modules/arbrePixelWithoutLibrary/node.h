#ifndef NODE_H
#define NODE_H
#include "../zPixel/zPixel.h"
#include "pixel.h"
#define SUBNODES_NB 4

typedef struct node
{
    zpixel_t *zp;
    int nextNodeIndex;
    struct node *subNodes[SUBNODES_NB];
} node_t;

node_t* createNode(zpixel_t *zp);
void afficheNode(node_t *node);
int addSubNode(node_t *sourceNode, node_t *newNode);
int initialiseZpixelNode(node_t *node, pixel_t *px);
#endif