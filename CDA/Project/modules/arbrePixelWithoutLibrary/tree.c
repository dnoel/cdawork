#include <stdio.h>
#include <stdlib.h>
#include "data.h"
#include "tree.h"
#include "node.h"
#include <math.h>
#include "pixel.h"

int computeZpixelSize(int pictureWidth, int pictureHeight)
{
    int res = 1; 
    int treshold = pictureWidth>=pictureHeight?pictureWidth:pictureHeight;
    while(res < treshold )
    {
        res = res*2;
    }

    return res;
}

rgb_t* colorAverage(node_t *node) 
{
    unsigned char r, g, b;
    int i=0;

    for(i; i<node->nextNodeIndex; i++)
    {
        node->subNodes[i];
        r+= node->subNodes[i]->zp->rgb->r; 
        g+=node->subNodes[i]->zp->rgb->g;
        b+= node->subNodes[i]->zp->rgb->b;
    }

    // printf("Moyennage des valeurs rgb \n");
    if(i!=0)
    {
        r=(unsigned char)(((int)r)/i);
        g=(unsigned char)(((int)g)/i);
        b=(unsigned char)(((int)b)/i);
    }
    
    // printf("Création d'un nouveau rgb\n");
    return createRgb(r,g,b);
}


double degradationComputing(int x,int y, int size)
{
    return (size-1)*(1+sqrt((x-y)*(x-y)));
}

int colorAndDegradationCompute(node_t *node, int x, int y, int size)
{
    node->zp->degradation = degradationComputing(x,y,size);
    // node->zp->rgb =colorAverage(node);
    node->zp->rgb = colorComputing(x,y,size*size);
    if(node->zp->rgb == NULL) return 0;
    return 1;
}

void treeBrowsingDegradation(node_t *node, data_t *data)
{
    afficheNode(node);    
    if(node->zp->degradation <= data->degradation)
    {
        printf("affiche d'un zpixel (%d, %d / %d, %d) rgb : %d %d %d \n", node->zp->x, node->zp->x + node->zp->size, node->zp->y, node->zp->y +node->zp->size, 
        node->zp->rgb->r, node->zp->rgb->g, node->zp->rgb->b);
        applyZpixel(data->pictureTable, node->zp, data->screen);
    }
    else
    {
        
        for(int i=0; i<node->nextNodeIndex; i++)
        {
            treeBrowsingDegradation(*(node->subNodes+i),data);
        }    
    }
}

node_t* createZpixelTree(int x,int y,int size, picture_t *pic)
{
    if(x> pic->width-1 || y>pic->height-1) return NULL;

    zpixel_t *zp = createZPixelWithNoRgb(size, x, y);
    node_t *node = createNode(zp);

    if(size==1)
    {
        pixel_t *px = createPixel(x,y);
        px->rgb= colorComputing(zp->x, zp->y, zp->size);
        
        if(!initialiseZpixelNode(node, px))
        {
            free(zp);
            free(node);
            free(zp);
            return NULL;
        } 
        return node;
    }

    //  printf("       Création du noeud haut gauche\n");
    node_t *upperLeftNode = createZpixelTree(x,y,size/2, pic);
    if(upperLeftNode != NULL)
    {
        if(!addSubNode(node, upperLeftNode))
        {
            printf("    !erreur fonction addSubNode() de upperLeftNodel!\n");
            free(upperLeftNode);
            free(node);
            free(zp);   
            return NULL;    
        };
        //  printf("       noeud haut gauche crée\n");
         
    }
    else
    {
        // printf("        Le noeud est en dehors de l'image \n");
    }
    
    // printf("        création du noeud haut droit\n");
    node_t *upperRightNode = createZpixelTree(x+size/2,y,size/2, pic);
    if(upperRightNode != NULL)
    {
        if(!addSubNode(node, upperRightNode))
        {
            printf("    !erreur fonction addSubNode() de upperRightNode!\n");
            free(upperRightNode);
            free(upperLeftNode);
            free(node);
            free(zp);

            return NULL;
        };
        //  printf("       noeud haut droit crée\n");
    }
    else
    {
        // printf("        Le noeud est en dehors de l'image \n");
    }
        // printf("Création du noeud bas gauche\n");
    node_t *downLeftNode = createZpixelTree(x,y+size/2,size/2, pic);
    if(downLeftNode != NULL)
    {
        if(!addSubNode(node, downLeftNode))
        {
            printf("    !erreur fonction addSubNode() de downLeftNode!\n");
            free(downLeftNode);
            free(upperRightNode);
            free(upperLeftNode);
            free(node);
            free(zp);   
            return NULL;
        };
        //  printf("       noeud bas gauche crée\n");
    }
    else
    {
        // printf("        Le noeud est en dehors de l'image \n");
    }
    // printf("Création du noeud bas droit\n");
    node_t *downRightNode = createZpixelTree(x+size/2,y+size/2,size/2, pic);
    if(downRightNode != NULL)
    {
        if(!addSubNode(node, downRightNode))
        {
            printf("    !erreur fonction addSubNode() de downRightNode!\n");
            free(downRightNode);
            free(downLeftNode);
            free(upperRightNode);
            free(upperLeftNode);
            free(node);
            free(zp);   
            return NULL;
        }
         //printf("       noeud bas droit crée\n");
    }
    else
    {
        // printf("        Le noeud est en dehors de l'image \n");
    }
    
    printf("On commence colorAndDegradationCompute\n");

    if(!colorAndDegradationCompute(node, x, y, size))
    {
        printf("    !erreur fonction colorAndDegradationCompute à la fin du remplissage de l'arbre\n");
        free(downRightNode);
        free(downLeftNode);
        free(upperRightNode);
        free(upperLeftNode);
        free(node);
        free(zp);
        exit(0); 
    }
    printf("Noeud complété (x: %d, y: %d, size: %d, rgb : %d %d %d, degra: %.2f)\n", x,y,size, node->zp->rgb->r, node->zp->rgb->g, node->zp->rgb->b, node->zp->degradation);
    printf("\n\n");

    return node;
}
