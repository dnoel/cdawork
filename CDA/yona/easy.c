    #include <stdio.h>
    #include <time.h>

//function calling write 
int my_putchar(char c)
{
    write(1,&c,1);
}

int main()
{
    clock_t begin = clock();
    int hundred; // hundreds digit
    int ten;   // tens digit
    int unit;   // units digit

    my_putchar(hundred +'0');
    my_putchar(ten +'0');
    my_putchar(unit +'0');
    my_putchar(',');
    my_putchar(' ');

    int nbBoucle = 0;
    for(int i=13; i<790; i++)
    {
        nbBoucle++;

        hundred = i/100;
        ten =(i%100)/10;
        unit = i%10;
        

        if(hundred < ten && ten < unit)
        {
            my_putchar(hundred +'0');
            my_putchar(ten +'0');
            my_putchar(unit +'0');
            my_putchar(',');
            my_putchar(' ');
        }
        
    }
    clock_t end = clock();
    double time = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("Nombre de tour de boucle effectués : %d\n", nbBoucle);
    printf("Temps écoulé en milliemes  = %lf", time*1000);

}


