struct personne {int id; char nom[10]; char prenom[10]; int ubo; int adherent;};
struct fiche{char description[10];};
struct outil{int id; char libelle[10]; int etat; fiche *fiche;};
struct location{int id; outil *outil; personne *personne; char dateDebut[75]; char dateFin[75];};
struct adhesion{int id; personne *personne; char dateAdhesion[75];};


program BRICOTEC
{
    version BRICOTEC_VERSION_1
    {
        
        /*INITIATION BDD*/
        void creationBdd()=1;

        /*GESTION DES ADHESIONS*/
        int enregistrerAdhesion(personne)=2;
        adhesion chercherAdhesion(personne)=3;
        int supprimerAdhesion(adhesion)=4;

        /*GESTION DES OUTILS*/
        int ajouterOutil(outil)=5;
        outil trouverOutil(outil)=6;

        /*GESTION DES LOCATION D'OUTILS*/
        int ajouterLocation(location)=7;
        location trouverLocation(outil)=8;
        int supprimerLocation(location)=9;
        
    }=1;
}=0x20000002;
