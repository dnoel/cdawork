struct param {long a; long b;};

program CALCULATRICE
{
    version CALC_VERSION_1
    {
        long AJOUTER(param)=1;
        long SOUSTRAIRE(param)=2;
        long MULTIPLIER(param)=3;
        void MEMORISER_DERNIER_RESULTAT(int)=4;
        long EXTRAIRE_MEMOIRE(int)=5;
        void ALLUMER()=6;
    }=1;
}=0x20000002;
