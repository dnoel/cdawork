#include <stdio.h>
#include <mpi.h>
//#include <unistd.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
    
	int rank,size;
	MPI_Init (&argc, &argv);
    //sleep(5);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);

    int res = 0;
    int val = atoi(argv[1]); 
    int start = rank*val+1;
    int fin = (rank+1)*val;

    for(int i = start; i<=fin; i++)
    {
        res += i*i;
    }

	printf ("La somme des carrés entre %d et %d est : %d\n", start, fin, res);
    
	MPI_Finalize();
	return 0;
}
