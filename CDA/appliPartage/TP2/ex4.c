#include <stdio.h>
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>
#define BUFFSIZE 32000
#define SHOW 10

int main (int argc, char *argv[])
{
    
	int rank,size;
	MPI_Init (&argc, &argv);
    //sleep(5);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);

    int buff[BUFFSIZE];
    MPI_Status status;
     
    for(int i = 0; i<BUFFSIZE; i++)
    {
        buff[i] = (rand()%10);
    }

    switch(rank)
    {
        case 0:  
       
            sleep(1);
            printf("Rang %d :  envoie %d entiers\n", rank, BUFFSIZE);     
            if(MPI_Send(&buff, 1, MPI_INT, 1, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(1);
            }

            printf("Rang %d: attend des données\n", rank);
            if(MPI_Recv(&buff,BUFFSIZE,MPI_INT,1,0,MPI_COMM_WORLD,&status)!=MPI_SUCCESS)
            {
                printf("Erreur lors de la réception des données\n");
                exit(1);
            }

            printf("Rang %d: a reçu les données\n", rank);
            for(int i=0; i<SHOW; i++)
            {
                printf("Rang : %d a reçu la valeur numéro %d : %d\n", rank, i, buff[i]);
            }    
                 
            break;

        case 1: 
            
            printf("Rang %d: attend des données\n", rank);
            if(MPI_Recv(&buff,BUFFSIZE,MPI_INT,0,0,MPI_COMM_WORLD,&status)!= MPI_SUCCESS)
            {
                printf("Erreur lors de la réception des données\n");
                exit(1);
            }
            printf("Rang %d: a reçu les données\n", rank);
    
            for(int i=0; i<SHOW; i++)
            {
                printf("Rang : %d a reçu la valeur numéro %d : %d\n", rank, i, buff[i]);
                buff[i] = i;
            }    

            printf("Rang %d :  envoie %d entiers\n", rank, BUFFSIZE);     
            if(MPI_Send(&buff, 1, MPI_INT, 0, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(1);
            }

            break;
    }  

    MPI_Finalize();
}
