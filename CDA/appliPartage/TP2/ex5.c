#include <stdio.h>
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>
#define BUFFSIZE 32000
#define SHOW 10

int main (int argc, char *argv[])
{
    
	int rank,size;
	MPI_Init (&argc, &argv);
    //sleep(5);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);

    int buff[BUFFSIZE];
    MPI_Status status;
     
    for(int i = 0; i<BUFFSIZE; i++)
    {
        buff[i] = (rand()%10);
    }

    switch(rank)
    {
        case 0:  
       
            printf("Rang %d :  envoie %d et reçoit des entiers\n", rank, BUFFSIZE);     
            if(MPI_Sendrecv(&buff, BUFFSIZE, MPI_INT, 1, 0,BUFFSIZE, MPI_INT, MPI_COMM_WORLD, &status)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(1);
            }

            printf("Rang %d: a reçu les données\n", rank);
            for(int i=0; i<SHOW; i++)
            {
                printf("Rang : %d a reçu la valeur numéro %d : %d\n", rank, i, buff[i]);
            }


        case 1: 
        
            printf("Rang %d :  envoie %d et reçoit des entiers\n", rank, BUFFSIZE);     
            if(MPI_Sendrecv(&buff, BUFFSIZE, MPI_INT, 1, 0,BUFFSIZE, MPI_INT, MPI_COMM_WORLD, &status)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(1);
            }

            printf("Rang %d: a reçu les données\n", rank);
            for(int i=0; i<SHOW; i++)
            {
                printf("Rang : %d a reçu la valeur numéro %d : %d\n", rank, i, buff[i]);
            }


                    break;
            }  

    MPI_Finalize();
}
