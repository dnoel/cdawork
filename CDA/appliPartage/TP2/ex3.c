#include <stdio.h>
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>
int main (int argc, char *argv[])
{
    
	int rank,size;
	MPI_Init (&argc, &argv);
    //sleep(5);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);

    int data;
    MPI_Status status;
    
    switch(rank)
    {
        case 0:  
            printf("Lancement du serveur\n");        
            if(MPI_Recv(&data,1,MPI_INT,1,0,MPI_COMM_WORLD,&status)!= MPI_SUCCESS)
            {
                printf("Erreur lors de la réception des données\n");
                exit(1);
            }

            printf("status : %p\n", &status);
            printf("Processus de rang : %d reçoit la valeur %d\n", rank, data);
            break;

        case 1: 
            sleep(1);
            printf("Lancement du client\n");        
            data = atoi(argv[1]);
            if(MPI_Send(&data, 1, MPI_INT, 0, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(1);
            }
            printf("Processus de rang : %d envoie la valeur : %d\n", rank, data);
            break;
    }  
  
	MPI_Finalize();
	return 0;
}
