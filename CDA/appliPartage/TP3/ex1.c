#include <stdio.h>
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>

int sommeCarre(int start, int end)
{
    int res=0;
    for(int i = start; i<=end; i++)
    {
        res += i*i;
    }
    return res;
}

int main (int argc, char *argv[])
{
    
	int rank,size, n, res, portion;
	MPI_Init (&argc, &argv);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);
    MPI_Status status;


    n = atoi(argv[1]);
    portion = n/size;
    
   

    if(rank==0)
    {
        int stock;
        res = sommeCarre(rank * portion+1, (rank+1)*portion);
        for(int i = 1; i<size; i++)
        {
            
            if(MPI_Recv(&stock,size-1,MPI_INT,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&status)!=MPI_SUCCESS)
            {
                printf("Erreur lors de la réception des données pour le processus %d\n", rank);
                exit(1);
            }
            printf("Processus %d reçoit une valeur %d/%d\n", rank, i, size-1);
            res += stock;
        }

        printf("Le résultat de la somme des carrées est : %d\n", res);
    }
    else if(rank == size-1)
    {
        res = sommeCarre((size-1)*portion +1, n);    
        if(MPI_Send(&res, 1, MPI_INT, 0, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
        {
            printf("Erreur lors de l'envoie des données\n");
            exit(1);
        }
            
    }  
    else
    {
        res = sommeCarre(rank * portion+1, (rank+1)*portion);    
        if(MPI_Send(&res, 1, MPI_INT, 0, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
        {
            printf("Erreur lors de l'envoie des données\n");
            exit(1);
        }
    }

    MPI_Finalize();
}
