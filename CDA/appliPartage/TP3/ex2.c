#include <stdio.h>
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>

int sommeCarre(int start, int end)
{
    int res=0;
    for(int i = start; i<=end; i++)
    {
        res += i*i;
    }
    return res;
}

int main (int argc, char *argv[])
{
    
	int rank,size, n, res, portion;
	MPI_Init (&argc, &argv);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);
    MPI_Status status;


    n = atoi(argv[1]);
    portion = n/size;
    int buff;
   

    if(rank==0)
    {
        int stock;
        res = sommeCarre(rank * portion+1, (rank+1)*portion);
        MPI_Reduce(&res, &buff, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

        printf("Le résultat de la somme des carrées est : %d\n", buff);
    }
    else if(rank == size-1)
    {
        res = sommeCarre((size-1)*portion +1, n);    
        MPI_Reduce(&res, &buff, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
            
    }  
    else
    {
        res = sommeCarre(rank * portion+1, (rank+1)*portion);    
        MPI_Reduce(&res, &buff, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}
