#include <stdio.h>
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>

int sommeCarre(int start, int end)
{
    int res=0;
    for(int i = start; i<=end; i++)
    {
        res += i*i;
    }
    return res;
}

int main (int argc, char *argv[])// argv[ex3, n, tranche]
{
    
	int rank,size, n, res, tranche, limite, nextProcess, debut, fin, value, stock, cont;
	MPI_Init (&argc, &argv);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);
    MPI_Status status;
    
    //PROCESSUS MAITRE 
    if(rank==0)
    {
        n = atoi(argv[1]);
        //limite = n;
        tranche = atoi(argv[2]);
        nextProcess = 1;
        res = 0;
        debut = 1;
        //fin = 1;
        
        while(debut <=n && nextProcess <size)
        {
            printf("Le maître envoie au processus %d la valeur de début : %d\n", nextProcess, debut);
            if(MPI_Send(&debut, 1, MPI_INT, nextProcess, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(1);
            }
    
            fin = debut + tranche-1;


            if(fin > n) fin = n;

            printf("Le maître envoie au processsus %d la valeur de fin : %d\n", nextProcess, fin);
            if(MPI_Send(&fin, 1, MPI_INT, nextProcess, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(1);
            }
            nextProcess++;
            debut = fin+1;       
        }

        nextProcess--;
        
        while(nextProcess > 0)
        {
            if(MPI_Recv(&stock,1,MPI_INT,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&status)!=MPI_SUCCESS)
            {
                printf("Erreur lors de la réception des données pour le processus %d\n", rank);
                exit(1);
            }
            printf("Le maître a reçu la valeur %d\n", stock);
            res += stock;
            nextProcess--;

            if(debut <=n)
            {
                printf("Le maître envoie au processsus %d la valeur  de début : %d\n", nextProcess, debut);
                if(MPI_Send(&debut, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
                {
                    printf("Erreur lors de l'envoie des données\n");
                    exit(1);
                }
        
                fin = debut + tranche-1;

                if(fin > n) fin = n;
                
                printf("Le maître envoie au processsus %d la valeur de fin  : %d\n", nextProcess, fin);
                if(MPI_Send(&fin, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
                {
                    printf("Erreur lors de l'envoie des données\n");
                    exit(1);
                }
                nextProcess ++;
                debut = fin+1;   
            }
        }
            


        for(int i = 1; i<size; i++)
        {
            if(MPI_Send(&debut, 1, MPI_INT, nextProcess, 1, MPI_COMM_WORLD)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(1);
            }
            
            
            res += stock;
        }

        printf("Le résultat de la somme des carrées est : %d\n", res);
    }

    //PROCESSUS ESCLAVES
    else
    {
        //traitement des processus esclaves
        cont = 1;
        while(cont)
        {
            if(MPI_Recv(&debut,1,MPI_INT,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&status)!=MPI_SUCCESS)
            {
                printf("Erreur lors de la réception des données pour le processus %d\n", rank);
                exit(1);
            }
            printf("Processus %d reçoit une valeur ", rank);
           
            
            if(status.MPI_TAG ==0)
            {
                if(MPI_Recv(&fin,1,MPI_INT,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&status)!=MPI_SUCCESS)
                {
                    printf("Erreur lors de la réception des données pour le processus %d\n", rank);
                    exit(1);
                }

                 res = sommeCarre(debut, fin);

                if(MPI_Send(&res, 1, MPI_INT, 0, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
                {
                    printf("Erreur lors de l'envoie des données\n");
                    exit(1);
                }
            }
            else
            {
                cont = 0;
            }
        }
    }

    MPI_Finalize();
}
