#include <stdio.h>
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

typedef struct reprise
{
    int rangProc;
    int debut;
    int fin;
    MPI_Status status;
    double tEnvoie;
   
} reprise_t;

int sommeCarre(int start, int end)
{
    int res=0;
    for(int i = start; i<=end; i++)
    {
        res += i*i;
    }
    return res;
}

//Return the first process ready to compute in the process table
//Return -1 if no process available 
int idWaitingProcess(int tab[], int n)
{
    for(int i=0; i<n; i++)
    {
        if(tab[i]==1) return i;
    }
    return -1;
}

//Retourne le nombre de calculs à envoyer pour trouver le résultat
int nbComputing(int n, int tranche)
{
    int i=0;
    int cptr = 0;
    for(int i; i<n; i+= tranche)
    {
        cptr++;
    }
    return cptr;
}

//Retourne 1 si rangProcessus ets présent dans le tableau
//Retourne 0 sinon
int isDefaillant(int tab[], int rangProcessus, int length)
{
    for(int i=0; i<length; i++)
    {
        if (rangProcessus==tab[i]) return 1;
    }
    return 0;
}

int main (int argc, char *argv[])// argv[test, n, tranche, tMax, nbProcessusEsclaves [Rank des processus défaillants](optionnel)]
//Si nbProcessus est égal à 0, le nombre maximumum de processus est utilsé
{
    if(argc <4)
    {
        printf("Pas assez d'arguments\n");
        exit(0);
    }
    if(atoi(argv[1]) < atoi(argv[2]))
    {
        printf("La tranche ne peut pas être supérieure supérieure à l'interval de calcul\n");
        exit(0);
    }

	int rank,size;
	MPI_Init (&argc, &argv);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);
    int processusDefaillants[argc-5];
    //Remplissage du tableau des processus défaillants
    for(int i=0; i<argc-5; i++)
    {
        processusDefaillants[i] = atoi(argv[5+i]);
    }
    
    //PROCESSUS MAITRE 
    if(rank==0)
    {
        int nbEsclave = size-1;
        
        
        if(argc > 4)
        {
            int nbProcess = atoi(argv[4]);
            if(nbProcess!=0)
            {
                if(nbProcess < nbEsclave) nbEsclave = nbProcess;
            }
        }

        
        int buff[nbEsclave][2];
        int debutFin[nbEsclave][2];
        MPI_Request sendRequest[nbEsclave], receiveRequest[nbEsclave];
        int res=0;
        int n = atoi(argv[1]);
        int tranche = atoi(argv[2]);
        double tMax = atoi(argv[3]);
        
        reprise_t repriseList[nbEsclave];
        int i = 1;
		int debut = 1;
		int fin = 1;
        int nbSend = nbComputing(n, tranche);
        int stock[nbEsclave];

        printf("%d requêtes sont à envoyer \n", nbSend);
        printf("Le processsus maître doit retourner la somme des carrés de 1 à %d par tranche de %d\n", n, tranche);
        printf("nb process esclaves : %d\n", nbEsclave);
        printf("Nombre d'envoie de calculs : %d\n\n", nbSend);
        //ENVOIE DES TRANCHES A TOUS LES PROCESSUS
        while(i<=nbSend && i<=nbEsclave)
        {
            int k=i-1;
            debutFin[k][0] = debut;
            fin = debut + tranche-1;
          	if(fin > n)
            {
                debutFin[k][1] = n;
            }
            else
            {
               debutFin[k][1] = fin; 
            }

            printf("Le maître envoie au processus %d la tranche (%d / %d)\n", i, debutFin[k][0], debutFin[k][1]);

            if(MPI_Isend(debutFin[k], sizeof(debutFin[k]), MPI_INT, i, 0, MPI_COMM_WORLD, &sendRequest[k])!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie de la donnée au processus %d\n", i);
                exit(0);
            }
            
            reprise_t r = repriseList[k];
            r.tEnvoie = MPI_Wtime();
            r.rangProc = i;
            r.debut = debutFin[k][0];
            r.fin = debutFin[k][1];
        
            MPI_Irecv(&stock[k],1,MPI_INT,i,0,MPI_COMM_WORLD,&receiveRequest[k]);
            debut = fin+1;
            i++;
        }
        
        //CONTROLE DES DONNÉES REÇUES
        printf("            Contrôle des défaillances \n");

        int state[nbEsclave];//Tableau permettant de déterminer l"état des processus esclaves 
        //state 0 : Le processus n'a pas encore renvoyé de résultat
        //state 1 : Le processus a renvoyé un résultat et est donc disponible pour calculer une tranche de nouveau
        //state 2 : Le processus est défectueux et sa tranche n'a pas encore étée calculée par un autre processus
        //state 3 : Le processus est défectueux et sa tranche a été calculée par un autre processus

        //Les processus ayant reçu une tranche à calculer on pour état 0
        for(int j=0; j<nbSend; j++)
        {
            state[j] = 0;
        }
        for(int j=nbSend; j<nbEsclave;j++)
        {
            state[j] = 1;
        }

        //Les autres processus ont pour état 1
        for(int j=nbSend; j<nbEsclave;j++)
        {
            state[j] = 1;
        }

		int remainingResults = nbSend;
        int nbDefaillant = 0;
        int nbTranchePasEnvoyes = nbSend - nbEsclave;

        //Tant que l'on  a pas reçu tous les résultats des processus esclaves et que tous les processsus ne sont pas défaillants
        while(remainingResults>0 && nbDefaillant < nbEsclave)
        {
        	//On récupère le temps écoulé
            double receiveTime = MPI_Wtime();

			//Pour chaque processus esclave
            for(int j=0; j<nbEsclave; j++)
            {
                int flags = 0;
                reprise_t r = repriseList[j];

                
				//Si le résultat n'a pas encore été reçu (etat à 0)
                if(state[j]==0)
                {
                    //On test si on a déjà reçu le résultat
                    MPI_Test(&receiveRequest[j], &flags, &r.status);

                    //Si le résultat n'a pas été reçu
                    if(!flags)
                    {
                        
                        if((receiveTime - r.tEnvoie)>tMax)
                        {
                            printf("LE TEMPS D'ATTENTE DU PROCESSUS %d EST DÉPASSÉ (%f SECONDES) \n", j+1, receiveTime - r.tEnvoie);
                            //On indique que le processus est défectueux et que sa tranche n'a pas encore été envoyée à un autre processus 
                            // printf("LE PROCESSUS %d EST DEFAILLANT\n", j+1);
                            state[j]=2;
                            nbDefaillant++;             
                        }    
                    }
                    //Si on l'a reçu 
                    else
                    {
                        //On indique que le résultat de ce processus  a bien été reçu et qu'il est disponible pour un nouveau calcul
                        state[j]=1;
                        res+= stock[j];
                        remainingResults--;

                        //Si l'on n'avait pas terminé d'envoyer les tranches aux processus esclaves lors de la première vague d'envois
                        if(nbTranchePasEnvoyes>0)
                        {
                            state[j]=0;
                            debutFin[j][0] = debut;
                            fin = debut + tranche-1;
                            if(fin > n)
                            {
                                debutFin[j][1] = n;
                            }
                            else
                            {
                            debutFin[j][1] = fin; 
                            }
                                printf("Le maître envoie au processus %d la tranche (%d / %d)\n", j+1, debutFin[j][0], debutFin[j][1]);

                            if(MPI_Isend(debutFin[j], sizeof(debutFin[j]), MPI_INT, j+1, 0, MPI_COMM_WORLD, &sendRequest[j])!= MPI_SUCCESS)
                            {
                                printf("Erreur lors de l'envoie de la donnée au processus %d\n", j+1);
                                exit(1);
                            }
                            
                            reprise_t r = repriseList[j];
                            r.tEnvoie = MPI_Wtime();
                            r.rangProc = j+1;
                            r.debut = debutFin[j][0];
                            r.fin = debutFin[j][1];
                        
                            MPI_Irecv(&stock[j],1,MPI_INT,j+1,0,MPI_COMM_WORLD,&receiveRequest[j]);
                            debut = fin+1;
                            nbTranchePasEnvoyes--;
                        }        
                    }
                    
                }

                //Si le processus est défectueux et que sa tranche n'a pas encore été recalculée 
                if(state[j]==2)
                {
                    //On récupère le rang d'un processus disponible pour un calcul 
                    int processId = idWaitingProcess(state, nbEsclave);

                    //Si on a trouvé un processus 
                    if(processId!=-1)
                    {
                        //On indique que ce processus est en cours de calcul
                        state[processId] = 0;
                        //On indique que le processus de rang j est defectueux mais que sa tranche a été traitée par un autre processus
                        state[j] = 3;

                        if(MPI_Isend(debutFin[j], sizeof(debutFin[j]), MPI_INT, processId+1, 0, MPI_COMM_WORLD, &sendRequest[processId])!= MPI_SUCCESS)
                        {
                            printf("Erreur lors de l'envoie de la donnée au processus %d\n", i);
                            exit(1);
                        }
                        
                        reprise_t r = repriseList[processId];
                        r.rangProc = processId+1;
                        r.debut = debutFin[j][0];
                        r.fin = debutFin[j][1];
                        r.tEnvoie = MPI_Wtime();

                        printf("Processus %d calcule la tranche (%d/%d)\n", processId+1, r.debut, r.fin);

                        MPI_Irecv(&stock[processId],1,MPI_INT,processId+1,0,MPI_COMM_WORLD,&receiveRequest[processId]);
                    }
                    
                }
                
                usleep(10000);
            } 
            // printf("                    Nombre de resultats encore attendus : %d / nombre de processus esclaves défaillants : %d/%d\n", 
            //         remainingResults, nbDefaillant, nbEsclave);
        }

        //Si à la fin de la récupération des résultats des processus esclaves, tous les processus esclaves sont défaillants
        if(nbDefaillant == size-1)
        {
            printf("TOUS LES PROCESSUS ESCLAVES SONT DEFAILLANTS !!!\n");
            res = 0;
        }
        else
        {
            printf("Le calcul des tranches est terminé !!!\n");
        }
        
        printf("Le résultat de la somme des carrées est : %d\n", res);
        //Arrêt de l'ensemble des processus esclaves 
        for(int j=0; j<nbEsclave; j++)
        {
            printf("Envoie du signal d'arrêt au processus %d \n", j+1);
            MPI_Isend(debutFin[j], sizeof(debutFin[j]), MPI_INT, j+1, 1, MPI_COMM_WORLD, &receiveRequest[j]);
        };
    } 
    //PROCESSUS ESCLAVES
    else
    {
        //traitement des processus esclaves
        int end = 0;
        int debutFin[2];
        int res=0;
        MPI_Status status;

        while(1)
        {
            if(MPI_Recv(&debutFin,sizeof(debutFin),MPI_INT,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&status)!=MPI_SUCCESS)
            {
                printf("Erreur lors de la réception des données pour le processus %d\n", rank);
                exit(0);
            }
            // printf("Processus %d reçoit la tranche (%d / %d)\n", rank, debutFin[0], debutFin[1]);
           
            
            printf("processus %d reçoit le tag de valeur %d\n", rank, status.MPI_TAG);

            //Si le tag est égal à 1 le processus s'arrête
            if(status.MPI_TAG) break;

            if(isDefaillant(processusDefaillants,  rank, argc-5))
            {
                break;
            }
                res = sommeCarre(debutFin[0], debutFin[1]);

            printf("Le processsus %d envoie la valeur %d\n", rank, res);
            if(MPI_Send(&res, 1, MPI_INT, 0, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(0);
            }

            
        }
        printf("Le processus %d s'arrête\n", rank);
        MPI_Finalize();
    }
}
