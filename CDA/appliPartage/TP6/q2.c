#include <stdio.h>
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

typedef struct reprise
{
    int rangProc;
    int debutTranche;
    int finTranche;
    int etat;
    double tEnvoie;
   
} reprise_t;

int sommeCarre(int start, int end)
{
    int res=0;
    for(int i = start; i<=end; i++)
    {
        res += i*i;
    }
    return res;
}

int main (int argc, char *argv[])// argv[ex3, n, tranche, tMax]
{
    
	int rank,size;
	MPI_Init (&argc, &argv);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);
    
    
    
    //PROCESSUS MAITRE 
    if(rank==0)
    {
        int buff[size-1][2];
        int debutFin[size][2];
        MPI_Request sendRequest[size-1], receiveRequest[size-1];
        int res;
        int stock[size-1];
        int n = atoi(argv[1]);
        tranche = atoi(argv[2]);
        double tMax = atoi(argv[3]);
        reprise_t repriseList[size];
        int i = 0;
		int debut = 1;
		int fin = 1;
        //ENVOIE DES TRANCHES A TOUS LES PROCESSUS
        while(fin <= n && i< size-1)
        {
            debutFin[i][0] = debut;
            fin = debut + tranche-1;
          	if(fin > n) debutFin[i][1] = n;
          
            printf("Le maître envoie au processus %d la tranche (%d / %d): %d\n", nextProcess, debutFin[i][0], debutFin[i][1]);

            if(MPI_Isend(debutFin[i]*, sizeof(debutFin[i]*), MPI_INT, i, 0, MPI_COMM_WORLD, &sendRequest[i])!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie de la donnée au processus %d\n", i);
                exit(1);
            }
            
            reprise_t r = repriseList[i];
            r.rangProc = i;
            r.debut = debutFin[i][0];
            r.fin = debutFin[i][1];
            r.status = 0;
            r.tEnvoie = MPI_Wtime();

            MPI_Irecv(&stock[i],1,MPI_INT,i,0,MPI_COMM_WORLD,&receiveRequest[i]);
            i++;
        }
        
        //CONTROLE DES DONNÉES REÇUES
        
        int flags[i];//Tableau permettant de déterminer si le temps d'attente pour le résultat d'un processus est dépassé
        int computed[i]; //Tableau permettant de savoir si le résultat du processus i a déjà été ajouté au résultat final
        int dispo[i]; 
        
        //REMPLISSAGE des TABLEAU flags, computed et dispos PAR 0 (on ne sait pas encore quel résultats on été reçus)
        for(int j = 0; j<size-1; j++)
        {
            flags[j] = computed[j] = dispo[i] = 0;
        }

		int remainingResults = i;
        //TANT QUE L'ON A PAS RECU TOUTES LES VALEURS CENSEES AVOIR ÉTÉ REÇUES
        while(remainingResults>0)
        {
        	//On récupère le temps écoulé
            double receiveTime = MPI_Wtime();

			//Pour chaque processus
            for(int j=0; j<i; j++)
            {
                reprise_t r = repriseList[j];

				//Si le résultat n'a pas encore été reçu 
                if(!flags[j])
                {
                	// Si le temps d'attente max du processus n'a pas déjà été dépassé
                	if((receiveTime - r.tEnvoie)<tMax)
                	{
		            	//On fait le test de la réception
		            	MPI_Test(receiveRequest[j], flags[j], &r.status);	                
		            }    
		            else
	                {
	                    r.status = 1;
	                    flags
	                    
	                }  
            	}
                else if(computed[i]==0) //Si le résultat a été reçu mais n'a pas déjà été traité
                {
                    res += stock[j];
                    computed[i] = 1;
                    dispo[i] = 1;
                    remainingResult--;
                }        
            }

                    if(!flag)
                    {
                        r.status = 1;
                    }
                    else
                    {
                        printf("Le processsus %d a répondu à temps \n", j);
                    }
                    usleep(10000);

                while(((MPI_Wtime()) < tMax))
                {
                    
                    
                }

               
            
        
            i--;

            if(debut <=n)
            {
                printf("Le maître envoie au processsus %d la valeur  de début : %d\n", nextProcess, debut);
                if(MPI_Send(&debut, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
                {
                    printf("Erreur lors de l'envoie des données\n");
                    exit(1);
                }
        
                fin = debut + tranche-1;

                if(fin > n) fin = n;
                
                printf("Le maître envoie au processsus %d la valeur de fin  : %d\n", nextProcess, fin);
                if(MPI_Send(&fin, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
                {
                    printf("Erreur lors de l'envoie des données\n");
                    exit(1);
                }
                nextProcess ++;
                debut = fin+1;   
            }
        }

        for(int i = 1; i<size; i++)
        {
            if(MPI_Send(&debut, 1, MPI_INT, nextProcess, 1, MPI_COMM_WORLD)!= MPI_SUCCESS)
            {
                printf("Erreur lors de l'envoie des données\n");
                exit(1);
            }
        }

        printf("Le résultat de la somme des carrées est : %d\n", res);
    }

    //PROCESSUS ESCLAVES
    else
    {
        //traitement des processus esclaves
        cont = 1;
        while(cont==1)
        {
            if(MPI_Recv(&debut,1,MPI_INT,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&status)!=MPI_SUCCESS)
            {
                printf("Erreur lors de la réception des données pour le processus %d\n", rank);
                exit(1);
            }
            printf("Processus %d reçoit une valeur ", rank);
           
            
            if(status.MPI_TAG ==0)
            {
                if(MPI_Recv(&fin,1,MPI_INT,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&status)!=MPI_SUCCESS)
                {
                    printf("Erreur lors de la réception des données pour le processus %d\n", rank);
                    exit(1);
                }

                 res = sommeCarre(debut, fin);

                if(rank ==1) MPI_Finalize();

                printf("Le processsus %d envoie la valeur %d\n", rank, res);
                if(MPI_Send(&res, 1, MPI_INT, 0, 0, MPI_COMM_WORLD)!= MPI_SUCCESS)
                {
                    printf("Erreur lors de l'envoie des données\n");
                    exit(1);
                }
            }
            else
            {
                cont = 0;
            }
        }
    }

    MPI_Finalize();
}
