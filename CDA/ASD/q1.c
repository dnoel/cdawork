#include <stdio.h>
#define N 9


void afficheTab(int tab[N], int g, int d)
{
     printf("{%d", tab[g]);

    for(int i=g+1; i<d; i++)
    {
        printf(",%d", tab[i]);
    }

    printf("}\n");
}


void Fusion(int T[N], int g, int d, int m)
{
    int tabAux[d-g+1];
    int i = g;
    int j = m+1;

    int k= 0;

    //On insère les valeurs des deux sous-tableaux dans l'ordre croissant
    while(i<=m && j<=d)
    {
        if(T[i] < T[j])
        {
            tabAux[k] = T[i];
            i++;
        }
        else
        {
            tabAux[k] = T[j];
            j++;
        }

        k++;        
    }
//Si à la fin des valeurs du premier tableau n'ont pas été insérées


        for(i; i<=m;i++)
        {
            tabAux[k] = i;
            k++;
        }

//Si à la fin des valeurs du deuxième tableau n'ont pas été insérées


        for(j; j<=d;j++)
        {
            tabAux[k] = j;
            k++;
        }

//ON recopie le tableau auxilliaire dans T
    for(int i=g; i<=d; i++)
    {
        T[i] = tabAux[i];
    }

    printf("TabAux : ");
    afficheTab(tabAux, g,d);

}

void TriFusion(int T[N], int g, int d)
{
    if(g < d)
    {

        int m = (g+d)/2;
        TriFusion(T, g, m);
        TriFusion(T, m+1, d);

        Fusion(T, g, d, m);
    }
    
}

int main()
{
    int tab[N] = {1,5,7,2,3,8,6,0,4};

    printf("tableau avant tri : ");

    afficheTab(tab, 0,8);

    TriFusion(tab,0,7);

    printf("tableau après tri: ");
    afficheTab(tab,0,8);

   

}