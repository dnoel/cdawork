

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <arpa/inet.h>
#include <string.h>
#define BUFFERSIZE 10

int nbClients = 0;
int meilleureOffre = 0;
struct sockaddr_in listeClients[BUFFERSIZE]; 

void arretParControlC(int sig) 
{
    printf("terminaison par un Control-C\n");
    fflush(stdout);
    exit(0);
    /* Actions a faire pour la partie 1.3*/
}


void printArray()
{
    printf("Liste des clients : ");
    for(int i = 0; i<BUFFERSIZE; i++)
    {
        if(&(listeClients[i]) == NULL)
        {
            printf(",[NULL]");
        }
        else
        {
            printf("[1]");
        }
    }
    printf("\n");
}

int main (int argc, char **argv) /* receveur portReceveur descriptionVente prixVente*/
{
    int sockAccueil, sockVente,recu,envoye;
    char confirmation[256], nomh[50];
    struct sockaddr_in adresseLocale;
    int lgadresseLocale;
    struct sockaddr_in adresseEmetteur, adresseReceveur;
    int lgadresseEmetteur,lgadresseReceveur;
    struct hostent *hote;
    struct sigaction action;

    /* handler de signal SIGINT */
    action.sa_handler = arretParControlC;
    sigaction(SIGINT, &action, NULL);

    /* cr'eation de la socket */
    if ((sockAccueil = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
    {
      	perror("socket"); 
      	exit(1);
    }


    /* preparation de l'adresse locale : port + toutes les @ IP */
    adresseLocale.sin_family = AF_INET;
    adresseLocale.sin_port = htons(atoi(argv[1]));
    adresseLocale.sin_addr.s_addr = htonl(INADDR_ANY);

    /* attachement de la socket a` l'adresse locale */
    lgadresseLocale = sizeof(adresseLocale);
    if((bind(sockAccueil, (struct sockaddr*)&adresseLocale, lgadresseLocale)) == -1) 
    {
      	perror("bind"); 
      	exit(1);
    }
    /* reception des chaînes */
    lgadresseLocale = sizeof(adresseLocale);

    int messageClient;
    char entree;
    int attenteClient = 1;
    while(attenteClient)
    {  
        printf("J'attend un client\n");
         lgadresseLocale = sizeof(adresseLocale);

        if((recu = recvfrom(sockAccueil, &messageClient, sizeof(messageClient), 0, (struct sockaddr*)&listeClients[nbClients++], &lgadresseLocale)) ==-1) 
        {
            perror("recvfrom string"); 
            close(sockAccueil); 
            exit(1);
        }
        
        printf("Un client s'est connecté à la vente aux enchères\n");
        printf("%d client(s) présent(s) pour la vente\n", nbClients);

       
        printf("Souhaitez vous commencer la vente ? (y/n)\n\n");
        scanf("%s",&entree);
        if(entree == 'y') attenteClient = 0;
    
    }

    printf("La vente commence\n");
    
    char descriptionVente[25]; 
    strcpy(descriptionVente, argv[2]);
    long int prixVente = atoi(argv[3]);

     close(sockAccueil);
   
    /* cr'eation de la socket */
    if ((sockVente = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
    {
      	perror("socket"); 
      	exit(1);
    }


    /* preparation de l'adresse locale : port + toutes les @ IP */
    adresseLocale.sin_family = AF_INET;
    adresseLocale.sin_port = htons(atoi(argv[1]));
    adresseLocale.sin_addr.s_addr = htonl(INADDR_ANY);

    /* attachement de la socket a` l'adresse locale */
    lgadresseLocale = sizeof(adresseLocale);
    if((bind(sockVente, (struct sockaddr*)&adresseLocale, lgadresseLocale)) == -1) 
    {
      	perror("bind"); 
      	exit(1);
    }
    printf("Envoie de la description et du prix de la vente\n");
    //Envoie de l'intitulé de la vente et du prix
    for(int i=0; i<nbClients; i++)
    {
        if (sendto(sockVente, &descriptionVente, strlen(descriptionVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i])) != strlen(descriptionVente)) 
        {
          	printf("Erreur de send du premier entier"); 
          	close(sockVente); 
          	exit(1);
        }

        if (sendto(sockVente, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i])) != sizeof(prixVente)) 
        {
          	printf("Erreur de send du premier entier"); 
          	close(sockVente); 
          	exit(1);
        }

        //sendto(sockVente, &descriptionVente, strlen(descriptionVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i]));
        
        //sendto(sockVente, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i]));
        printf("Envoie %d/%d fait\n", i+1, nbClients);
    }
   
    int venteEnCours = 1;
    long int stock =0;
    int nbReception=0;
    int rien;
    char continuer;
    while(venteEnCours)
    {
        lgadresseLocale = sizeof(adresseLocale);
        recvfrom(sockVente, &stock, sizeof(prixVente), 0, (struct sockaddr*)&adresseReceveur, &lgadresseReceveur);
        nbReception++;
        printf("Réception de l'offre: %ld\n", stock);
        if(stock > prixVente)
        {
            prixVente = stock;
            printf("Meilleure offre : %ld\n", prixVente); 
        }
        
        if(nbReception == nbClients)
        {
            nbReception=0;
            printf("Souhaitez-vous continuer la vente ? (y/n) : ");
            scanf(" %c", &continuer);
            if(continuer =='n')
            {
                for(int i=0; i<nbClients; i++)
                {
                    if (sendto(sockVente, &rien, 0, 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i])) != 0) 
                    {
                      	printf("Erreur de send du premier entier"); 
                      	close(sockVente); 
                      	exit(1);
                    }
                    sendto(sockVente, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i]));
                    
                    printf("Envoie de l'arrêt de l'enchère %d/%d fait\n", i+1, nbClients);
                }
                venteEnCours = 0;
            }
            else
            {
                for(int i=0; i<nbClients; i++)
                {
                   
                    
                    if (sendto(sockVente, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i])) != sizeof(prixVente)) 
                    {
                      	printf("Erreur de send du premier entier"); 
                      	close(sockVente); 
                      	exit(1);
                    }
                    
                    printf("Envoie du nouveau prix %d/%d fait\n", i+1, nbClients);
                }
            }

        }
    }
     
    close(sockAccueil);
    close(sockVente);
}
