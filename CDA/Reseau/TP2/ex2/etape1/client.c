#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/types.h>


int main(int argc, char **argv)/* emetteur portReceveur machineReceveur */
{
    int sock, envoye, recu;
    struct sockaddr_in adresseReceveur;
    int lgadresseReceveur;
    struct hostent *hote;

    /* création de la socket */
    if ((sock = socket(AF_INET, SOCK_DGRAM,0)) == -1) 
    {
        perror("socket"); 
        exit(1);
    }

    /* recherche de l'@ IP de la machine distante */
    if ((hote = gethostbyname(argv[2])) == NULL)
    {
        perror("gethostbyname"); 
        close(sock); 
        exit(2);
    }

    /* pr'eparation de l'adresse distante : port + la premier @ IP */
    adresseReceveur.sin_family = AF_INET;
    adresseReceveur.sin_port = htons(atoi(argv[1]));
    bcopy(hote->h_addr, &adresseReceveur.sin_addr, hote->h_length);
    printf("L'adresse en notation pointee %s\n", inet_ntoa(adresseReceveur.sin_addr));
    lgadresseReceveur = sizeof(adresseReceveur);

    
    /* envoi de la participation à l'enchère */
    
    if ((envoye = sendto(sock, NULL, 0, 0, (struct sockaddr*)&adresseReceveur, lgadresseReceveur)) !=0) 
    {
      	printf("Erreur de send de la participation "); 
      	close(sock); 
      	exit(1);
    }

    

    close(sock);
}
