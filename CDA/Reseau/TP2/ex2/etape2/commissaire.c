

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <arpa/inet.h>
#include <string.h>
#define BUFFERSIZE 10

int nbClients = 0;
int meilleureOffre = 0;
struct sockaddr_in listeClients[BUFFERSIZE]; 

void arretParControlC(int sig) 
{
    printf("terminaison par un Control-C\n");
    fflush(stdout);
    exit(0);
    /* Actions a faire pour la partie 1.3*/
}


void printArray()
{
    printf("Liste des clients : ");
    for(int i = 0; i<BUFFERSIZE; i++)
    {
        if(&(listeClients[i]) == NULL)
        {
            printf(",[NULL]");
        }
        else
        {
            printf("[1]");
        }
    }
    printf("\n");
}

int main (int argc, char **argv) /* receveur portReceveur descriptionVente prixVente*/
{
    int sockAccueil, socketVente,recu,envoye;
    char confirmation[256], nomh[50];
    struct sockaddr_in adresseLocale;
    int lgadresseLocale;
    struct sockaddr_in adresseEmetteur, adresseReceveur;
    int lgadresseEmetteur,lgadresseReceveur;
    struct hostent *hote;
    struct sigaction action;

    /* handler de signal SIGINT */
    action.sa_handler = arretParControlC;
    sigaction(SIGINT, &action, NULL);

    /* cr'eation de la socket */
    if ((sockAccueil = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
    {
      	perror("socket"); 
      	exit(1);
    }

    /* preparation de l'adresse locale : port + toutes les @ IP */
    adresseLocale.sin_family = AF_INET;
    adresseLocale.sin_port = htons(atoi(argv[1]));
    adresseLocale.sin_addr.s_addr = htonl(INADDR_ANY);

    /* attachement de la socket a` l'adresse locale */
    lgadresseLocale = sizeof(adresseLocale);
    if ((bind(sockAccueil, (struct sockaddr*)&adresseLocale, lgadresseLocale)) == -1) 
    {
      	perror("bind"); 
      	exit(1);
    }
    
    int messageClient;
    char entree;
    int attenteClient = 1;
    while(attenteClient)
    {  
        /* reception des chaînes */
        lgadresseLocale = sizeof(adresseLocale);

        if((recu = recvfrom(sockAccueil, &messageClient, sizeof(messageClient), 0, (struct sockaddr*)&listeClients[nbClients++], &lgadresseLocale)) ==-1) 
        {
            perror("recvfrom string"); 
            close(sockAccueil); 
            exit(1);
        }
        
        printf("Un client s'est connecté à la vente aux enchères\n");
        printf("%d client(s) présent(s) pour la vente\n", nbClients);

       
        printf("Souhaitez vous commencer la vente ? (y/n)\n\n");
        scanf("%s",&entree);
        if(entree == 'y') attenteClient = 0;
    
    }

    printf("La vente commence\n");
    
    char descriptionVente[25]; 
    strcpy(descriptionVente, argv[2]);
    long int prixVente = atoi(argv[3]);

    printf("Envoie de la description et du prix de la vente\n");
    //Envoie de l'intitulé de la vente et du prix
    for(int i=0; i<nbClients; i++)
    {

    sendto(sockAccueil, descriptionVente, strlen(descriptionVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i]));
    
    sendto(sockAccueil, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i]));
    }
    close(sockAccueil);
}
