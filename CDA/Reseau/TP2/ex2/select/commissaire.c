

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <arpa/inet.h>
#include <string.h>
#define BUFFERSIZE 10

int nbClients = 0;
int meilleureOffre = 0;
struct sockaddr_in listeClients[BUFFERSIZE]; 

void arretParControlC(int sig) 
{
    printf("terminaison par un Control-C\n");
    fflush(stdout);
    exit(0);
    /* Actions a faire pour la partie 1.3*/
}

int removeClient(struct sockaddr_in adresseClient)
{
    for(int i =0; i<nbClients; i++)
    {
        if(listeClients[i].sin_addr.s_addr == adresseClient.sin_addr.s_addr)
        {
            listeClients[i] = listeClients[nbClients-1];
            nbClients--;
            return 1;
        }
    }
    return 0;
}

int main (int argc, char **argv) /* receveur portReceveur descriptionVente prixVente*/
{
    int sockAccueil, sockVente,recu,envoye;
    char confirmation[256], nomh[50];
    int lgadresseLocale, lgadresseClient;
    struct sockaddr_in adresseLocale, adresseClient;
    struct hostent *hote;
    struct sigaction action;

    /* handler de signal SIGINT */
    action.sa_handler = arretParControlC;
    sigaction(SIGINT, &action, NULL);

    /* création de la socket d'acceuil*/
    if ((sockAccueil = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
    {
      	perror("socket"); 
      	exit(1);
    }


    /* preparation de l'adresse locale : port + toutes les @ IP */
    adresseLocale.sin_family = AF_INET;
    adresseLocale.sin_port = htons(atoi(argv[1]));
    adresseLocale.sin_addr.s_addr = htonl(INADDR_ANY);

    printf("bind socketACcueille\n");
    /* attachement de la socket d'accueil a` l'adresse lgadresseAccueil */
    lgadresseLocale = sizeof(adresseLocale);
    if((bind(sockAccueil, (struct sockaddr*)&adresseLocale, lgadresseLocale)) == -1) 
    {
      	perror("bind 4"); 
      	exit(1);
    }


      /* création de la socket */
    if ((sockVente = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
    {
      	perror("socket"); 
      	exit(1);
    }


    /* preparation de l'adresse lgadresseAccueil : port + toutes les @ IP */
    adresseLocale.sin_family = AF_INET;
    adresseLocale.sin_port = 0;
    adresseLocale.sin_addr.s_addr = htonl(INADDR_ANY);

    printf("bind sockVente\n");
    /* attachement de la socket a` l'adresse lgadresseAccueil */
    lgadresseLocale = sizeof(adresseLocale);
    if((bind(sockVente, (struct sockaddr*)&adresseLocale, lgadresseLocale)) == -1) 
    {
      	perror("bind"); 
      	exit(1);
    }


    fd_set rfds;
    struct timeval tv;
    int retval;
    
    int messageClient;
    char entree;

    long int stock =0;
    int rien;
    char continuer;
    char descriptionVente[25]; 
    strcpy(descriptionVente, argv[2]);
    long int prixVente = atoi(argv[3]);

    printf("Ouverture de la vente\n");

    while(1)
    {
        FD_ZERO(&rfds); /* Surveiller stdin (fd 0) en attente d'entrées */  
        FD_SET(sockAccueil, &rfds); //Ajout du socket d'acceuil
        FD_SET(sockVente, &rfds);//Ajout du socket de vente
           
        /* Mise en place d'une attente de 10 secondes. */
        tv.tv_sec = 10;
        tv.tv_usec = 0;

        retval = select(sockVente+1, &rfds, NULL, NULL, &tv);
        if(FD_ISSET(sockAccueil, &rfds))
        {
          printf("Un client est arrivé\n");
            
             lgadresseLocale= sizeof(adresseLocale);

            if((recu = recvfrom(sockAccueil, &messageClient, sizeof(messageClient), 0, (struct sockaddr*)&listeClients[nbClients], &lgadresseLocale)) ==-1) 
            { 
                perror("Erreur socketAccueil\n");
                close(sockAccueil); 
                close(sockVente);
                exit(1);
            }
            
            printf("Un client s'est connecté à la vente aux enchères\n");
            printf("%d client(s) présent(s) pour la vente\n", nbClients+1);
        
            if (sendto(sockVente, &descriptionVente, strlen(descriptionVente), 0, (struct sockaddr*)&listeClients[nbClients], sizeof(listeClients[nbClients])) != strlen(descriptionVente)) 
            {
              	printf("Erreur d'envoie de la description au client\n"); 
                close(sockAccueil);
              	close(sockVente); 
              	exit(1);
            }

            if (sendto(sockVente, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&listeClients[nbClients], sizeof(listeClients[nbClients])) != sizeof(prixVente)) 
            {
              	printf("Erreur de 'lenvoie du prix au client\n"); 
                close(sockAccueil);
              	close(sockVente); 
              	exit(1);
            }
            nbClients++;
            
        }

        // GESTION SOCKET DE VENTE

        else if(FD_ISSET(sockVente, &rfds))
        {
            lgadresseClient= sizeof(listeClients[nbClients]);
            recvfrom(sockVente, &stock, sizeof(prixVente), 0, (struct sockaddr*)&adresseClient, &lgadresseClient);
        
            if(stock ==-1)
            {
                printf("Un client souhaite quitter la vente\n");
                if(removeClient(adresseClient))
                {
                    printf("Client retiré de la vente avec succès\n");
                }
                else
                {
                    printf("Echec de la tentative de retirer le client de la vente\n");
                }
            }
            else if(stock>prixVente)
            {
                prixVente = stock;
                printf("Meilleure offre : %ld\n", prixVente); 

                //Envoie du nouveau prix à tous les clients
                for(int i=0; i<nbClients; i++)
                {
                    sendto(sockVente, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i]));
                }
            }
        }

        //Si le temps d'attente a été dépassé
        else
        {
            printf("Aucune nouvelle offre, cloture de la vente\n");
            for(int i=0; i<nbClients; i++)
            {
                if (sendto(sockVente, &rien, 0, 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i])) != 0) 
                {
                  	printf("Erreurs lors de la cloture de la vente\n"); 
                  	close(sockVente); 
                  	exit(1);
                }
                sendto(sockVente, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&listeClients[i], sizeof(listeClients[i]));
            } 
            break;
        }
    }

   
    
     
    close(sockAccueil);
    close(sockVente);
}
