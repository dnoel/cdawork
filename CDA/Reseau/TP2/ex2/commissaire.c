/* receveur portReceveur */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <arpa/inet.h>
#include <string.h>



void arretParControlC(int sig) 
{
    printf("terminaison par un Control-C\n");
    fflush(stdout);
    exit(0);
    /* Actions a faire pour la partie 1.3*/
}


int main (int argc, char **argv)
{
    int sockAccueil, sockVente, recu,envoye;
    char confirmation[256], nomh[50];
    struct sockaddr_in adresseLocale;
    int lgadresseLocale;
    struct sockaddr_in adresseEmetteur;
    int lgadresseEmetteur;
    struct hostent *hote;
    struct sigaction action;

    char str1[10];
    char str2[10];

    /* handler de signal SIGINT */
    action.sa_handler = arretParControlC;
    sigaction(SIGINT, &action, NULL);

    /* creation de la socket Accueil*/
    if ((sockAccueil = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
    {
      	perror("socket"); 
      	exit(1);
    }

    /* creation de la socket Accueil*/
    if ((sockVente = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
    {
      	perror("socket"); 
      	exit(1);
    }

    /* préparation de l'adresse locale : port + toutes les @ IP */
    adresseLocale.sin_family = AF_INET;
    adresseLocale.sin_port = htons(atoi(argv[1]));
    adresseLocale.sin_addr.s_addr = htonl(INADDR_ANY);

    /* attachement de la socket a` l'adresse locale */
    lgadresseLocale = sizeof(adresseLocale);
    if ((bind(sock, (struct sockaddr*)&adresseLocale, lgadresseLocale)) == -1) 
    {
      	perror("bind"); 
      	exit(1);
    }
    
    int entree;
    printf("Appuyez sur une touche pour débloquer le receveur\n");
    scanf("%d",&entree);
    
        
    /* reception des chaînes */
    lgadresseLocale = sizeof(adresseLocale);
    if((recu = recvfrom(sock, str1, sizeof(str1), 0, (struct sockaddr*)&adresseLocale, &lgadresseLocale)) ==-1) 
    {
        perror("recvfrom string"); 
        close(sock); 
        exit(1);
    }
    printf("j'ai reçu une première string : %s \n", str1);
    lgadresseLocale = sizeof(adresseLocale);
    if ((recu = recvfrom(sock, str2, sizeof(str2), 0, (struct sockaddr*)&adresseLocale, &lgadresseLocale)) ==-1)
    {
        perror("recvfrom string"); 
        close(sock); 
        exit(1);
    }
    printf("j'ai reçu une deuxième string : %s \n", str2);
    

    close(sock);
}
