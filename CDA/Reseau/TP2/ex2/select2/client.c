#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/select.h>

int main(int argc, char **argv)/* emetteur portReceveur machineReceveur */
{
    int sock, envoye, recu;
    struct sockaddr_in adresseReceveur;
    int lgadresseReceveur;
    struct hostent *hote;

    /* création de la socket */
    if ((sock = socket(AF_INET, SOCK_DGRAM,0)) == -1) 
    {
        perror("socket"); 
        exit(1);
    }

    /* recherche de l'@ IP de la machine distante */
    if ((hote = gethostbyname(argv[2])) == NULL)
    {
        perror("gethostbyname"); 
        close(sock); 
        exit(2);
    }

    /* pr'eparation de l'adresse distante : port + la premier @ IP */
    adresseReceveur.sin_family = AF_INET;
    adresseReceveur.sin_port = htons(atoi(argv[1]));
    bcopy(hote->h_addr, &adresseReceveur.sin_addr, hote->h_length);
    printf("L'adresse en notation pointee %s\n", inet_ntoa(adresseReceveur.sin_addr));
    lgadresseReceveur = sizeof(adresseReceveur);

    
    /* envoi de la participation à l'enchère */
    int message;
    if ((envoye = sendto(sock, &message, 0, 0, (struct sockaddr*)&adresseReceveur, lgadresseReceveur)) !=0) 
    {
      	printf("Erreur de send de la participation "); 
      	close(sock); 
      	exit(1);
    }

    //Attente du début de la vente
    char descriptionVente[25];
    long int prixVente=0;

    printf("Attente du début de la vente\n");

    lgadresseReceveur = sizeof(adresseReceveur);
    recvfrom(sock, &descriptionVente, strlen(descriptionVente), 0, (struct sockaddr*)&adresseReceveur, &lgadresseReceveur);
    printf("Description de la vente : %s\n", descriptionVente);

    lgadresseReceveur = sizeof(adresseReceveur);
    recvfrom(sock, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&adresseReceveur, &lgadresseReceveur);

    printf("Prix de la vente: %ld\n", prixVente);

    long int stock =0;

        fd_set rfds;
        int retval;     
        printf("Veuillez proposer une offre : ");
        while(1)
        {
            FD_ZERO(&rfds); /* Surveiller stdin (fd 0) en attente d'entrées */  
            FD_SET(0, &rfds); //Ajout de l'entée standard
            FD_SET(sock, &rfds);//Ajout du socket*

            retval = select(sock+1, &rfds, NULL, NULL, NULL);
            if(FD_ISSET(0, &rfds))
            {
                int deconnexion = 0;
                scanf("%ld", &stock);
                if(stock ==-1) deconnexion = 1;
                if( deconnexion || stock>prixVente)
                {
                    if(deconnexion)
                    {
                        printf("Envoie de la déconnexion de la vente au serveur\n");
                    }
                    else
                    {
                    printf("Envoie de l'offre %ld\n", stock);
                    }

                    sendto(sock, &stock, sizeof(stock), 0, (struct sockaddr*)&adresseReceveur, lgadresseReceveur);
                    
                    if(deconnexion)
                    {
                        printf("Arrêt du client\n");
                        break;
                    }
                }
                else
                {
                    printf("Veuillez proposer une offre plus élevée :"); 
                }
            }

            if(FD_ISSET(sock, &rfds))
            {
                printf("On entre dans fd_isset(sock);\n");
                lgadresseReceveur = sizeof(adresseReceveur);
                if(recvfrom(sock, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&adresseReceveur, &lgadresseReceveur)==0)
                {
                    printf("La vente est terminée\n");
                    lgadresseReceveur = sizeof(adresseReceveur);
                    recvfrom(sock, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&adresseReceveur, &lgadresseReceveur);
    
                    if(prixVente == 1)
                    {
                        printf("J'ai gagné la vente avec mon offre de %ld !!!\n", stock);    
                        break;
                    }
                    else if( prixVente==0)
                    {
                        printf("Un autre acheteur a fait une offre plus élevée, j'ai perdu\n");
                        break;
                    }      
                }
                else
                {
            
                    if(stock==-1)
                    {
                        printf("L'acheteur gagnant est parti de la vente\n");
                        lgadresseReceveur = sizeof(adresseReceveur);
                        recvfrom(sock, &prixVente, sizeof(prixVente), 0, (struct sockaddr*)&adresseReceveur, &lgadresseReceveur);
                        printf("Le prix a été réinitialisé à %ld\n", prixVente);
                    }
                    else
                    {
                        printf("Nouvelle offre  reçue: %ld\n", prixVente);
                        printf("Proposez une offre : ");
                    }
                   
                }       
            }
        }
    close(sock);
}
