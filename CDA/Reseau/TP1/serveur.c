/* compilation sous linux : gcc <nom_du_fichier> -o serveur_tcp * /
/* Lancement d'un serveur : serveur_tcp port */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#define TRUE 1
int main(int argc, char **argv)
{
int socket_RV, socket_service;
char buf[256];
int entier_envoye;
struct sockaddr_in adresseRV;
int lgadresseRV;
struct sockaddr_in adresseClient;
int lgadresseClient;
struct hostent *hote;
unsigned short port;
/* creation de la socket de RV */
if ((socket_RV = socket(AF_INET, SOCK_STREAM, 0)) == -1)
{
 perror("socket");
 exit(1);
}
/* preparation de l'adresse locale */
port = (unsigned short) atoi(argv[1]);
adresseRV.sin_family = AF_INET;
adresseRV.sin_port = htons(port);
adresseRV.sin_addr.s_addr = htonl(INADDR_ANY); /* toutes les addresses de la machine locale */
lgadresseRV = sizeof(adresseRV);
/* attachement de la socket a l'adresse locale */
if ((bind(socket_RV, (struct sockaddr *) &adresseRV, lgadresseRV)) == -1)
{
 perror("bind");
 exit(3);
}
/* declaration d'ouverture du service */
if (listen(socket_RV, 10)==-1)
{
perror("listen");
 exit(4);
}
//Ouverture du descripteur

    int fd1 =  open(", O_RDONLY);
/* boucle d'attente de connexion */
 while (TRUE) /* !!! boucle infinie !!! */
 {
 printf("Debut de boucle\n");
 fflush(stdout);
 /* attente d'un client */
lgadresseClient = sizeof(adresseClient);
socket_service = accept(socket_RV, (struct sockaddr *) &adresseClient, &lgadresseClient);

 if (socket_service == -1)
{
 perror("accept");
 exit(5);
}
 /* un client est arrive */
printf("connexion acceptee\n");
 fflush(stdout);

/* Le serveur fait le traitement sans créer de fils – cas particulier de traitements courts */
/* lecture dans la socket d'une chaine de caractères */
if (read(socket_service, buf, 256) < 0)
{
 perror("read");
 exit(6);
}
 printf("Chaine recue : %s\n", buf);
 fflush(stdout);

 /* ecriture dans la socket d'un entier */
 entier_envoye = 2222;
 if (write(socket_service, &entier_envoye, sizeof(int)) != sizeof(int))
{
 perror("write");
 exit(7);
}
 printf("Entier envoye : %d\n", entier_envoye);
 close(socket_service);
 } /* fin du while – le serveur peut attendre un nouveau client */
   close(socket_RV);
}
