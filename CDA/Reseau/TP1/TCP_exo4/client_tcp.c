/* Lancement d'un client :  client_tcp port serveur */
/* compilation : gcc -o client_tcp client_tcp.c   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

main (int argc, char **argv)
{
int sock;
char buf[256];
struct sockaddr_in adresse_serveur;
struct hostent *hote;
unsigned short port;
int val;

/* creation de la socket locale */
if ((sock = socket( ... ) )
	{
  	perror("socket");
  	exit(1);
	}

/* recuperation de l'adresse IP du serveur (a partir de son nom) */
if ((hote = gethostbyname( ... ))
	{
  	perror("gethostbyname");
  	exit(2);
	}

/* preparation de l'adresse du serveur */
port = ... 

adresse_serveur.sin_family = ...
adresse_serveur.sin_port = ...
bcopy( ... );

printf("L'adresse en notation pointee %s\n", inet_ntoa(adresse_serveur.sin_addr));
fflush(stdout);

/* demande de connexion au serveur */
if (connect( ... ) )
	{
  	perror("connect");
  	exit(3);
	}

/* le serveur a accepte la connexion */
printf("connexion acceptee\n");
fflush(stdout);

/* ecriture dans la socket */
strcpy(buf, "Message ");
if (write( ... ) )
	{
  	perror("write");
  	exit(4);
	}
printf("Message envoye\n");
fflush(stdout);

/* lecture dans la socket */
if (read( ... )
	{
  	perror("read");
  	exit(5);
	}
printf("j'ai recu %d ... bye-bye\n", val);

/* fermeture de la socket */

close (....)
fflush(stdout);
}
