/* Lancement d'un serveur :  serveur_tcp port */
/* compilation : gcc -o serveur_tcp serveur_tcp.c   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define TRUE 1
 
main (int argc, char **argv)
{
int socket_RV, socket_service;
char buf[256], nomh[50];
struct sockaddr_in adresseRV;
int lgadresseRV;
struct sockaddr_in adresseClient;
int lgadresseClient;
struct hostent *hote;
unsigned short port;
int val;

/* creation de la socket de RV */
if ((socket_RV = socket( ... ))
	{
  	perror("socket");
  	exit(1);
	}

/* facultatif : affichage du nom de la machine du serveur */
if (gethostname( ... ))
	{
  	perror("gethostname");
  	exit(2);
	}
printf("Je m'execute sur %s\n", nomh);
fflush(stdout);

/* preparation de l'adresse locale */
port = ...

adresseRV.sin_family = ...
adresseRV.sin_port = ...
adresseRV.sin_addr.s_addr = ...

lgadresseRV = sizeof(adresseRV);

/* attachement de la socket a l'adresse locale */
if ((bind( ... ))
	{
  	perror("bind");
  	exit(3);
	}

/* declaration d'ouverture du service */
if (listen( ... ))
	{
  	perror("listen");
  	exit(4);
	}

/* attente d'un client */
lgadresseClient = sizeof(adresseClient);
socket_service = accept( ... );
if (socket_service==-1)
	{  /* erreur */
  	perror("accept");
  	exit(5);
	}

/* un client est arrive : il est connecte via la socket de service */
printf("connexion acceptee\n");
fflush(stdout);

/* je ne peux traiter qu'un client dans cet exemple, je ferme la socket de rendez-vous */
close( ... );

/* lecture dans la socket de service */
if (read( ... ))
	{
  	perror("read");
  	exit(6);
	}
printf("j'ai recu %s\n", buf);
fflush(stdout);

/* ecriture dans la socket de service  d'un entier */
/* on envoie ici la taille de la chaine reçue   */
val = strlen(buf);

if (write( ... )
	{
  	perror("write");
  	exit(7);
	}
printf("reponse envoyee ... adios\n");

/* fermeture de la socket */
close(socket_service);
}
