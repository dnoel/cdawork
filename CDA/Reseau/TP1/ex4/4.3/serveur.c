/* compilation sous linux : gcc <nom_du_fichier> -o serveur_tcp * /
/* Lancement d'un serveur : serveur_tcp port */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <netinet/in.h>
#include<pthread.h>
#define TRUE 1
#define SIZE 30
int main(int argc, char **argv)
{
    int socket_RV, socket_service;
    char buf[256];
    int entier_envoye;
    struct sockaddr_in adresseRV;
    int lgadresseRV;
    struct sockaddr_in adresseClient;
    int lgadresseClient;
    struct hostent *hote;
    unsigned short port;
    /* creation de la socket de RV */
    if ((socket_RV = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }
    /* preparation de l'adresse locale */
    port = (unsigned short) atoi(argv[1]);
    adresseRV.sin_family = AF_INET;
    adresseRV.sin_port = htons(port);
    adresseRV.sin_addr.s_addr = htonl(INADDR_ANY); /* toutes les addresses de la machine locale */
    lgadresseRV = sizeof(adresseRV);
    /* attachement de la socket a l'adresse locale */
    if ((bind(socket_RV, (struct sockaddr *) &adresseRV, lgadresseRV)) == -1)
    {
        perror("bind");
        exit(3);
    }
    /* declaration d'ouverture du service */
    if (listen(socket_RV, 10)==-1)
    {
        perror("listen");
        exit(4);
    }

    //Ouverture du registre 



  
    /* boucle d'attente de connexion */

   
    while (TRUE) /* !!! boucle infinie !!! */
    {
        printf("Debut de boucle\n");
        fflush(stdout);
        /* attente d'un client */
        lgadresseClient = sizeof(adresseClient);
        socket_service = accept(socket_RV, (struct sockaddr *) &adresseClient, &lgadresseClient);
        

        if (socket_service == -1)
        {
            perror("accept");
            exit(5);
        }       
        /* un client est arrive */        

        int filsPid = fork();

        switch(filsPid)
        {
            case -1://Erreur
                printf("Erreur de fork()\n");
                exit(5);
            case 0: //code du fils 
                printf("connexion acceptee\n");
                close(socket_RV);
                fflush(stdout);
                char fileName[13];
                int pid = getpid();
                sprintf(fileName, "copy%d.txt", pid);
                
                int fd2 = open(fileName, O_CREAT|O_WRONLY|O_TRUNC, 0640);
                char stock[SIZE];
                int nbOctets;

                while(nbOctets = read(socket_service, stock, SIZE))
                {
                    if(nbOctets <SIZE)
                    {
                        stock[nbOctets] = '\0';
                    }
                    printf("J'écris %d octets dans le fichier appelé %s le texte: %s\n", nbOctets, fileName, stock);
                    write(fd2, stock, nbOctets);
                }
               
                close(socket_service);
                close(fd2);
                exit(0);
                break;

            default :  close(socket_service); //Code du père
        }

    } /* fin du while – le serveur peut attendre un nouveau client */
    waitpid(-1, NULL,0);
    close(socket_RV);
}
