/* compilation sous linux : gcc <nom_du_fichier> -o serveur_tcp * /
/* Lancement d'un serveur : serveur_tcp port */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#define TRUE 1
#define SIZE 50
int main(int argc, char **argv)
{
    int socket_RV, socket_service;
    char buf[256];
    int entier_envoye;
    struct sockaddr_in adresseRV;
    int lgadresseRV;
    struct sockaddr_in adresseClient;
    int lgadresseClient;
    struct hostent *hote;
    unsigned short port;
    /* creation de la socket de RV */
    if ((socket_RV = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
         perror("socket");
         exit(1);
    }
    /* preparation de l'adresse locale */
    port = (unsigned short) atoi(argv[1]);
    adresseRV.sin_family = AF_INET;
    adresseRV.sin_port = htons(port);
    adresseRV.sin_addr.s_addr = htonl(INADDR_ANY); /* toutes les addresses de la machine locale */
    lgadresseRV = sizeof(adresseRV);
    /* attachement de la socket a l'adresse locale */
    if ((bind(socket_RV, (struct sockaddr *) &adresseRV, lgadresseRV)) == -1)
    {
         perror("bind");
         exit(3);
    }
    /* declaration d'ouverture du service */
    if (listen(socket_RV, 10)==-1)
    {
        perror("listen");
        exit(4);
    }

    //Ouverture du registre 

    if(socket_service ==-1)
    {
        printf("Erreur de service\n");
    }
  
    int fd2 = open(argv[2], O_CREAT|O_WRONLY|O_TRUNC, 0640);
    /* boucle d'attente de connexion */
     while (TRUE) /* !!! boucle infinie !!! */
     {
         printf("Debut de boucle\n");
         fflush(stdout);
         /* attente d'un client */
        lgadresseClient = sizeof(adresseClient);
        socket_service = accept(socket_RV, (struct sockaddr *) &adresseClient, &lgadresseClient);
        
        if (socket_service == -1)
        {
             perror("accept");
             exit(5);
        }       
         /* un client est arrive */
        printf("connexion acceptee\n");
         fflush(stdout);

        char stock[SIZE];
        int nbOctets;

        while(nbOctets = read(socket_service, stock, sizeof(stock)))
        {
            if(nbOctets <SIZE)
            {
                stock[nbOctets] = '\0';
            }

            printf("J'écris %d octets dans le fichier appelé %s le texte: %s\n", nbOctets, argv[2], stock);
            write(fd2, stock, nbOctets);
        }

       /* ecriture dans la socket d'un entier */
        entier_envoye = 2222;
        if (write(socket_service, &entier_envoye, sizeof(int)) != sizeof(int))
        {
         perror("write");
         exit(7);
        }
         printf("Entier envoye : %d\n", entier_envoye);
         close(socket_service);
     } /* fin du while – le serveur peut attendre un nouveau client */
    close(fd2);
    close(socket_RV);
}
