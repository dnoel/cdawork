#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int length = 255;
    char name[length];
    if(gethostname(name, length)==-1)
    {
        printf("Erreur\n");
        exit(1);
    }

    printf("hostname : %s\n", name);

    struct hostent *host;
    struct in_addr adr; 
    host = gethostbyname(name);
    bcopy(host->h_addr, &adr, host->h_length);
    printf("Entier et %u\n", adr.s_addr);
    printf("Adresse : %s\n", inet_ntoa(adr));
}

