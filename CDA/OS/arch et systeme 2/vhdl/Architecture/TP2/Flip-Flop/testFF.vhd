library IEEE;
use IEEE.std_logic_1164.all;

entity testFF is
end testFF;

architecture sim of testFF is
	signal CK		: STD_LOGIC := '0';
	signal Din, S	: STD_LOGIC;

	component FF
		port( D, clk	: in STD_LOGIC;
			Q		: out STD_LOGIC);
	end component;

	begin
		CK <= not CK after 50 ns;

		Din <= '0', '1' after 100 ns, '0' after 120 ns, '1' after 130 ns, '0' after 150 ns;

		req : FF port map(Din, CK, S);
end sim;
