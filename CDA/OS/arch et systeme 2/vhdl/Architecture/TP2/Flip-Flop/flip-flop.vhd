library IEEE;
use IEEE.std_logic_1164.all;

entity FF is
	port( D, clk	: in STD_LOGIC;
			Q		: out STD_LOGIC);
end FF;

architecture archiFF of FF is
	begin
		process(clk)
		begin
			if clk'event and clk = '0' then
				Q <= D;
			end if;
	end process;
end archiFF;
