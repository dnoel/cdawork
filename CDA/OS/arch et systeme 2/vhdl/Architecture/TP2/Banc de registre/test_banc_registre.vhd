library IEEE;
use IEEE.std_logic_1164.all;

entity testBancRegistre is
end testBancRegistre;

architecture sim of testBancRegistre is

	signal Ew, Ea, Eb			: STD_LOGIC_VECTOR('31' downto '0');
	signal Ecr, Eck				: STD_LOGIC;
	signal Edest, Esrc1, Esrc2	: STD_LOGIC_VECTOR('3' downto '0');

	component banc_registre

	port(	portW							: in STD_LOGIC_VECTOR('31' downto '0');
			Banc_Ecr						: in STD_LOGIC;
			Dest, src1, src2				: in STD_LOGIC_VECTOR('3' downto '0');
			CK								: in STD_LOGIC;
			portA, portB					: out STD_LOGIC_VECTOR('31' downto '0'));

			Ea <= "0001";
			Eb <= "0010";
			
	begin

	end sim;
