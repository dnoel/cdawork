library IEEE;
use IEEE.std_logic_1164.all;

entity banc_registre is
	port(	portW							: in STD_LOGIC_VECTOR('31' downto '0');
			Banc_Ecr						: in STD_LOGIC;
			Dest, src1, src2				: in STD_LOGIC_VECTOR('3' downto '0');
			CK								: in STD_LOGIC;
			portA, portB					: out STD_LOGIC_VECTOR('31' downto '0'));
end entity

architecture archiBancRegistre of banc_registre is

	type type_banc is array('0' to '15') of STD_LOGIC_VECTOR('31' downto '0');
	signal banc	: type_banc

	begin
		portA <= banc(to_integer(unsigned(src1));
		portB <= banc(to_integer(unsigned(src2));
		portW <= portA + portB;

		process(CK)	begin

			if clk'event and clk = '0' then
				if Banc_Ecr = '1' then
					banc(to_integer(unsigned(Dest)) <= portW;
				end if;
			end if;

		end process;

end archiBancRegistre;
