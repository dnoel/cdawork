entity horloge is
end horloge;

architecture sim of horloge is
	signal Eclk		: BIT := '0';

	begin

		Eclk <= not Eclk after 10 ns;

	end sim;
