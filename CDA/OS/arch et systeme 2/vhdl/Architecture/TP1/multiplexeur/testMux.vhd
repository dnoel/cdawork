entity testMux is
end testMux;

architecture sim of testMux is
	signal Ea, Eb, Ec	: BIT;
	signal S			: BIT;

	component mux

	port (A, B, C	: in BIT;
			S		: out BIT);
	end component;

	begin
		Eb <= '0', '1' after 100 ns, '0' after 150 ns;
		Ea <= '0', '1' after 50 ns, '0' after 120 ns;
		Ec <= '0', '1' after 110 ns;

		m : mux port map(Ea, Eb, Ec, S);
end sim;
