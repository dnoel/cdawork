entity mux is
	port(A, B, C 	: in BIT;
			S		: out BIT
	);
end mux;

architecture archiMux of mux is
	begin
		S <= A when C = '0' else B;
	end archiMux;
