-- Unite de controle processeur pipeline
-- Version à compléter
-- Auteur : 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity control_unit is
port (	Clk 		: in std_logic;

	Inst 		: in std_logic_vector(31 downto 0);
	Src_PC 		: out std_logic;

	Src1, Src2	: out std_logic_vector(3 downto 0);

	Imm     	: out std_logic_vector(15 downto 0);
	Src_Op_B 	: out std_logic; 	
	Cmd_UAL 	: out std_logic_vector(1 downto 0);
	Z,N		: in std_logic;
	
	Src_Adr_Branch 	: out std_logic; 	
	RW, Bus_Val 	: out std_logic;

	Banc_Src 	: out std_logic_vector(1 downto 0);
	Dest 		: out std_logic_vector(3 downto 0);
	Banc_Ecr 	: out std_logic 
);
end control_unit;

architecture beh of control_unit is

signal di_ri :  std_logic_vector(31 downto 0) := "00000000000000000000000000000000";
...
	
begin

	-- registre RI
	ri : process(clk)
	begin
		if Clk'event and Clk='0' then
			di_ri <= Inst;
		end if;
	end process;

	signal codeop : std_logic_vector(3 downto 0);
	codeop <= di_ri(31 downto 28);

	case codeop is
		when "0000" =>
		NOP
			
		when "0001" =>
		Src_PC 		<= '1';
		Banc_Src 	<= "01";
		Src1 		<= dri_ri(27 downto 24);
		Src2 		<= dri_ri(23 downto 20);;
		Dest 		<= dri_ri(19 downto 16);
		Banc_Ecr	<= '1';
		Cmd_Ual         <= "00";     -- Z=0, N=0;
		Src_Op_B        <= '0';
		Src_adr_branch  <= '-';
		Bus_Donnees_in  <= "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU";
		imm             <= dri_ri(15 downto 0);

		when "0010" =>
		Src_PC 			<= '1';
		Banc_Src	 	<= "01";
		Src1 			<= "0000";
		Src2 			<= "0010";
		Dest 			<= "0100";
		Banc_Ecr		<= '1';
		Cmd_Ual         <= "01";     -- Z=0, N=0;
		Src_Op_B        <= '1';
		Src_adr_branch  <= '-';

		when "0011" =>
		Src_PC 			<= '1';
		Banc_Src	 	<= "01";
		Src1 			<= "0010";
		Src2 			<= "0100";
		Dest 			<= "0100";
		Banc_Ecr		<= '0';
		Cmd_Ual         <= "00";     -- Z=0, N=0;
		Src_Op_B        <= '0';
		Src_adr_branch  <= '-';
		imm             <= dri_ri(15 downto 0);

		when "0100" =>
		Src_PC 			<= '1';
		Banc_Src	 	<= "00";
		Src1 			<= "0010";
		Src2 			<= "0100";
		Dest 			<= "0100";
		Banc_Ecr		<= '1';
		Cmd_Ual         <= "00";     -- Z=0, N=0;
		Src_Op_B        <= '0';
		Src_adr_branch  <= '-';
		imm             <= dri_ri(15 downto 0);

		when "0101" =>
		JSR

		when "0110" =>
		RTS

		when "0111" =>
		BEQ

		when "1000" =>
		BRA

	 end case;



	

end beh;
