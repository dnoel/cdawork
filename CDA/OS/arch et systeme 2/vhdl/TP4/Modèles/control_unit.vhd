-- Unite de controle processeur pipeline
-- Version à compléter
-- Auteur : 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity control_unit is
port (	Clk 		: in std_logic;

	Inst 		: in std_logic_vector(31 downto 0);
	Src_PC 		: out std_logic;

	Src1, Src2	: out std_logic_vector(3 downto 0);

	Imm     	: out std_logic_vector(15 downto 0);
	Src_Op_B 	: out std_logic; 	
	Cmd_UAL 	: out std_logic_vector(1 downto 0);
	Z,N		: in std_logic;
	
	Src_Adr_Branch 	: out std_logic; 	
	RW, Bus_Val 	: out std_logic;

	Banc_Src 	: out std_logic_vector(1 downto 0);
	Dest 		: out std_logic_vector(3 downto 0);
	Banc_Ecr 	: out std_logic 
);
end control_unit;

architecture beh of control_unit is

signal di_ri :  std_logic_vector(31 downto 0) := "00000000000000000000000000000000";
signal codeop std_logic_vector(3 downto 0);
	
begin

	-- registre RI
	ri : process(clk)
	begin
		if Clk'event and Clk='0' then
			di_ri <= Inst;
		end if;
	end process;

		codeop 	<= di_ri(31 downto 28);
		Src1 	<= di_ri(27 downto 24);
		Src2 	<= di_ri(23 downto 20);
		Dest 	<= di_ri(19 downto 16);
		Dest 	<= di_ri(15 downto 0);

		case codeop is
			--NOP
			when "0000" =>
			
			--ADD
			when "0001" =>
				Src_PC 		<= '1';
				Banc_Src 	<= "01";
				Banc_Ecr	<= '1';
				Cmd_Ual         <= "00";
				Src_Op_B        <= '0';
				Src_adr_branch  <= '-';

			--SUB
			when "0010" =>
				Src_PC 			<= '1';
				Banc_Src	 	<= "01";
				Banc_Ecr		<= '1';
				Cmd_Ual         <= "01";  
				Src_Op_B        <= '1';
				Src_adr_branch  <= '-';
			
			--SW
			when "0011" =>
				Src_PC 			<= '1';
				Banc_Src	 	<= "01";
				Banc_Ecr		<= '0';
				Cmd_Ual         <= "00";
				Src_Op_B        <= '0';
				Src_adr_branch  <= '-';
			
			--LW
			when "0100" =>
				Src_PC 			<= '1';
				Banc_Src	 	<= "00";
				Banc_Ecr		<= '1';
				Cmd_Ual         <= "00";     
				Src_Op_B        <= '0';
				Src_adr_branch  <= '-';

			--JSR
			when "0101" =>
				Src_PC 			<= '0';
				Banc_Src	 	<= "10";
				Banc_Ecr		<= '1';
				Cmd_Ual         <= "00";     
				Src_Op_B        <= '1';
				Src_adr_branch  <= '1';

			--RTS
			when "0110" =>
				Src_PC 			<= '0';
				Banc_Src	 	<= "10";
				Banc_Ecr		<= '1';
				Cmd_Ual         <= "00";     
				Src_Op_B        <= '1';
				Src_adr_branch  <= '1';

			--BEQ
			when "0111" =>
				if (Src1= Src2) then
					Src_PC 			<= '0';
					Banc_Src	 	<= "-";
					Banc_Ecr		<= '0';
					Cmd_Ual         <= "-";     
					Src_Op_B        <= '1';
					Src_adr_branch  <= '0';

			--BRA
			when "1000" =>
				Src_PC 			<= '0';
				Banc_Src	 	<= "10";
				Banc_Ecr		<= '1';
				Cmd_Ual         <= "00";     
				Src_Op_B        <= '1';
				Src_adr_branch  <= '1';

		 end case;

	
		wait for 10 ns;


end beh;
