#define _GNU_SOURCE
#include <stdio.h>
#include <sched.h>

int main()
{
    int cpu = sched_getcpu();
    int newcpu;
    while(1)
    {
        newcpu = sched_getcpu();
        if(cpu!=newcpu) printf("Passage du cpu %d au cpu %d\n", cpu, newcpu);
        cpu = newcpu;
    }
    return 0;
}