#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char * argv[])
{
    if(argc != 2)
    {
        printf("./ex2 int cpuId\n");
        exit(1);
    }
    cpu_set_t cpuEnsemble; 
    CPU_SET(atoi(argv[1]), &cpuEnsemble);

    if(sched_setaffinity(getpid(), sizeof(cpuEnsemble), &cpuEnsemble) ==-1)
    {
        printf("Error while setting affinity of the process\n");
        exit(1);
    }

    if(CPU_ISSET(atoi(argv[1]), &cpuEnsemble)==0)
    {
        printf("Le cpu entré n'a pas été ajouté dans l'ensemble\n");
        exit(1);
    }
    int cpu = sched_getcpu();
    printf("Le cpu utilisé est %d\n", cpu);
    int newcpu= 0;
    while(1)
    {
        newcpu = sched_getcpu();
        if(cpu!=newcpu) printf("Passage du cpu %d au cpu %d\n", cpu, newcpu);
        cpu = newcpu;
    }
    return 0;
}