#define _GNU_SOURCE

#include <sched.h>
#include <pthread.h>           /* produit.c */
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

/*********** Data Type ***********/

typedef enum
{
	STATE_WAIT,
  	STATE_MULT,
  	STATE_ADD,
  	STATE_PRINT
} State;

typedef struct
{
	State state;
  	int * pendingMult;
  	pthread_cond_t cond;
  	pthread_mutex_t mutex;
  	size_t nbIterations;
  	size_t size;
  	double * v1;
  	double * v2;
  	double * v3;
  	double result;
} Product;

/*********** Data structure ***********/

Product prod;

/*********** CPU data ************/
long cpu_nb;
int cpu_next = 0;


/*********** Function ***********/

void initPendingMult(Product * prod)
{
	size_t i;
	for(i=0;i<prod->size;i++)
	{
		prod->pendingMult[i]=1;
	}
}

int nbPendingMult(Product * prod)
{
	size_t i;
	int nb=0;
	for(i=0;i<prod->size;i++)
	{
		nb+=prod->pendingMult[i];
	}
	return(nb);
}

void wasteTime(unsigned long ms)
{
	unsigned long t,t0;
	struct timeval tv;
	gettimeofday(&tv,(struct timezone *)0);
	t0=tv.tv_sec*1000LU+tv.tv_usec/1000LU;
	do
	{
		gettimeofday(&tv,(struct timezone *)0);
		t=tv.tv_sec*1000LU+tv.tv_usec/1000LU;
	} while(t-t0<ms);
}

/*****************************************************************************/
void * mult(void * data)
{
	size_t index;
	size_t iter;

	cpu_set_t * cpu_set = malloc(sizeof(cpu_set_t)* cpu_nb);

	pthread_mutex_lock(&prod.mutex);
	CPU_SET(cpu_next, cpu_set);

	sched_setaffinity(0, sizeof(cpu_set_t), cpu_set);

	CPU_ZERO(cpu_set);

	if(((cpu_next+1) % cpu_nb) == 0) cpu_next = 0;
	else cpu_next++;

	free(cpu_set);
	pthread_mutex_unlock(&prod.mutex);

	index = *(size_t *) &data;

	fprintf(stderr,"Begin mult(%ld) | CPU : %d\n",index, sched_getcpu());
		                                       /* Tant que toutes les iterations */
	for(iter=0;iter<prod.nbIterations;iter++)  /* n'ont pas eu lieu              */
	{
		pthread_mutex_lock(&prod.mutex);

		while(prod.state != STATE_MULT || prod.pendingMult[index] != 1)
		{
			pthread_cond_wait(&prod.cond, &prod.mutex);
		}

		pthread_mutex_unlock(&prod.mutex);

		fprintf(stderr,"--> mult(%ld)\n",index); /* La multiplication peut commencer */

		prod.v3[index] = prod.v1[index] * prod.v2[index];

		wasteTime(200+(rand()%200)); /* Perte du temps avec wasteTime() */

	 	/* Affichage du calcul sur l'index */
		fprintf(stderr,"<-- mult(%ld) : %.3g*%.3g=%.3g\n", index,prod.v1[index],prod.v2[index],prod.v3[index]);

		/*=>Marquer la fin de la multiplication en cours... */
		prod.pendingMult[index] = 0;

		/*=>Si c'est la derniere... */
		pthread_mutex_lock(&prod.mutex);

		if(nbPendingMult(&prod) == 0){
			/*=>Autoriser le demarrage de l'addition... */
			prod.state = STATE_ADD;
			pthread_cond_broadcast(&prod.cond);
		}
		   
		pthread_mutex_unlock(&prod.mutex);
	}
	fprintf(stderr,"Quit mult(%ld)\n",index);
	pthread_exit(NULL);
}

/*****************************************************************************/
void * add(void * data)
{
	cpu_set_t * cpu_set = malloc(sizeof(cpu_set_t)* cpu_nb);

	pthread_mutex_lock(&prod.mutex);
	CPU_SET(cpu_next, cpu_set);

	sched_setaffinity(0, sizeof(cpu_set_t), cpu_set);

	CPU_ZERO(cpu_set);

	if(((cpu_next+1) % cpu_nb) == 0) cpu_next = 0;
	else cpu_next++;

	free(cpu_set);
	pthread_mutex_unlock(&prod.mutex);

	size_t iter;
	fprintf(stderr,"Begin add() | CPU : %d\n", sched_getcpu());
		                                       /* Tant que toutes les iterations */
	for(iter=0;iter<prod.nbIterations;iter++)  /* n'ont pas eu lieu              */
	{
		size_t index;

		/*=>Attendre l'autorisation d'addition... */
		pthread_mutex_lock(&prod.mutex);

		while(prod.state != STATE_ADD)
		{
			pthread_cond_wait(&prod.cond, &prod.mutex);
		}

		pthread_mutex_unlock(&prod.mutex);

		fprintf(stderr,"--> add\n"); /* L'addition peut commencer */

		/* Effectuer l'addition... */
		prod.result=0.0;
		for(index=0;index<prod.size;index++)
		{
			prod.result += prod.v3[index];
		}

		wasteTime(100+(rand()%100)); /* Perdre du temps avec wasteTime() */

		fprintf(stderr,"<-- add\n");

		/*=>Autoriser le demarrage de l'affichage... */
		pthread_mutex_lock(&prod.mutex);
		prod.state = STATE_PRINT;
		pthread_cond_broadcast(&prod.cond);
		pthread_mutex_unlock(&prod.mutex);

		//printf("ETAT : %d\n", prod.state);
	}
	fprintf(stderr,"Quit add()\n");
	pthread_exit(NULL);
}

/*****************************************************************************/
int main(int argc,char ** argv)
{
	size_t i, iter;
	pthread_t *multTh;
	size_t    *multData;
	pthread_t  addTh;
	void      *threadReturnValue;
	pthread_attr_t attr;
	struct stat sb;
	char *addr, *endptr;
	long val;
	int fd;

    if((cpu_nb = sysconf(_SC_NPROCESSORS_ONLN)) == -1) perror("sysconf");

	/* A cause de warnings lorsque le code n'est pas encore la...*/
	 (void)addTh; (void)threadReturnValue;

	/* Lire le nombre d'iterations et la taille des vecteurs  */
	fd = open(argv[1], O_RDONLY);

	fstat(fd, &sb);

	addr = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

	prod.nbIterations = strtol(addr, &endptr, 10);
	addr = endptr;

	prod.size = strtol(addr, &endptr, 10);
	addr = endptr;

	/* Initialisations (Product, tableaux, generateur aleatoire,etc) */

	prod.state=STATE_WAIT;

	prod.pendingMult=(int *)malloc(prod.size*sizeof(int));
	

	/*=>initialiser prod.mutex ... */

	if(pthread_mutex_init(&prod.mutex, NULL) == -1) perror("pthread_mutex_init"); 

	/*=>initialiser prod.cond ...  */

	if(pthread_cond_init(&prod.cond, NULL) == -1) perror("pthread_cond_init");

	/* Allocation dynamique des 3 vecteurs v1, v2, v3 */

	prod.v1=(double *)malloc(prod.size*sizeof(double));
	prod.v2=(double *)malloc(prod.size*sizeof(double));
	prod.v3=(double *)malloc(prod.size*sizeof(double));

	/* Allocation dynamique du tableau pour les threads multiplieurs */

	multTh=(pthread_t *)malloc(prod.size*sizeof(pthread_t));

	/* Allocation dynamique du tableau des MulData */

	multData=(size_t *)malloc(prod.size*sizeof(size_t));

	/* Initialisation du tableau des MulData */

	for(i=0;i<prod.size;i++)
	{
		multData[i]=i;
	}

	/*=>Creer les threads de multiplication... */

	pthread_attr_init(&attr);

	for(i = 0; i < prod.size; i++)
	{
		if(pthread_create(&multTh[i], &attr, mult, (void *) multData[i]) == -1) perror("pthread_create");
	}

	/*=>Creer le thread d'addition...          */

	if(pthread_create(&addTh, &attr, add, NULL) == -1) perror("pthread_create");

	srand(time((time_t *)0));   /* Init du generateur de nombres aleatoires */

	/* Pour chacune des iterations a realiser, c'est a dire :                   */
	for(iter=0;iter<prod.nbIterations;iter++) /* tant que toutes les iterations */
	{                                       /* n'ont pas eu lieu              */
		size_t j;

	  	initPendingMult(&prod);
	 
	  	/* Initialiser aleatoirement les deux vecteurs */

		for(j=0;j<prod.size;j++)
		{
			prod.v1[j] = strtol(addr, &endptr, 10);
			addr = endptr;
		}
		for(j=0;j<prod.size;j++)
		{
			prod.v2[j] = strtol(addr, &endptr, 10);
			addr = endptr;
		}

	  	/*=>Autoriser le demarrage des multiplications pour une nouvelle iteration..*/

		pthread_mutex_lock(&prod.mutex);
        
        prod.state = STATE_MULT;

        pthread_cond_broadcast(&prod.cond);
        
        pthread_mutex_unlock(&prod.mutex);

	  	/*=>Attendre l'autorisation d'affichage...*/
        
        pthread_mutex_lock(&prod.mutex);

        while(prod.state != STATE_PRINT)
		{
            pthread_cond_wait(&prod.cond, &prod.mutex);
        }

        pthread_mutex_unlock(&prod.mutex);

	  	/*=>Afficher le resultat de l'iteration courante...*/
        
        printf("ITERATION : %ld, RESULT = %f\n", iter, prod.result);
	}
	/*=>Attendre la fin des threads de multiplication...*/
    for(i = 0; i < prod.size; i++){
		pthread_join(multTh[i], NULL);
	}

	/*=>Attendre la fin du thread d'addition...*/
    pthread_join(addTh, NULL);

	/*=> detruire prod.cond ... */
    pthread_cond_destroy(&prod.cond);

	/*=> detruire prod.mutex ... */
    pthread_mutex_destroy(&prod.mutex);

	/* Detruire avec free ce qui a ete initialise avec malloc */

	//free(prod.pendingMult);
	free(prod.v1);
	free(prod.v2);
	free(prod.v3);
	free(multTh);
	free(multData);
	return(EXIT_SUCCESS);
}
