#define _GNU_SOURCE
#define PROT PROT_READ
#define RIGHTS MAP_PRIVATE
#define BASE 10
#include <pthread.h>           /* produit.c */
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>




/*********** Data Type ***********/

typedef enum
{
  STATE_WAIT,
  STATE_MULT,
  STATE_ADD,
  STATE_PRINT
} State;

typedef struct
{
  State state;
  int * pendingMult;
  pthread_cond_t cond;
  pthread_mutex_t mutex;
  size_t nbIterations;
  size_t size;
  double * v1;
  double * v2;
  double * v3;
  double result;
} Product;

/*********** Data structure ***********/

Product prod;

/*********** Function ***********/
int NBCPU = 0;
int nextCPU = 0;

void setnbCPU()
{
    NBCPU = (int)sysconf(_SC_NPROCESSORS_ONLN);
}

int setAffinity(cpu_set_t cpuEnsemble)
{
    CPU_SET(nextCPU, &cpuEnsemble);

    if(sched_setaffinity(0, sizeof(cpuEnsemble), &cpuEnsemble) ==-1)
    {
        printf("Error while setting affinity of the process\n");
        exit(1);
    }

    if(CPU_ISSET(nextCPU, &cpuEnsemble)==0)
    {
        printf("Le cpu entré n'a pas été ajouté dans l'ensemble\n");
        exit(1);
    }
    nextCPU = (nextCPU+1)%NBCPU;
}



void initPendingMult(Product * prod)
{
  size_t i;
  for(i=0;i<prod->size;i++)
  {
    prod->pendingMult[i]=1;
  }
}

int nbPendingMult(Product * prod)
{
  size_t i;
  int nb=0;
  for(i=0;i<prod->size;i++)
  {
    nb+=prod->pendingMult[i];
  }
  return(nb);
}

void wasteTime(unsigned long ms)
{
  unsigned long t,t0;
  struct timeval tv;
  gettimeofday(&tv,(struct timezone *)0);
  t0=tv.tv_sec*1000LU+tv.tv_usec/1000LU;
  do
  {
    gettimeofday(&tv,(struct timezone *)0);
    t=tv.tv_sec*1000LU+tv.tv_usec/1000LU;
  } while(t-t0<ms);
}

/*****************************************************************************/
void * mult(void * data)
{
  size_t index = *(size_t*)data ;
  size_t iter;
  cpu_set_t cpuEnsemble;

    pthread_mutex_lock(&prod.mutex);
    setAffinity(cpuEnsemble);
    pthread_mutex_unlock(&prod.mutex);

  // fprintf(stderr,"Begin mult(%ld) on CPU %d\n",index, sched_getcpu());
                                    /* Tant que toutes les iterations */
  for(iter=0;iter<prod.nbIterations;iter++)  /* n'ont pas eu lieu              */
  {
    /*=>Attendre l'autorisation de multiplication POUR UNE NOUVELLE ITERATION...*/

    pthread_mutex_lock(&prod.mutex);
    while(prod.state != STATE_MULT || prod.pendingMult[index]==0)
    {
      pthread_cond_wait(&prod.cond, &prod.mutex);
    }    
    pthread_mutex_unlock(&prod.mutex);

    fprintf(stderr,"--> mult(%ld) on CPU %d\n",index, sched_getcpu()); /* La multiplication peut commencer */

    /*=>Effectuer la multiplication a l'index du thread courant... */

    // pthread_mutex_lock(&prod.mutex);
    prod.v3[index] = prod.v1[index] * prod.v2[index];
    // pthread_mutex_unlock(&prod.mutex);

    wasteTime(200+(rand()%200)); /* Perte du temps avec wasteTime() */

    // fprintf(stderr,"<-- mult(%ld) on CPU %d : %.3g*%.3g=%.3g\n", index, sched_getcpu(), prod.v1[index],prod.v2[index],prod.v3[index]);          /* Affichage du */
    /* calcul sur   */
                                                        /* l'index      */
    /*=>Marquer la fin de la multiplication en cours... */
    // pthread_mutex_lock(&prod.mutex);
    prod.pendingMult[index] = 0;
    // pthread_mutex_unlock(&prod.mutex);
    /*=>Si c'est la derniere... */
    
    pthread_mutex_lock(&prod.mutex);
    if(nbPendingMult(&prod)==0)
    {
      /*=>Autoriser le demarrage de l'addition... */  
      prod.state = STATE_ADD; 
      pthread_cond_broadcast(&prod.cond);
    }
    pthread_mutex_unlock(&prod.mutex);

  } 
  // fprintf(stderr,"Quit mult(%ld) on CPU %d\n",index, sched_getcpu());
  return(data);
}

/*****************************************************************************/
void * add(void * data)
{
  size_t iter;
  cpu_set_t cpuEnsemble;
    fprintf(stderr,"Begin add() on CPU %d\n", sched_getcpu());
     pthread_mutex_lock(&prod.mutex);
    setAffinity(cpuEnsemble);
    pthread_mutex_unlock(&prod.mutex);
                                  /* Tant que toutes les iterations */
  for(iter=0;iter<prod.nbIterations;iter++)  /* n'ont pas eu lieu              */
  {
    size_t index;

    /*=>Attendre l'autorisation d'addition... */
    pthread_mutex_lock(&prod.mutex);
    while(prod.state != STATE_ADD)
    {     
      pthread_cond_wait(&prod.cond, &prod.mutex);
    }
    pthread_mutex_unlock(&prod.mutex);
    fprintf(stderr,"--> add on CPU %d\n", sched_getcpu()); /* L'addition peut commencer */

    /* Effectuer l'addition... */
    prod.result=0.0;
    for(index=0;index<prod.size;index++)
    {
      prod.result += prod.v3[index];
    }

    wasteTime(100+(rand()%100)); /* Perdre du temps avec wasteTime() */

    fprintf(stderr,"<-- add on CPU %d\n", sched_getcpu());

    /*=>Autoriser le demarrage de l'affichage... */
    pthread_mutex_lock(&prod.mutex);
    prod.state = STATE_PRINT;  
    pthread_cond_broadcast(&prod.cond);
    pthread_mutex_unlock(&prod.mutex);

  }
  fprintf(stderr,"Quit add() on CPU %d\n", sched_getcpu());
  return(data);
}

long int getVal(char *pDebut, char *pFin)
{
  long long int res = strtol(pDebut, &pFin, BASE);
  pDebut = pFin;
  return res;
}


/*****************************************************************************/
int main(int argc,char ** argv)
{
  if(argc!=2)
  {
    printf("fileName\n"); exit(0);
  } 
  
  size_t i, iter;
  pthread_t *multTh;
  size_t    *multData;
  pthread_t  addTh;
  void      *threadReturnValue;
  int fd;
  struct stat buff;
  char *pDebut, *pFin;

  /* A cause de warnings lorsque le code n'est pas encore la...*/
  (void)addTh; (void)threadReturnValue;

  /* Lire le nombre d'iterations et la taille des vecteurs */

    fd=open(argv[1], O_RDONLY);
    
    fstat(fd, &buff);
    pDebut = mmap(NULL, buff.st_size , PROT_READ, MAP_PRIVATE, fd, 0);

    //prod.nbIterations = getVal(pDebut, pFin);
    prod.nbIterations = strtol(pDebut, &pFin, BASE);
    pDebut = pFin;
    printf("nbIteration: %d\n", (int)prod.nbIterations);

    prod.size = strtol(pDebut, &pFin, BASE);
    pDebut = pFin;
    printf("vectorSize: %d\n", (int)prod.size);

  /* Initialisations (Product, tableaux, generateur aleatoire,etc) */

  prod.state=STATE_WAIT;

  prod.pendingMult=(int *)malloc(prod.size*sizeof(int));

  /*=>initialiser prod.mutex ... */
  if(pthread_mutex_init(&prod.mutex, NULL) == -1)
  {
    printf("Erreur lors de la création du mutex\n");
    return (EXIT_FAILURE);
  }
  
  /*=>initialiser prod.cond ...  */
  if(pthread_cond_init(&prod.cond, NULL)==-1)
  {
    printf("Erreur lors de la création de la condition\n");
    return(EXIT_FAILURE);
  }

  /* Allocation dynamique des 3 vecteurs v1, v2, v3 */

  prod.v1=(double *)malloc(prod.size*sizeof(double));
  prod.v2=(double *)malloc(prod.size*sizeof(double));
  prod.v3=(double *)malloc(prod.size*sizeof(double));

  /* Allocation dynamique du tableau pour les threads multiplieurs */

  multTh=(pthread_t *)malloc(prod.size*sizeof(pthread_t));

  /* Allocation dynamique du tableau des MulData */

  multData=(size_t *)malloc(prod.size*sizeof(size_t));

  /* Initialisation du tableau des MulData */

  for(int i = 0; i<prod.size; i++)
  {
    multData[i] = i;
  }

  setnbCPU();

  /*=>Creer les threads de multiplication... */
  for(i = 0; i<prod.size; i++)
  { 
    if(pthread_create(&multTh[i], NULL, mult, (void*)&multData[i])==-1)
    {
      printf("Erreur lors de la création du thread %ld\n", i);
      return(EXIT_FAILURE);
    }
  }

  /*=>Creer le thread d'addition...          */
  if(pthread_create(&addTh, NULL, add, NULL)==-1)
  {
    printf("Erreur lors de la création du thread d'addition\n");
    return(EXIT_FAILURE);
  }

  /* Pour chacune des iterations a realiser, c'est a dire :                   */
  for(iter=0;iter<prod.nbIterations;iter++) /* tant que toutes les iterations */
  {                                       /* n'ont pas eu lieu              */
    size_t j;

    initPendingMult(&prod);

    //Initialisation des arguments des vecteurs
    for(j=0;j<prod.size;j++)
		{
			prod.v1[j] = strtol(pDebut, &pFin, 10);
			pDebut = pFin;
		}
		for(j=0;j<prod.size;j++)
		{
			prod.v2[j] = strtol(pDebut, &pFin, 10);
			pDebut = pFin;
		}


    /*=>Autoriser le demarrage des multiplications pour une nouvelle iteration..*/
    pthread_mutex_lock(&prod.mutex);
    prod.state = STATE_MULT; 
    
    pthread_cond_broadcast(&prod.cond);
    pthread_mutex_unlock(&prod.mutex);
    
    /*=>Attendre l'autorisation d'affichage...*/
    pthread_mutex_lock(&prod.mutex);
    while(prod.state != STATE_PRINT)
    {
      pthread_cond_wait(&prod.cond, &prod.mutex);
    }
    pthread_mutex_unlock(&prod.mutex);

    /*=>Afficher le resultat de l'iteration courante...*/

    printf("Résultat de l'itération %ld est : %.1f\n", iter+1, prod.result);
  }
 
  /*=>Attendre la fin des threads de multiplication...*/
  for(int j = i-1; j>=0; j--)
  {
    if(pthread_join(multTh[j], threadReturnValue)==-1)
    {
      printf("Erreur lors de l'attente de la fin du thread de multiplication  du thread %ld\n", i);
      return(EXIT_FAILURE);
    }
  }

  /*=>Attendre la fin du thread d'addition...*/
  if(pthread_join(addTh, threadReturnValue)==-1)
  {
    printf("Erreur lors de l'attente de la fin du thread de multiplication  du thread %ld\n", i);
    return(EXIT_FAILURE);
  }

  /*=> detruire prod.cond ... */
   pthread_cond_destroy(&prod.cond);
  /*=> detruire prod.mutex ... */
  pthread_mutex_destroy(&prod.mutex);

  /* Detruire avec free ce qui a ete initialise avec malloc */

  free(prod.pendingMult);
  free(prod.v1);
  free(prod.v2);
  free(prod.v3);
  free(multTh);
  free(multData);
  return(EXIT_SUCCESS);
}
