#define _GNU_SOURCE
#define PROT PROT_READ
#define RIGHTS MAP_PRIVATE
#define BASE 10
#include <pthread.h>           /* produit.c */
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>




/*********** Data Type ***********/

typedef enum
{
  STATE_WAIT,
  STATE_MULT,
  STATE_WRITE
} State;

typedef struct 
{
    int tailleLigne;
    int tailleColonne;
    int * valeurs;
}   matrice_t;

typedef struct
{
    State state;
    int * pendingMult;
    pthread_cond_t cond;
    pthread_mutex_t mutex;
    size_t nbIterations;
    size_t nbMult;
    matrice_t * mat1;
    matrice_t * mat2;
    matrice_t * mat3;
    double result;
    char *pDebut, *pFin;
} Product;

/*********** Data structure ***********/

Product prod;

/*********** Function ***********/
int NBCPU = 0;
int nextCPU = 0;

void setnbCPU()
{
    NBCPU = (int)sysconf(_SC_NPROCESSORS_ONLN);
}

int setAffinity(cpu_set_t cpuEnsemble)
{
    CPU_SET(nextCPU, &cpuEnsemble);

    if(sched_setaffinity(0, sizeof(cpuEnsemble), &cpuEnsemble) ==-1)
    {
        printf("Error while setting affinity of the process\n");
        exit(1);
    }

    if(CPU_ISSET(nextCPU, &cpuEnsemble)==0)
    {
        printf("Le cpu entré n'a pas été ajouté dans l'ensemble\n");
        exit(1);
    }
    nextCPU = (nextCPU+1)%NBCPU;
}



void initPendingMult(Product * prod)
{
  size_t i;
  for(i=0;i<prod->nbMult;i++)
  {
    prod->pendingMult[i]=1;
  }
}

int nbPendingMult(Product * prod)
{
  size_t i;
  int nb=0;
  for(i=0;i<prod->nbMult;i++)
  {
    nb+=prod->pendingMult[i];
  }
  return(nb);
}

void wasteTime(unsigned long ms)
{
  unsigned long t,t0;
  struct timeval tv;
  gettimeofday(&tv,(struct timezone *)0);
  t0=tv.tv_sec*1000LU+tv.tv_usec/1000LU;
  do
  {
    gettimeofday(&tv,(struct timezone *)0);
    t=tv.tv_sec*1000LU+tv.tv_usec/1000LU;
  } while(t-t0<ms);
}

/*****************************************************************************/
void * mult(void * data)
{
  size_t index = *(size_t*)data;
  size_t iter;
  cpu_set_t cpuEnsemble;

    char * pDebut = prod.pDebut;
    char * pFin = prod.pFin;

    pthread_mutex_lock(&prod.mutex);
    setAffinity(cpuEnsemble);
    pthread_mutex_unlock(&prod.mutex);

//   fprintf(stderr,"Begin mult(%ld) on CPU %d\n",index, sched_getcpu());

    int ligne[prod.mat1->tailleLigne];
    int colonne[prod.mat2->tailleColonne];

    int rangLigne = index/prod.mat3->tailleLigne;
    int rangColonne = index%prod.mat3->tailleLigne;

    printf(" Processus %ld : rangLigne / rangColonne : (%d, %d)\n", index, rangLigne, rangColonne);

    /* Tant que toutes les iterations */
    for(iter=0;iter<prod.nbIterations;iter++)  /* n'ont pas eu lieu              */
    {
        /*=>Attendre l'autorisation de multiplication POUR UNE NOUVELLE ITERATION...*/
        pthread_mutex_lock(&prod.mutex);
        while(prod.state != STATE_MULT || prod.pendingMult[index]==0)
        {
            pthread_cond_wait(&prod.cond, &prod.mutex);
        }    
        pthread_mutex_unlock(&prod.mutex);

        // fprintf(stderr,"--> mult(%ld) on CPU %d\n",index, sched_getcpu()); /* La multiplication peut commencer */
       
       //Lock du mutex afin de faire la lecture du ficheir par les threads
       printf("\n");
        pthread_mutex_lock(&prod.mutex);
        int value=0;        
        //On souhaite se placer à la  ligne de rang "rangLigne de la première matrice
        for(int i = 0; i<rangLigne; i++)
        {      
            for(int j=0; j< prod.mat1->tailleLigne; j++)
            {
                value = strtol(pDebut, &pFin, BASE);
                pDebut = pFin;
            }
        }

        //On récupère la ligne du rang voulu
        for(int i = 0; i<prod.mat1->tailleLigne; i++)
        {   
            ligne[i] = strtol(pDebut, &pFin, BASE);
            pDebut = pFin;
            printf(" Processus %ld récupère la %d valeur  de la ligne %d de la première matrice : %d\n", index, i, rangLigne, ligne[i]);

        }
        //On déplace le curseur jusqu'a la deuxième matrice
        for(int i=rangLigne; i< prod.mat1->tailleColonne-1; i++)
        {
            for(int j=0; j<prod.mat1->tailleLigne; j++)
            {
                value = strtol(pDebut, &pFin, BASE);
                pDebut = pFin;
            }
        }
        //La prochaine valeur à récupérer dans fichier est la première valeur de la deuxième matrice

        //On se positionne au début de la colonne de rang "rangColonne" voulue  de la deuxième matrice
        for(int i=0; i<rangColonne; i++)
        {
            value = strtol(pDebut, &pFin, BASE);
            pDebut = pFin;
        }

        //On récupère les valeurs de la colonne

        for(int i=0; i<prod.mat2->tailleColonne; i++)
        {
            colonne[i] = strtol(pDebut, &pFin, BASE);
            pDebut = pFin;
            printf(" Processus %ld récupère la valeur %d dans la colonne\n", index, colonne[i]);

            //On passe une ligne dans le fichier pour accéder à la valeur suivante de la colonne
            for(int j=0; j<prod.mat2->tailleLigne-1; j++)
            {
                value = strtol(pDebut, &pFin, BASE);
                pDebut = pFin;
            }
            //printf(" Processus %ld s'est positionné sur la prochaine valeur de la colonne\n", index);  
        }

        pthread_mutex_unlock(&prod.mutex);
        printf("\n");
        wasteTime(200+(rand()%200)); /* Perte du temps avec wasteTime() */

        //SOMME DES PRODUITS DES VALEURS

        int val = 0;
        for(int i = 0; i< prod.mat1->tailleLigne; i++)
        {
            val+= ligne[i] * colonne[i];
        }

        //On met le résultat dans le tableau de valeurs de mat3
        prod.mat3->valeurs[index] = val;
        //fprintf(stderr,"<-- mult(%ld) on CPU %d : %d\n", index, sched_getcpu(), val);          /* Affichage du */
                                            /* l'index      */
        /*=>Marquer la fin de la multiplication en cours... */

        prod.pendingMult[index] = 0;
        pthread_mutex_lock(&prod.mutex);
        //Si le processus est le processus avec l'index le plus elevé, sa variable pDebut pointe alors vers les valeurs de la deuxième itération
        if(index == prod.nbMult-1)
        {
            printf("PROCESSUS %ld est le processus le plus haut, il donne ses valeurs pdebut et pfin\n", index);
            prod.pDebut = pDebut;
            prod.pFin = pFin;
        }
        if(nbPendingMult(&prod)==0)
        {

            /*=>Autoriser le demarrage de l'addition... */  
            prod.state = STATE_WRITE; 
            pthread_cond_broadcast(&prod.cond);
        }
        pthread_mutex_unlock(&prod.mutex);
    } 

    fprintf(stderr,"Quit mult(%ld) on CPU %d\n",index, sched_getcpu());
    return(data);
}


/*****************************************************************************/
int main(int argc,char ** argv)
{
    if(argc!=3)
    {
        printf("dataFile resultFile\n"); exit(0);
    } 

    size_t i, iter;
    pthread_t *multTh;
    size_t    *multData;
    pthread_t  addTh;
    void      *threadReturnValue;
    int fd;
    struct stat buff;
    long int val; 
    char *pDebut, *pFin;

    /* A cause de warnings lorsque le code n'est pas encore la...*/
    (void)addTh; (void)threadReturnValue;

    /* Lire le nombre d'iterations et la taille des vecteurs */
    fd=open(argv[1], O_RDONLY);

    fstat(fd, &buff);
    pDebut = mmap(NULL, buff.st_size , PROT_READ, MAP_PRIVATE, fd, 0);

    //prod.nbIterations = getVal(pDebut, pFin);
    prod.nbIterations = strtol(pDebut, &pFin, BASE);
    pDebut = pFin;
    printf("nbIteration: %d\n", (int)prod.nbIterations);

    /* Initialisations (Product, tableaux, generateur aleatoire,etc) */

    prod.state=STATE_WAIT;

    /*=>initialiser prod.mutex ... */
    if(pthread_mutex_init(&prod.mutex, NULL) == -1)
    {
        printf("Erreur lors de la création du mutex\n");
        return (EXIT_FAILURE);
    }

    /*=>initialiser prod.cond ...  */
    if(pthread_cond_init(&prod.cond, NULL)==-1)
    {
        printf("Erreur lors de la création de la condition\n");
        return(EXIT_FAILURE);
    }

    /* Allocation dynamique des 3 matrices mat1, mat2, mat3 */

    prod.mat1=(matrice_t *)malloc(sizeof(matrice_t));
    prod.mat2=(matrice_t *)malloc(sizeof(matrice_t));
    prod.mat3=(matrice_t *)malloc(sizeof(matrice_t));


    matrice_t* mat1 = prod.mat1;
    matrice_t* mat2 = prod.mat2;
    matrice_t* mat3 = prod.mat3;


    //POUR CHAQUE CALCUL MATRICIEL À EFFECTUER
    for(int iter = 0; iter<prod.nbIterations; iter++)
    {
        //Taille des colonnes de la matrice 1(nombre de lignes)
        mat1->tailleColonne = strtol(pDebut, &pFin, BASE);
        printf("TailleCOlonne  : %d\n", mat1->tailleColonne);
        pDebut = pFin;
        
        //Taille des lignes de la matrice 1(nombre de colonnes)
        mat1->tailleLigne = strtol(pDebut, &pFin, BASE);
        printf("tailleLigne  : %d\n", mat1->tailleLigne);
        pDebut = pFin;

        //Taille des colonnes de la matrice 2(nombre de lignes)
        mat2->tailleColonne = strtol(pDebut, &pFin, BASE);
        printf("TailleCOlonne2  : %d\n", mat2->tailleColonne);

        pDebut = pFin;

        //Taille des lignes de la matrice 2(nombre de colonnes)
        mat2->tailleLigne = strtol(pDebut, &pFin, BASE);
        printf("tailleLigne  : %d\n", mat2->tailleLigne);

        pDebut = pFin;

        printf("mat1 tailleLignes/tailleColonne : (%d/%d)\n", mat1->tailleLigne, mat1->tailleColonne);
        printf("mat2 tailleLignes/tailleColonne : (%d/%d)\n", mat2->tailleLigne, mat2->tailleColonne);

        //Taille de la troisième matrice : taille des ligne de la seconde matrice / Taille des colonnes de la première
        mat3->tailleLigne = mat2->tailleLigne;
        mat3->tailleColonne = mat1->tailleColonne;

        printf("mat3 tailleLignes/tailleColonne : (%d/%d)\n", mat3->tailleLigne, mat3->tailleColonne);

        //prod.nbMult correspond au nombre de valeurs dans la troisième matrice, soit la nombre de calculs à effectuer
        prod.nbMult = mat3->tailleColonne * mat3->tailleLigne;

        printf("NB Mult = %ld\n", prod.nbMult);

        //Allocation et initialisation de pendingMult 
        prod.pendingMult=(int *)malloc(prod.nbMult *sizeof(int));
        initPendingMult(&prod);

        //Allocation des 3 matrice 
        mat1->valeurs = (int *)malloc(mat1->tailleLigne* mat1->tailleColonne *sizeof(int*));
        mat2->valeurs = (int *)malloc(mat2->tailleLigne* mat2->tailleColonne *sizeof(int*));
        mat3->valeurs = (int *)malloc(prod.nbMult* sizeof(int*));

        //Sauvegarde de pDebut et pFIn dans la structure prod afin que les processus puissent récupérer les valeurs dans le fichier
        prod.pDebut = pDebut; 
        prod.pFin = pFin;

        /* Allocation dynamique du tableau pour les threads multiplieurs */
        multTh=(pthread_t *)malloc(prod.nbMult*sizeof(pthread_t));

        /* Allocation dynamique du tableau des MulData */

        multData=(size_t *)malloc(prod.nbMult*sizeof(size_t));

        /* Initialisation du tableau des MulData */

        for(int i = 0; i<prod.nbMult; i++)
        {
            multData[i] = i;
        }

        setnbCPU();

        /*=>Creer les threads de multiplication... */
        for(i = 0; i<prod.nbMult; i++)
        { 
            if(pthread_create(&multTh[i], NULL, mult, (void*)&multData[i])==-1)
            {
                printf("Erreur lors de la création du thread %ld\n", i);
                return(EXIT_FAILURE);
            }
        }

        /*=>Autoriser le demarrage des multiplications pour une nouvelle iteration..*/
        pthread_mutex_lock(&prod.mutex);
        prod.state = STATE_MULT; 
        pthread_cond_broadcast(&prod.cond);
        pthread_mutex_unlock(&prod.mutex);

        /*=>Attendre l'autorisation d'écriture...*/
        pthread_mutex_lock(&prod.mutex);
        while(prod.state != STATE_WRITE)
        {
            pthread_cond_wait(&prod.cond, &prod.mutex);
        }
        //Les processus attendent de nouveau
        prod.state != STATE_WAIT;
        pthread_mutex_unlock(&prod.mutex);

        //Mettre le résultat du calcul matriciel dans le fichier de sortie    
        printf(" ECRITURE DU RESULTAT DANS LE FICHIER DE RESULTATS\n");

        //Ouverture du fichier de résultat
        int f = open(argv[2], O_RDWR | O_APPEND | O_CREAT);
        char str[6];

        //Écriture de chaque valeur contenue dans le tableau de valeurs de la 3eme matrice
        for(int i=0; i<prod.nbMult; i++)
        {
            //sortie standards pour les tests
            printf("valeur : %d\n", prod.mat3->valeurs[i]);
            //Récupération de la valeur dans une string
            sprintf(str, "%d", prod.mat3->valeurs[i]); 

            //Si on est arrivé au bout de la ligne de la matrice
            if((i+1)%prod.mat3->tailleLigne ==0)
            {
                //On passe une ligne 
                strcat(str, "\n");
            }
            else
            {
                //On laisse un espace
                strcat(str, " ");
            }  
            //Insertion de la valeur en char dans le fichier
            write(f, str, strlen(str));
        }
        printf("Fin de la multiplication matricielle de l'itération %dn\n", iter+1);

        //ON récupère la valeur de pDebut modifiée par le processus le processus de plus haut index utilisé
        //pDébut pointe alors directement sur la première valeur recherchée pour la seconde itération 
        pDebut = prod.pDebut;
        pFin = prod.pDebut;
         free(prod.pendingMult);
       free(prod.mat1->valeurs);
    free(prod.mat2->valeurs);
    free(prod.mat3->valeurs);
    free(multTh);
    free(multData);
    }

    /*=> detruire prod.cond ... */
    pthread_cond_destroy(&prod.cond);
    /*=> detruire prod.mutex ... */
    pthread_mutex_destroy(&prod.mutex);

    /* Detruire avec free ce qui a ete initialise avec malloc */
    free(prod.mat1);
    free(prod.mat2);
    free(prod.mat3);
    return(EXIT_SUCCESS);
}
