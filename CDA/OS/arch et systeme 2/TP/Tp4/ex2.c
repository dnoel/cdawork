#define _GNU_SOURCE
#define PROT PROT_READ
#define RIGHTS MAP_PRIVATE
#include <pthread.h>           /* produit.c */
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>


int main(int argc, char * argv[])
{
    char ligne1[3], ligne2[3], colonne1[3], colonne2[3], buff[3], end[6];

    int f = open(argv[1], O_RDWR | O_APPEND | O_CREAT);
    printf("nb MUlt : ");
    scanf("%s", buff);
    printf("\n");
    strcat(buff, "\n");
    write(f, buff, strlen(buff));
    int nbMult = atoi(buff);

    
    for(int iter = 0; iter<nbMult; iter++)
    {
        printf("Paramètrage des matrices pour la %d multiplication\n", iter+1);
        printf("    nombre de lignes de la matrice 1 : "); 
        scanf("%s", ligne1);
        strcat(ligne1, " ");
        write(f, ligne1, strlen(ligne1));

        printf("    nombre de colonnes de la matrice 1 : ");
        scanf("%s", colonne1);
        strcat(colonne1, "\n");
        write(f, colonne1, strlen(colonne1));

        printf("    nombre de lignes de la matrice 2 : ");
        scanf("%s", ligne2);  
        strcat(ligne2, " ");
        write(f, ligne2, strlen(ligne2));

        printf("    nombre de colonnes de la matrice 2 : ");
        scanf("%s", colonne2);
        strcat(colonne2, "\n");
        write(f, colonne2, strlen(colonne2));
    
        printf("Valeurs de la matrice 1\n");
        for(int i = 0; i< atoi(ligne1); i++)
        {        
            int cl1 = atoi(colonne1);
            for(int j = 0; j<cl1; j++)
            {
                printf("    valeur %d de la ligne %d: ", j+1, i+1);
                scanf("%s", end);
                if(j==cl1-1)
                {
                    strcat(end, "\n");
                }
                else
                {
                    strcat(end, " ");
                }  
                write(f, end, strlen(end));
            }        
        }
        
        printf("Valeurs de la matrice 2\n");
        for(int i = 0; i< atoi(ligne2); i++)
        {
            int cl2 = atoi(colonne2);
            for(int j = 0; j<cl2; j++)
            {
                printf("    valeur %d de la colonne %d: ", j+1, i+1);
                scanf("%s", end);
                if(j==cl2-1)
                {
                    strcat(end, "\n");
                }
                else
                {
                    strcat(end, " ");
                }  
                write(f, end, strlen(end));
            }        
        }
    }       
}