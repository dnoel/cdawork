#define _GNU_SOURCE 

#include <stdio.h>             /* nbOctets.c */
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "partage.h"


/* Decrire le handler de signal pour SIGUSR1 */
/* ========================================= */

//CREATION DU FILS
/* Creation du processus qui fera le exec ...   */
/* ============================================ */

/* pidWC=... */

   /* Dans le processus cree :
      - Rediriger la sortie standard vers le tube
      - Fermer le(s) descripteur(s) inutile(s) a cet enfant
      - Recouvrir par la commande ``wc'' 
   */
void fils1(int tube[2], char *fileName)
{
  printf("Fils 1 commence\n"); 
  dup2(tube[1], STDOUT_FILENO);

  close(tube[1]);
  close(tube[0]);
  execlp("wc","wc","-c",fileName, NULL);
  perror("execlp");
}



/* Creation du processus qui fera la lecture ...*/
/* ============================================ */

/* pidREAD=... */

   /* Dans le processus cree :
      - Fermer le(s) descripteur(s) inutile(s) a cet enfant
      - Ouvrir un flux fIn sur la sortie du tube: fIn=fdopen(tube[0],"r");
      - Lire le resultat via le flux fIn et le mettre dans la memoire partagee
      - Fermer le flux fIn et le(s) descripteur(s) encore ouvert(s)
      - Attendre un peu pour que le pere puisse faire pause avant
      - Envoyer le signal SIGUSR1 au pere
   */
void fils2(int tube[2], Zone *z, FILE *f, int *ptDeb)
{
  printf("Fils 2 se lance\n");
  close(tube[1]); 
    if((f=fdopen(tube[0], "r")) == NULL)
  {
    printf("Il n'y a pas de valeur\n");
    exit(0);
                    /* Le fils n'ecrit pas dans le tube              */
   /* FONDAMENTAL pour atteindre la "fin du tube" ! */
  }
  

  if(fscanf(f, "%d", ptDeb)==-1)
  {
    printf("Erreur lors de la récupération du int par le fils 2\n");
    exit(5);
  };
  fclose(f);
  printf("Le fils a récuprée la valeur %d\n", *ptDeb);

  close(tube[0]);                     /* Pour faire propre */
  kill(getppid(),SIGUSR1);
  printf("Fils 2 à effectué son kill et va se terminer\n");
  exit(0);
}

void handler(int signum)
{
  printf("Je suis revenu du signal : %d\n", signum);
}

/* Le main */
/* ======= */

int main(int argc,char **argv)
{
 pid_t  pidWC;
 pid_t  pidREAD;
 int    status;   /* Pour les waitpid                        */

 int    tube[2];
 FILE  *fIn = fdopen(tube[0], "r");      /* Pour faire un fdopen : int -> FILE *    */


 Zone z;
 int *ptDeb;      /* Un pointeur (int*) sur la zone debut    */

 char  *fileName = NULL;

 if (argc!=2) { fprintf(stderr,"Usage: %s fileName\n",argv[0]); return 1; }

 fileName=argv[1];
 printf("           Filename : %s\n", fileName);

/* A cause de warnings lorsque le code n'est pas encore la ...*/

 /*(void)action;*/ (void)fIn; (void)tube; (void)status; (void)pidREAD; (void)pidWC;

/* Gestion des signaux */

  /* Preparation de la structure action pour recevoir le signal SIGUSR1 */
   struct sigaction action;
   
  /* action.sa_handler = ... */
  action.sa_handler = handler;

  /* Appel a l'appel systeme sigaction */
  if(sigaction(SIGUSR1  , &action, NULL) ==-1)
  {
    printf("Erreur sigaction \n");
    exit(-1);
  };

/* Creation de la zone de memoire partagee */
/* ======================================= */

  if(creerZonePartagee(sizeof(int), &z) ==-1) 
  {
    printf("Erreur lors de la création de la zone mémoire partagée\n");
    exit(0);
  }
    
/* ... */

 ptDeb=(int*)z.debut;    /* *ptDeb <=> *((int*)z.debut) */

/* Creation du tube */
/* ================ */
pipe(tube);
/* ... */
printf("fork du premier fils\n");
pidWC = fork();


switch(pidWC)
{
  case -1: printf("Erreur retournée\n"); break;
  case  0: fils1(tube, fileName); break;
  default : break;
}

printf("fork du deuxième fils\n");
pidREAD = fork();
switch(pidREAD)
{
  case -1: printf("Erreur retournée\n"); break;
  case  0: fils2(tube, &z, fIn, ptDeb); break;
  default: break;
}




/* La suite du pere */
/* ================ */

  /* Fermer les descripteurs de tube inutiles au pere */

printf("On est à la suite du père\n");

  /* Attente d'un signal */
  close(tube[0]);
  close(tube[1]);

  printf("Le père entâme sa pause\n");
  pause();
  /* Recuperer le resultat dans la memoire partagee */
    printf("Le père à terminé sa pause\n");

  

  printf("La valeur en octet récupérée dans la mémoire partagée est %d\n", *ptDeb);


  /* Attendre le 1er enfant  */
  waitpid(pidWC, &status, 0);
  printf("Le père à attendu le premier fils\n");


  /* Attendre le 2eme enfant */
  waitpid(pidREAD , &status, 0);
  printf("Le père à attendu le deux fils\n");

  /* Supprimer la memoire partagee */
    supprimerZonePartagee(&z);

}
