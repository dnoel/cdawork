#include "sem.c"


int main (void)
{ 
    key_t k =  ftok("./sema",0);
    int semList= semget(k,0,0);

    printf("Reader veux l'accès à la ressource\n");
     
    readStart(semList);
    sleep(5);
    printf("Reader ne veux plus l'accès à la ressource\n");
    
    readEnd(semList); 
    return 0;    
}