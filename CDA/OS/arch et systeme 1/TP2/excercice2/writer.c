#include "sem.c"


int main (void)
{ 
    key_t k =  ftok("./sema",0);
    int semList= semget(k,0,0);

    printf("writer veux l'accès à la ressource\n");
     
    writeStart(semList);  
    sleep(5);
    printf("writer ne veux plus l'accès à la ressource\n");
    
    writeEnd(semList); 
    return 0;    
}