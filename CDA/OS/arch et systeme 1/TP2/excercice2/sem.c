#include <sys/types.h>     /* key_t  */
#include <sys/ipc.h>       /* ftok   */
#include <sys/sem.h>       /* semget, semctl, semop */
#include <sys/stat.h>      /* stat, struct stat  */
#include <stdlib.h>        /* malloc */
#include <stdio.h>         /* perror, printf */
#include <errno.h>         /* errno */
#include <unistd.h> 

/* retourne -1 en cas d'erreur           */
int P(int semList, int semId)
{

    struct sembuf op = {semId,-1,0};
    if( semop(semList,&op,1)==-1 )return-1;
    return 0;  
}

/* retourne -1 en cas d'erreur           */
int V(int semList, int semId)
{
    struct sembuf op = {semId,1,0};
    if( semop(semList,&op,1)==-1)return-10;

    return 0;
}

int getReaderCounter()
{
    FILE *file;
    int value;

    file = fopen("readerCounter.txt", "r");
    fscanf(file, "%d", &value);
    printf("value : %d\n", value);
    return value;
}

void changeReaderCounter( int i)
{
    FILE *file;

    file = fopen("readerCounter.txt", "w");
    fprintf(file, "%d", i);
}

void readStart(int semList)
{
    int readerCounter = getReaderCounter();
    printf("readerCounter : %d\n", readerCounter);

    //On attend qu'un éventuel processus finisse de débuter ou d'arrêter sa lecture
    P(semList, 1);
    printf("Il accède à la ressource\n");
    //On indique qu'un lecteur se rajoute à la file des lecteur de la ressource
    readerCounter++;
    printf("Nombre de lecteur sur la ressource : %d\n", readerCounter);
    //Si on est le premier lecteur de la ressource, on bloque l'accès en écriture à la ressource
    if(readerCounter==1)
    {
        printf("Il bloque l'accès en écriture à la ressource car il est le premier lecteur\n");
        P(semList, 3);
    }
    //On termine notre initialisation de lecture
    changeReaderCounter(readerCounter);
    V(semList, 1);
}

void readEnd(int semList)
{
    int readerCounter = getReaderCounter();

    //On attend qu'un éventuel processus finisse de débuter ou d'arrêter sa lecture
    P(semList, 1);
    printf("Il l'obtient le droit\n");

    //On indique qu'un lecteur s'enleve de la file des lecteur de la ressource
    readerCounter--;
    printf("Nombre de lecteur sur la ressource : %d\n", readerCounter);

    //Si on était le dernier lecteur de la ressource, on débloque l'accès en écriture à la ressource
    if(readerCounter==0)
    {
        printf("Le processus était le dernier lecteur de la ressource\n");
        printf("Ainsi il débloque l'accès en écriture à la ressource\n");
        V(semList, 3);
    }
    

    //On termine notre fin de lecture
    printf("Il finit d'arrêter son accès à la ressource\n");
    changeReaderCounter(readerCounter);
    V(semList, 1);
}

void writeStart(int semList)
{
    //On attend que l'accès en écriture à la ressource soit possible
    P(semList,2);
    //On bloque l'accès en lecture à la ressource
    P(semList, 3);
}

void writeEnd(int semList)
{
    //On libère l'accès à la ressource en écriture
    V(semList,2);
    //On libère l'accès à la ressource en lecture
    V(semList, 3);
}

// 1er semaphore : AccesLecture, initialisé à 1 qui permet de faire attendre à un lecteur que le lecteur devant lui est fini d'ouvrir/fermer son accès à la ressource
// 2eme semaphore : RedactionPossible, initialisé à 1 qui permet de bloquer les tâches de rédaction (priorité aux lecteurs)
// 3eme semaphore :  LecturePossible, initialisé à 1 qui permet de bloquer les tâches de lecture si une rédaction est en cours.