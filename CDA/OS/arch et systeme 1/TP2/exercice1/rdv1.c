#include <sys/types.h>     /* key_t  */
#include <sys/ipc.h>       /* ftok   */
#include <sys/sem.h>       /* semget, semctl, semop */
#include <sys/stat.h>      /* stat, struct stat  */
#include <stdlib.h>        /* malloc */
#include <stdio.h>         /* perror, printf */
#include <errno.h>         /* errno */
#include <unistd.h> 
/* retourne -1 en cas d'erreur           */
int P(int semList, int semId)
{
    struct sembuf op = {semId,-1,0};
    if( semop(semList,&op,1)==-1 )return-1;
    return 0;  
}

/* retourne -1 en cas d'erreur           */
int V(int semList, int semId)
{
    struct sembuf op = {semId,1,0};
    if( semop(semList,&op,1)==-1)return-1;
    return 0; 
}

int main (void)
{ 
    key_t k =  ftok("./sema",0);
    int semList= semget(k,0,0);
    V(semList,1);
    P(semList,2);
    printf("rdv1 s'endort pour 5 secondes\n");
    sleep(5);
    printf("rdv1 se réveille\n");
    printf(" rdv1 retourne : %d \n", semList);
    printf("Rdv1 se termine\n");
    return 0;
}