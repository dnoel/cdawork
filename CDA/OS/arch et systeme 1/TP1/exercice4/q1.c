#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <wait.h>


int main()
{
   int fileRead = open("toto.txt", O_RDWR);
    int fileWrite = open("toto.txt", O_RDWR);

   
    char buffer[10];
    int code_retour;
    printf("Test du fork\n");
    code_retour = fork();
    switch(code_retour)
    {
        case -1:
            printf("IL y a une erreur\n");
            break;

        case 0:
        printf("Début du fils: \n");
        printf("Le fils écrit 'Fils'\n");
        write(fileWrite, "Fils", 4);
        printf("le fils s'endort pour 2 secondes\n");
        sleep(2);
        printf("Le fils se réveille \n");

        printf("Le fils lit 4 caractères\n");
        read(fileRead, buffer,4);

        printf("Le fils affiche les 4 caractères qu'il a lu : ");
        for(int i=0; i<4;i++)
        {
            printf("%c", buffer[i]);
        }
        printf("\n");
        break;

        default:
        printf("Début du père \n");
        printf("le père s'endort pour 1 seconde\n");
        sleep(1);
        printf("Le père se réveille\n");
        printf("Le père lit 4 caractères \n");
        read(fileRead,buffer, 4);

        printf("Le père affiche les 4 caractères qu'il a lu: ");
        for(int i=0; i<4;i++)
        {
            printf("%c", buffer[i]);
        }
        printf("\n");
        
        printf("Le père écrit 'Pere'\n");
        write(fileWrite, "Pere", 4);
        wait(&code_retour);

    }
}