#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>


int main()
{
   int fileRead = open("toto.txt", O_RDWR);
    int fileWrite = open("toto.txt", O_RDWR);

   
    char buffer[10];
    char buf[80];
    int code_retour;
    int statut;
    printf("Test du fork\n");
    code_retour = fork();
    switch(code_retour)
    {
        case -1:
            printf("IL y a une erreur\n");
            break;

        case 0:
            
            sprintf (buf," -p %d",getpid());
            int code_retour  = execl("/bin/ps","ps",buf, NULL);
            printf("Le fils attend la fin de execl");
            wait(&code_retour);
            printf("Le fils a fini d'attendre\n");
            exit(3);

        default:
            printf("Début du père \n");
            printf("Je viens de créer le processus de pid %d\n-", code_retour);
            printf("Le père attend la fin du fils\n");
            //waitpid(code_retour, &statut, 0);
            waitpid(code_retour, &statut, 0);

            printf("Le père a fini d'attendre le fils\n");
            
            if(WIFEXITED(statut)==0)
            {
                printf("Le processus ne s'est pas terminé normalement\n");
                //exit(0);
            } 

            printf("Code retour renvoyé par le fils : %d \n",WEXITSTATUS(statut));
            if(WIFSIGNALED(statut)!=0) 
            {
                printf("Le processus fils s'est terminé à cause d'un signal\n");
                printf("Signal ayant provoqué la terminaison du processus fils : %d\n", WTERMSIG(statut));
            }
            else
            {
                printf("Le processus n'a pas été terminé par un signal\n");
            }

            printf("Le père lit 4 caractères \n");
            read(fileRead,buffer, 4);

            printf("Le père affiche les 4 caractères qu'il a lu: ");
            for(int i=0; i<4;i++)
            {
                printf("%c", buffer[i]);
            }
            printf("\n");
            
            printf("Le père écrit 'Pere'\n");
            write(fileWrite, "Pere", 4);
                    
            
            printf("Fin du père\n");
    }
}