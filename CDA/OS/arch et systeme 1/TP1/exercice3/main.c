#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main()
{
    char str[256];
    int code_retour;
    printf("Test du fork\n");
    code_retour = fork();
    switch(code_retour)
    {
        case -1:
            printf("IL y a une erreur\n");
            break;

        case 0:
        printf("INformations du fils : \n");
        break;

        default:
        sleep(5);
            printf("Informations du père : \n");
    }

    printf("Identifiant du processus : %d\n", getpid());
    printf("Identifiant du père du processus : %d\n", getppid());
    printf("Répertoire de travail du processus : %s\n", getcwd(str, 256));
    printf("Propriétaire réel : %d\n", getuid());
    printf("Propriétaire réel effectif  : %d\n", geteuid());
    printf("Groupe propriétaire réel : %d\n", getgid());
    printf("Groupe propriétaire effectif : %d\n", getgid());
}