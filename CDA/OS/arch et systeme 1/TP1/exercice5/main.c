#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>


int main()
{
   int fileRead = open("toto.txt", O_RDWR);
    int fileWrite = open("toto.txt", O_RDWR);

   
    char buffer[10];
    int code_retour;
    printf("Test du fork\n");
    code_retour = fork();
    switch(code_retour)
    {
        case -1:
            printf("IL y a une erreur\n");
            break;

        case 0:
        printf("Début du fils: \n");
        printf("Le fils écrit 'Fils'\n");
        write(fileWrite, "Fils", 4);
        printf("le fils s'endort pour 2 secondes\n");
        sleep(2);
        printf("Le fils se réveille \n");

        printf("Le fils lit 4 caractères\n");
        read(fileRead, buffer,4);

        printf("Le fils affiche les 4 caractères qu'il a lu : ");
        for(int i=0; i<4;i++)
        {
            printf("%c", buffer[i]);
        }
        printf("\n");
        printf("Le fils meurt\n");
        exit(0);
        break;

        default:
        printf("Début du père \n");
        printf("Le père attend la fin du fils\n");
        wait(&code_retour);    
        
        switch(code_retour)
        {
            case -1:
                printf("Le fils a retourné -1\n");
                break;

            case 0:
                
                printf("Le père a fini d'attendre le fils\n");
                printf("Le fils  a renvoyé le code suivant : %d\n", code_retour);
                printf("Le père lit 4 caractères \n");
                read(fileRead,buffer, 4);

                printf("Le père affiche les 4 caractères qu'il a lu: ");
                for(int i=0; i<4;i++)
                {
                    printf("%c", buffer[i]);
                }
                printf("\n");
                
                printf("Le père écrit 'Pere'\n");
                write(fileWrite, "Pere", 4);
                
        }
        printf("Fin du père\n");
    }
}