#include <stdio.h>
// #include <stdlib>
#include <unistd.h>

    
int main(){


    int code_retour; 
    printf("Début du testfork");
    code_retour = fork();
    switch(code_retour)
    {
        case -1:
            printf("Pbm \n");
            break;
        
        case 0:
            printf("Je suis le fils \n");
            break;
        
        default:
            printf("Je suis le processus père \n");
            printf("Je viens de créer le processus fils dont le pid est %d\n", code_retour);
    }

    printf("code_retour %d\n", code_retour);
    printf("Findu test fork()\n");
}