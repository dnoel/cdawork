#include <stdio.h>
#include <stdlib.h>
#define ESSAI 5


int main()
{
    char mystere = 'a' + (random() % 26);
    int reussite =0;
    for(int i =0; i< ESSAI; i++)
    {
        printf("Veuillez rentrer une lettre  minuscule. (%d essais restants)  \n", ESSAI - i);
        char lettre = getchar();

        if(lettre == mystere)
        {
            reussite = 1;
            printf("Vous avez trouvé la bonne lettre qui est : %c", lettre);
            break;
        }
        else
        {
            printf("Ce n'est pas la bonne lettre ! \n");
        }
    }

    if(reussite == 0)
    {
        printf("VOUS AVEZ PERDU ! \n");
    }
}

