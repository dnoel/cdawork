#include <stdio.h>
long facto(int nb, long value)
{

    if(nb == 0)
    {
        return value;
    }
    else
    {
        facto(nb-1, nb*value);
    }
    
}

int main()
{
    int nb; 

    printf("Veuillez saisir votre nombre : ");
    scanf("%d", &nb);

    long factorielle = facto(nb, 1);

    printf("\n");
    printf("Factorielle = %ld\n ", factorielle);

}