//point.h
#include <stdio.h>
#include <math.h>


struct point
{
    int x;
    int y;
};

typedef struct point point_t;


void affichePoint(point_t p)
{
    printf("point : (%d,%d)\n", p.x, p.y);
};

point_t newPoint(int x, int y)
{
    point_t p = {x,y};
    return p;
};

void afficheNorme(point_t* p)
{
    int x = p->x;
    int y = p->y;

    printf("Norme du point : %.2f\n",sqrt(x*x + y*y));

};


