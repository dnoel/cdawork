//rect.h

#include <point.h>

struct rect
{
    int larg;
    int lg;

    point_t hg;

};  

typedef struct rect rect_t;


rect_t newRect(int larg, int lg, int x, int y)
{
    rect_t rect;
    rect.larg = larg;
    rect.lg = lg;
    rect.hg = newPoint(x,y);

    return rect;
};

