#include <stdio.h>

int maxi2(int a, int b)
{
    if (a > b)
    {
        return a;
    }

        return b;
}

int maxAll(int vals[], int length)
{
    int max = vals[0];
    for(int i =1; i<length; i++)
    {
       max =  maxi2(max, vals[i]);
    }
    return max;
   
}
int main()
{
    int vals[8] = {3,4,5,7,19,4,8,17};
    int length = 8;
    int max = maxAll(vals, length);
    printf("%d \n", max);

}