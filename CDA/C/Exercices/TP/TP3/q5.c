#include <stdio.h>

int myatoi( char tab[])
{
    int i =0;
    int number=0;
    while(tab[i] != '\0')
    {
        number = number *10 + (tab[i]-'0');
        i++;
    }
    return number;
}

int main()
{
    char tab[5];
    scanf("%s", tab);
    int result = myatoi(tab);
    printf("Le nombre saisi est : %d\n", result);

}