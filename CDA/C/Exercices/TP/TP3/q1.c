#include <stdio.h>

void afficheTab(int tab[], int len)
{
    printf("[%d", tab[0]);

    for(int i=1; i<len; i++)
    {
        printf(",%d", tab[i]);
    }
    printf("]\n");
}

void lireTab(int tab[], int n)
{
    for(int i = 0; i <5; i++)
    {
        printf("Veuillez entrer votre %d valeur : \n", i+1); 
        scanf("%d", &tab[i]);
    }
}

int main()
{
    int tab[5];

    lireTab(tab, 5);
    afficheTab(tab, 5);
}