#include <stdio.h>

void afficheTab(int tab[], int len)
{
    printf("[%d", tab[0]);

    for(int i=1; i<len; i++)
    {
        printf(",%d", tab[i]);
    }
    printf("]\n");
}


void occurence(int lettre[26])
{
    int i=0;
    char saisie;

    while(saisie = getchar() != EOF)
    {
        if(saisie>= 'a' && saisie <= 'z')
        {
            lettre[(saisie - 'a')]++;
        }
        else if(saisie>= 'A' && saisie <= 'Z')
        {
            lettre[(saisie - 'A')]++;
        }
        
        i++;
    }
}

void lireTab(int tab[], int n)
{
    for(int i = 0; i <5; i++)
    {
        printf("Veuillez entrer votre %d valeur : \n", i+1); 
        scanf("%d", &tab[i]);
    }
}

int main()

{
    int lettre[26];

    occurence(lettre);

    afficheTab(lettre, 26);
    

}
