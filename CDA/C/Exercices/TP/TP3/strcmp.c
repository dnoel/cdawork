#include <stdio.h>

int myStrcmp(char ch1[], char ch2[])
{
    int i=0;
    int etat =0;

    while(ch1[i] != '\0' && etat == 0)
    {

        if(ch1[i] > ch2[i])
        {
            etat = 1;
        }
        else if(ch1[i] < ch2[i])
        {
            etat = -1;
        }
        i++;

      printf("caractere 1 : %c / caractere 2 : %c / etat : %d\n", ch1[i], ch2[i], etat);  

    }

    if(etat == 0 && ch2[i++]) etat = 1;
    return etat;
}

int main()
{
    char str1[11];
    char str2[11];
    printf("Veuillez saisir la première chaîne : ");
    scanf("%s", str1);
    printf("\n");

    printf("Veuillez saisir la deuxième chaîne : ");
    scanf("%s", str2);
    printf("\n");

    int etat = myStrcmp(str1, str2);

    switch(etat)
    {
        case 0: printf("Les deux strings ont la même taille \n"); break;
        case 1: printf("La première string est la plus grande \n"); break;
        case -1: printf("La deuxième string est la plus grande \n"); break;

    }


}