#include <string.h>
#include "ville.h"
#include <stdio.h> 

void createVille(ville_t *ptr, char nom[15], int x, int y)
{
    strcpy(ptr->nom, nom);
    ptr->point = createPoint(x,y);
}

void initialiseMap(map_t *ptr)
{
    ville_t ville;
    strcpy(ville.nom, "");
    for (int i = 0; i<sizeof(ptr->listVille); i++)
    {
        ptr->listVille[i] = ville;
    }
}

void afficheVilleParNom(char nom[], map_t *ptr)
{
    for(int i=0; i<sizeof(ptr->listVille); i++)
    {
        printf("%d ville : %s\n", i, ptr->listVille[i].nom);
        if(strcmp(ptr->listVille[i].nom, nom)==0)
        {
            printf("La ville du nom de %s a pour coordonnées", ptr->listVille[i].nom);
            affichePoint(ptr->listVille[i].point);
            return;
        }
    }

    printf("Nous n'avons pas trouvé la ville de %s\n", nom);
}

void insertVilleDansMap(map_t *map, ville_t *ville)
{
    ville_t insertVille = {ville->nom, {ville->point.x, ville->point.y}};
    for(int i=0; i<sizeof(map->listVille); i++)
    {
        if( map->listVille[i].nom =="")
        {
            map->listVille[i] = insertVille;

            printf("Vient d'être rajouté : nom : %s / x : %d / y : %d\n",  map->listVille[i].nom,
                map->listVille[i].point.x, map->listVille[i].point.y);   
            return;
        }
    }
    
}

void supprimeVille(map_t *ptr, char nom[15])
{
    for(int i=0; i<sizeof(ptr->listVille); i++)
    {
        ville_t ville = ptr->listVille[i];
        printf("Nom de la ville : %s \n", ville.nom);
        printf("resultat de strcmp : %d\n", strcmp(ville.nom, nom));

        
        if(strcmp(ville.nom, nom) == 0)
        {
            ville_t suppr;
            ptr->listVille[i] = suppr;
            return;
        }
    }
}

// void afficheMap(map_t *map)
// {
//     for(int i = 0; i < sizeof(map->listVille); i++)
//     {
//         ville_t ville = map->listVille[i];
//         afficheVille(&ville);
//     }
        
// }