#include <stdio.h>
#include "ville.h"


int main()
{
    map_t map;
    void initialiseMap(map);
    

    int quit =0;
    char choice;
    int counter = 0;
while(!quit)
{
    counter++;   
    printf("La boucle est lancée %d fois \n", counter);
    printf("Que souhaitez-vous faire ? (q)uitter, (a)jouter une ville, (p)rint une villes : \n");
    
    scanf("%c", &choice);

    
        if(choice == 'q')
        {
            quit =1; 
            printf("L'application s'arrête\n");
        } 
        if(choice == 'a') 
        {
            ville_t ville;
            char nom[15];
            int x, y;

            printf("Veuillez saisir le nom de la ville : ");
            scanf("%s", nom); 
            printf("\n");

            printf("Veuillez saisir la coordonnée x de la ville: ");
            scanf("%d", &x); 
            printf("\n");

            printf("Veuillez saisir la coordonnée y de la ville: ");
            scanf("%d", &y); 
            printf("\n");

            createVille(&ville, nom, x, y);
            printf("%s a été ajoutée\n", nom);
            insertVilleDansMap(&map, &ville);
        }

            
        
        if(choice =='p')
        {
        
            char nom[15];
            printf("Veuillez rentrer le nom de la ville :");
            scanf("%s", nom);
            afficheVilleParNom(nom, &map);
        }

            
}
    

   
}
