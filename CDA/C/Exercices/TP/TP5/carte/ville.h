//ville.h

#include "point.h"

struct ville
{
    char nom[15];
    point_t point;
};

typedef struct ville ville_t;

struct map
{
    ville_t listVille[10];
};
typedef struct map map_t;

void createVille(ville_t *ptr, char nom[15], int x, int y);
void afficheVille(ville_t *ptr);
void afficheMap(map_t *map);
void insertVilleDansMap(map_t *map, ville_t *ville);
void afficheVilleParNom(char nom[], map_t *ptr);
void initialiseMap(map_t *ptr);

