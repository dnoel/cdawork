#include "point.h"
#include <stdio.h>

void affichePoint(point_t point)
{
    printf("x : %d / y: %d \n", point.x, point.y);
}

point_t createPoint(int x, int y)
{
    point_t point = {x,y};
    return point;
}