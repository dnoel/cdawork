//point.h

struct point
{
    int x;
    int y;
};

typedef struct point point_t;

void affichePoint(point_t point);

point_t createPoint(int x, int y);



