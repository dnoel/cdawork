#include <stdio.h>
#include "point.h"

int main()
{
    int x, y;
    point_t pt1;

    printf("Veuillez saisir x : ");
    scanf("%d", &x);

    printf("\n");

    printf("Veuillez saisir y : ");
    scanf("%d", &y);
    printf("\n");

    pt1 = createPoint(x,y);

    affichePoint(pt1);
}