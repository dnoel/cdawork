#include <stdio.h>
#include <stdlib.h>


int NBL;
int NBC;

void afficheMatrice(int mat[NBL][NBC])
{
    for(int i=0; i<NBL; i++)
    {
        for(int j=0; j<NBC; j++)
        {
            printf("[%d] ", mat[i][j]);
        }
        printf("\n");
    }
}

void multiplieMatrice(int mat1[NBL][NBC], int mat2[NBL][NBC], int mat3[NBL][NBC])
{
    for(int i=0; i<NBL; i++)
    {
        for(int j=0; j<NBC; j++)
        {
            mat3[i][j] = 0;

            for(int k=0; k<NBC; k++)
            {
                mat3[i][j] = mat3[i][j] + mat1[i][k]*mat2[k][j];
            }
        }
    }
}


void newRandomMatrice(int lig, int col, int mat[NBL][NBC], int maxValue)
{

    for(int i=0; i<lig; i++)
    {
        for(int j=0; j<col; j++)
        {
            mat[i][j] = rand()% maxValue;
        }

    }

}
int main()
{
    int lig, col;
    int maxValue;

    printf("NOmbre de ligne : \n");

    scanf("%d", &lig);

    printf("NOmbre de colonnes : \n");

    scanf("%d", &col);

    printf("Valeur max: \n");

    scanf("%d", &maxValue);

    NBL = lig;

    NBC = col;

    int matrice1[lig][col], matrice2[lig][col], matrice3[lig][col];

    


    newRandomMatrice(lig, col, matrice1, maxValue);
    newRandomMatrice(lig, col, matrice2, maxValue);
    multiplieMatrice(matrice1, matrice2, matrice3);

    printf("Matrice 1: \n");
    afficheMatrice(matrice1);

    printf("Matrice 2: \n");
    afficheMatrice(matrice2);

    printf("Matrice 3: \n");
    afficheMatrice(matrice3);
}