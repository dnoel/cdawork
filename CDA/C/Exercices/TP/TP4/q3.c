#include <stdio.h>
#include <stdlib.h>
// ./a.out 3 4
int main(int argc, char *argv[])
{
    int a,b;

    if(argc !=3)
    {
        printf("usage: %s nb1 nb2\n", argv[0]);
        return 1;
    }  

    a= atoi(argv[1]);
    b= atoi(argv[2]);

    if(a>b) printf("a est supérieur");
    else printf("b est supérieur");
    printf("\n");
    return 0;
}