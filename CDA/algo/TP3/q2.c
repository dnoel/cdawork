#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#define N 8
#define MAX 1
#define N1 N+2
#define random(N) (int)((rand()-1)/(double)RAND_MAX *N)

typedef int matrice_t[N1][N1];


void matInit(matrice_t mat)
{

    for(int j=0; j<N1; j++)
    {
        mat[0][j]  = mat[N1-1][j]= 0;
    }
    for(int i=1; i<N1-1; i++)
    {
        mat[i][0] = 0;
        for(int j=1; j<N1-1;j++)
        {
            mat[i][j] = rand() % (1 + 1 - 0) + 0;
        }
        mat[i][N1-1]=0;
    }
}

void printMat(matrice_t mat)
{
    for(int i=0; i<N1;i++)
    {
        for(int j=0; j<N1; j++)
        {
            printf("[%d]", mat[i][j]);
        }
        printf("\n");
    }
}

int nbVoisinsVivants(matrice_t mat, int i, int j)
{
    return (mat[i-1][j-1] + mat[i-1][j+1] + mat[i][j-1] + mat[i][j+1] + mat[i+1][j-1] + mat[i+1][j] + mat[i+1][j+1]);
}

void matUpdate(matrice_t mat)
{
    for(int i=1; i<N; i++)
    {
        for(int j=1; j<N; j++)
        {
            int nbVoisin = nbVoisinsVivants(mat, i,j);
            if( mat[i][j] == 0 || nbVoisin==3) mat[i][j] = 1;
            if( mat[i][j] == 1 || nbVoisin <2 || nbVoisin>3) mat[i][j]=0;

        }
    }
}
int main()
{
   matrice_t mat;
   matInit(mat);
   while(1)
   {
        matUpdate(mat);
        printMat(mat);
        sleep(1.5);
        printf("******************************************\n"); 
   }
   

}