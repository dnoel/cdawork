#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define DICE 6
#define GAME_SIZE 66
#define THROW_NUMBER 2
#define random(n) (int)((rand()-1)/(double)RAND_MAX *n)

unsigned int g;
int progression=0;
int lastThrow = 0;
int throwNumber =0;

void win()
{
    printf("Vous avez gagné !\n");
    if(1<=throwNumber && throwNumber <=5) printf("Vous êtes un excellent joueur ! \n");
    if(6<=throwNumber && throwNumber <=12) printf("Vous êtes un bon joueur ! \n");
    if(throwNumber >12) printf("Vous êtes un petit joueur débutant ( %d lancers sale merde) \n", throwNumber);
    exit(1);
}

void diceThrow()
{
    int throwResult=0;
    for(int i=0; i<THROW_NUMBER; i++)
    {
        int throw = 1+random(DICE);
        printf("[%d]", throw);
        throwResult+= throw;
    }
    printf("\n");
    
    progression += throwResult;
    lastThrow = throwResult;
    throwNumber++;
}

void eventHandler()
{
    printf("ON est arrivé sur la case %d\n", progression);
    if(progression==GAME_SIZE) win();
    if(progression>GAME_SIZE)
    {
        printf("On a dépassé la dernière case du jeu \n"); 
        progression = GAME_SIZE - (progression-GAME_SIZE);
        printf("On repart sur la case %d\n", progression);

    } 
    if(progression==58) 
    {
        printf("ON TOMBE SUR LA TETE DE MORT!\n");
        progression ==0; 
        printf("On repart sur la case %d\n", progression);

        return;
    }
    while(progression%9==0 && progression!=63)
    {
        progression+=lastThrow;
        printf("ON REBONDIT en %d!\n",progression);
    } 

    // printf("ON a fini les comparaisons \n");
}

void play()
{
    printf("On lance les dés : ");
    diceThrow();
    eventHandler();
}

int main()
{
    printf("Valeur du germe ? : ");
    scanf("%u", &g);
    srand(g);

    while(1)
    {
        play();
    }

}