#include <stdio.h>
#define N 15


int elemA[N];
int elemB[N];
int elemC[N];
int CPT[N];

void init()
{
    for(int i=0; i<N; i++)
    {
        elemA[i] = elemB[i] = elemC[i] = i;
        CPT[i] = 0;
    }

}

void rechercheA(int recherche)
{
    for(int i=0; i<N; i++)
    {
        if(elemA[i] == recherche)
        {
            if(i >0)
            {
                elemA[i] = elemA[i-1];
                elemA[i-1] = recherche;
            }

            printf("élement trouvé\n\n"); 
            return;
        }
    }
    printf("élement inconnu\n\n");
}

void rechercheB(int recherche)
{
    int i=0;

    while(i<N && elemB[i] !=recherche)
    {
        i++;
    }

    if(i!=N)
    {
        if(i>0)
        {
            for(i; i>0; i--)
            {
                elemB[i] = elemB[i-1];
            }

            elemB[0] = recherche;
        }
        printf("élement trouvé\n\n");
        return;
    }
        
    printf("élement inconnu\n\n");
}

void rechercheC(int recherche)
{
    int i=0;

    while(i<N && elemC[i] !=recherche)
    {
        i++;
    }

    if(i!=N)
    {
        CPT[i]++;

        //Si l'élément n'est pas déjà en première position de elemC
        if(i>0)
        {
            //Tant que le nombre d'occurence de recherche est supérieur aux nombre d'occurence des autres avleurs précédant le tableau
            int stock = CPT[i];
            while( i>0 && CPT[i]>CPT[i-1])
            {   
                CPT[i] = CPT[i-1];
                elemC[i] = elemC[i-1];
                i--;
            }

            CPT[i] = stock;
            elemC[i] = recherche;
        }
        printf("élement trouvé\n\n");
        return;
    }
        
    printf("élement inconnu\n\n");
}


void afficheTab(int tab[N])
{
    if(tab==CPT) printf("\nCPT : ");
    if(tab == elemA) printf("elemA : ");
    if(tab == elemB) printf("elemB : ");
    if(tab == elemC)
    {
        afficheTab(CPT);
        printf("elemc : ");    
    }

    printf("[%d", tab[0]);

    for(int i=1; i<N; i++)
    {
        printf(",%d", tab[i]);
    }
    printf("]\n");

}


int main()
{
    init();
    
    afficheTab(elemA);
    afficheTab(elemB);
    afficheTab(elemC);


    int recherche;
    while(1)
    {
        printf("Valeur recherchée ('-1' to quit): "); 

        scanf("%d", &recherche);

        if(recherche == -1)
        {
            break;
        }

        rechercheA(recherche);
        rechercheB(recherche);
        rechercheC(recherche);

        
        afficheTab(elemA);
        afficheTab(elemB);
        afficheTab(elemC);

    }

    printf("fin du programme\n");
}