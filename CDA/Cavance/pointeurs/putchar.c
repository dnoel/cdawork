#include <stdio.h>

int main()
{
    const char *str = "Hello world";
    
    while(*str !='\0')
    {
        putchar(*str);
        str++;
    }
    putchar('\n');
}
