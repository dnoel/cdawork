#include <stdio.h>



int main()
{
    int ln =1, word =0, letter = 0;
    char c;
    int wordStart = 0;
    while((c = getchar()) != EOF)
    {
        

        if('a' < c < 'z' || 'A' < c < 'Z')
        {
            letter++;
            wordStart = 1;
        }
        if(c == '\n')
        {
            if(wordStart) wordStart++;
            ln++;
        }

        if(c == ' ' && wordStart == 1)
        {
            word++;
            wordStart = 0;
        }
        
    }

    printf("line : %d / word : %d / letter : %d",ln, word, letter);
    
}