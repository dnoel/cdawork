#include <stdio.h>

int alphaEntier(char *ptr)
{
    int nb = 0;
    for(int i=0; *(ptr+i) !='\0'; i++)
    {
        if(*(ptr+i) !=' ')
        {
            nb = nb*10 + (*(ptr+i) - '0');
        }
 
    }
    return nb;
}

int main()
{
    char nbChar[10];

    printf("Veuillez rentrer votre nombre en chaine de caractère : ");
    scanf("%s", nbChar);
    int nbInt = alphaEntier(nbChar);
    printf("%d\n",nbInt);
    
}