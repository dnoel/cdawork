#include <stdio.h>

int aModifier(char *phrase, char *modif, char *subst)
{
    int i=0;
    while(*(modif+i)!='\0')
    {
        printf("nb boucle : %d\n", i);
        
        char *modifChar = modif+i;
        char *substChar = subst+i;

        printf("substChar: %c\n", *substChar);


        for(int j =0; *(phrase+j) !='\0'; j++)
        {
            if(*(phrase+j) == *modifChar)
            {
                *(phrase+j) = *substChar;
            }
        }

        i++;
    }
}

void afficheString(char *str)
{
    for(int i=0; *(str+i) !='\0'; i++)
    {
        printf("%c", *(str+i));
    }
    printf("\n");
}

int main()
{
    char phrase[]= "la cabane au Canada";
    char modif[4];
    char subst[4] = {'*', '%', '!'};

    printf("Phrase: ");
    afficheString(phrase);

    printf("Veuillez saisir les 3 caractères à modifier : ");
    scanf("%c%c%c", modif,modif+1, modif+2);

    printf("\n");


    
    printf("Vous remplacez  %c %c %c par %c %c %c\n", *modif, *(modif+1), *(modif+2), *subst, *(subst+1), *(subst+2));
    aModifier(phrase, modif, subst);
    printf("Phrase après modification : ");

    afficheString(phrase);
}