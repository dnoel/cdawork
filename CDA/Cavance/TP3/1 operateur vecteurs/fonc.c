#include <stdio.h>
#include <math.h>
#include "fonc.h"

void afficheVec(vecteur_t vec)
{
    printf("x : %lf / y : %lf\n", vec.x, vec.y);
}
double normeVec(vecteur_t *vec)
{
    double a = (vec->x * vec->x) + (vec->y * vec->y);
    return sqrt(a);
}

vecteur_t produit(double lambda, vecteur_t vec)
{
    vec.x = vec.x * lambda;
    vec.y = vec.y *lambda;
    return vec;
}

vecteur_t projectionX(vecteur_t vec)
{
    vec.y = 0;
    return vec;
}

vecteur_t addition(vecteur_t vec1, vecteur_t vec2)
{
    vecteur_t vec3 = {vec1.x + vec2.x, vec1.y + vec2.y};
    return vec3;

}

double produitScalaire(struct vecteur vec1, struct vecteur vec2)
{
    return vec1.x * vec2.x + vec1.y * vec2.y;
}
