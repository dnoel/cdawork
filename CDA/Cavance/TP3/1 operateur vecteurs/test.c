#include <stdio.h>
#include "fonc.h"

int main()
{
    vecteur_t vec1 = {4,5};

    vecteur_t vec2 = {7,8};

    vecteur_t vec3; 


    double pScal;
    afficheVec(vec1);

    printf("Norme = %lf\n", normeVec(&vec1));

    vec1 = produit(5, vec1);

    afficheVec(vec1);

    vec1 = projectionX(vec1);

    afficheVec(vec1);

    vec3 = addition(vec1, vec2);

    afficheVec(vec3);

    vec1.x = 1;
    vec1.y = 2;

    vec2.x = 3;
    vec2.y = 4;
    pScal = produitScalaire(vec1, vec2);

    printf("Produit scalaire : %lf\n", pScal);

    


}


