#ifndef FONC_H
#define  FONC_H

struct vecteur
{
    double x;
    double y;
};

typedef struct vecteur vecteur_t;

void afficheVec(vecteur_t vec);
double normeVec(vecteur_t *vec);
vecteur_t produit(double lambda, vecteur_t vec);
vecteur_t projectionX(vecteur_t vec);
vecteur_t addition(vecteur_t vec1, vecteur_t vec2);
double produitScalaire(struct vecteur vec1, struct vecteur vec2);

#endif