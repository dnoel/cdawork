#include <stdio.h>
#include "fonc.h"

int main()
{
    vecteur_t vec1 = {{1,2,3,4,5,6,7,8,9}, 9};
    vecteur_t vec2;

    printf("Valeurs de vec1 : ");
    afficher_vecteur(&vec1);

    affecter_vecteur(&vec2, &vec1);

    printf("Valeurs de vec : ");
    afficher_vecteur(&vec2);

}