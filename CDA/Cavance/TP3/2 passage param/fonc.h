#ifndef FONC_H
#define FONC_H

#define N 10

struct vecteur
{
    double *compo;
    int nbCompo;
};

typedef struct vecteur vecteur_t;

void afficher_vecteur(vecteur_t* vec);

void affecter_vecteur(vecteur_t * vec1, const vecteur_t * vec2);

double normeVec(vecteur_t *vec);
// vecteur_t produit(double lambda, vecteur_t vec);
// vecteur_t projectionX(vecteur_t vec);
// vecteur_t addition(vecteur_t vec1, vecteur_t vec2);
// double produitScalaire(struct vecteur vec1, struct vecteur vec2);


#endif