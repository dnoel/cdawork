#include <stdio.h>

int longueur(char str[80])
{
    int i;

    while(str[i] != '\0')
    {
        i++;
    }

    return i;

}

int main()
{
  
  char str[80] = "Test";
  printf("La longueur de la ligne est : %d\n", longueur(str));
}