#include <stdio.h>

void supLettre(char *ptrLettre, char chaine[50])
{
    int l =0;
    int e =0;

    while(chaine[l] != '\0')
    {
        if(chaine[l] != *ptrLettre)
        {
            chaine[e] = chaine[l];
            e++;
        }

        l++;
    }
    chaine[e] = '\0';

}
int main()
{
    char chaine[50] = "Boanajaoauara";

    printf("Chaine avant la fonction : %s \n", chaine);
    char lettre = 'a';

    printf("lettre: %c \n", lettre);

    supLettre(&lettre, chaine);

    printf("chaine après la fonction : %s\n", chaine);
    
}