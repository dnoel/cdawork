#include <stdio.h>
#include <stdlib.h>


int strcmpLike(char str1[50], char str2[50])
{
    int i =0;
    while(str2[i] != '\0' && str1[i] !='\0' && str1[i] == str2[i])
    {
        i++;
    }

    if(str1[i] == '\0' && str2[i] == '\0')
    {
        return 0;
    }

    if(str1[i] < str2[i])
    {
        return -1;
    }
    else
    {
        return 1;
    }
    
}

int main()
{
    char str1[50] = "Coucou comment";
    char str2[50] = "Coucou comment";

    int state = strcmpLike(str1, str2);
    printf("Valeur de state : %d\n", state);
}