#include <stdio.h>
#include <stdarg.h>

int add(int nb, ... )
{
    int res = 0;

    va_list arg;
    va_start(arg, 0);

    int a = va_arg(arg, int);
    while( a!=0)
    {
        res+=a;
        a = va_arg(arg, int);

    }
    va_end(arg);
    return res;

}

int main()
{

    int a,b,c,d,e,f;
    a=1;
    b=2;
    c=3;
    d=4;
    
    printf("%d\n", add(a,b,c,d,0));
 
}