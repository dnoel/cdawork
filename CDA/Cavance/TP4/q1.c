#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


unsigned int factIterative(unsigned int n )
{
    int result = n;
    for(int i=n-1; i >= 2; i--)
    {
        if(i < UINT_MAX/result)
        {
            result = result * i;
        }
        else
        {
            printf("%s", "Taille du tampon dépassée \n");
            return 0;
        }
        
        
    }

    return result;
}

unsigned int factRecursive(unsigned int n, int result )
{
    if(n <=1)
    {
        return result;
    }
    

    if(n-1 < UINT_MAX/result)
    {
        factRecursive(n-1, result*n);
    }
    else
    {
        printf("%s", "Taille du tampon dépassée \n");
        return 0;
    }

}
int main()
{

    int n;
    scanf("%d", &n);
    printf(" Fonction iterative : %d\n", factIterative(n));
    printf(" FOnction recursive : %d\n", factRecursive(n, 1));

}