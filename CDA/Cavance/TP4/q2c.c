#include <stdio.h>
#include <string.h>


int main(int argc, char *argv[])
{

    //index de la chaine fournie en argument
    int i=1;

    //Variable contenant les mots recupérés dans la string
    int nullIndex = 0;

    //Lecture de la string
   
   for(int i = 0; i<argc; i++)
   {
       for(int j=0; j<argc; j++)
       {
           if(!strcmp(argv[i], argv[j]))
           {
               argv[i] = NULL;
               break;
           }
       }

   }

    char result[argc-nullIndex][50];

    for(int i = 0; i<argc-nullIndex; i++)
    {
        if(argv[i] != NULL) 
        {
            int j=0;
            while(argv[i][j] != '\0')
            {
                result[i][j] = argv[i][j];
                j++;
            }
        }
    }

    *argv = *result;
    argc = argc-nullIndex;

    for(int i=0; i<argc; i++)
    {
        printf(" %s", argv[i]);
    }
    printf("\n");

    return 0;
}