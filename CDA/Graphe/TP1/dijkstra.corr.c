/*
 * dijkstra.c
 * L. Lemarchand
 * 29/02/2000
 *
 * implantation de l'algorithme de Dijkstra
 */

#include <stdio.h>
#include <stdlib.h>
#include "graphe.h"

/*
 * algorithme de Dijkstra.
 * a completer pour le TP 1.
 *
 * retourne un tableau D (de taille graphe->nb_sommets)
 * contenant pour chaque sommet i la
 * distance minimale de i0 a i
 */

int *dijkstra(int i0, graphe_t *graphe, int *C)
{
	int i;
	/* E est un tableau de nb_sommets. 
	 * E[i] == 0 si i n'est pas encore inclus dans E,
	 * E[i] == 1 sinon
	 *
	 * D t un tableau de nb_sommets.
	 * a la fin de l'algorithme, D[i] vaut la distance
	 * minimale de i0 a i
	 */

	int *D = int_alloc(graphe->nb_sommets);
	int *E = int_alloc(graphe->nb_sommets);

	// A vous
	// init
	for (i=0; i<graphe->nb_sommets; i++) {
		D[i] = PLUS_INF;
		C[i] = i;
		E[i] = 0;
	}
	D[i0] = 0;


	while (1) {
		// calcul du sommet imin suivant
		int imin = -1, vmin = PLUS_INF;
		for (i=0; i<graphe->nb_sommets; i++) {
			if (E[i] == 1) continue;
			if (imin == -1) { imin = i; vmin = D[i]; }
			if (D[i] < vmin) { imin = i; vmin = D[i]; }
		}

		if (imin == -1 || vmin == PLUS_INF) break;
		E[imin] = 1;
		// mise à jour des successeurs j de imin
		for(int a=graphe->lp[imin]; a<graphe->lp[imin+1]; a++) {
			int j = graphe->final[a];
			int v = D[imin] + graphe->poids[a];

			if (v < D[j]) {
				D[j] = v;
				C[j] = imin;
			}
		}
	}
	free(E);
	return D;
}

