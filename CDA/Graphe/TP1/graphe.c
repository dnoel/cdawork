/*
 * graphe.c
 * L. Lemarchand
 * 29/02/2000
 *
 * representation par cocycles positifs d'un graphe
 * pondere
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graphe.h"

/*
 * creation d'un tableau d'entiers.
 */
int *int_alloc(int nb)
{
	int octets = nb*sizeof(int);
	int *ptr = (int *)malloc(octets);

	bzero((char *)ptr, octets);

	return ptr;
}

/*
 * impression d'un tableau d'entiers
 */
void int_print(int *tab, int taille)
{
	if (tab == (int *)0) 
		printf("tableau vide\n");
	else {
		int i;

		for (i=0; i<taille; i++) 
			printf("%2d:   %3d\n", i, tab[i]);
	}
}

/*
 * alloue une structure graphe_t pour un
 * graphe de nb_sommets et nb_arcs.
 */
graphe_t *graphe_alloc(int nb_sommets, int nb_arcs)
{
	graphe_t *graphe;

	graphe = (graphe_t *)malloc(sizeof(graphe_t));

	graphe->nb_sommets = nb_sommets;
	graphe->lp = int_alloc(nb_sommets + 1);

	graphe->nb_arcs = nb_arcs;
	graphe->initial = int_alloc(nb_arcs);
	graphe->final = int_alloc(nb_arcs);
	graphe->poids = int_alloc(nb_arcs);

	return graphe;
}

/*
 * lit le graphe represente par cocycles positifs
 * stocke dans le fichier de nom fichier.
 */
graphe_t *graphe_read(char *fichier)
{

	int idx_sommet, idx_arc;
	int nb_sommets, nb_arcs;
	graphe_t *graphe;
	FILE *fp = fopen(fichier, "r");

	fscanf(fp, "%d", &nb_sommets);
	fscanf(fp, "%d", &nb_arcs);

	graphe = graphe_alloc(nb_sommets, nb_arcs);

	idx_arc = 0;
	for (idx_sommet=0; idx_sommet<nb_sommets; idx_sommet++) {
		int idx_final, arc_poids;

		graphe->lp[idx_sommet] = idx_arc;
		while (1) {
			fscanf(fp, "%d", &idx_final);
			if (idx_final == -1) break;
			fscanf(fp, "%d", &arc_poids);
			graphe->initial[idx_arc] = idx_sommet;
			graphe->final[idx_arc] = idx_final;
			graphe->poids[idx_arc] = arc_poids;
			idx_arc++;
		}
	}
	graphe->lp[nb_sommets] = idx_arc;

	fclose(fp);
	return graphe;
}

/*
 * sauvegarde un graphe dans un fichier
 */
void graphe_save(graphe_t *graphe, char *fichier)
{
	int idx_sommet;
	FILE *fp = fopen(fichier, "w");

	fprintf(fp, "%2d %2d\n", graphe->nb_sommets, graphe->nb_arcs);

	for (idx_sommet=0; idx_sommet<graphe->nb_sommets; idx_sommet++) {
		int idx_arc;

		for (idx_arc=graphe->lp[idx_sommet]; idx_arc<graphe->lp[idx_sommet+1]; idx_arc++) {
			fprintf(fp, "%2d %2d    ", graphe->final[idx_arc], graphe->poids[idx_arc]);
		}
		fprintf(fp, "-1\n");	
	}

}

/*
 * libere les tableaux de la structure de donnees graphe_t
 */

void graphe_free(graphe_t *graphe)
{
	free(graphe->lp);
	free(graphe->initial);
	free(graphe->final);
	free(graphe->poids);
}

/*
 * affiche un graphe sous la forme d'une liste d'arcs
 * sommet initial -> sommet final  ( poids )
 */
void graphe_print(graphe_t *graphe)
{
	int idx_sommet;

	printf("sommets: %2d    arcs: %2d\n", graphe->nb_sommets, graphe->nb_arcs);

	for (idx_sommet=0; idx_sommet<graphe->nb_sommets; idx_sommet++) {
		int idx_arc;

		for (idx_arc=graphe->lp[idx_sommet]; idx_arc<graphe->lp[idx_sommet+1]; idx_arc++) {
			printf("%2d -> %2d    ( %3d )\n", idx_sommet, graphe->final[idx_arc], graphe->poids[idx_arc]);
		}
	}
}

// Exemple d'utilisation: Parcours en profondeur
void graphe_recdfs(int i0, graphe_t *graphe, int *closed)
{
	// si i0 ouvert
	if (closed[i0] == 0) {
		printf("%d\n", i0);
		closed[i0] = 1;
		for (int arc=graphe->lp[i0]; arc<graphe->lp[i0 + 1]; arc++) {
			int j = graphe->final[arc];
			graphe_recdfs(j, graphe, closed);
		}
	}
}

// fonction principale
void graphe_dfs(int i0, graphe_t *graphe)
{
	int *closed = int_alloc(graphe->nb_sommets); // closed[i]==1 if i is closed. initialized by 0
	graphe_recdfs(i0, graphe, closed);
	free(closed);
}



