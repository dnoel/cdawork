/*
 * testgraphe.c
 * L. Lemarchand
 * 29/02/2000
 *
 * representation par cocycles positifs d'un graphe
 * pondere
 */

#include <stdio.h>
#include "graphe.h"
#include "dijkstra.c"

/*
 * exemple d'utilisation
 * des routines de creation de 
 * de graphes.
 */

int main()
{
	graphe_t *g;

	g = graphe_read("exemple.gr");
	graphe_print(g);
	graphe_save(g, "save.gr"); 

	/* ajouter les tests de l'algorithme de Dijkstra */
	int *C = int_alloc(g->nb_sommets);
	int *D = dijkstra(0, g, C);
	printf("dst 0 -> 2 %d\n", D[2]);
	printf("prd 2 %d\n", C[2]);
	// printPath(0, 2, C);

	graphe_free(g);
}

