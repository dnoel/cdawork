/*
 * graphe.h
 * L. Lemarchand
 * 29/02/2000
 *
 * representation par cocycles positifs d'un graphe
 * pondere
 */
struct st_graphe {
	int nb_sommets;
	int nb_arcs;
	int *lp;	/* liste des cocycles positifs. LP[] */
	int *initial;	/* sommets initiaux de arcs. Non utilisé. EI[] */
	int *final;	/* sommets finaux des arcs. EF[] LS[] */
	int *poids;	/* poids des arcs, 
			 * dans le meme ordre que EI[] / EF[] (comme LA[]) 
			 */
};

typedef struct st_graphe graphe_t;

/* defines */
#define PLUS_INF 99999


/* graphe.c */
extern int *int_alloc(int nb);
void int_print(int *tab, int taille);
extern graphe_t *graphe_alloc(int nb_sommets, int nb_arcs);
extern graphe_t *graphe_read(char *fichier);
extern void graphe_save(graphe_t *graphe, char *fichier);
extern void graphe_free(graphe_t *graphe);
extern void graphe_print(graphe_t *graphe);

/* dijkstra.c */
//int *dijkstra(int i0, graphe_t *graphe);
int *dijkstra(int i0, graphe_t *graphe, int *C);
