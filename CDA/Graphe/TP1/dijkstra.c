/*
 * dijkstra.c
 * L. Lemarchand
 * 29/02/2000
 *
 * implantation de l'algorithme de Dijkstra
 */

#include <stdio.h>
#include <stdlib.h>
#include "graphe.h"

/*
 * algorithme de Dijkstra.
 * a completer pour le TP 1.
 *
 * retourne un tableau D (de taille graphe->nb_sommets)
 * contenant pour chaque sommet i la
 * distance minimale de i0 a i
 */

int *dijkstra(int i0, graphe_t *graphe, int *C)
{
	int i;
	/* E est un tableau de nb_sommets. 
	 * E[i] == 0 si i n'est pas encore inclus dans E,
	 * E[i] == 1 sinon
	 *
	 * D t un tableau de nb_sommets.
	 * a la fin de l'algorithme, D[i] vaut la distance
	 * minimale de i0 a i
	 */

	int *D = int_alloc(graphe->nb_sommets);
	int *E = int_alloc(graphe->nb_sommets);

	// A vous
	// init

	for(int i=0; i<graphe->nb_sommets; i++)
	{
		D[i]= PLUS_INF;
		E[i] = 0;
		
	}
	
	D[i0] = 0;

	while (1) 
	{
		
		int imin = -1;
		int vmin = PLUS_INF;

		// calcul du sommet imin suivant
		
		//On détermine par quel sommet continuer
		for(int i=0; i<graphe->nb_sommets;i++)
		{
			
			if( E[i] ==0 && D[i] < vmin)
			{
				imin = i;
				vmin = D[i];
			}
		}
		 
		if(imin ==-1) break;

		E[imin] = 1;
		
		//Pour chaque sommet ans LP[]
		for (int sommet_initial=0; sommet_initial<graphe->nb_sommets; sommet_initial++)
		{
			//Pour chaque arc partant d'un sommet
			for(int sommet_final = graphe->lp[sommet_initial]; sommet_final< graphe->lp[sommet_initial+1]; sommet_final++)
			{
				//on additionne le poid du sommet avec le poid de l'arc 
				int nouveauPoids = vmin + graphe->poids[sommet_final];

				// Si le poid du sommet visé par l'arc est supérieur au nouveau poid crée
				if(D[graphe->final[sommet_final]] > nouveauPoids)
				{
					//On donne ce nouveau poid au sommet visé par l'arc
					D[sommet_final] = nouveauPoids;
				}
				
			}

		}

	}
	free(E);
	return D;
}

