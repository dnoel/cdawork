/* lecture du fichier des distances
 * dans une matrice carree de taille
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


// remplissage de la matrice Distances
// de taille (TOWNS+1) * (TOWNS+1)
// pour numerotation à partir de 1
#define TOWNS 24
#define THRESH  300
#define INFTY  INT_MAX

int Distances[TOWNS+1][TOWNS+1];

typedef struct gps_s
{
	char name[128];
	float lgt, lat;
}gps_t;

gps_t TownInfos[TOWNS+1];

void afficheDistances()
{
	for(int i=0; i<=TOWNS; i++)
	{
		for(int j=0; j<=TOWNS; j++)
		{
			printf("[%d]", Distances[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void readTowns(char villes[])
{
	FILE *fp = fopen(villes, "r");
	//remplit TownInfos
	for(int i=1; i<=TOWNS; i++)
	{
		fscanf(fp, "%s %f %f ", TownInfos[i].name, &TownInfos[i].lat, &TownInfos[i].lgt);
	}
	fclose(fp);
}

void readDistances (char *filename)
{
	int i, j, k;
	int distance;
	FILE *fp = fopen(filename, "r");

	if (!fp) exit(-1);

	for (j=1; j<=TOWNS; j++) 
	{
		Distances[j][j] = 0;
		for (i=1; i<j; i++) 
		{
			fscanf(fp, "%d", &(Distances[i][j]));
			if (Distances[i][j] < THRESH)
				Distances[j][i] = Distances[i][j];
			else
				Distances[i][j] = Distances[j][i] = INFTY;
		}
	}
	fclose(fp);
}

void printPath(char traj[], int dep, int arr, int C[])
{
	FILE *fp = fopen(traj, "w");
	//villes intermédiaires grâce à C[]
	int vi = arr;
	while(1)
	{
		fprintf(fp, "%s %2.2f %2.2f", TownInfos[vi].name, 
									  TownInfos[vi].lat,
									  TownInfos[vi].lgt);
		if(vi==dep) break;
		vi = C[vi];
	}
	
	fclose(fp);
}

int cheminDijkstra(int dep, int arr, int C[TOWNS+1])
{
	int D[TOWNS+1];
	int E[TOWNS+1];

	for(int i=1; i<=TOWNS;i++)
	{
		D[i] = INFTY;
		E[i] = 0;
		C[i] = i;

	}
	D[dep]=0;

	
	while (1) {
		// calc min
		int imin= -1, vmin=INFTY;
		for(int i=1; i<=TOWNS;i++)
		{
			if(E[i]==0 && D[i] < vmin)
			{
				vmin = D[i];
				imin = i;
			}
		}
		// maj succs
		if(imin == -1) break;
		E[imin] = -1;
		for(int i=1; i<=TOWNS; i++)//successeur	
		{		
			int v = D[imin] + Distances[imin][i];
			if(v < D[i])
			{
				D[i] = v;
				C[i] = imin;
			}
		}
	}

	return D[arr];
}

// exemple d'utilisation

int main() 
	int som, dep = 3 /* Brest */, arr = 17 /* Nice */;
	int dist;
	int C[TOWNS+1];

	readDistances("distances.dat");
	readTowns("villes.dat");
	// test de l'algorithme de Dijkstra
	dist = cheminDijkstra(dep, arr, C);

	printf("distance totale : %d\n", dist);

	printPath("trajet.dat", dep /* Brest */, arr /* Nice */, C);
	exit(0);

	system("gnuplot plot.script");

	// generer dans aretes.dat la liste des aretes (2 lignes / arete + 1 ligne blanche)
	// system("gnuplot plotGraph.script");
	// le graphe utilisé sera dans graph.gif

}

