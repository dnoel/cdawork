#include <stdio.h>
#define P 10
#define N 600
#define S "bbb_cif_baseline_mean"
#define PlUS_INF 99999


int tab[N];

void afficheTab(int tab[N])
{
    printf("tableau : [%d", tab[0]);

    for(int i=1; i<N; i++)
    {
        printf(", %d", tab[i]);
    }
    printf(" ]\n");

}
int readBitrates(char *filename)
{
    int i=0;
    int val;
    FILE *file = fopen(filename, "r");
    while(fscanf(file, "%d", &val)==1)
    {
        tab[i++]=val;
    }
    fclose(file);
    return i;
}

// int costInterval(int i, int j)
// {
//     int cost =0;
//     int pValues[P];
//     int k=0;

//     int max=0;
//     //ON parcours l'intervalle (i,j)
//     for(i; i<j; i++)
//     {
//         //Pour chaque P valeurs récupérées
//         for(k; k<P || j<k; k++)
//         {
//             //On récupère les P valeurs du tableau
//             pValues[k] = tab[i+k];

//             //ON détermine la valeur max
//             if(tab[i+k]>max)
//             {
//                 max = tab[i+k];
//             }
//         }
//         i = i+k;
//         k=0;
//         //On détermine le coup 

//         for(int l = 0; l<P; l++)
//         {
//             cost += (max -pValues[l]);
//         }
//     }

//     return cost;
// }

int costInterval(int i, int j)
{
    int cost =0;
    int max=0;
    //ON parcours l'intervalle (i,j)
    for(int k=i; k<j; k++)
    {
        if(tab[k]>max)
        {
            max = tab[k];
        }

       
    }

    for(int k=i; k<j; k++)
    {
        cost += (max-tab[k]);
    }

    return cost;
}

int bellman(int n)
{
    int D[n+1];
    D[0] = 0;
    for(int j=1; j<=n; j++)
    {
        D[j] = PlUS_INF;
        //maj grâce à tous les prédecesseurs i de j
        int first = j-2*P;
        if(first <0) first =0;
        int last =(j-P);
        if(j==n) last = n-1;
        for(int i=first; i<=last; i++)
        {
            int v = D[i] + costInterval(i,j);
            if(v<D[j])
            {
                D[j] =v;
            }
        }
    }
    return D[n];
}

int main()
{
    
    int n = readBitrates(S);
    

    readBitrates(S);

    // afficheTab(tab);
    int cost = bellman(n);

    printf("cost : %d\n", cost);


}