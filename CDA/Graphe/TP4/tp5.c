// TP 5 AFG
// 22 9 2014
// Laurent Lemarchand

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define INFTY 1000000.0
#define MAXNODE 100


// read position files, and return the number of nodes read
int readPositions(char fname[], int nodeX[MAXNODE], int nodeY[MAXNODE])
{
	int i;
	int x, y;
	FILE *fp;

	fp = fopen(fname, "r");
	if (!fp) {
		printf("error opening postions\n");
		exit(-1);
	}

	i = 0;
	while (fscanf(fp, "%d%d", &x, &y) == 2) {
		nodeX[i] = x;
		nodeY[i] = y;
		i++;
	}
	fclose(fp);
	return i;
}
	
 
// norm
float computeDistance(int i, int j, int nodeX[MAXNODE], int nodeY[MAXNODE])
{
	int xDist = nodeX[j]-nodeX[i];
	int yDist = nodeY[j]-nodeY[i];
	return sqrtf(xDist*xDist + yDist*yDist);
}

void fillDistances(int nodes, int nodeX[MAXNODE], int nodeY[MAXNODE], float as[MAXNODE][MAXNODE])
{
	for(int i=0; i<nodes; i++)
	{
		as[i][i] = 0;
		for(int j=0; j<i; j++)
		{
			as[i][j]=as[j][i]=computeDistance(i,j, nodeX, nodeY);
		}
	}
}

void prim(int root, int nodes, float as[MAXNODE][MAXNODE], int edgelist[MAXNODE])
{
	int i, j;
	int E[nodes];	// connected or not ?
	float D[nodes];	// distance to the tree

	for (i=0; i<nodes; i++) {
		E[i] = 0;
		D[i] = INFTY;
	}
	D[root] = 0.0;
	edgelist[root] = root;

	while (1) 
	{
		// TP5 Q4 
		// complete the current step of Prim's algorithm here
		// when does it end ?
		float min = INFTY; // for next current node finding
		int imin = -1; // for next current node finding

		for(i=0; i<nodes; i++)
		{
			if(E[i]==0 && D[i]<min)
			{
				imin = i;
				min = D[i];
			}
		}

		if(imin== -1) break;
		E[imin] = 1;

		//mise à jour voisins de imin
		for(int j=0; j<nodes; j++)
		{
			if(E[j] ==0 && D[j]>as[j][imin])
			{
				D[j] = as[j][imin];
				edgelist[j] = imin;
			}
		}


	}
}

// for TP5 Q5 
void drawGraph(int nodes, int root, int edgelist[MAXNODE], int nodeX[MAXNODE], int nodeY[MAXNODE])
{
	char cmd[1024];
	FILE *fp;
	int i;

	fp = fopen("tree.gv", "w");
	if (!fp) { printf("cannot open tree.gv\n"); exit(-1); }
        fprintf(fp, "graph theTree {\n");

	for (i = 0; i<nodes;  i++)
		fprintf (fp, "%d [ pos=\"%d,%d!\" ];\n", 
				i, nodeX[i], nodeY[i]);
	
	for (i=0; i<nodes; i++)
			if (i != root)
				fprintf(fp, "%2d -- %2d ;\n", i, edgelist[i]);
	
	// completion and generation of the drawing
	fprintf(fp, "}\n");
	fclose(fp);

        system("cat tree.gv");
        printf("\nDRAW THE CONTENTS OF tree.gv AT http://www.webgraphviz.com/\n");

	//system("neato -Tpdf -o tree.pdf tree.gv");
}

int main()
{
	float matrix[MAXNODE][MAXNODE]; 
	int  nodeX[MAXNODE], nodeY[MAXNODE];
	int edgelist[MAXNODE];
	int n;

	n = readPositions("tp5_er.dat", nodeX, nodeY);
	printf("%d nodes\n", n);

	fillDistances(n, nodeX, nodeY, matrix);

	// for(int i =0; i<n; i++)
	// {
	// 	for(int j=0; i<n; i++)
	// 	{
	// 		printf("[%f]", matrix[i][j]);
	// 	}
	// 	printf("\n");
	// }
	printf("\n");
	prim(0,n,matrix,edgelist);

	// TP5 Q5 -- draw the resulting MST
	drawGraph(n, 10, edgelist, nodeX, nodeY);
	
}

