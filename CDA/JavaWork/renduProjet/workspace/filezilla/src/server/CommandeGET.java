package server;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;


//Envoie au client le fichier vis� par la requ�te si et seulement si c'est un fichier
//Cr�er un socket de communication pour le partage des fichiers avec le client
public class CommandeGET extends Commande 
{
	
	public CommandeGET(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	public void execute(CommandExecutor ce) throws IOException
	{
		//On cr�er un Objet File avec comme chemin celui du fichier vis� par la commande
		File f = new File(ce.currentPath + "/" + commandeArgs[0]);
		//S'il n'existe pas
		if(!f.exists())
		{
			System.out.println("Le fichier demandé n'existe pas");
			ps.println("2 Le fichier demandé n'existe pas");
			return;
		}
		//Si c'est un r�pertoire
		if(f.isDirectory())
		{
			System.out.println("Le fichier demandé est un répertoire -> REFUS");
			ps.println("2 Demande de copie de répertoire -> REFUS");
			return;
		}
		
		System.out.println("Le fichier existe et n'est pas un répertoire");
		//Cr�ation d'un nouveau ServerSocket 
		ServerSocket ss= new ServerSocket(0);
		System.out.println("Envoie du port du socket de transmission de fichiers : "+ ss.getLocalPort());
		
		//Renvoie du port du socket de partage de fichiers
		ps.println("0 " + ss.getLocalPort());
		//Attente de la connexion du clien t au nouveau socket
		Socket socket= ss.accept();
		
		//Fermeture du ServerSocket 
		ss.close();
		//Envoie des informations au client	
		
		//Cr�ation d'un tableau d'octets de taille �gale � la taille du fichier
		byte[] buffer = new byte[packetSize]; 
		
		//PrintStream permettant de lire le cotnenu du fichier
		FileInputStream in = new FileInputStream(f);
		
		//OutputStream permettant de renvoyer le tableau d'octets au client
		OutputStream out = socket.getOutputStream();
		
		
		int count;
		//Tant qu'il y a des octets � lire dans le fichier
		while ((count = in.read(buffer)) > 0)
		{
		//On envoie le contenu des count premi�res cases du tableau au client 
		  out.write(buffer, 0, count);
		}
		
		System.out.println("Fin d'envoie du contenu des fichiers");
		
		//On ferme le socket, le FileInputStream et le OutputStream
		socket.close();
		in.close();
		out.close();
	}
}
