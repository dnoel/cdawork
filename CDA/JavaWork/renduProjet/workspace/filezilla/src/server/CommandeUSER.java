package server;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class CommandeUSER extends Commande {
	
	public CommandeUSER(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}
	//V�rifie si l'identifiant donn� par le client existe 
	public void execute(CommandExecutor ce) 
	{
		//A chaque nouvelle tentative on re-initialise userOK et pwOK � false
		ce.userOk = false;
		ce.pwOk = false;
		
		//On r�cup�re le dossier ayant pour nom l'identifiant donn� par l'utilisateur 
		File f = new File(System.getProperty("user.dir").toString() + "/" + commandeArgs[0]);
		//Si il n'existe pas
		if(!f.exists())
		{
			//On renvoei l'echec � l'utilisateur
			System.out.println("Utilisateur " + commandeArgs[0] + " Introuvable");
			ps.println("2 Le user " + commandeArgs[0] + " n'existe pas");
			return;
		}
		
		//Sinon on met userOK � true
		ce.userOk = true;
		try 
		{
			//La valeur de chemin du r�pertoire utilisateur est �gale � la valeur du chemin du fichier cr�e 
			ce.userPath = f.getCanonicalPath();
			//Le chemin actuel c�t� serveur est �gale au chemin du r�pertoire utilisateur 
			ce.currentPath = ce.userPath.toString();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//On renvoie le succ�s
		this.ps.println("0 User valide");
		
	}

}
