package server;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

//Classe dont h�ritent toutes les classes g�rant les commandes du serveur 
public abstract class Commande 
{
	//Taille des paquets pourla transmission et r�cup�ration des contenus de fichier
	protected  int packetSize;
	protected String commandeNom = "";
	
	//Arguments des commandes
	protected String [] commandeArgs ;
	public PrintStream ps;
	public Commande(PrintStream ps, String commandeStr) 
	{
		this.ps = ps;
		String [] args = commandeStr.split(" ");
		commandeNom = args[0];
		commandeArgs = new String[args.length-1];
		packetSize = 100;
		
		for(int i=0; i<commandeArgs.length; i++) 
		{
			commandeArgs[i] = args[i+1];
		}
	}
	
	public abstract void execute(CommandExecutor ce) throws FileNotFoundException, IOException;

}
