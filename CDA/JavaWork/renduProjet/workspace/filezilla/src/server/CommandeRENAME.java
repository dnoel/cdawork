package server;
import java.io.File;
import java.io.PrintStream;

public class CommandeRENAME extends Commande {
	
	public CommandeRENAME(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	//Renomme le fichier choisi
	public void execute(CommandExecutor ce) 
	{
		//Fichier vis� par la commande re�ue 
		File f = new File(ce.currentPath+ "/" + this.commandeArgs[0]);
	     
		//Si le fichier n'existe pas 
	      if(!f.exists())
	      {
	          System.out.println("Le fichier � renommer n'existe pas");
	          ps.println("2 Le fichier � renommer n'existe pas");
	          return;
	      }
	      //Cr�ation d'un deuxi�me fichier avec le nom de substitution
	      File f2 = new File(ce.currentPath+ "/" +this.commandeArgs[1]);
	      
	      //Si le changement de nom s'est effectu�
	      if(f.renameTo(f2)) 
	      {
	    	  //On renvoie le succ�s
	    	  System.out.println("fichier " + f.getName() + "renomm� avec succ�s");
		      ps.println("0 fichier renomm� en " + f.getName());
   
		   //Sinon
	       } else {
	          System.out.println("Rename failed");
	          ps.println("2 Echec du renommage");
	       } 
	      
	}

}
