package server;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

//Commande permettant de se d�placer dans l'arborescence du serveur
public class CommandeCD extends Commande 
{
	
	public CommandeCD(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	public void execute(CommandExecutor ce)
	{
		
		//R�f�rencement du fichier vis� par la commande
		File file = new File(ce.currentPath + "/" + this.commandeArgs[0]);
		
		//Si le fichier n'existe pas
		if(!file.exists()) 
		{
			//Le r�pertoire n'existe pas
			System.out.println("Le répertoir n'existe pas");
			ps.println("2 Le répertoire n'existe pas");
			return;

		}
		//Si ce n'est pas un r�pertoire 
		if(!file.isDirectory())
		{
			//Ce n'est pas un r�pertoire 
			System.out.println(this.commandeArgs[0] + "n'est pas un r�pertoire");
			ps.println(this.commandeArgs[0] + "n'est pas un r�pertoire");
			return;
		}
		
		//Variale contenant la nouveau r�pertoire courant � la fin du traitement 
		String filePath = "";
		try 
		{
			//On r�cup�re le chemin du dossier
			filePath = file.getCanonicalPath().toString();
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("filePath : " + filePath);
		
		//Si le nouveau chemin est au moins de la taille de chemin original
		if((filePath.length() >=ce.userPath.length()))
		{
			//Si celle-ci commence bien par le chemin original
			if(filePath.substring(0, ce.userPath.length()).equals(ce.userPath))
			{
				//On change le chemin actuel dans le CommandExecutor
				try 
				{
					ce.currentPath = file.getCanonicalPath().toString();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				//On renvoie le nouveau chemin au client
				ps.println("0 " + ce.currentPath);
				return;
			}
		}
		
		//Si le répertoire demandé n'est pas accessible
		System.out.println("Tentative de dépasser le répertoire original");
		ps.println("2 Vous ne pouvez dépasser le répertoir original du serveur");
		return;

	}
}
