package server;
import java.io.File;
import java.io.PrintStream;

public class CommandeRM extends Commande {
	
	public CommandeRM(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	//Fonction r�cursive pour la suppression d'un dossier
	boolean deleteDirectory(File directory) 
	{
		//On r�cup�re l'ensemble des fichiers contenus dans le r�pertoire 
	    File[] content = directory.listFiles();
	    
	    //Si le dossier contient d'autres fichiers
	    if (content != null) 
	    {	
	        for (File f : content) {
	        	//On fait un appel r�cursif � la fonction
	            deleteDirectory(f);
	        }
	    }
	    //Sinon on renvoei le boolean de la suppression de la suppression
	    return directory.delete();
	}
	
	//Commande pour supprimer un fichier ou r�cursivement un dossier
	public void execute(CommandExecutor ce) 
	{
		//On r�cup�re le fichier vis� par la commande
		File f = new File(ce.currentPath+ "/" + this.commandeArgs[0]);
	      //Si il n'existe pas
	      if(!f.exists())
	      {
	    	  //Envoie de l'eerreur � l'utilisateur
	          System.out.println("Le fichier � supprimer n'existe pas");
	          ps.println("2 Le fichier � supprimer n'existe pas");
	          return;
	      }
	      //Si c'est un r�pertoire
	      if(f.isDirectory())
	      {
	    	  //Si la fonction r�cursive de suppression de r�pertoire renvoie un succ�s
	    	  if(deleteDirectory(f)) 
		      {
		    	  System.out.println("r�pertoire " + f.getName() + " supprim� avec succ�s");
			      ps.println("0 r�pertoire" + f.getName() + "supprim�");
		      } 
	    	  //Sinon
		      else 
		      {
		          System.out.println("Probl�me de suppression");
		          ps.println("2 Echec de la suppression");
		      } 
	    	  
	    	  return;
	      }
	      //Si c'est un simple fichier
	      //Si la suppression a fonctionn�
	      if(f.delete()) 
	      {
	    	  System.out.println("fichier " + f.getName() + " supprim� avec succ�s");
		      ps.println("0 fichier" + f.getName() + "supprim�");
	      } 
	      //Sinon
	      else 
	      {
	          System.out.println("Probl�me de suppression");
	          ps.println("2 Echec de la suppression");
	      } 
	      
	}

}
