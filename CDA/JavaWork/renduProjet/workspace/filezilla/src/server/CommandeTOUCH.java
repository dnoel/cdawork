package server;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class CommandeTOUCH extends Commande {
	
	public CommandeTOUCH(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	//Cr�er un fichier
	public void execute(CommandExecutor ce) 
	{
		//On r�cup�re le fichier donn� param�tre de la commande 
		File f = new File(ce.currentPath+ "/" + this.commandeArgs[0]);
	      //Si le fichier existe d�j�
	      if(f.exists())
	      {
	          System.out.println("Le fichier existe d�j�");
	          ps.println("2 Le fichier existe d�j�");
	          return;
	      }
	      //Sinon
	      try 
	      {
	    	//On cr�er le fichier et on renvoie le succ�s
			f.createNewFile();
			 ps.println("fichier " + f.getName() + "cr�e avec succ�s");
	      } 
		  catch (IOException e) 
		  {
			// Si une erreur se produit pendant la cr�ation du fichier on renvoie l'echec � l'utilisateur
			ps.println("2 " + e.toString());
		  }
	      //Message c�t� serveur
	      System.out.println("fichier " + f.getName() + "cr�e avec succ�s");
	     
	}

}
