package server;
import java.io.File;
import java.io.PrintStream;

public class CommandeMKDIR extends Commande {
	
	public CommandeMKDIR(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	//Cr�er un dossier 
	public void execute(CommandExecutor ce) 
	{
		
		
		//Cr�ation de l'objet File
		File f = new File(ce.currentPath + "/" + this.commandeArgs[0]);
	      //Creating the directory	
	     
		 //Si le fichier existe d�j�
		 if(f.exists())
		 {
			 //On renvoei l'erreur au client
			 System.out.println("Le r�pertoire existe d�j�");
	         ps.println("2 Le r�pertoire " + this.commandeArgs[0] + " existe d�j�");
	         return;
		 }
		 //Si la cr�ation du fichier s'est bein effectu�e 
		 if(f.mkdir())
		 {
			 //Message de succ�s
		      System.out.println("Directory created successfully");
		      ps.println("0 R�pertoire " + this.commandeArgs[0] + " cr�e avec succ�s");
		      return;
      
		 }else
		 {
			 //Message d'echec
			  System.out.println("Sorry couldn�t create specified directory");
			  ps.println("2 Erreur lors de la cr�ation du r�pertoire: " + this.commandeArgs[0]);
		 }
	}

}
