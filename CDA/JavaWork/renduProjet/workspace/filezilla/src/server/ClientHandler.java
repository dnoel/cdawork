package server;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;


//Le clientHandler est la classe lanc�e par un thread assurant la communication avec le client apr�s que celui-ci se soit conenct� au port de connexion du serveur
//Celui-ci cr�er alors un nouveau socket de communication dont il envoie le port au client afin que celui-ci ne communique plus qu'avec le clientHandler
public class ClientHandler implements Runnable
{
	public  Socket socket;
	public  BufferedReader br;
	public  PrintStream ps;

	public ClientHandler(Socket socket)
	{
		this.socket = socket;
	
		//Cr�ation du printStream pour envoyer le nouveau socket au client
		try 
		{
			this.ps = new PrintStream(socket.getOutputStream());
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void run() 
	{
		//Cr�ation d'un nouveau serverSocket sur un port disponible
		ServerSocket newSS;

			try 
			{
				//Cr�ation d'un nouveau socket serveur
				newSS = new ServerSocket(0);
				
				//Envoie du port du nouveau socket serveur au client gr�ce au socket de communication re�u en parametre du constructeur
				this.ps.println("0 " + newSS.getLocalPort());
				
				//Changement du socket en acceptant la connexion du client au nouveau serverSocket newSS
				this.socket = newSS.accept();
				newSS.close();
				
				//Cr�ation du buffereader et printstream du nouveau socket
				this.br = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
				this.ps = new PrintStream(this.socket.getOutputStream());
			} 
			catch (IOException e1) 
			{
				// Si il y a une erreur 
				e1.printStackTrace();
				//On renvoi le message d'erreur au client
				try
				{
					
					//On envoie l'erreur au client si possibilit� 
					ps.println("2 Erreur lors de la cr�ation du socket");
				}
				catch(Exception e)
				{
				}
				
			}
		

		//Message de bienvenu au client si tout s'est bien pass�
		this.ps.println("1 Bienvenue ! ");
		this.ps.println("1 Serveur FTP Personnel.");
		this.ps.println("0 Authentification : ");
		
		//String destin�e � contenir la requ�te envoy�e par le client
		String commande = "";
		
		//Cr�ation d'un nouveel Objet CommandExecutor permettant le traitement des requ�tes 
		CommandExecutor ce = new CommandExecutor();
		
		// Attente de reception de commandes et leur execution
		try 
		{
			//Tant que la requ�te re�ue est diff�rente de bye
			while(!(commande=br.readLine()).equalsIgnoreCase("bye")) 
			{
				System.out.println(">> "+commande);
				
				//On excecute la commande en la donnant en param�tre � l'objet CommandExecutor
				ce.executeCommande(this.ps, commande);			
			}
			
			//On affiche que le client s'est deconnect� 
			System.out.println("Le client " + socket.getInetAddress().toString() + "s'est d�connect�");
			this.ps.close();
			this.br.close();
			this.socket.close();
		} 
		catch (IOException e) 
		{
			//Si on perd la connexion avec le client
			System.out.println("Arret impromptu de la communication avec l'ip: " + socket.getInetAddress().toString());
			try 
			{
				//On ferme le socket de communication
				this.socket.close();
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		
		
	}


}
