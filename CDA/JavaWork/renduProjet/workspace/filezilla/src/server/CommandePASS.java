package server;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.io.Reader;

public class CommandePASS extends Commande 
{
	
	public CommandePASS(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}
	
	//V�rifie le mot de passe du de l'utilisateur en v�rifiant si le fichier "pw.txt" du dossier au nom de l'utilisateur contient la valeur donn�e 
	public void execute(CommandExecutor ce) 
	{
		//fichier de mot de passe dans le dossier de l'utilisateur
		File f = new File(ce.userPath + "/pw.txt");
		//Si l'utilisateur existe et est bien un fichier
		
		if(!f.exists() && !f.isDirectory())
		{
			System.out.println("Fichier de mot de passe introuvable");
			ps.println("2 Mot de passe introuvable");
		}
		
		//Variable �gale au contenu du fichier pw.txt
		String pwd = "";
		try
		{	//Pour lire le contenu du fichier
			Reader r = new FileReader(f.getCanonicalPath());
			BufferedReader br = new BufferedReader(r);
			
			//R�cup�ration du contenu
			 pwd= br.readLine();
			 br.close();
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			ps.println("2 Impossibilit� de r�cup�rer le mot de passe");
		}
		
		//Si le mot de passe entr� par l'utilisateur est bien "gal au mot de passe dans le dossier de l'utilisateur
		if(commandeArgs[0].contentEquals(pwd)) 
		{
			//On met e mot de passe � true dans le CommandExecutor
			ce.pwOk = true;
			ps.println("1 Commande pass OK");
			ps.println("0 Vous êtes bien connecté sur notre serveur");
		}
		else
		{
			// On renvoie l'echec au client
			System.out.println("Mot de passe rentr� ne correspond pas");
			ps.println("2 Le mode de passe est faux");
		}
		
	}

}
