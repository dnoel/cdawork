package server;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class CommandExecutor {
	
	//Boolean permettant de savoir si le dernier identifiant donn� est valable 
	public  boolean userOk;
	//Boolean permztatnt de savori si le mot de passe correspond au dernier identifiant donn�
	public  boolean pwOk;
	
	//Chemi ndu r�pertoir du serveur o� sont sstock�s les dossier des utilisateurs 
	public  final String originalPath;
	
	//Chemin du r�pertoire de l'utilisateur connect�
	public  String userPath = new String("");
	
	//chemin actuel dans le r�pertoir de l'utilisateur
	public  String currentPath;
	

	//Constructeur 
	public CommandExecutor()
	{ 	this.originalPath = new File("").getAbsolutePath();
		this.userOk = false;
		this.pwOk = false;
		this.currentPath = new String(this.originalPath.toString());
	}
	
	
	//Traite la valeur de la requ�te re�u et lance la m�thode correspondante � la requ�te
	public void executeCommande(PrintStream ps, String commande) throws IOException 
	{
		//Si l'utilisateur est connect� 
		if(this.userOk && this.pwOk) 
		{
			// Changer de repertoire. Un (..) permet de revenir au repertoire superieur
			
			if(commande.split(" ")[0].equals("cd")) new CommandeCD(ps, commande).execute(this);
	
			// Telecharger un fichier
			if(commande.split(" ")[0].equals("get")) (new CommandeGET(ps, commande)).execute(this);
			
			// Afficher la liste des fichiers et des dossiers du repertoire courant
			if(commande.split(" ")[0].equals("ls")) (new CommandeLS(ps, commande)).execute(this);
		
			// Afficher le repertoire courant
			if(commande.split(" ")[0].equals("pwd")) (new CommandePWD(ps, commande)).execute(this);
			
			// Envoyer (uploader) un fichier
			if(commande.split(" ")[0].equals("store")) (new CommandeSTOR(ps, commande)).execute(this);
			
			if(commande.split(" ")[0].equals("bye")) (new CommandeBYE(ps, commande)).execute(this);
			
			if(commande.split(" ")[0].equals("mkdir")) new CommandeMKDIR(ps, commande).execute(this);
			if(commande.split(" ")[0].equals("touch")) new CommandeTOUCH(ps, commande).execute(this);
			if(commande.split(" ")[0].equals("rename")) new CommandeRENAME(ps, commande).execute(this);
			if(commande.split(" ")[0].equals("rm")) new CommandeRM(ps, commande).execute(this);

		}
		else 
		{
			//Si l'utilisateur n'est pas encore connect� 
			if(commande.split(" ")[0].equals("pass") || commande.split(" ")[0].equals("user")) 
			{
				// Le mot de passe pour l'authentification
				if(commande.split(" ")[0].equals("pass"))
				{
					new CommandePASS(ps, commande).execute(this);
				}
	
				// Le login pour l'authentification
				if(commande.split(" ")[0].equals("user")) 
				{
					new CommandeUSER(ps, commande).execute(this);
				}
				
			}
			//Si l'utilisateur a tent� une commande dans �ter connect�
			else
			{
				ps.println("2 Vous n'êtes pas connecté !");
			}
		}
	}

}
