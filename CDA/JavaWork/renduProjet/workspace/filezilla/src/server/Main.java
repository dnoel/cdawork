package server;
/*
 * TP JAVA RIP
 * Min Serveur FTP
 * */

import java.net.ServerSocket;
import java.net.Socket;

public class Main 
{
	//Partie du serveur acceptant les connexion sur le port (2121)
	public static void main(String[] args) throws Exception 
	{
		//Attente constante d'une connexion
		@SuppressWarnings("resource")
		//Socket serveur sur le port 2121, on ne le close jamais
		ServerSocket serveurFTP = new ServerSocket(2121);
		while(true)
		{	
			System.out.println("Le Serveur FTP");
			
			System.out.println("Attente d'une connexion");
			
			//Attente d'une connexion sur le port 2121 et cr�ation d'un socket 
			Socket socket = serveurFTP.accept();
			System.out.println("Connexion établie");
			
			//Cr�ation d'un thread de la classe ClientHandler(), classe assurant le reste de la communication avec l'utilisateur gr�ce au socket de connextion donn�e en param�tre
			Thread t = new Thread(new ClientHandler(socket));
			//D�marrage du thread 
			t.start();
		}
	}
}
