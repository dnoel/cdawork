package server;
import java.io.PrintStream;

public class CommandeBYE extends Commande {
	
	public CommandeBYE(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}
	//La fermeture du socket, du PrintStream et du BufferedReader se font dans le ClientHandler directement
	public void execute(CommandExecutor ce) 
	{

			this.ps.println("0 Déconnexion du serveur");	
	}

}
