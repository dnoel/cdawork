package application;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.TextFlow;

public class Controlleur
{

	//Objet contenant les informations n�cessaires � la communication et la sauvegarde des informations entre le client et le serveur
    public Data data;
    public Commandes commandes;
	   
    //Adresse du serveur entr�e par l'utilisateur
    @FXML
    public TextField host;

    //identifiant entr� par l'utilisateur
    @FXML
    public TextField username;
    
    //mot de passe entr� par l'utilisateur
    @FXML
    public TextField password;
    
    //port du serveur  entr� par l'utilisateur
    @FXML
    public TextField port;

    //requ�te �crite par l'utilisateur
    @FXML

    public TextField requete;

    //El�ment de l'ihm contenant les communications entre le serveur et le client
    @FXML
    public TextFlow console;
    
    //El�ment de l'ihm contenant le chemin du r�pertoire courant du client    
    @FXML
    public TextField localPath;

    //El�ment de l'ihm contenant le chemin du r�pertoire courant du serveur
    @FXML 
    public TextField distantPath;
    
    //El�ment de l'ihm contenant les fichiers du r�pertoire courant du client
    @FXML
    public TextFlow localDirectory;

    //El�ment de l'ihm contenant les fichiers du r�pertoire courant du serveur
    @FXML
    public TextFlow distantDirectory;
    
    
    //El�ment de l'ihm contenant les commandes pour manipuler le r�pertoi
    @FXML
    public TextFlow localCommandPanel;
    
    //El�ment de l'ihm contenant les commandes pour manipuler le r�pertoire c�t� serveur
    @FXML
    public TextFlow distantCommandPanel;
    
    
    //Liste des commandes affich�es dans localCommandPanel
    public String localCommand = 
    		"Cr�ation fichier : touchcli {nomFichier}\r\n" + 
    		"Renommage fichier : renamecli {nomFichierARenommer} {nouveauNom}\r\n" + 
    		"Cr�ation r�pertoire : mkdircli {nomRepertoire}\r\n" + 
    		"Visualisation du r�pertoire courant : lscli\r\n" + 
    		"suppression d�un fichier ou r�pertoire : rmcli {nomFichier/Repertoire}\r\n" + 
    		"D�placement dans l�arborescence de fichiers : cdcli [cheminDepuisLeRepertoireCourant]\r\n" + 
    		"R�cup�ration d'un fichier du serveur: get {nomFichier}\r\n";
    
    public String distantCommand = 
		"Cr�ation fichier : touch {nomFichier}\r\n" + 
		"Renommage fichier : rename {nomFichierARenommer} {nouveauNom}\r\n" + 
		"Cr�ation r�pertoire : mkdir {nomRepertoire}\r\n" + 
		"Visualisation du r�pertoire courant : ls\r\n" + 
		"suppression d�un fichier ou r�pertoire : rm {nomFichier/Repertoire}\r\n" + 
		"D�placement dans l�arborescence de fichiers : cd [cheminDepuisLeRepertoireCourant]\r\n" +
		"Envoie d�un fichier au serveur: store {nomFichier}\r\n";    


 
    	    
    
    //Lorsque l'utilisateur saisie une commande dans le champ "Commande" 
    //Lance la methode RequqeteManager() qui lance la m�thode correspondante � la requ�te saisie par l'utilisateur
    @FXML
    public void requeteEnter(KeyEvent event) throws Exception
    {
		if(event.getCode() == KeyCode.ENTER)
		{
			this.data.requete = this.requete.getText();
			this.requete.setText("");
			
			//Si le requeteManager renvoie une exception alors la connexion au serveur a �t� perdue 
			try
			{
				this.requeteManager();
			}
			catch(Exception e)
			{
				//Pr�vention de l'utilisateur et 
				this.commandes.print(console, "La connexion au serveur a �t� interrompue");
				this.commandes.clearServerData();
			}
			
			
		}
      
    }   
    
    //M�thode permettant de g�rer les diff�rentes requ�tes saisies par l'utilisateur
    
    //Lance la m�thode correpondante � la requ�te saisie, si la requ�te ne n�cessite pas de traitement particulier, le requeteManager lance la m�thode sendReceive
    
    //On lance les m�thodes ls ou lscli quand une requ�te induit la modification du r�pertoire courant c�t� client ou serveur
    
    //Le requete manager v�rifie le nombre de param�tres dans la requete afin de v�irfier seulement les requ�tes acceptant ce nombre de param�etres
    //cela permet de g�rer les cas d'erreurs o� l'utilisateur se trompe dans la saisie de la requ�te 
    public void requeteManager() throws Exception
	{
    		
    		int count = this.data.requete.split(" ").length;
			String s = this.data.requete.split(" ")[0];
			
			try 
			{
				//Si la m�thode n'a que 1 parametre
				//On v�rifie les m�thodes � 1 param�etre 
				if(count ==1)
				{
					if(s.equals("pwd"))
					{
						this.commandes.pwd();
						this.data.requete = "ls";
						this.commandes.ls();
						return;
					}
					if(s.equals("ls"))
					{
						this.commandes.ls();
						return;
					}
					if(s.equals("lscli"))
					{
						this.commandes.lsCli();
						return;
					}
					if(s.equals("bye"))
					{
						this.commandes.bye();
						this.commandes.clearServerData();
						return;
					}
				}
				
				//Si la m�thode n'a que 2 parametre
				//On v�rifie les m�thodes � 2 param�etre 
				if(count==2)
				{
					if(s.equals("user")) 
					{
						this.commandes.sendReceive();
						return;
					}
					if(s.equals("pass")) 
					{
						this.commandes.sendReceive();
						return;
					}
					
					if(s.equals("cd"))
					{
						this.commandes.cd();
						this.data.requete = "ls";
						this.commandes.ls();
						return;
					}
					if(s.equals("cdcli"))
					{
						this.commandes.cdCli();
						this.commandes.lsCli();
						return;
					}
					
					if(s.equals("mkdir"))
					{
						this.commandes.sendReceive();
						 this.data.requete = "ls";
						 this.commandes.ls();
						 return;
					}
					if(s.equals("mkdircli"))
					{
						this.commandes.mkdirCli();
						this.commandes.lsCli();
						 return;
					}
					if(s.equals("touch")) 
					{
						this.commandes.sendReceive();
						 this.data.requete = "ls";
						 this.commandes.ls();
						 return;
					}
					if(s.equals("touchcli"))
					{
						this.commandes.touchCli();
						this.commandes.lsCli();
						 return;
					}
					
					if(s.equals("get"))
					{
						this.commandes.get();
						this.commandes.lsCli();
						 return;
					}
					if(s.equals("store"))
					{
						this.commandes.store();
						 this.data.requete = "ls";
						 this.commandes.ls();
						 return;
					}
					if(s.equals("rm"))
					{
						this.commandes.sendReceive();
						 this.data.requete = "ls";
						 this.commandes.ls();
						 return;
					}
					if(s.equals("rmcli"))
					{
						this.commandes.rmCli();
						this.commandes.lsCli();
						 return;
					}
				}
				
				//Si la m�thode n'a que 3 parametre
				//On v�rifie les m�thodes � 3 param�etre 
				if(count==3)
				{
					if(s.equals("rename"))
					{
						this.commandes.sendReceive();
						 this.data.requete = "ls";
						 this.commandes.ls();
						return;
					}
					if(s.equals("renamecli"))
					{
						this.commandes.renameCli();
						this.commandes.lsCli();
						 return;
					}
				}
				
				this.commandes.print(this.console, "Erreur commande, assurez d'avoir bien �cris la commande et d'avoir donn� le bon nombre d'argument");
				

				
				

			} 
			catch (NumberFormatException e) 
			{
				// TODO Auto-generated catch block
				throw e;
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				throw e;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				throw e;
			}
	}
  //METHODE EXECUTEE PAR LE BOUTON "CONNEXION" LORSQUE L'on TENTE UNE NOUVELLE CONNEXION
    public void serverConnexion()
	{
    	//On efface les informations du serveur 

    		this.console.getChildren().clear();
    		
		try
		{
			//Si on est d�j� connect� � un serveur (un objet Data data existe d�j�)
			if(this.data!=null)
			{
				//Lancement de la commande bye pour se d�connecter proprement de la connexion en cours
				this.data.requete = "bye";
				requeteManager();
			}
			
			//Cr�ation d'un nouvel Objet Data vierge
			this.data= new Data();
			
			//Cr�ation du nouvel objet de m�thodes
			this.commandes = new Commandes(this);
			//Cr�ation d'un socket avec les informations entr�es par l'utilisateur
			this.data.sock = new Socket(this.host.getText(), Integer.parseInt(this.port.getText()));
			
			//Mise en place de la connexion
			this.data.addr= this.data.sock.getInetAddress();
			this.commandes.print(this.console, "Connecté à l'adresse: " + this.data.addr);
			
			//Cr�ation du bufferReader, PrintStream pour la communication avec le serveur
			this.data.br= new BufferedReader(new InputStreamReader(this.data.sock.getInputStream()));
			this.data.ps = new PrintStream(this.data.sock.getOutputStream());
			
			//Réception du nouveau socket de communication pour se connecter au thread cr�e par le serveur	
			this.commandes.print(this.console, "r�ception du nouveau socket de communication");
			this.commandes.receive();
			this.data.sock.close();
			this.commandes.print(this.console, "Cr�ation du nouveau socket de communication");
			
			
			this.data.sock = new Socket(this.host.getText(), Integer.parseInt(this.data.reponse.split(" ")[1]));
			this.data.addr= this.data.sock.getInetAddress();
			
			//Recreation du BufferReader et du PrintStream
			this.data.br= new BufferedReader(new InputStreamReader(this.data.sock.getInputStream()));
			this.data.ps = new PrintStream(this.data.sock.getOutputStream());
			
			//R�ception des messages de bienvenue
			this.commandes.receive();
			
			//R�cup�ration de l'identifiant et du mot de passe
			String username = this.username.getText();
			String password = this.password.getText();
			
			//Envoie de la requ�te USER
			//Si le user n'est pas bon, on annule l'op�ration
			this.data.requete = "user " + username;
			this.requeteManager();
			
			if(this.data.reponse.charAt(0)=='2')
			{
				this.commandes.clearServerData();
				return;
			}

//			//Envoie de la requ�te PASS
			//Si le user n'est pas bon, on annule l'op�ration
			this.data.requete = "pass " + password;
			this.requeteManager();
			if(this.data.reponse.charAt(0)=='2')
			{
				this.commandes.clearServerData();
				return;
			}
			
			//Envoie de la requ�te PWD
			//Si le user n'est pas bon, on annule l'op�ration
			this.data.requete = "pwd";
			this.requeteManager();
			if(this.data.reponse.charAt(0)=='2')
			{
				this.commandes.clearServerData();
				return;
			}
			
			//Affichage du chemin du r�pertoire courant du client 
			this.localPath.setText(this.data.clientPATH);
			
			//Affichage des commandes disponibles pour l'utilisateur
			this.localCommandPanel.getChildren().clear();
			this.distantCommandPanel.getChildren().clear();

			this.commandes.print(this.localCommandPanel, this.localCommand);
			this.commandes.print(this.distantCommandPanel, this.distantCommand);

			//Affichage du r�pertoire courant c�t� client
			this.commandes.lsCli();
		}
		//Si erreur critique lors de la connexion
		catch(Exception e)
		{
			
			this.commandes.print(this.console, "Erreur lors de la connexion au serveur, veuillez v�rifier les informations de connection entr�es\n");
			//On efface les informations du serveur de l'applciation
			this.commandes.clearServerData();
			//On supprime l'objet data
			this.data=null;
			return;
		}
		
		
   }
		
	

}
