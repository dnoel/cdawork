package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;

import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class Commandes
{
	  
	private Controlleur c;
	
	public Commandes(Controlleur c)
	{
		this.c = c;
	}
	//Efface les donn�es de tous les �l�ments de l'IHM concernant le serveur
    public void clearServerData()
    {
    	this.c.distantPath.clear();
    	this.c.distantDirectory.getChildren().clear();
    }
    
    //Fonction ajoutant une String s dans un TextFlow t de l'IHM
    public void print(TextFlow t, String s)
    {
    	t.getChildren().add(new Text(s + "\n"));
    }
    
    
    //Fonction Envoyant la requ�te contenue dans this.c.data.requete et appelant la fonction receive
    public  void sendReceive() throws Exception
	{
		//Envoie de la requ�te au serveur
		try
		{
			this.c.data.ps.println(this.c.data.requete);
			
			//Réception de la r�ponse du serveur
			receive();
		}
		catch(Exception e)
		{
			throw e;
		}
	
		
	}
    
    //Fonction recevant les r�ponses du serveur et les affichants dans la this.c.console de l'ihm tant que l'id de la r�ponse n'est pas un id d'arr�t de r�ception
    public void receive() throws IOException
	{
		while(true)
		{	
			try {
				this.c.data.reponse = this.c.data.br.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				throw e;
			}
			print(this.c.console, this.c.data.reponse.substring(2));
			if(!(this.c.data.reponse.charAt(0)=='1'))
			{			
				break;
			}
		}
	}

	//Commande pour r�cup�rer le chemin du r�pertoire courant du serveur
    public void pwd() throws Exception
	{
		//Demande d urépertoir courant du serveur
		sendReceive();
		this.c.data.serverPATH = this.c.data.reponse.split(" ")[1];
		this.c.distantPath.setText(this.c.data.serverPATH);
	}
	//Commande pour se d�placer dans les fichiers c�t� serveur
	public void cd() throws Exception
	{
		sendReceive();
		if(!this.c.data.reponse.substring(0, 1).contentEquals("2"))
		{
			this.c.data.serverPATH = this.c.data.reponse.split(" ")[1];
			this.c.distantPath.setText(this.c.data.serverPATH);
		}
	}
	//Commande pour se d�placer dans les fichiers c�t� client
	public void cdCli() throws Exception
	{
		
		File f = new File(this.c.data.clientPATH + "/" + this.c.data.requete.split(" ")[1]);
		if(!f.exists()) 
		{
			print(this.c.console, "Le répertoir c�t� client n'existe pas");
			return;
		}
		
		if(!f.isDirectory())
		{
			print(this.c.console, this.c.data.requete.split(" ")[1] + "n'est pas un r�pertoire");
			return;
		}
		
		String filePath = "";
		try 
		{
			filePath = f.getCanonicalPath().toString();
			
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		this.c.data.clientPATH = filePath;
		
		this.c.localPath.setText(this.c.data.clientPATH);
	}
	
	//Commande pour afficher le contenu du r�pertoire courant du serveur (nomFichier, typeFichier, tailleFichier, DateDerniereModification=
	public void ls() throws Exception
	{
		this.c.distantDirectory.getChildren().clear();
		sendReceive();
		if(this.c.data.reponse.length()>0)
		{
			this.c.data.serverDirectory = this.c.data.reponse.split("!");
			this.c.data.serverDirectory[0] = this.c.data.serverDirectory[0].substring(1);
			
			print(this.c.distantDirectory, "..");
			for (String fileData : this.c.data.serverDirectory) 
			{
				print(this.c.distantDirectory, fileData);
			}
		}
			
	}
	
	//Fonction utilis�e par lsCli() pour r�cup�rer les diff�rentes informatiosn d'un fichier
	public static String getInfo(File f)
	{
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String result = "";
		result += f.getName();
		
		if(f.isDirectory())
		{
			result += " 1 0";			
		}
		else
		{
			result+= " 0 " + f.length();			
		}
		result += " " + sdf.format(f.lastModified());
		return result;	
	}
	
	//Commande pour afficher le contenu du r�pertoire courant du serveur (nomFichier, typeFichier, tailleFichier, DateDerniereModification)
	public void lsCli() throws Exception
	{
		this.c.localDirectory.getChildren().clear();
		File file = new File(this.c.data.clientPATH);
		String result = new String("");
		File[] content = file.listFiles();
		
		if(content.length > 0)
		{
			result += getInfo(content[0]);
			for(int i=1; i<content.length; i++)
			{
				result+= "!" + getInfo(content[i]);
			}
		}
		
		this.c.data.clientDirectory = result.split("!");
		
		print(this.c.localDirectory, "..");
		for (String fileData : this.c.data.clientDirectory) 
		{
			print(this.c.localDirectory, fileData);
		}
				
	}
	
	
	//Commande pour r�cup�rer un fichier depuis le r�pertoire courant du serveur
	public void get() throws NumberFormatException, UnknownHostException, IOException
	{
		//R�cup�ration du nom du fichier 
		String fileName = this.c.data.requete.split(" ")[1];
		
		File f = new File(this.c.data.clientPATH + "/" + fileName);
		//Si le fichier existe d�j� dans le r�pertoire utilisateur
		if(f.exists())
		{
			print(this.c.console, "Un fichier avec ce nom existe d�j� dans ce r�pertoire");
			return;
		}
		//Envoie de la requ�te au serveur
//		print("Envoie de la demande de get du fichier : " + fileName);
		this.c.data.ps.println(this.c.data.requete);
		
		//R�ception du port de la socket pour le partage de fichiers
		
		try 
		{
			this.c.data.reponse = this.c.data.br.readLine();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			this.c.data.fileSharing.close();
		}
		if(this.c.data.reponse.charAt(0)=='2')
		{
//			print(reponse);
			return;
		}
		
		print(this.c.console, "port du socket fileDharing : " + this.c.data.reponse.split(" ")[1]);
		
		this.c.data.fileSharing = new Socket("localhost", Integer.parseInt(this.c.data.reponse.split(" ")[1]));
		this.c.data.fileSharing.getInetAddress();
		//Création du fichier 
		byte[] buffer = new byte[this.c.data.packetSize]; 
		
		
		InputStream in = this.c.data.fileSharing.getInputStream();
		FileOutputStream out = new FileOutputStream(f);
		
		
		int count=1;
		while ((count = in.read(buffer)) >0)
		{
		  out.write(buffer, 0, count);
		}
			
			print(this.c.console, "Ecriture du contenu du fichier termin�e");
			in.close();
			out.close();
			this.c.data.fileSharing.close();
	}
	
	//Commande pour envoyer un fichier au serveur depusi le r�pertoire courant du client

	public void store() throws NumberFormatException, UnknownHostException, IOException
	{
		String filePath = this.c.data.clientPATH + "/" + this.c.data.requete.split(" ")[1];
		File f = new File(filePath);
		if(!f.exists())
		{
			print(this.c.console, "Le fichier " + filePath + "n'existe pas");
			return;
		}
		
		if(f.isDirectory())
		{
			print(this.c.console, "Le fichier " + filePath + " est un r�pertoire -> ERREUR");
			return;
		}
		
		this.c.data.ps.println(this.c.data.requete);
		
		this.c.data.reponse = this.c.data.br.readLine();

		if(this.c.data.reponse.charAt(0)=='2')
		{
			print(this.c.console, this.c.data.reponse);
			return;
		}
		
		this.c.data.fileSharing = new Socket("localhost", Integer.parseInt(this.c.data.reponse.split(" ")[1]));
		this.c.data.fileSharing.getInetAddress();
		byte[] content = new byte[(int) f.length()]; 

		FileInputStream fis = new FileInputStream(f);
		fis.read(content); //read file into bytes[]
		fis.close();
		java.io.OutputStream socketOutputStream = this.c.data.fileSharing.getOutputStream();
		socketOutputStream.write(content);
		socketOutputStream.close();
		this.c.data.fileSharing.close();
	}
	
	
	//Commande pour cr�er un dossier c�t� client
	public void mkdirCli() throws Exception
	{
		File f = new File(this.c.data.clientPATH + "/" + this.c.data.requete.split(" ")[1]);
	      //Creating the directory	
	      if(f.exists())
	      {
	          print(this.c.console, "Le r�pertoire existe d�j�");
	          return;
	      }
	      
	      if(f.mkdir())
	      {
	          print(this.c.console, "Directory created successfully");

	          
	      }else{
			  print(this.c.console, "Sorry couldn�t create specified directory");
	      }
	}
	
	//Commande pour cr�er un fichier c�t� client
	public void touchCli() throws Exception
	{
		File f = new File(this.c.data.clientPATH + "/" + this.c.data.requete.split(" ")[1]);
	      //Creating the directory	
	      if(f.exists())
	      {
	          print(this.c.console, "Le fichier existe d�j�");
	          return;
	      }
	      
	      f.createNewFile();
	      print(this.c.console, "fichier " + f.getName() + "cr�e avec succ�s");
	     
	}
	
	//Commande pour renommer un fichier c�t� client
	public void renameCli()
	{
		if(this.c.data.requete.split(" ").length !=3)
		{
			print(this.c.console, "Nombre d'argument �rron� pour la commande rename");
			return;
		}
		
		File f = new File(this.c.data.clientPATH + "/" + this.c.data.requete.split(" ")[1]);
	      //Creating the directory	
	      if(!f.exists())
	      {
	          print(this.c.console, "Le fichier n'existe pas");
	          return;
	      }
	      
	      File f2 = new File(this.c.data.clientPATH + "/" + this.c.data.requete.split(" ")[2]);
	      
	      if(f.renameTo(f2)) {
	    	  print (this.c.console, "fichier renomm� en " + f.getName());
	       } else {
	          print(this.c.console, "Rename failed");
	       } 
	     
	}
	
	//Fonction r�cursive pour la suppression d'un dossier
	boolean deleteDirectory(File directory) 
	{
		//On r�cup�re l'ensemble des fichiers contenus dans le r�pertoire 
	    File[] content = directory.listFiles();
	    
	    //Si le dossier contient d'autres fichiers
	    if (content != null) 
	    {	
	        for (File f : content) {
	        	//On fait un appel r�cursif � la fonction
	            deleteDirectory(f);
	        }
	    }
	    //Sinon on renvoei le boolean de la suppression de la suppression
	    return directory.delete();
	}
	
	//Commande pour supprimer un fichier c�t� client
	public void rmCli()
	{
		File f = new File(this.c.data.clientPATH+ "/" + this.c.data.requete.split(" ")[1]);
	      //Creating the directory	
	      if(!f.exists())
	      {
	          print(this.c.console, "Le fichier � supprimer n'existe pas");
	          return;
	      }
	      
	      //Si c'est un r�pertoire
	      if(f.isDirectory())
	      {
	    	  //Si la fonction r�cursive de suppression de r�pertoire renvoie un succ�s
	    	  if(deleteDirectory(f)) 
		      {
		    	  print(this.c.console, "r�pertoire " + f.getName() + " supprim� avec succ�s");
		      } 
	    	  //Sinon
		      else 
		      {
		    	  print(this.c.console,"Probl�me de suppression");
		      } 
	    	  
	    	  return;
	      }
	      
	      
	      if(f.delete()) {
	    	  print(this.c.console, "fichier " + f.getName() + " supprim� avec succ�s");
	       } else {
	          print(this.c.console, "Probl�me de suppression");
	       } 
	     
	}
	
	//Commande pour se d�connecter du serveur
	public void bye() throws Exception
	{
		try
		{
			this.c.data.ps.println(this.c.data.requete);	
			this.c.data.ps.close();
			this.c.data.br.close();
			this.c.data.sock.close();
		}
		catch(Exception e)
		{
			throw e;
		}
	}
}
