package application;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;

public class Data 
{

	public String serverPATH;
	public String clientPATH;
	public Socket sock;
	public Socket fileSharing;
	public int packetSize;
	public InetAddress addr;
	public PrintStream ps;
	public BufferedReader br;
	public String reponse;
	public String requete;
	public String[] serverDirectory;
	public String[] clientDirectory;
	
	public Data()
	{
		this.serverPATH = new String("");
		this.clientPATH = "C:\\Damien\\workspace\\filezilla\\clientPATH";
		this.sock = null;
		this.fileSharing = null;
		this.packetSize = 100;
		this.addr = null;
		this.ps = null;
		this.br = null;
		this.reponse = null;
		this.requete = null;
		this.serverDirectory = null;
		this.clientDirectory = null;
		
	}
}
