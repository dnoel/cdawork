import java.io.File;
import java.io.PrintStream;
import java.net.ServerSocket;

public abstract class Commande 
{
	protected static ServerSocket fileSharing;
	protected static final String originalPath = new File("").getAbsolutePath();
	protected static String currentPath = new String(originalPath.toString());
	protected PrintStream ps;
	protected String commandeNom = "";
	protected String [] commandeArgs ;
	
	public Commande(PrintStream ps, String commandeStr) 
	{
		this.ps = ps;
		String [] args = commandeStr.split(" ");
		commandeNom = args[0];
		commandeArgs = new String[args.length-1];
		
		for(int i=0; i<commandeArgs.length; i++) 
		{
			commandeArgs[i] = args[i+1];
		}
	}
	
	public abstract void execute();

}
