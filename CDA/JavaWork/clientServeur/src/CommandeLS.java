import java.io.File;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
public class CommandeLS extends Commande 
{
	
	public CommandeLS(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	public String getInfo(File f)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String result = "";
		result += f.getName();
		
		if(f.isDirectory())
		{
			result += " 1 0";			
		}
		else
		{
			result+= " 0 " + f.length();			
		}
		result += " " + sdf.format(f.lastModified());
		return result;	
	}
	public void execute() 
	{
		//Renvoie le nom, le type de fichier (fichier(0) ou repertoire(1)), la taille du fichier (0 si répertoire), et la date de dernière modification de chaque fichier
		File file = new File(currentPath);
		String result = new String("");
		File[] content = file.listFiles();
		
		result += getInfo(content[0]);
		for(int i=1; i<content.length; i++)
		{
			result+= "!" + getInfo(content[i]);
		}

		System.out.println("Renvoie de la liste des fichiers");
		ps.println("0" + result);
			
	}

}
