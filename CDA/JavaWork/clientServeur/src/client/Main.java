package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
public class Main 
{
	private static String serverPATH;
	private static String clientPATH = "H:\\WorkspaceJava\\clientPATH";;
	private static Socket sock;
	private static Socket fileSharing;
	private static InetAddress addr;
	private static Scanner scanner;
	private static PrintStream ps;
	private static BufferedReader br;
	private static String reponse;
	private static String requete;
	private static String[] directory;

	public static void sendReceive()
	{
		//Envoie de la requ�te au serveur
		ps.println(requete);
		
		//Réception de la r�ponse du serveur
		receive();
		
	}
	public static void receive()
	{
		while(true)
		{	
			try {
				reponse = br.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!(reponse.charAt(0)=='1'))
			{
				System.out.println(reponse);
				break;
			}
		}
	}
	
	public static void requeteManager()
	{
		System.out.println("Envoie de la commande : " + requete);
		String s = requete.split(" ")[0];
		if(s.equals("user")) sendReceive();
		if(s.equals("pass")) sendReceive();
		if(s.equals("pwd")) pwd();
		if(s.equals("cd")) cd();
		if(s.equals("ls")) ls();
		if(s.equals("sock")) sock();
		if(s.equals("get")) get();
		if(s.equals("bye")) sendReceive();
		
	}
	public static void pwd()
	{
		//Demande d urépertoir courant du serveur
		sendReceive();
		serverPATH = reponse.split(" ")[1];
	}
	
	public static void cd()
	{
		sendReceive();
		if(!reponse.substring(0, 1).contentEquals("2"))
		{
			serverPATH = reponse.split(" ")[1];
		}
	}
	
	public static void ls()
	{
		sendReceive();
		directory = reponse.split("!");
		directory[0] = directory[0].substring(1);
		System.out.println();
		 for (String fileData : directory) {
			 
	            System.out.println(fileData);
	        }
	}
	
	public static void sock()
	{
		sendReceive();
		try 
		{
			fileSharing = new Socket("localhost", Integer.parseInt(reponse.split(" ")[1]));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void get()
	{
		sendReceive();
		try 
		{
			fileSharing = new Socket("localhost", Integer.parseInt(reponse.split(" ")[1]));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String [] args) throws UnknownHostException, IOException
	{
		sock = new Socket("localhost", 2121);
		br= new BufferedReader(new InputStreamReader(sock.getInputStream()));
		ps = new PrintStream(sock.getOutputStream());
		scanner = new Scanner(System.in);
		addr= sock.getInetAddress();
			
		//Connexionau serveur
		System.out.println("Connecté à l'adresse: " + addr);
		//Réception de la réponse serveur
		receive();	
		
		//Envoie du username
		requete = "user personne";
		requeteManager();
		
		//Envoie du password
		requete = "pass abcd";
		requeteManager();
		
		requete = "pwd";
		requeteManager();
		
		requete = "sock";
		requeteManager();
		
		clientPATH = "H:/WorkspaceJava/clientPATH";
		
		System.out.println("Socket pour partager les fichiers sur le port : " + fileSharing.getPort());
		
		System.out.println("Répertoire sur le serveur: " + serverPATH);
		
		//Traitement de l'senmble des requ�tes faites par l'utilisateur
		
		while(true)
		{
			System.out.print("Veuillez rentrer une commande : ");
			requete = scanner.nextLine();
			if(requete.equals("bye")) break;
			requeteManager();
			System.out.println();
			System.out.println();
		}
		
		System.out.println("Je me déconnecte");			
		scanner.close();
		sock.close();
	}
	
}
