package exercice6;

import java.util.Set;

import java.util.HashSet;
import java.util.Collections;

/**
 * Représente un livre caractérisé par un titre, un auteur et une série
 * éventuellement vide de mots clés.
 */
public class Livre {

	// Attributs statiques -------------------------------------------------

	/**
	 * Ensemble de tous les livres existants.
	 */
	@SuppressWarnings("unused")
	private static final Set<Livre> LIVRES = new HashSet<>();

	// Attributs d'instances -----------------------------------------------

	/**
	 * Titre du livre (public et non modifiable).
	 */
	public final String TITRE;

	/**
	 * Référence de l'auteur (publique et non modifiable).
	 * <p>
	 * 
	 * La référence est par construction différente de null.
	 */
	public final Auteur AUTEUR;

	/**
	 * Ensemble éventuellement vide des mots clés du livre.
	 * <p>
	 * 
	 * L'ensemble ne peut pas être mis à jour à l'extérieur de la classe. Il est
	 * donc privé et un accesseur spécifique permet d'en récupérer une vue
	 * constante.
	 */
	private final Set<MotCle> MOTSCLES;

	// Constructeur --------------------------------------------------------

	/**
	 * Constructeur unique et privé.
	 * 
	 * @param titre  Titre du livre.
	 * @param auteur Auteur du livre.
	 */
	private Livre(String titre, Auteur auteur) {
		this.TITRE    = titre;
		this.AUTEUR   = auteur;
		this.MOTSCLES = new HashSet<>();
	}

	// Accesseurs et usines à objets ---------------------------------------

	/**
	 * Rend les mots clés accessibles en lecture seule.
	 * 
	 * @return vue non-modifiable de l'attribut MOTSCLES.
	 */
	public Set<MotCle> getMotsCles() 
	{
		return Collections.unmodifiableSet(this.MOTSCLES);
	}

	/**
	 * Rajoute un mot clé au livre si possible.
	 * 
	 * @param mot Mot clé à rajouter.
	 * @return true si le mot correspond à un mot clé qui n'était pas déjà associé
	 *         au livre avant (false sinon).
	 */
	public boolean addMotCle(String mot) {
		MotCle m = MotCle.getOrCreate(mot);
		if (m == null) return false; // le mot n'est pas valide
		return this.MOTSCLES.add(m); // le résultat de add indique si le mot est nouveau
	}

	/**
	 * Si possible, renvoie une instance de Livre correspondant à un titre de livre,
	 * à un nom d'auteur et à un prénom d'auteur.
	 * <p>
	 * 
	 * Le résultat est null si une de ces trois informations est null. Le résultat
	 * est également null si le titre ou le nom de l'auteur correspond à la chaîne
	 * vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon :
	 * <ul>
	 * <li>soit la création n'est pas demandée et dans ce cas null est renvoyé en
	 * résultat</li>
	 * <li>soit la création est demandée et dans ce cas une nouvelle instance de
	 * Livre est créée, rajoutée à la liste des livres existants et renvoyée en
	 * résultat</li>
	 * </ul>
	 * 
	 * @param titre        Titre du livre à créer ou à récupérer
	 * @param nomAuteur    Nom de l'auteur du livre à créer ou à récupérer
	 * @param prenomAuteur Prénom de l'auteur du livre à créer ou à récupérer
	 * @param getOnly      indique si on ne veut que récupérer une instance
	 *                     existante (pas de création dans ce cas)
	 * @return selon les cas : null, ou une instance existante, ou une nouvelle
	 *         instance
	 */
	private static Livre getOrCreate(String titre, String nomAuteur, String prenomAuteur, boolean getOnly) 
	{
		if (titre ==null || titre=="") return null;
		
		Auteur a = Auteur.getOrCreate(nomAuteur, prenomAuteur);
		if(a==null) return null;
		
		Livre toCompare = new Livre(titre, a);
		
		
		for(Livre l : LIVRES) 
		{
			if(l.equals(toCompare)) 
			{
				return l;
			}
		}
		
		if(!getOnly)
		{
			LIVRES.add(toCompare);
			return toCompare;
		}
		return null;

	}

	/**
	 * Si possible, renvoie une instance de Livre correspondant à un titre de livre,
	 * à un nom d'auteur et à un prénom d'auteur.
	 * <p>
	 * 
	 * Le résultat est null si une de ces trois informations est null. Le résultat
	 * est également null si le titre ou le nom de l'auteur correspond à la chaîne
	 * vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon, une nouvelle
	 * instance de Livre est créée, rajoutée à la liste des livres existants et
	 * renvoyée en résultat.
	 * 
	 * @param titre        Titre du livre à créer ou à récupérer
	 * @param nomAuteur    Nom de l'auteur du livre à créer ou à récupérer
	 * @param prenomAuteur Prénom de l'auteur du livre à créer ou à récupérer
	 * @return selon les cas : null, ou une instance existante, ou une nouvelle
	 *         instance
	 */
	public static Livre getOrCreate(String titre, String nomAuteur, String prenomAuteur) {
		return getOrCreate(titre, nomAuteur, prenomAuteur, false);
	}

	/**
	 * Si possible, renvoie une instance de Livre correspondant à un titre de livre,
	 * à un nom d'auteur et à un prénom d'auteur.
	 * <p>
	 * 
	 * Le résultat est null si une de ces trois informations est null. Le résultat
	 * est également null si le titre ou le nom de l'auteur correspond à la chaîne
	 * vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon, null est renvoyé en
	 * résultat.		if(motClePartielOuComplet==null)
	 * 
	 * @param titre        Titre du livre à récupérer
	 * @param nomAuteur    Nom de l'auteur du livre à récupérer
	 * @param prenomAuteur Prénom de l'auteur du livre à récupérer
	 * @return selon les cas : null, ou une instance existante
	 */
	public static Livre get(String titre, String nomAuteur, String prenomAuteur) {
		return getOrCreate(titre, nomAuteur, prenomAuteur, true);
	}

	// toString et equals --------------------------------------------------

	@Override
	public String toString() 
	{
		String res = "\"";
		
		res+= this.TITRE + "\" ";
		
		res+= "(" + this.AUTEUR.toString() + ")";
		return res;
		
		
	}

	@Override
	public boolean equals(Object obj) 
	{
		
		if(obj.toString().compareToIgnoreCase(this.toString())==0)

		{
			return true;
		}
		return false;
		
	}

	// Requêtes ------------------------------------------------------------

	/**
	 * Sélection de livres à partir d'un titre.
	 * 
	 * @param titrePartielOuComplet titre ou partie du titre à rechercher
	 * @return ensemble des livres dont le titre correspond au paramètre fourni,
	 *         indépendamment de la casse
	 */
	public static Set<Livre> select(String titrePartielOuComplet) {
		
		if(titrePartielOuComplet==null) return null;
		Set<Livre> res  = new HashSet<Livre>();
		for(Livre l: LIVRES)
		{
			String mot = l.TITRE.toUpperCase();
			if(mot.contains(titrePartielOuComplet.toUpperCase()))res.add(l);
		}
		return res;	
	}

	/**
	 * Sélection de livres à partir d'un auteur.
	 * 
	 * @param nomDAuteurPartielOuComplet    nom ou partie du nom de l'auteur à
	 *                                      rechercher
	 * @param prenomDAuteurPartielOuComplet prénom ou partie du prénom de l'auteur à
	 *                                      rechercher
	 * @return ensemble des livres dont l'auteur correspond aux paramètres fournis,
	 *         indépendamment de la casse
	 */
	public static Set<Livre> selectParAuteur(String nomDAuteurPartielOuComplet, String prenomDAuteurPartielOuComplet) {
		
		if(nomDAuteurPartielOuComplet==null ||!nomDAuteurPartielOuComplet.contains("") || prenomDAuteurPartielOuComplet==null) return null;
		Set<Auteur> auteurs = Auteur.select(nomDAuteurPartielOuComplet, prenomDAuteurPartielOuComplet);
		Set<Livre> res  = new HashSet<Livre>();
		for(Livre l: LIVRES)
		{

			if(auteurs.contains(l.AUTEUR))
			{
				res.add(l);
			}
		}
		return res;
		
	}

	/**
	 * Sélection de livres à partir d'un mot clé.
	 * 
	 * @param motClePartielOuComplet mot clé ou partie du mot clé à rechercher
	 * @return ensemble des livres dont au moins un mot clé correspond au paramètre
	 *         fourni, indépendamment de la casse
	 */
	public static Set<Livre> selectParMotCle(String motClePartielOuComplet) 
	{
		if(motClePartielOuComplet==null ||!motClePartielOuComplet.contains("")) return null;
		
		Set<Livre> res  = new HashSet<Livre>();
		Set<MotCle> motscles = MotCle.select(motClePartielOuComplet);
		for(Livre l: LIVRES)
		{
			if(!Collections.disjoint(l.MOTSCLES, motscles)){
				res.add(l);
			}
		}
		return res;
		
	}
}
