package exercice8;

import java.util.Set;
import java.util.HashMap;
import java.util.Map;

/**
 * Représente un auteur caractérisé par un nom et un prénom.
 */
public class Auteur {

	// Attributs statiques -------------------------------------------------

	/**
	 * Requêtes pré-câblées Auteur&rarr;{Livre}.
	 */
	static final Map<Auteur, Set<Livre>> AUTEURLIVRES = new HashMap<>();

	// Attributs d'instances -----------------------------------------------

	/**
	 * Nom de l'auteur (public et non modifiable).
	 */
	public final String NOM;

	/**
	 * Prénom de l'auteur (public et non modifiable).
	 */
	public final String PRENOM;

	// Constructeur --------------------------------------------------------

	/**
	 * Constructeur unique et privé.
	 * 
	 * @param nom    Nom de l'auteur.
	 * @param prenom Prénom de l'auteur.
	 */
	private Auteur(String nom, String prenom) {
		this.NOM    = nom;
		this.PRENOM = prenom;
	}

	// Accesseurs et usines à objets ---------------------------------------

	/**
	 * Si possible, renvoie une instance de Auteur correspondant à un nom et à un
	 * prénom.
	 * <p>
	 * 
	 * Le résultat est null si une de ces deux informations est null. Le résultat
	 * est également null si le nom de l'auteur correspond à la chaîne vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon :
	 * <ul>
	 * <li>soit la création n'est pas demandée et dans ce cas null est renvoyé en
	 * résultat</li>
	 * <li>soit la création est demandée et dans ce cas une nouvelle instance de
	 * Auteur est créée, rajoutée à la liste des auteurs existants et renvoyée en
	 * résultat</li>
	 * </ul>
	 * 
	 * @param nom     Nom de l'auteur à créer ou à récupérer
	 * @param prenom  Prénom de l'auteur à créer ou à récupérer
	 * @param getOnly indique si on ne veut que récupérer une instance existante
	 *                (pas de création dans ce cas)
	 * @return selon les cas : null, ou une instance existante, ou une nouvelle
	 *         instance
	 */
	private static Auteur getOrCreate(String nom, String prenom, boolean getOnly) {

		// TODO à compléter...

		// ATTENTION ! Résultat factice,
		// utile uniquement pour les tests de toString et equals (exercice 1) :
		return new Auteur(nom, prenom); // <- TODO résultat à modifier (exercice 2)

	}

	/**
	 * Si possible, renvoie une instance de Auteur correspondant à un nom et à un
	 * prénom.
	 * <p>
	 * 
	 * Le résultat est null si une de ces deux informations est null. Le résultat
	 * est également null si le nom de l'auteur correspond à la chaîne vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon, une nouvelle
	 * instance de Auteur est créée, rajoutée à la liste des auteurs existants et
	 * renvoyée en résultat.
	 * 
	 * @param nom    Nom de l'auteur à créer ou à récupérer
	 * @param prenom Prénom de l'auteur à créer ou à récupérer
	 * @return selon les cas : null, ou une instance existante, ou une nouvelle
	 *         instance
	 */
	public static Auteur getOrCreate(String nom, String prenom) {
		return getOrCreate(nom, prenom, false);
	}

	/**
	 * Si possible, renvoie une instance de Auteur correspondant à un nom et à un
	 * prénom.
	 * <p>
	 * 
	 * Le résultat est null si une de ces deux informations est null. Le résultat
	 * est également null si le nom de l'auteur correspond à la chaîne vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon, null est renvoyé en
	 * résultat.
	 * 
	 * @param nom    Nom de l'auteur à récupérer
	 * @param prenom Prénom de l'auteur à récupérer
	 * @return selon les cas : null, ou une instance existante
	 */
	public static Auteur get(String nom, String prenom) {
		return getOrCreate(nom, prenom, true);
	}

	// toString et equals --------------------------------------------------

	@Override
	public String toString() {
		
		// TODO à compléter...
		
		return null; // <- TODO résultat à adapter
		
	}

	@Override
	public boolean equals(Object obj) {
		
		// TODO à compléter...
		
		return false; // <- TODO résultat à adapter
		
	}

	// Requêtes ------------------------------------------------------------

	/**
	 * Sélection d'auteurs à partir d'un nom et d'un prénom.
	 * 
	 * @param nomDAuteurPartielOuComplet    nom ou partie du nom à rechercher
	 * @param prenomDAuteurPartielOuComplet prénom ou partie du prénom de l'auteur à
	 *                                      rechercher
	 * @return ensemble des auteurs dont le nom et le prénom correspondent aux
	 *         paramètres fournis, indépendamment de la casse
	 */
	public static Set<Auteur> select(String nomDAuteurPartielOuComplet, String prenomDAuteurPartielOuComplet) {
		
		// TODO à compléter...
		
		return null; // <- TODO résultat à adapter
		
	}

	/**
	 * Sélection d'auteurs à partir d'un titre de livre.
	 * 
	 * @param titrePartielOuComplet titre ou partie du titre à rechercher
	 * @return ensemble des auteurs de livres dont le titre correspond au paramètre
	 *         fourni, indépendamment de la casse
	 */
	public static Set<Auteur> selectParLivre(String titrePartielOuComplet) {
		
		// TODO à compléter...
		
		return null; // <- TODO résultat à adapter
		
	}
}
