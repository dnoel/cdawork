package exercice2;

class Test {

	public static void main(String[] args) {
		Livre l;
		l = Livre.getOrCreate("Les Misérables", "Hugo", "Victor");
		l.addMotCle("roman");
		l.addMotCle("social");
		l = Livre.getOrCreate("Le Roman bourgeois", "Furetière", "Antoine");
		l.addMotCle("ROMAN");
		l.addMotCle("comédie");
		l = Livre.getOrCreate("Marie Tudor", "HUGO", "victor");
		l.addMotCle("théatre");
		l.addMotCle("historique");
		l = Livre.getOrCreate("Germinal", "Zola", "Émile");
		l = Livre.getOrCreate("GERMINAL", "zola", "émile");
		l.addMotCle("roman");
		l.addMotCle("social");
		l = Livre.getOrCreate("Le Bourgeois gentilhomme", "Molière", "");
		l.addMotCle("théatre");
		l.addMotCle("comédie");

		System.out.println("----- Livre -----");
		System.out.println(Livre.get("germinal", "zola", "émile"));
		System.out.println(Livre.get("Les Misérables", "Hugo", "Victor"));
		System.out.println(Livre.get("Le Bourgeois gentilhomme", "Molière", ""));
		
		System.out.println("----- Auteur -----");
		System.out.println(Auteur.get("zola", "émile"));
		System.out.println(Auteur.get("hugo", "victor"));
		
		System.out.println("----- Mot clé -----");
		System.out.println(MotCle.get("Roman"));
		System.out.println(MotCle.get("comédie"));
	}

}

/*

----- Livre -----
"Germinal" (ZOLA Émile)
"Les Misérables" (HUGO Victor)
"Le Bourgeois gentilhomme" (MOLIÈRE)
----- Auteur -----
ZOLA Émile
HUGO Victor
----- Mot clé -----
ROMAN
COMÉDIE

*/
