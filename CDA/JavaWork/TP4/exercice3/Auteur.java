package exercice3;

import java.util.HashSet;
import java.util.Set;

/**
 * Représente un auteur caractérisé par un nom et un prénom.
 */
public class Auteur {

	// Attributs statiques -------------------------------------------------

	/**
	 * Ensemble de tous les auteurs existants.
	 */
	@SuppressWarnings("unused")
	private static final Set<Auteur> AUTEURS = new HashSet<>();

	// Attributs d'instances -----------------------------------------------

	/**
	 * Nom de l'auteur (public et non modifiable).
	 */
	public final String NOM;

	/**
	 * Prénom de l'auteur (public et non modifiable).
	 */
	public final String PRENOM;

	// Constructeur --------------------------------------------------------

	/**
	 * Constructeur unique et privé.
	 * 
	 * @param nom    Nom de l'auteur.
	 * @param prenom Prénom de l'auteur.
	 */
	private Auteur(String nom, String prenom) {
		this.NOM    = nom;
		this.PRENOM = prenom;
	}

	// Accesseurs et usines à objets ---------------------------------------

	/**
	 * Si possible, renvoie une instance de Auteur correspondant à un nom et à un
	 * prénom.
	 * <p>
	 * 
	 * Le résultat est null si une de ces deux informations est null. Le résultat
	 * est également null si le nom de l'auteur correspond à la chaîne vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon :
	 * <ul>
	 * <li>soit la création n'est pas demandée et dans ce cas null est renvoyé en
	 * résultat</li>
	 * <li>soit la création est demandée et dans ce cas une nouvelle instance de
	 * Auteur est créée, rajoutée à la liste des auteurs existants et renvoyée en
	 * résultat</li>
	 * </ul>
	 * 
	 * @param nom     Nom de l'auteur à créer ou à récupérer
	 * @param prenom  Prénom de l'auteur à créer ou à récupérer
	 * @param getOnly indique si on ne veut que récupérer une instance existante
	 *                (pas de création dans ce cas)
	 * @return selon les cas : null, ou une instance existante, ou une nouvelle
	 *         instance
	 */
	private static Auteur getOrCreate(String nom, String prenom, boolean getOnly) {

		if (nom ==null || nom=="" || prenom==null) return null;
				
			Auteur toCompare = new Auteur(nom, prenom);
			
			for(Auteur a : AUTEURS) if(a.equals(toCompare)) return a;
			
			if(!getOnly)
			{
				AUTEURS.add(toCompare);
				return toCompare;
			}
			return null;
	}

	/**
	 * Si possible, renvoie une instance de Auteur correspondant à un nom et à un
	 * prénom.
	 * <p>
	 * 
	 * Le résultat est null si une de ces deux informations est null. Le résultat
	 * est également null si le nom de l'auteur correspond à la chaîne vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon, une nouvelle
	 * instance de Auteur est créée, rajoutée à la liste des auteurs existants et
	 * renvoyée en résultat.
	 * 
	 * @param nom    Nom de l'auteur à créer ou à récupérer
	 * @param prenom Prénom de l'auteur à créer ou à récupérer
	 * @return selon les cas : null, ou une instance existante, ou une nouvelle
	 *         instance
	 */
	public static Auteur getOrCreate(String nom, String prenom) {
		return getOrCreate(nom, prenom, false);
	}

	/**
	 * Si possible, renvoie une instance de Auteur correspondant à un nom et à un
	 * prénom.
	 * <p>
	 * 
	 * Le résultat est null si une de ces deux informations est null. Le résultat
	 * est également null si le nom de l'auteur correspond à la chaîne vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon, null est renvoyé en
	 * résultat.
	 * 
	 * @param nom    Nom de l'auteur à récupérer
	 * @param prenom Prénom de l'auteur à récupérer
	 * @return selon les cas : null, ou une instance existante
	 */
	public static Auteur get(String nom, String prenom) {
		return getOrCreate(nom, prenom, true);
	}

	// toString et equals --------------------------------------------------

	@Override
	public String toString() 
	{
		
		String res = this.NOM.toUpperCase();
		
		if(this.PRENOM.compareTo("")==0) return res;
		
		res = res + " " + String.valueOf(this.PRENOM.charAt(0)).toUpperCase() + this.PRENOM.substring(1).toLowerCase();
		return res;
		
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(obj.toString().compareTo(this.toString())==0)
		{
			return true;
		}
		return false;
		
	}

	// Requêtes ------------------------------------------------------------

	/**
	 * Sélection d'auteurs à partir d'un nom et d'un prénom.
	 * 
	 * @param nomDAuteurPartielOuComplet    nom ou partie du nom à rechercher
	 * @param prenomDAuteurPartielOuComplet prénom ou partie du prénom de l'auteur à
	 *                                      rechercher
	 * @return ensemble des auteurs dont le nom et le prénom correspondent aux
	 *         paramètres fournis, indépendamment de la casse
	 */
	public static Set<Auteur> select(String nomDAuteurPartielOuComplet, String prenomDAuteurPartielOuComplet) {
		if(nomDAuteurPartielOuComplet==null || prenomDAuteurPartielOuComplet==null) return null;
		Set<Auteur> setAuteur  = new HashSet<Auteur>();
		for(Auteur a: AUTEURS)
		{
			String nom = a.NOM.toLowerCase();
			String prenom =a.PRENOM.toLowerCase();
			if(nom.contains(nomDAuteurPartielOuComplet.toLowerCase()) && prenom.contains(prenomDAuteurPartielOuComplet.toLowerCase()))
			{
				setAuteur.add(a);
			}
		}
		return setAuteur;
		
	}
}
