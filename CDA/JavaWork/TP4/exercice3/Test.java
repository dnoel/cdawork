package exercice3;

class Test {

	public static void main(String[] args) {
		Livre l;
		l = Livre.getOrCreate("Les Misérables", "Hugo", "Victor");
		l.addMotCle("roman");
		l.addMotCle("social");
		l = Livre.getOrCreate("Le Roman bourgeois", "Furetière", "Antoine");
		l.addMotCle("ROMAN");
		l.addMotCle("comédie");
		l = Livre.getOrCreate("Marie Tudor", "HUGO", "victor");
		l.addMotCle("théatre");
		l.addMotCle("historique");
		l = Livre.getOrCreate("Germinal", "Zola", "Émile");
		l = Livre.getOrCreate("GERMINAL", "zola", "émile");
		l.addMotCle("roman");
		l.addMotCle("social");
		l = Livre.getOrCreate("Le Bourgeois gentilhomme", "Molière", "");
		l.addMotCle("théatre");
		l.addMotCle("comédie");

		System.out.println("----- Livres -----");
		System.out.println("Tous les livres : " + Livre.select(""));
		System.out.println("Livres contenant \"bourgeois\" : " + Livre.select("bourgeois"));

		System.out.println("\n----- Auteurs -----");
		System.out.println("Tous les auteurs : " + Auteur.select("", ""));
		System.out.println("Auteurs dont le nom contient un \"o\" : " + Auteur.select("o", ""));
		System.out.println("Auteurs dont le prénom contient un \"o\" : " + Auteur.select("", "o"));

		System.out.println("\n----- Mots clés -----");
		System.out.println("Tous les mots clés : " + MotCle.select(""));
		System.out.println("Mots clé contenant \"om\": " + MotCle.select("om"));
	}

}

/*

----- Livres -----
Tous les livres : ["Germinal" (ZOLA Émile), "Marie Tudor" (HUGO Victor), "Le Bourgeois gentilhomme" (MOLIÈRE), "Les Misérables" (HUGO Victor), "Le Roman bourgeois" (FURETIÈRE Antoine)]
Livres contenant "bourgeois" : ["Le Bourgeois gentilhomme" (MOLIÈRE), "Le Roman bourgeois" (FURETIÈRE Antoine)]

----- Auteurs -----
Tous les auteurs : [HUGO Victor, FURETIÈRE Antoine, ZOLA Émile, MOLIÈRE]
Auteurs dont le nom contient un "o" : [HUGO Victor, ZOLA Émile, MOLIÈRE]
Auteurs dont le prénom contient un "o" : [HUGO Victor, FURETIÈRE Antoine]

----- Mots clés -----
Tous les mots clés : [SOCIAL, ROMAN, HISTORIQUE, COMÉDIE, THÉATRE]
Mots clé contenant "om": [ROMAN, COMÉDIE]

*/
