package exercice1;

import java.util.Set;
import java.util.HashSet;

/**
 * Représente un mot-clé associé à un livre.
 */
public class MotCle {

	// Attributs statiques -------------------------------------------------

	/**
	 * Ensemble de tous les mots clés existants.
	 */
	@SuppressWarnings("unused")
	private static final Set<MotCle> MOTSCLES = new HashSet<>();

	// Attributs d'instances -----------------------------------------------

	/**
	 * Mot clé (public et non modifiable).
	 */
	public final String MOT;

	// Constructeur --------------------------------------------------------

	/**
	 * Constructeur unique et privé.
	 * 
	 * @param mot Mot clé
	 */
	private MotCle(String mot) {
		this.MOT = mot;
	}

	// Accesseurs et usines à objets ---------------------------------------

	/**
	 * Si possible, renvoie une instance de MotCle correspondant à une chaîne de
	 * caractères.
	 * <p>
	 * 
	 * Le résultat est null si le paramètre est null ou s'il correspond à la chaîne
	 * vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon :
	 * <ul>
	 * <li>soit la création n'est pas demandée et dans ce cas null est renvoyé en
	 * résultat</li>
	 * <li>soit la création est demandée et dans ce cas une nouvelle instance de
	 * MotCle est créée, rajoutée à la liste des mots clés existants et renvoyée en
	 * résultat</li>
	 * </ul>
	 * 
	 * @param mot     Mot clé à créer ou à récupérer
	 * @param getOnly indique si on ne veut que récupérer une instance existante
	 *                (pas de création dans ce cas)
	 * @return selon les cas : null, ou une instance existante, ou une nouvelle
	 *         instance
	 */
	private static MotCle getOrCreate(String mot, boolean getOnly) 
	{

//		if(getOnly)
//		{
//			for(MotCle mc : MOTSCLES)
//			{
//				if(mc.toString() == mot.toUpperCase()) return mc;
//			}
//			return null;
//		}

		return new MotCle(mot);
	}

	/**
	 * Si possible, renvoie une instance de MotCle correspondant à une chaîne de
	 * caractères.
	 * <p>
	 * 
	 * Le résultat est null si le paramètre est null ou s'il correspond à la chaîne
	 * vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon, une nouvelle
	 * instance de MotCle est créée, rajoutée à la liste des mots clés existants et
	 * renvoyée en résultat
	 * 
	 * @param mot Mot clé à créer ou à récupérer
	 * @return selon les cas : null, ou une instance existante, ou une nouvelle
	 *         instance
	 */
	public static MotCle getOrCreate(String mot) {
		return getOrCreate(mot, false);
	}

	/**
	 * Si possible, renvoie une instance de MotCle correspondant à une chaîne de
	 * caractères.
	 * <p>
	 * 
	 * Le résultat est null si le paramètre est null ou s'il correspond à la chaîne
	 * vide.
	 * <p>
	 * 
	 * Si les informations fournies permettent de désigner une instance existante,
	 * c'est cette instance qui est renvoyée en résultat. Sinon, null est renvoyé en
	 * résultat.
	 * 
	 * @param mot Mot clé à récupérer
	 * @return selon les cas : null, ou une instance existante
	 */
	public static MotCle get(String mot) {
		return getOrCreate(mot, true);
	}

	// toString et equals --------------------------------------------------

	@Override
	public String toString() 
	{
		String res = this.MOT.toUpperCase();
		return res;
		
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(!(obj instanceof MotCle)) return false;
		
		if(obj.toString().compareTo(this.toString())==0)
		{
			return true;
		}
		return false;
		
	}
}
