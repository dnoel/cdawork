package exercice1;

class Test {

	public static void main(String[] args) {
		MotCle m1 = MotCle.getOrCreate("Roman");
		MotCle m2 = MotCle.getOrCreate("ROMAN");
		MotCle m3 = MotCle.getOrCreate("comédie");

		System.out.println("----- Mot clé -----");
		System.out.println("m1    : " + m1);
		System.out.println("m2    : " + m2);
		System.out.println("m3    : " + m3);
		System.out.println("m1=m2 ? " + m1.equals(m2));
		System.out.println("m1=m3 ? " + m1.equals(m3));

		Auteur a1 = Auteur.getOrCreate("Hugo", "Victor");
		Auteur a2 = Auteur.getOrCreate("HUGO", "victor");
		Auteur a3 = Auteur.getOrCreate("HUGO", "Kévin");
		Auteur a4 = Auteur.getOrCreate("Molière", "");

		System.out.println("----- Auteur -----");
		System.out.println("a1    : " + a1);
		System.out.println("a2    : " + a2);
		System.out.println("a3    : " + a3);
		System.out.println("a4    : " + a4);
		System.out.println("a1=a2 ? " + a1.equals(a2));
		System.out.println("a1=a3 ? " + a1.equals(a3));
		System.out.println("a1=a4 ? " + a1.equals(a4));

		Livre l1 = Livre.getOrCreate("Les Misérables", "Hugo", "Victor");
		Livre l2 = Livre.getOrCreate("les misérables", "HUGO", "VICTOR");
		Livre l3 = Livre.getOrCreate("Les Abrutis", "Hugo", "Jacqueline");
		
		System.out.println("----- Livre -----");
		System.out.println("l1    : " + l1);
		System.out.println("l2    : " + l2);
		System.out.println("l3    : " + l3);
		System.out.println("l1=l2 ? " + l1.equals(l2));
		System.out.println("l1=l3 ? " + l1.equals(l3));
	}

}

/*

----- Mot clé -----
m1    : ROMAN
m2    : ROMAN
m3    : COMÉDIE
m1=m2 ? true
m1=m3 ? false
----- Auteur -----
a1    : HUGO Victor
a2    : HUGO Victor
a3    : HUGO Kévin
a4    : MOLIÈRE
a1=a2 ? true
a1=a3 ? false
a1=a4 ? false
----- Livre -----
l1    : "Les Misérables" (HUGO Victor)
l2    : "les misérables" (HUGO Victor)
l3    : "Les Abrutis" (HUGO Jacqueline)
l1=l2 ? true
l1=l3 ? false

*/
