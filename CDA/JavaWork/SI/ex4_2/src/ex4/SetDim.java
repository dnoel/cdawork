package ex4;

import java.awt.Dimension;

import graphicLayer.GBounded;
import graphicLayer.GElement;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

public class SetDim implements Command {

	@Override
	
	//Permet de changer la dimension de l'�lement donn� en param�tre
	 public Expr run(Reference receiver, ExprList method)
	{
		//R�cup�ration de l'�l�ment par la r�f�rence donn�e en param�tre
		Object Recv = receiver.getReceiver();
        
		//Si l'�lement est bien un GElement
        if(Recv instanceof GElement)
        {       
        	//On le convertir en GElement
            GElement element = (GElement) Recv;
            
            //R�cup�ration des coordonn�es entr�e par l'utilisateur
            int x, y;
            try
            {
            	 x = Integer.parseInt(method.get(2).getValue());
            	 y = Integer.parseInt(method.get(3).getValue());
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            	return method;
            }

            //Cr�ation d'un objet Dimension avec les param�tres r�cup�r�s pr�c�dement
            Dimension dim = new Dimension(x,y);
            
            //Changement des dimensions de l'�lement
            ((GBounded) element).setDimension(dim);

        }
		return method;
    }
}
