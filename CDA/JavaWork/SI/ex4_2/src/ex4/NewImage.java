package ex4;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import graphicLayer.GImage;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

//Permet de cr�er une nouvelle image
public class NewImage implements Command {

	@Override
	public Expr run(Reference receiver, ExprList method) 
	{
	    try
	    {
	    	//R�cup�rationd de l'image dont le chemin a �t� donn�e dans l'expression methode
	        Image image = ImageIO.read(new File(method.get(2).toString()));
	        
	        //Cr�ation de l'�l�ment GImage avec l'image r�cup�r�e
	        GImage img = new GImage(image);
	        
	        //Cr�ation de la r�f�rence de la GImage
	        Reference ref = new Reference(img);
	        
	        //Ajout de la possibilit� d'utiliser la commande "translate" pourla GImage cr�e
			ref.addCommand("translate", (Command) new TranslateElement());
	        return ref;
	    }
	
	    catch (IOException e)
	    {
	        e.printStackTrace();
	    }
		return method;

    }

}

