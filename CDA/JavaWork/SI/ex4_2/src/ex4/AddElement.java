package ex4;



import graphicLayer.GElement;
import graphicLayer.GSpace;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

public class AddElement implements Command {

	Environment environment;
	
	public AddElement(Environment environment) {
		this.environment = environment;
	}

	@Override
	public Expr run(Reference receiver, ExprList method)
    {
		//Récupération du receiver
        Object o = receiver.getReceiver();
        
        //Si l'objet est bien 
        if(o instanceof GSpace)
        {
            GSpace space = (GSpace) o;
            Reference nr = (Reference) new Interpreter().compute(this.environment, (ExprList) method.get(3));
            
            Object o2 = nr.getReceiver();
            if(o2 != null)
            {           	        	
            	if(o2 instanceof GElement)
                {
                    GElement e = (GElement) o2;
                    environment.addReference(method.get(2).toString(), nr);
                    space.addElement(e);
                    space.repaint();
                }
            }
            
        }
        

        return receiver;
    }

}
