package ex4;

import ex4.Exercice4_2_0;
import tools.Tools;

public class Test {

	
	
	
	public  void launch() 
	{
		//Met la couleur du space � rouge, d�place robi, faire sleep le space pendant 2 secondes, met la couleur de robi en noir
		String commandes = 	""+ 
				"	(space sleep 2000) !" + //Sleep pour permettre le temps d'argandir la fen�tre pour voir le test
				"	(space add robi (rect.class new))!" + 
				"	(robi translate 130 50)!" + 
				"	(robi setColor yellow)!" + 
				"	(space add momo (oval.class new))!" + 
				"	(momo setColor red)!" + 
				"	(momo translate 80 80)!" + 
				"	(space add pif (image.class new france.jpg))!" + 
				"	(pif translate 100 0)!" + 
				"	(space add hello (label.class new \"Hello world\"))!" + 
				"	(hello translate 10 10)!" + 
				"	(hello setColor black)!" + 
				"	(space del pif)!" + 
				"	(space del momo)!" + 
				"	(space del robi)!" + 
				"	(space del hello)";
		
		
		String[] testCommande = commandes.split("!");
		
		Exercice4_2_0 exo = new Exercice4_2_0();
		
		System.out.println("DEBUT DU TEST, VEUILLEZ VOIR LES COMMENTAIRES DE LA CLASSE TEST.JAVA AFIN DE COMPRENDRE LE DEROULEMENT DU TEST");
		Tools.sleep(2000);
		
		for(String command : testCommande)
		{
			exo.executeTest(command);
			Tools.sleep(500);
		}
		System.out.println("VOUS POUVEZ EXECUTER DIRECTEMENT LA CLASSE EXERCICE... POUR TESTER MANUELLEMENT LE PROGRAMME ");

		
	}
	
	public static void main(String[] args) {
		new Test().launch();
	}
}
