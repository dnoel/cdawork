package ex4;

import java.awt.Dimension;

import graphicLayer.GImage;
import graphicLayer.GOval;
import graphicLayer.GRect;
import graphicLayer.GSpace;
import graphicLayer.GString;
import jfkbits.ExprList;
import jfkbits.LispParser;
import jfkbits.LispParser.Expr;
import tools.Tools;
/*
	
	(space add robi (rect.class new))
	(robi translate 130 50)
	(robi setColor yellow)
	(space add momo (oval.class new))
	(momo setColor red)
	(momo translate 80 80)
	(space add pif (image.class new france.jpg))
	(pif translate 100 0)
	(space add hello (label.class new "Hello world"))
	(hello translate 10 10)
	(hello setColor black)
	(space del pif)
	(space del momo)
	(space del robi)
	(space del hello)

*/
public class Exercice4_2_0 {
	static // Une seule variable d'instance
	Environment environment = new Environment();

	public Exercice4_2_0() {
		GSpace space = new GSpace("Exercice 4", new Dimension(200, 100));
		space.open();

		Reference spaceRef = new Reference(space);
		Reference rectClassRef = new Reference(GRect.class);
		Reference ovalClassRef = new Reference(GOval.class);
		Reference imageClassRef = new Reference(GImage.class);
		Reference stringClassRef = new Reference(GString.class);

		spaceRef.addCommand("setColor", new SetColor());
		spaceRef.addCommand("sleep", new Sleep());

		spaceRef.addCommand("add", new AddElement(environment));
		spaceRef.addCommand("del", new DelElement(environment));
		
		rectClassRef.addCommand("new", new NewElement());
		ovalClassRef.addCommand("new", new NewElement());
		imageClassRef.addCommand("new", new NewImage());
		stringClassRef.addCommand("new", new NewString());

		environment.addReference("space", spaceRef);
		environment.addReference("rect.class", rectClassRef);
		environment.addReference("oval.class", ovalClassRef);
		environment.addReference("image.class", imageClassRef);
		environment.addReference("label.class", stringClassRef);
		
	}

	//M�thode appel�e par la classe Test, ce changement permet de lancer des tests tout en gardant la possibilit� d'utiliser manuellement la classe Exercice4_2_0.java
	public void executeTest(String script)
	{
		try 
		{	
			LispParser parser = new LispParser(script);
			Expr e = parser.parseExpr();
			if (e instanceof ExprList) {
				ExprList compiled = (ExprList) e;
				new Interpreter().compute(environment, compiled);
			}
		} catch (Exception e1) {
			System.out.println("Erreur lors du traitement de la commande, veuillez v�rifier sa validit�");
		}
	}
	
	public static void mainLoop() 
	{
		while (true) {
			System.out.print("> ");
			String input = Tools.readKeyboard();
			try 
			{		
				LispParser parser = new LispParser(input);
				Expr e = parser.parseExpr();
				if (e instanceof ExprList) {
					ExprList compiled = (ExprList) e;
					new Interpreter().compute(environment, compiled);
				}
			} catch (Exception e1) {
				System.out.println("Erreur lors du traitement de la commande, veuillez v�rifier sa validit�");
			}
		}
	}

	public static void main(String[] args) 
	{
		new Exercice4_2_0();
		mainLoop();
	}

}