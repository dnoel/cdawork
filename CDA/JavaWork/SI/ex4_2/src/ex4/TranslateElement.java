package ex4;

import java.awt.Point;

import graphicLayer.GElement;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

public class TranslateElement implements Command
{

	@Override
	//Permet de d�placer un �l�ment aux coordonn�esdonn�es en param�tre
	public Expr run(Reference receiver, ExprList method) 
	{
		//On r�cup�re l'�lement r�f�renc� dans les param�tres 
		Object o = receiver.getReceiver();
		
		if(o instanceof GElement)
		{
			Point p = new Point(Integer.parseInt(method.get(2).toString()), Integer.parseInt(method.get(3).toString()));
			((GElement) o).translate(p);
		}
		return null;
	}
	
}
