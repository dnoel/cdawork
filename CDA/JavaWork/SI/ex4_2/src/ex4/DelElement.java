package ex4;

import graphicLayer.GElement;
import graphicLayer.GSpace;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

//Supprime les �l�ments donn�es en r�f�rence
public class DelElement implements Command {

	private Environment environment;


	public DelElement(Environment environment) {
		this.environment = environment;
	}

	@Override
	public Expr run(Reference reference, ExprList method) 
	{
		//R�cup�re l'�lement dans lequel on souhaite supprimer des �l�ments
		Object o = reference.getReceiver();
		
		//Si l'objet est bien un GSpace(le seul �l�ment dans lequel on peut supprimer d'autres �l�ments pour le moment)
        if(o instanceof GSpace) 
        {
        	//On convertit l'objet en GSpace
            GSpace space = (GSpace)o;
            
            //On r�cup�re la r�f�rence de l'objet vis� dans l'expression)
            Reference ref = this.environment.getReferenceByName(method.get(2).getValue());
            
            //On r�cup�re l'�l�ment � supprimer
            Object objet = ref.getReceiver();
            
            //Si l'objet est bien un GElement
            if(objet instanceof GElement) 
            {
            	//On le convertir en GElement
                GElement ge = (GElement)objet;
                //On supprime l'�l�ment dans space
                space.removeElement(ge);
                //On supprime la r�f�rence de l'objet supprim� dans l'environnement
                this.environment.delReference(method.get(2).getValue());
                //On actualise la vue
                space.repaint();
            }
        }
        //On retourne l'expression
		return method;
	}
}
