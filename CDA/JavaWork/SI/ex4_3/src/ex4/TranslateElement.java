package ex4;

import java.awt.Point;

import graphicLayer.GElement;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

public class TranslateElement implements Command
{

	@Override
	//Permet de d�placer un �l�ment aux coordonn�esdonn�es en param�tre
	public Expr run(Reference receiver, ExprList method) 
	{
		//L'interpr�teur s'est d�j� assur� que le receiver existait
		
		//On r�cup�re l'�lement r�f�renc� dans les param�tres 
		Object o = receiver.getReceiver();
		
		if(o instanceof GElement)
		{
			int x,y;
			try
            {
            	 x = Integer.parseInt(method.get(2).getValue());
            }
            catch(Exception e)
            {
            	System.out.println("Valeur x invalide : " + method.get(2).getValue());
            	return null;
            }
            
            try
            {
            	 y = Integer.parseInt(method.get(3).getValue());
            }
            catch(Exception e)
            {
            	System.out.println("Valeur y invalide : " + method.get(3).getValue());
            	return null;
            }
            
			Point p = new Point(x,y);
			((GElement) o).translate(p);
		}
		
		return null;
	}
	
}
