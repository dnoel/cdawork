package ex4;

import java.util.HashMap;

//Contient un Hashmap permettant d'associer la r�f�rence des GElements aux nom que l'utilisateur
//Contient des m�thodes  permetatnt de manipuler le contenu du  Hashmap
public class Environment 
{
	//Le Hashmap 
	private HashMap<String, Reference> referencesList = new HashMap<String, Reference>();
	
	//Ajoute une r�f�rence dans le Hashmap
	public void addReference(String name, Reference r)
	{
		referencesList.put(name, r);
	}
	
	//Supprime une r�f�rence dans le Hashmap et l'ensembles de ses r�f�rences filles
	public void delReference(String name)
	{
		referencesList.remove(name);
	}
	
	
	//Permet de r�cup�rer une r�f�rence d'�lement en fonction du nom d'�l�ment recherch�
	public Reference getReferenceByName(String name)
	{
		return(referencesList.get(name));		
	}
}
