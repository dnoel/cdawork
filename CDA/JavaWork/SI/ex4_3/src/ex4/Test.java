package ex4;

import ex4.Exercice4_3;
import tools.Tools;

public class Test {

	
	
	
	public  void launch() 
	{
		//Met la couleur du space � rouge, d�place robi, faire sleep le space pendant 2 secondes, met la couleur de robi en noir
		String commandes = "(space add robi (rect.class new))!" + 
				"(space.robi setDim 300 300)!" + 
				"(space.robi setColor yellow)!" + 
				"(space.robi add ala (rect.class new))!" + 
				"(space.robi.ala setDim 200 200)	!" + 
				"(space.robi.ala setColor white)!" + 
				"(space.robi.ala add hello (label.class new \"Hello world\"))!" + 
				"(space.robi.ala.hello translate 10 10)!" + 
				"(space.robi.ala.hello setColor black)!" + 
				"(space.robi.ala add pif (image.class new france.jpg))!" + 
				"(space.robi.ala.pif translate 100 0)!" + 
				"(space.robi.ala.pif del pif)!" + 
				"(space del robi)";
		
		String[] testCommande = commandes.split("!");
		
		Exercice4_3 exo = new Exercice4_3();
		
		System.out.println("DEBUT DU TEST, VEUILLEZ VOIR LES COMMENTAIRES DE LA CLASSE TEST.JAVA AFIN DE COMPRENDRE LE DEROULEMENT DU TEST");
		Tools.sleep(2000);

		for(String command : testCommande)
		{
			exo.executeTest(command);
			Tools.sleep(500);
		}
		
		System.out.println("VOUS POUVEZ EXECUTER DIRECTEMENT LA CLASSE EXERCICE... POUR TESTER MANUELLEMENT LE PROGRAMME ");

	}
	
	public static void main(String[] args) {
		new Test().launch();
	}
}
