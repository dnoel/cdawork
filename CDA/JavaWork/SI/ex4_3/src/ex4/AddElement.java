package ex4;



import graphicLayer.GElement;
import graphicLayer.GOval;
import graphicLayer.GContainer;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

public class AddElement implements Command 
{

	Environment environment;
	
	public AddElement(Environment environment) {
		this.environment = environment;
	}

	@Override
	public Expr run(Reference receiver, ExprList method)
    {
		
		String s = new String(method.get(0).toString() + "." + method.get(2).toString());
		//Si le nom de l'�l�ment est d�j� utilis�, on ne l'ajoute pas
        if(environment.getReferenceByName(s)!= null )
        {
        	System.out.println("La r�f�rence \"" +s+ "\" est d�j� utilis�");
        	return receiver;
        }
        
		//R�cup�ration du receiver
        Object o = receiver.getReceiver();
        
        //Si on ajoute bien un �l�ment dans un GContainer autre que GOval
        if(o instanceof GContainer && !(o instanceof GOval))
        {
        	GContainer cont = (GContainer) o;
            //On interprete l'expression de l'�lement � ajouter
            Reference nr = (Reference) new Interpreter().compute(this.environment, (ExprList) method.get(3));
            nr.addCommand("add", new AddElement(this.environment));
            nr.addCommand("del", new DelElement(this.environment));
            
            Object o2 = nr.getReceiver();
            
            //Si l'�l�ment � ajouter a bien �t� cr�e
            if(o2 != null)
            {           	        	
     
            	//Si on ajoute bien un GElement, on l'ajoute et actualise le GContainer
            	if(o2 instanceof GElement)
                {
                    GElement e = (GElement) o2;
                    //On rajoute la r�f�rence point�e de l'�l�ment
                    environment.addReference(s, nr);
                    cont.addElement(e);
                    
                    //On ajoute le nom de l'objet ajout� dans le tableau "content" du GContainer, celui-ci est utilis� afin de supprimer recursivement un un �l�ment et son contenu
                    receiver.addContent(method.get(2).toString());
                    
                    cont.repaint();
                    
                }
            }
            
        }
        else
        {
        	System.out.println("Vous ne pouvez ajouter d'�l�ments dans \"" + s + "\"");
        }
        

        return receiver;
    }

}
