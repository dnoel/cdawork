package ex4;

import java.awt.Dimension;

import graphicLayer.GBounded;
import graphicLayer.GElement;
import graphicLayer.GSpace;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

public class SetDim implements Command {

	@Override
	
	//Permet de changer la dimension de l'�lement donn� en param�tre
	public Expr run(Reference receiver, ExprList method)
	{
		
		  int x, y;
          
          //On g�re le cas o� la valeur de x n'est pas un entier
          try
          {
          	 x = Integer.parseInt(method.get(2).getValue());
          }
          catch(Exception e)
          {
          	System.out.println("Valeur x invalide : " + method.get(2).getValue());
          	return method;
          }
          
          //On g�re le cas o� la valeur de x n'est pas un entier
          try
          {
          	 y = Integer.parseInt(method.get(3).getValue());
          }
          catch(Exception e)
          {
          	System.out.println("Valeur y invalide : " + method.get(3).getValue());
          	return method;
          }

          //Cr�ation d'un objet Dimension avec les param�tres r�cup�r�s pr�c�dement
          if(x<0 || y<0)
          {
          	System.out.println("Les valeurs rentr�es doivent �tres positives");
          	return null;
          }
          
		//L'interpr�teur s'est d�j� assur� que le receiver existait

		//R�cup�ration de l'�l�ment par la r�f�rence donn�e en param�tre
		Object Recv = receiver.getReceiver();
        
		//Si l'�lement est bien un GElement
        if(Recv instanceof GElement)
        {       
            GElement element = (GElement) Recv;
            
            //R�cup�ration des coordonn�es entr�e par l'utilisateur
          
            Dimension dim = new Dimension(x,y);
            
            //Changement des dimensions de l'�lement
            ((GBounded) element).setDimension(dim);

        }
        
        if(Recv instanceof GSpace)
        {
        	GSpace element = (GSpace) Recv;
        	
        	//Cr�ation d'une nouvelle Dimension
        	Dimension dim = new Dimension(x,y);
        	
        	//Changement de la dimension du GSpace
        	element.changeWindowSize(dim);
        }
		return method;
    }
}
