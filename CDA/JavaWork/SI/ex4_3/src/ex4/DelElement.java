package ex4;

import graphicLayer.GContainer;
import graphicLayer.GElement;
import jfkbits.ExprList;
import jfkbits.LispParser;
import jfkbits.LispParser.Expr;

//Supprime des �l�ments 
public class DelElement implements Command {

	private Environment environment;


	public DelElement(Environment environment) {
		this.environment = environment;
	}

	
	//M�thode r�cursive permettant de supprimer l'�l�ment donn� en r�f�rence ainsi que tout les �l�ments que clui-ci contient 
	@Override
	public Expr run(Reference reference, ExprList method) 
	{
		
		String s = new String(method.get(0).toString() + "." + method.get(2).toString());
		if(environment.getReferenceByName(s) == null )
	    {
	    	System.out.println("La r�f�rence \"" + s + "\" n'existe pas");
	    	return method;
	    }
		//R�cup�re l'�lement dans lequel on souhaite supprimer des �l�ments
		Object o = reference.getReceiver();
		
		//Si l'objet est bien un GContainer(le seul �l�ment dans lequel on peut supprimer d'autres �l�ments pour le moment)
        if(o instanceof GContainer) 
        {
        	//On convertit l'objet en GContainer
        	GContainer space = (GContainer)o;
            
            //On r�cup�re la r�f�rence de l'objet vis� dans l'expression)
            Reference ref = this.environment.getReferenceByName(s);
            
            //On r�cup�re l'�l�ment � supprimer
            Object objet = ref.getReceiver();
           
            //Si l'objet est bien un GElement
            if(objet instanceof GElement) 
            {
            
            	//On le convertit en GElement
            	  GElement ge = (GElement)objet;
            	  
        		//On recup�re son contenu 
            	String[] content = ref.getContent();
            	
            	//Si le GElement contient d'autres �l�ments, on  les supprime (d�but de la r�cursivit�)
            	
            
            	for(int i=0; i< content.length; i++)
            	{
            		try 
            		{
            			//On cr�e une nouvelle ExprList 
            			LispParser parser = new LispParser("(" + s + " del " + content[i] + ")");
	    				Expr e = parser.parseExpr();
	    				if (e instanceof ExprList) 
	    				{
	    					ExprList compiled = (ExprList) e;
	    					
	    					//On appelle r�cursivement la methode run avec la nouvelle r�f�rence et la nouvelle expression
	    					this.run(ref, compiled);
	    				}
            		} 
            		catch (Exception e1) 
            		{
            			e1.printStackTrace();
            		}   		           		
            	}

                //On supprime l'�l�ment m�re apr�s avoir supprim� tout les �l�ments filles et leur r�f�rences
                space.removeElement(ge);
                //On supprime la r�f�rence m�re
                environment.delReference(s);
                
                System.out.println("Supression de " + s + " effectu�e");
                //On actualise la vue
                space.repaint();
            }
        }
        //On retourne l'expression
		return method;
	}
}
