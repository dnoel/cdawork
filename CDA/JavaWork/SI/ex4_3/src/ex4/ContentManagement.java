package ex4;

import java.util.Arrays;

public abstract class ContentManagement 
{
	String[] content = new String[0];
	
	public void addContent(String element)
	{
		content = Arrays.copyOf(content, content.length+1);
		content[content.length-1] = element;
	}
	
	public String[] getContent()
	{
		return this.content;
	}
}
