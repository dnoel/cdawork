	(space setDim 800 800)

	//Test des éléments avec suppression en cascade
	(space add robi (rect.class new))
	(space.robi setDim 300 300)
	(space.robi setColor yellow)
	(space.robi add ala (rect.class new))
	(space.robi.ala setDim 200 200)	
	(space.robi.ala setColor white)
	(space.robi.ala add hello (label.class new "Hello world"))
	(space.robi.ala.hello translate 10 10)
	(space.robi.ala.hello setColor black)
	(space.robi.ala add pif (image.class new france.jpg))
	(space.robi.ala.pif translate 100 0)
	(space.robi.ala translate 100 0)
	(space.robi.ala.pif del pif)
	(space del robi)

	