package ex4;

import jfkbits.ExprList;

public interface Command 
{
	abstract public void run(Object receiver, ExprList method);
}
