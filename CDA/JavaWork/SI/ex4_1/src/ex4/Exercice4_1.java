package ex4;

// 
//	(space setColor black)  
//	(robi setColor yellow) 
//	(space sleep 2000) 
//	(space setColor white)  
//	(space sleep 1000) 	
//	(robi setColor red)		  
//	(space sleep 1000)
//	(robi translate 100 0)
//	(space sleep 1000)
//	(robi translate 0 50)
//	(space sleep 1000)
//	(robi translate -100 0)
//	(space sleep 1000)
//	(robi translate 0 -40) ) 
//

import java.awt.Dimension;
import graphicLayer.GRect;
import graphicLayer.GSpace;
import jfkbits.ExprList;
import jfkbits.LispParser;
import jfkbits.LispParser.ParseException;
import tools.Tools;


public class Exercice4_1 {
	// Une seule variable d'instance
	static Environment environment = new Environment();

	public Exercice4_1() 
	{
		// space et robi sont temporaires ici
		GSpace space = new GSpace("Exercice 4", new Dimension(200, 100));
		GRect robi = new GRect();

		space.addElement(robi);
		space.open();

		Reference spaceRef = new Reference(space);
		Reference robiRef = new Reference(robi);

		// Initialisation des references : on leur ajoute les primitives qu'elles comprenent 
		spaceRef.addCommand("setColor", new SpaceChangeColor());
		spaceRef.addCommand("sleep", new SpaceSleep());
		
		robiRef.addCommand("setColor", new RobiChangeColor());
		robiRef.addCommand("translate", new RobiTranslate());

		
		// Enrigestrement des references dans l'environement par leur nom
		environment.addReference("space", spaceRef);
		environment.addReference("robi", robiRef);
	}

	//M�thode appel�e par la classe Test, ce changement permet de lancer des tests tout en gardant la possibilit� d'utiliser manuellement la classe Exercice4_1.java
	public void executeTest(String script)
	{
		// prompt
		// lecture d'une s-expression au clavier
		if(script.isEmpty() || script.split(" ").length < 3)
		{
			System.out.println("Veuillez rentrer un nombre d'arguments valide");
			return;
		}
		// creation du parser
		LispParser parser = new LispParser(script);
		try 
		{
			// compilation 
			ExprList compiled = (ExprList) parser.parseExpr();
			// execution de la s-expression compilee
			run(compiled);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
	}
	private static void mainLoop() 
	{
		while (true) {
			// prompt
			System.out.print(">");
			// lecture d'une s-expression au clavier
			String input = Tools.readKeyboard();
			if(input.isEmpty() || input.split(" ").length < 3)
			{
				System.out.println("Veuillez rentrer un nombre d'arguments valide");
				continue;
			}
			// creation du parser
			LispParser parser = new LispParser(input);
			try {
				// compilation 
				ExprList compiled = (ExprList) parser.parseExpr();
				// execution de la s-expression compilee
				run(compiled);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}
	}
//
	private static void run(ExprList expr) {
		// quel est le nom du receiver
		String receiverName = expr.get(0).getValue();
		// quel est le receiver
		Reference receiver = environment.getReferenceByName(receiverName);
		// demande au receiver d'executer la s-expression compilee
		receiver.run(expr);
	}

	public static void main(String[] args) {
		new Exercice4_1();
		mainLoop();
	}

}