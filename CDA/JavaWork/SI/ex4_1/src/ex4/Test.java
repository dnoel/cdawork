package ex4;

import ex4.Exercice4_1;
import tools.Tools;

public class Test {

	
	
	
	public  void launch() 
	{
		//Met la couleur du space � rouge, d�place robi, faire sleep le space pendant 2 secondes, met la couleur de robi en noir
		String commandes = 	"(space setColor red) !"
				+ "(robi translate 25 25)!"
				+ "(space sleep 2000)!"
				+ "(robi setColor black)";
		
		
		String[] testCommande = commandes.split("!");
		
		Exercice4_1 exo = new Exercice4_1();
		
		System.out.println("DEBUT DU TEST, VEUILLEZ VOIR LES COMMENTAIRES DE LA CLASSE TEST.JAVA AFIN DE COMPRENDRE LE DEROULEMENT DU TEST");
		Tools.sleep(2000);

		for(String command : testCommande)
		{
			exo.executeTest(command);
			Tools.sleep(500);
		}
		
		System.out.println("VOUS POUVEZ EXECUTER DIRECTEMENT LA CLASSE EXERCICE... POUR TESTER MANUELLEMENT LE PROGRAMME ");

	}
	
	public static void main(String[] args) {
		new Test().launch();
	}
}
