package ex4;

import java.awt.Color;
import java.lang.reflect.Field;

import graphicLayer.GRect;
import jfkbits.ExprList;

public class RobiChangeColor implements Command
{
	
	@Override
	public void run(Object receiver, ExprList method)
	{
		Color c=null;
		try 
		{
			
		    Field field = Class.forName("java.awt.Color").getField(method.get(2).toString());
		    c = (Color)field.get(null);
		} catch (Exception e) {
		}
		((GRect) receiver).setColor(c);
	}
}
