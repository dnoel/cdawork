package ex4;

import java.util.HashMap;

import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

public class Reference implements Expr
{
	HashMap<String, Command> commandList = new HashMap<String, Command>();
	
	Object receiver;
	
	
	public Reference(Object receiver)
	{
		this.receiver = receiver;
	}
	
	public Command getCommandeByName(String selector)
	{
		
		return this.commandList.get(selector);
		
	}
	
	public void addCommand(String selector, Command primitive)
	{
		this.commandList.put(selector, primitive);
	}
	
	public Expr run(ExprList method)
	{
		Command c = this.getCommandeByName(method.get(1).toString());
		c.run(this.receiver, method);
		return null;	
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
