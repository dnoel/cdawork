package ex4;

import java.util.HashMap;

public class Environment 
{
	public Environment()
	{
	}
	private HashMap<String, Reference> referencesList = new HashMap<String, Reference>();
	
	public void addReference(String name, Reference r)
	{
		referencesList.put(name, r);
	}
	
	public Reference getReferenceByName(String name)
	{
		return(referencesList.get(name));		
	}
}
