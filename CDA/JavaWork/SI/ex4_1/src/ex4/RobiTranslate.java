package ex4;

import java.awt.Point;

import graphicLayer.GRect;
import jfkbits.ExprList;

public class RobiTranslate implements Command
{

	@Override
	public void run(Object receiver, ExprList method) 
	{
		Point p = new Point(Integer.parseInt(method.get(2).toString()), Integer.parseInt(method.get(3).toString()));
		((GRect) receiver).translate(p);	
	}
}
