package ex1;

import java.awt.Color;
import java.awt.Dimension;
import graphicLayer.GRect;
import graphicLayer.GSpace;
public class Exercice1 
{
	GSpace space = new GSpace("Exercice 1", new Dimension(200, 150));
	GRect robi = new GRect();
	public Exercice1() 
	{
		space.addElement(robi);
		space.open();
		
		//Chaque tour de boucle repr�sente un tour du carr� dans le cadre
		//Le d�placement est progressif
		while(true)
		{
			//D�placement progressif responsif de gauche � droite
			while(robi.getX() < space.getWidth() - robi.getWidth())
			{
				robi.setX(robi.getX()+1);
				robi.setY(0);
				tools.Tools.sleep(1);
			}
			//Changement de la couleur
			robi.setColor(new Color((int) (Math.random() * 0x1000000)));
			
			//D�placement progressif responsif de haut en bas
			while(robi.getY() < space.getHeight() - robi.getHeight())
			{
				robi.setY(robi.getY()+1);
				robi.setX(space.getWidth()-robi.getWidth());
				tools.Tools.sleep(1);
			}
			robi.setColor(new Color((int) (Math.random() * 0x1000000)));
			
			//D�placement progressif responsif de droite � gauche		
			while(robi.getX() > 0)
			{
				robi.setX(robi.getX()-1);
				robi.setY(space.getHeight()-robi.getHeight());
				tools.Tools.sleep(1);
			}
			robi.setColor(new Color((int) (Math.random() * 0x1000000)));
			
			//D�placement progressif responsif de bas en haut
			while(robi.getY() > 0)
			{
				robi.setY(robi.getY()-1);
				tools.Tools.sleep(1);
			}
			robi.setColor(new Color((int) (Math.random() * 0x1000000)));
		}
		
	}
	
	public static void main(String[] args) 
	{
		new Exercice1();
	
	}
}
