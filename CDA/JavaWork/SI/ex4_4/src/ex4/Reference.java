package ex4;

import java.util.HashMap;
import java.util.Map;

import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

class Reference extends ContentManagement implements Expr {
	Object receiver;
	Map<String, Command> primitives;
	Map<String, ExprList> scripts;
	Environment environment;

	public Reference(Object receiver) {
		this.receiver = receiver;
		primitives = new HashMap<String, Command>();
		scripts = new HashMap<String, ExprList>();
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public Command getCommandByName(String selector) {
		return primitives.get(selector);
	}
	
	public ExprList getScriptByName(String selector)
	{
		return scripts.get(selector);
	}
	public Expr run(ExprList e) 
	{
		String selector = e.get(1).getValue();
		Command c = this.getCommandByName(selector);
		Expr script = this.getScriptByName(selector);
		if(script!=null)
		{
			c = this.getCommandByName("runScript");
		}
		else if (c == null)
		{
			System.out.println("La commande/script n'eexiste pas dans cet �l�ment");
			return null;
		}
		return c.run(this, e);
	}

	public void addCommand(String selector, Command p) {
		primitives.put(selector, p);
	}
	
	public void addScript(String selector, ExprList expr) {
		scripts.put(selector, expr);
	}
	
	public void delScriptbyName(String selector) {
		scripts.remove(selector);
	}

	public Object getReceiver() {
		return receiver;
	}

	@Override
	public String getValue() {
		return null;
		
	}
}

