package ex4;

import java.awt.Color;

import graphicLayer.GElement;
import graphicLayer.GSpace;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;
import tools.Tools;

//Change la couleur de l'�l�ment en fonction de l'expression donn�e
public class SetColor implements Command
{

	@Override
	public Expr run(Reference receiver, ExprList method) 
	{
		//L'interpr�teur s'est d�j� assur� que le receiver existait

		//Cr�ation de la couleur
        Color color = Tools.getColorByName(method.get(2).toString());
        if(color != null)
        {
        	//R�cup�ration de l'�l�ment par la r�f�rence donn�e en param�tre
        	Object r = receiver.getReceiver();
        	
        	//Distinction entre un GSpace et els auters �l�ments graphiques
        	if(r instanceof GSpace)
        	{
        		((GSpace) r).setColor(color);
        	}
        	if(r instanceof GElement)
        	{
        		((GElement) r).setColor(color);
        	}
        	
        }
        return null;
	}

}
