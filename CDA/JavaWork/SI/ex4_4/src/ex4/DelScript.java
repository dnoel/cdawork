package ex4;

import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

public class DelScript implements Command
{
	
	Environment environment;
	
	public DelScript(Environment environment) {
		this.environment = environment;
	}
	
	@Override
	public Expr run(Reference receiver, ExprList method) 
	{
		if(receiver.getScriptByName(method.get(2).toString())==null)
		{
			System.out.println("le script " + method.get(2).toString() + "n'existe pas dans "+ method.get(0).toString());
			return null;
		}
		receiver.delScriptbyName(method.get(2).toString());
		System.out.println("suppression du script " + method.get(2).toString());
		return null;
	}
}
