package ex4;

import graphicLayer.GString;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

//Permet la cr�ation d'un �l�ment GString
public class NewString implements Command {

	@Override
	public Expr run(Reference receiver, ExprList method) 
	{
		//Cr�ation de lz GString avec ce qu'a marqu� l'utilisateur
	    GString s = new GString(method.get(2).getValue());
	    
	    //Cr�ationd e la r�f�rence de la strin cr�e
		Reference ref = new Reference(s);
		
		//Ajout de la pssibilit� d'utiliser les commandes "translate" et "setColor" pour la GString cr�e
		ref.addCommand("translate", (Command) new TranslateElement());
		ref.addCommand("setColor", new SetColor());
		return ref;
    }
}
