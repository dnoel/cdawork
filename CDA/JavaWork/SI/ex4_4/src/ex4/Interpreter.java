package ex4;

import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

class Interpreter
{

	public Expr compute(Environment env, ExprList expr) 
	{
		if (expr.size() < 2) return null;
		String receiverName = expr.get(0).getValue();
		Reference receiver = env.getReferenceByName(receiverName);
		if (receiver == null)
		{
	    	System.out.println("La reference\"" + expr.get(0).toString() + "\" n'existe pas");
			return null;
		}
		
		Expr e = receiver.run(expr);
		return e;
	}

}
