package ex4;

import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

public class AddScript implements Command
{
	private Environment environment;


	public AddScript(Environment environment) {
		this.environment = environment;
	}
	
	@Override
	public Expr run(Reference receiver, ExprList method) 
	{
		if(receiver.getScriptByName(method.get(2).toString())!=null)
		{
			System.out.println("Le script " + method.get(2).toString() + " est d�j� utilis�");
			return null;
		}
		receiver.addScript(method.get(2).toString(), (ExprList)method.get(3));
		
		System.out.println("Ajout du script " + method.get(2).toString() + ": " + method.get(3).toString());
		return null;
	}
	
	
}
