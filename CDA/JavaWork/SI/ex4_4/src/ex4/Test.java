package ex4;

import ex4.Exercice4_4;
import tools.Tools;

public class Test {
	
	/* 
	 * Ajoute un script newSquare  � Rect 
	 * newSquare est un constructeur qui prend en argument une largeur et retourne un carre
	 * On ne voit que la fenetre par d�faut, vide
	 * 
	 * Le constructeur de fonctionne pas car je n'ai pas pu faire en sorte d'ajouter un script � une reference de classe
	 * De plus, l'op�rateur return (le ^) n'est pas du tout mis en oeuvre
	 * 
	 * Pour permettre � ce type de script de fonctionner, il faudrait revoir la conception de la fa�on suivante :
	 * bla bla constructeur bla bla
	 * bla bla bla le return (^) pourrait �tre naturellement trait� par l'interpr�teur bla bla 
	 * 
	 */
	
	
	
	public  void launch() 
	{
		//Liste des commandes � executer permet de s'assurer que le fonctionnement des commandes n'a pas �t� alt�r� par l'ajout des scripts*
		String commandes = "(space add robi (rect.class new))!" + 
				"(space.robi setDim 300 300)!" + 
				"(space.robi setColor yellow)!" + 
				"(space.robi add ala (rect.class new))!" + 
				"(space.robi.ala setDim 200 200)	!" + 
				"(space.robi.ala setColor white)!" + 
				"(space.robi.ala add hello (label.class new \"Hello world\"))!" + 
				"(space.robi.ala.hello translate 10 10)!" + 
				"(space.robi.ala.hello setColor black)!" + 
				"(space.robi.ala add pif (image.class new france.jpg))!" + 
				"(space.robi.ala.pif translate 100 0)!" + 
				"(space.robi.ala.pif del pif)!" + 
				"(space del robi)";
		
		
		//Liste des scripts utilis�s pour*
		String scripts = ""
			+"("
				+ "space addScript test"
				+ "( "
					+ "(self name name2 val1 val2 color)"
					+ "( "
						+ "(self add name(rect.class new)) "
						+ "(self.name setDim val1 val1) "
						+ "(self.name setColor color) "
						+ "(self.name add name2(rect.class new))"
						+ "(self.name.name2 translate val2 val2)"
						+ "(self sleep 2000)"
						+ "(self del name)"
					+ " ) "
				+ ") "
			+ ")!"
			+ "(space test robi robo 300 50 yellow)!"//On lance le script test -> succ�s
			+ "(space addScript test(self color)( (self setColor red) ) )!"//On essaye de cr�er un nouveau script test dans space-> impossible
			
			//On recr�e robi dans space
			+ "(space add robi (rect.class new))!"
			
			//On ajoute un nouveau script test, mais dans robi au lieu de space -> succ�s
			//On n'utilsie pas ce script, il sert seulement � tester le fait que chaque �l�ment � sa propre liste ind�pendante de scripts
			+ "(space.robi addScript test( (self color) () ) )!"
			//Suppression du script test que l'on vient d'ajouter
			+ "(space.robi delScript test)!"
			
			//On ajoute un script changecolor dans space
			//Ce script change la couleur de l'�l�ment donn�  
			+ "(space.robi addScript changeColor( (self color) ( (self setColor color) ) ) )!"
			
			//On ajoute un script launch dans space
			//Ce script permet de lancer un script sp�cifique d'un �l�ment donn�. Cet �l�ment doit �tre contenu dans l'�l�ment qui lance le script
			+ "(space addScript launch( (self element scriptName color) ( (self.element scriptName color) ) ) )!" 
			//On lance le script launch dans space pour modifier la couleur de robi
			+ "(space launch robi changeColor red)!"
			//Suppression en casacde de robi
			+ "(space del robi)";
		
		String[] testCommande = commandes.split("!");
		
		String[] testScripts = scripts.split("!");
			
			
		Exercice4_4 exo = new Exercice4_4();
		System.out.println("DEBUT DU TEST, VEUILLEZ VOIR LES COMMENTAIRES DE LA CLASSE TEST.JAVA AFIN DE COMPRENDRE LE DEROULEMENT DU TEST");
		Tools.sleep(2000);

		
		System.out.println("LANCEMENT DES COMMANDES DE TEST POUR VERIFIER LEUR BON FONCTIONNEMENT ");
		Tools.sleep(750);
		for(String command : testCommande)
		{
			exo.executeTest(command);
			Tools.sleep(500);
		}
		
		System.out.println("LANCEMENT DES SCRIPTS DE TEST");
		Tools.sleep(500);
		for(String script : testScripts)
		{
			exo.executeTest(script);
			Tools.sleep(1000);
		}
		
		System.out.println("VOUS POUVEZ EXECUTER DIRECTEMENT LA CLASSE EXERCICE... POUR TESTER MANUELLEMENT LE PROGRAMME ");

	}
	
	public static void main(String[] args) {
		new Test().launch();
	}
}
