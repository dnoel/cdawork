package ex4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import jfkbits.ExprList;
import jfkbits.LispParser;
import jfkbits.LispParser.Expr;
public class RunScript implements Command
{
	public Environment environment;

	//Hashmap of the arguments
	public Map<String, String> args = new HashMap<String, String>();
	
	
	public RunScript(Environment environment) {
		this.environment = environment;
	}
	
	
	private ExprList convertArgs(ExprList el)
	{
		//Pour chaque expression de el
		for(int i = 0; i< el.size(); i++)
		{
			//On r�cup�re l'expression dans la liste d'expression
			Expr e = el.get(i);
			
			
			//Si l'expression se r�v�le �tre une liste d'expressions (contient plus qu'un �l�ment), on rapelle la m�thode 
			if(e instanceof ExprList)
			{
				//Cette expression est �gale � l'appel de la methode changeMethod
				el.set(i, this.convertArgs((ExprList)e));
			}
			else
			{
				String s = new String("");
				
				//On v�rifie si l'expression est une expression point�e
				String[] arrOfStr = e.toString().split("\\.");
				
				
				//Si l'expression est une expression point�e, on la convertit une listExpression contenant les expression s�par�es
				if(arrOfStr.length>1)
				{
					//Cr�ation de la nouvelle expression
					
					//On modifie les valeurs des arguments existants dans la liste chain�e en regrdant dans le Hashmap d'arguments
					
					//On recr�e une expression avec les valeurs des arguments
					
					String stock = "";
					
					//On v�rifie si le premier �l�ment est un argument
					stock = this.args.get(arrOfStr[0]);
					
					//Si il est un argument
					if(stock!=null)
					{
						//On ajoute sa valeur � la string s
						s += stock;
					}
					else
					{
						//Sinon a on ajoute l'expression de base � la string s
						s += arrOfStr[0].toString();
					}
					
					
					for(int j=1; j<arrOfStr.length; j++)
					{
						//On refait la m�m echose que au dessus sauf que nous ajoutons le caract�re '.' � l'�lement que nous concatenons pour recr�er la liste chain�e
						stock = this.args.get(arrOfStr[j]);
						if(stock!=null)
						{
							s += ("." + stock);
						}
						else
						{
							s += ("." + arrOfStr[j].toString());
						}
						
						
					}
					
					//On modifie la liste d'expression
					el.set(i,fromStringToExpr(s));
				}
				//Si ce n'est pas une expression point�e
				else
				{
					//On v�rifie si celle-ci est un des arguments du script

					s = this.args.get(e.toString());
					
					//Si il l'est on change l'argument par sa valeur
					if(s!=null)
					{		
						e = fromStringToExpr(s);	
					}
					el.set(i,e);
				}
				
				
			}
			
			
		}
		return el;
	}
	
	//Permet de convertir une cha�ne de caract�re en expression
	private Expr fromStringToExpr(String s)
	{
		//LispParser permettant de convertir la string en expr
		LispParser parser = new LispParser(s);
		Expr e = null;
		try 
		{	//Conversion
			e = parser.parseExpr();
		} catch (Exception e1) 
		{
			e1.printStackTrace();
		}
		
		//Renvoie de l'expression
		return e;
		
	}
	@Override
	public Expr run(Reference receiver, ExprList method) 
	{
		//Iterator pour parcourir l'expression;
		
		
		ExprList script = receiver.getScriptByName(method.get(1).toString());
		if(script ==null)
		{
			System.out.println("Le script " + method.get(1).toString() + " n'existe pas" );
			return null;
		}
		
		//It�rateur pour r�cup�rer les valeurs souhait�es 
		Iterator<Expr> it = script.iterator();
			
		//Si il n'y a rien dans le script
		if(!it.hasNext())
		{
			System.out.println("Le script est vide");
			return null;
		}

		// Liste des arguments entr�s par l'utilisateur
		ExprList callArgs = new ExprList();
		
		//Ajout du receiver comme premier argument du script
		callArgs.add(method.get(0));
		
		//On r�cup�re les autres arguments entr�es lors de l'appel du script 
		for(int i=2; i<method.size(); i++)
		{
			callArgs.add(method.get(i));
		}
		
		//On r�cup�re les arguments originaux du script
		ExprList scriptArgList = (ExprList) it.next();
		
		
		//Si le nombre d'argument donn�e par l'utilisateur ne correspond pas au nombre d'arguments attendus par le script
		if(callArgs.size()!= scriptArgList.size())
		{
			System.out.println("Mauvais nombre d'argument lors de l'appel du script " + method.get(1).toString());
			return null;
		}
		
		
		//On met dans le hashmap les arguments
		for(int i = 0; i< callArgs.size(); i++)
		{
			this.args.put(scriptArgList.get(i).toString(), callArgs.get(i).toString());
		}
				
		//Si il n'y aucune commande dans le script
		if(!it.hasNext())
		{
			System.out.println("Le script n'as pas de commandes");
			return null;
		}
		
		
		//On r�cup�re les commandes
		ExprList commandes = (ExprList) it.next();
		
		
		//On modifie les commandes du script en changeant les noms des arguments par leur valeur		
		
		commandes = this.convertArgs(commandes);
		
		for(Expr e : commandes)
		{
			new Interpreter().compute(environment, (ExprList)e);
		}
		
		return null;
	}
		
	
}
