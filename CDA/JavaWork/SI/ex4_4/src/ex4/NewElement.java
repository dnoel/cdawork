package ex4;

import graphicLayer.GElement;
import jfkbits.ExprList;
import jfkbits.LispParser.Expr;

class NewElement implements Command 
{
	public Expr run(Reference reference, ExprList method) {
		try 
		{
			@SuppressWarnings("unchecked")
			GElement e = ((Class<GElement>) reference.getReceiver()).getDeclaredConstructor().newInstance();
			Reference ref = new Reference(e);
			ref.addCommand("setColor", new SetColor());
			ref.addCommand("translate", (Command) new TranslateElement());
			ref.addCommand("setDim", new SetDim());
			return ref;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void run(Object receiver, ExprList method) {
		// TODO Auto-generated method stub
		
	}
}