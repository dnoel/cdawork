package ex4;

import java.util.Arrays;

//Classe h�rit�e par Reference pour pouvoir faire la suppression en cascade d'�lements
public abstract class ContentManagement 
{
	String[] content = new String[0];
	
	//Rajoute le nom d'un �l�ment dans la r�f�rence
	public void addContent(String element)
	{
		content = Arrays.copyOf(content, content.length+1);
		content[content.length-1] = element;
	}
	
	//Rajoute le nom d'un �l�ment dans la r�f�rence
	public String[] getContent()
	{
		return this.content;
	}
}
