package ex4;

import java.awt.Dimension;

import graphicLayer.GImage;
import graphicLayer.GOval;
import graphicLayer.GRect;
import graphicLayer.GSpace;
import graphicLayer.GString;
import jfkbits.ExprList;
import jfkbits.LispParser;
import jfkbits.LispParser.Expr;
import tools.Tools;

public class Exercice4_4 {
	// Une seule variable d'instance
	static Environment environment = new Environment();

	public Exercice4_4() 
	{
		GSpace space = new GSpace("Exercice 4", new Dimension(500, 500));
		space.open();

		Reference spaceRef = new Reference(space);
		Reference rectClassRef = new Reference(GRect.class);
		Reference ovalClassRef = new Reference(GOval.class);
		Reference imageClassRef = new Reference(GImage.class);
		Reference stringClassRef = new Reference(GString.class);

		spaceRef.addCommand("setColor", new SetColor());
		spaceRef.addCommand("sleep", new Sleep());
		spaceRef.addCommand("setDim", new SetDim());
		
		spaceRef.addCommand("add", new AddElement(environment));
		spaceRef.addCommand("del", new DelElement(environment));
		spaceRef.addCommand("addScript", new AddScript(environment));
		spaceRef.addCommand("delScript", new DelScript(environment));
		spaceRef.addCommand("runScript", new RunScript(environment));

		
		rectClassRef.addCommand("new", new NewElement());
		ovalClassRef.addCommand("new", new NewElement());
		imageClassRef.addCommand("new", new NewImage());
		stringClassRef.addCommand("new", new NewString());

		environment.addReference("space", spaceRef);
		environment.addReference("rect.class", rectClassRef);
		environment.addReference("oval.class", ovalClassRef);
		environment.addReference("image.class", imageClassRef);
		environment.addReference("label.class", stringClassRef);
		

	}

	
	//M�thode appel�e par la classe Test, ce changement permet de lancer des tests tout en gardant la possibilit� d'utiliser manuellement la classe Exercice4_4.java
	public void executeTest(String script)
	{
		try 
		{	
			LispParser parser = new LispParser(script);
			Expr e = parser.parseExpr();
			if (e instanceof ExprList) {
				ExprList compiled = (ExprList) e;
				new Interpreter().compute(environment, compiled);
			}
		} catch (Exception e1) {
			System.out.println("Erreur lors du traitement de la commande, veuillez v�rifier sa validit�");
		}
	}
	
	public static void mainLoop() 
	{
		while (true) {
			System.out.print("> ");
			String input = Tools.readKeyboard();
			try 
			{		
				LispParser parser = new LispParser(input);
				Expr e = parser.parseExpr();
				if (e instanceof ExprList) {
					ExprList compiled = (ExprList) e;
					new Interpreter().compute(environment, compiled);
				}
			} catch (Exception e1) {
				System.out.println("Erreur lors du traitement de la commande, veuillez v�rifier sa validit�");
			}
		}
	}

	public static void main(String[] args) 
	{
		new Exercice4_4();
		Exercice4_4.mainLoop();
	}

}