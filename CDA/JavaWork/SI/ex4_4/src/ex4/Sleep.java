package ex4;

import jfkbits.ExprList;
import jfkbits.LispParser.Expr;
import tools.Tools;

//Fais faire une pause � la fen�tre 
public class Sleep implements Command{

	@Override
	public Expr run(Reference receiver, ExprList method)
	{
		//Utilisation de la m�thode sleep de la classe Tools
		int time = Integer.parseInt(method.get(2).toString());
		
		//Si le temps donn� est n�gatif 
		if(time<=0) 
		{
			System.out.println("Veuillez entrer une valeur positive pour le sleep");
			return null;
		}
		//On lance le sleep
		Tools.sleep(time);
		return null;
	}

}
