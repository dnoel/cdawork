package ex3;

import java.awt.Point;

import graphicLayer.GRect;

public class RobiTranslate implements Command
{
	GRect robi;
	Point p;
	
	public RobiTranslate(GRect robi, Point p) 
	{
	
		this.robi = robi;
		this.p = p;
	}
	@Override
	public void run() 
	{
		robi.translate(p);
		
	}

}
