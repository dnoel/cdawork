package ex3;

import java.awt.Color;

import graphicLayer.GSpace;

public class SpaceChangeColor implements Command 
{	
	GSpace space;
	Color newColor;
	public SpaceChangeColor(GSpace space, Color newColor) {
	
		this.space = space;
		this.newColor = newColor;
	}
	
	@Override
	public void run()
	{
		this.space.setColor(this.newColor);
	}
}
