package ex3;

import tools.Tools;

public class SpaceSleep  implements Command 
{
	int time;
	
	public SpaceSleep(int time) {
		
		this.time = time;
	}

	@Override
	public void run() 
	{
		Tools.sleep(this.time);
	}
}
