package ex3;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.lang.reflect.Field;
import java.util.Iterator;

import graphicLayer.GRect;
import graphicLayer.GSpace;
import jfkbits.ExprList;
import jfkbits.LispParser;
import jfkbits.LispParser.Expr;
import jfkbits.LispParser.ParseException;

public class Exercice3 
{
	GSpace space = new GSpace("Exercice 3", new Dimension(200, 100));
	GRect robi = new GRect();
	String init = "(script (space color white) (robi color red))";
	String animLoop = "(script (robi translate 10 0) (space sleep 100) (robi translate 0 10) (space sleep 100) (robi translate -10 0) (space sleep 100) (robi translate 0 -10) (space sleep 100))";


	public Exercice3() 
	{
		space.addElement(robi);
		space.open();
		this.runScript();
	}

	private void runScript() 
	{
		LispParser parser = new LispParser(init);
		try 
		{
			ExprList result = (ExprList) parser.parseExpr();
			Iterator<Expr> itor = result.iterator();
			itor.next(); // eat the "script" keyword atom
			while (itor.hasNext()) {
				this.run((ExprList) itor.next());
			}
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		while(true)
		{
			parser = new LispParser(animLoop);			
			try 
			{
				ExprList result = (ExprList) parser.parseExpr();
				Iterator<Expr> itor = result.iterator();
				itor.next(); // eat the "script" keyword atom
				
				while (itor.hasNext()) 
				{
					this.run((ExprList) itor.next());
				}
				
			} 
			catch (jfkbits.LispParser.ParseException e) 
			{
				e.printStackTrace();
			}
		}
	}

	private void run(ExprList expr) {
		Command cmd = getCommandFromExpr(expr);
		if (cmd == null) throw new Error("unable ti get command for: " + expr);
		cmd.run();
	}
	
	private Color getColor(String color)
	{
		try 
		{
			
		    Field field = Class.forName("java.awt.Color").getField(color);
		    return (Color)field.get(null);
		} catch (Exception e) {
		    return null; // Not defined
		}
	}
	

	//Traite les commandes
	Command getCommandFromExpr(ExprList expr) 
	{	
		//It�rateur permettant de parcourir l'expression
		Iterator<Expr> itor = expr.iterator();

		//Tant qu'il y a une commande � lire, on la traite
		String n = itor.next().toString();
		if(n.equals("space"))
		{
			n = itor.next().toString();
			if(n.equals("color"))
			{
				return new SpaceChangeColor(space, this.getColor(itor.next().toString()));
			}
			
			if(n.equals("sleep"))
			{
				return new SpaceSleep(Integer.parseInt(itor.next().toString()));								
			}
		}
		
		if(n.equals("robi"))
		{
			n = itor.next().toString();
			
			if(n.equals("color"))
			{
				return new RobiChangeColor(robi, this.getColor(itor.next().toString()));
			}
			
			if(n.equals("translate"))	
			{
				Point p = new Point(Integer.parseInt(itor.next().toString()), Integer.parseInt(itor.next().toString()));
				return new RobiTranslate(robi, p);
			}
		}
		
		return null;
	}
	
	
	public static void main(String[] args) {
		new Exercice3();
	}

}
