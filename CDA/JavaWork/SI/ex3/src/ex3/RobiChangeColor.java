package ex3;

import java.awt.Color;

import graphicLayer.GRect;

public class RobiChangeColor implements Command
{
	GRect robi;
	Color newColor;
	public RobiChangeColor(GRect robi, Color newColor) 
	{
	
		this.robi = robi;
		this.newColor = newColor;
	}
	
	@Override
	public void run()
	{
		this.robi.setColor(this.newColor);
	}
}
