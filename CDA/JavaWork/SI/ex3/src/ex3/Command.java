package ex3;

public interface Command 
{
	abstract public void run();
}
