package ex2_2;

import java.awt.Dimension;
import java.awt.Point;
import java.util.Iterator;

import graphicLayer.GRect;
import graphicLayer.GSpace;
import jfkbits.ExprList;
import jfkbits.LispParser;
import jfkbits.LispParser.Expr;
import tools.Tools;

public class Exercice2_2 
{
	GSpace space = new GSpace("Exercice 2_1", new Dimension(200, 100));
	GRect robi = new GRect();
	
	//Script pour initier l'environnement
	String init = "(script (space color white) (robi color red))";
	
	//Script pour animer en boucle robi
	String animLoop = "(script (robi translate 10 0) (space sleep 100) (robi translate 0 10) (space sleep 100) (robi translate -10 0) (space sleep 100) (robi translate 0 -10) (space sleep 100))";
	
	public Exercice2_2() 
	{
		space.addElement(robi);
		space.open();
		this.runScript();
	}
	
	//Initialisation de l'environnement en utilisant la chaine script
	private void runScript() 
	{
		LispParser parser = new LispParser(init);
		ExprList result;
		
		try 
		{
			result = (ExprList) parser.parseExpr();
			Iterator<Expr> itor = result.iterator();
			itor.next(); // eat the "script" keyword atom
			
			while (itor.hasNext()) 
			{
				this.runCommand((ExprList) itor.next());
			}
			
		} 
		catch (jfkbits.LispParser.ParseException e) 
		{
			e.printStackTrace();
		}
		
		while(true)
		{
			parser = new LispParser(animLoop);			
			try 
			{
				result = (ExprList) parser.parseExpr();
				Iterator<Expr> itor = result.iterator();
				itor.next(); // eat the "script" keyword atom
				
				while (itor.hasNext()) 
				{
					this.runCommand((ExprList) itor.next());
				}
				
			} 
			catch (jfkbits.LispParser.ParseException e) 
			{
				e.printStackTrace();
			}
		}
		
	}

	
	//Execute une commande
	private void runCommand(ExprList expr)
	{
		//It�rateur permettant de parcourir l'expression
		Iterator<Expr> itor = expr.iterator();

		//Tant qu'il y a une commande � lire, on la traite
		while(itor.hasNext())
		{
			//It�rateur permettant de lire la commande
			String n = itor.next().toString();
			if(n.equals("space"))
			{
				n = itor.next().toString();
				if(n.equals("color"))
				{
					//Changement de la couleur de space
					space.setColor(Tools.getColorByName(itor.next().toString()));
					continue;
				}
				
				if(n.equals("sleep"))
				{
					//Sleep
					Tools.sleep(Integer.parseInt(itor.next().toString()));
					continue;
				}
				
				continue;						
			}
			
			if(n.equals("robi"))
			{
				n = itor.next().toString();
				
				if(n.equals("color"))
				{
					//Changement de la couleur de robi
					robi.setColor(Tools.getColorByName(itor.next().toString()));
					continue;
				}
				
				if(n.equals("translate"))	
				{
					//D�placement de robi
					Point p = new Point(Integer.parseInt(itor.next().toString()), Integer.parseInt(itor.next().toString()));
					robi.translate(p);
					continue;
				}
				continue;
			}
		}
		
	}
	public static void main(String[] args) 
	{
		new Exercice2_2();
	}
}