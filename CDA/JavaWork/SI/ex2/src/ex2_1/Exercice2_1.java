package ex2_1;

import java.awt.Dimension;
import java.util.Iterator;

import graphicLayer.GRect;
import graphicLayer.GSpace;
import jfkbits.ExprList;
import jfkbits.LispParser;
import jfkbits.LispParser.Expr;
import tools.Tools;

public class Exercice2_1 
{
	GSpace space = new GSpace("Exercice 2_1", new Dimension(200, 100));
	GRect robi = new GRect();
	String script = "(script (space setColor black) (robi setColor yellow) )";
	
	public Exercice2_1() 
	{
		space.addElement(robi);
		space.open();
		this.runScript();
	}
	
	private void runScript() 
	{
		LispParser parser = new LispParser(script);
		ExprList result;
		
		try 
		{
			result = (ExprList) parser.parseExpr();
			Iterator<Expr> itor = result.iterator();
			itor.next(); // eat the "script" keyword atom
			
			while (itor.hasNext()) 
			{
				this.runCommand((ExprList) itor.next());
			}
			
		} 
		catch (jfkbits.LispParser.ParseException e) 
		{
			e.printStackTrace();
		}
		
	}

	//Execute les commandes du script 
	private void runCommand(ExprList expr)
	{
		//It�rateur permettant de parcourir l'expression
		Iterator<Expr> itor = expr.iterator();

		//Tant qu'il y a une commande � lire, on la traite
		while(itor.hasNext())
		{
			String n = itor.next().toString();
			
			if(n.equals("space"))
			{
				if(itor.next().toString().equals("setColor"))
				{
					space.setColor(Tools.getColorByName(itor.next().toString()));
				}
				//Make a new iteration without checking all other conditions 
				continue;
			}
			
			if(n.equals("robi"))
			{
				//Changement de la couleur
				if(itor.next().toString().equals("setColor"))
				{
					robi.setColor(Tools.getColorByName(itor.next().toString()));

				}
				continue;
			}
		}
		
	}
	public static void main(String[] args) 
	{
		new Exercice2_1();
	}
}