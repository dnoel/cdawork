package exercice09;

import java.util.Arrays;

/**
 * Représente une commande de produits auprès de différents fournisseurs.
 */
public class Commande {

	// ----- Attributs -----

	LigneDeCommande[] ligneTable;
	int numero;
	static int compteur;


	// ----- Constructeur -----

	Commande()
	{
		this.ligneTable = new LigneDeCommande[0];
		this.numero = compteur;
		compteur++;
	}

	public void addLigne(Produit p, Fournisseur f, int quantite)
	{
		for(LigneDeCommande ldc : this.ligneTable)
		{
			if(ldc.mev.produit == p && ldc.mev.fournisseur == f) ldc.quantite = quantite;
			break;
		}
		LigneDeCommande ldc = new LigneDeCommande(f,p,quantite);

		if(ldc.mev!= null)
		{
			this.ligneTable = Arrays.copyOf(this.ligneTable, this.ligneTable.length + 1);
			this.ligneTable[this.ligneTable.length - 1] = ldc;
		}
	}

}