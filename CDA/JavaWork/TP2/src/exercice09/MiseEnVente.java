package exercice09;
/**
 * Représente la mise en vente d'un produit par un fournisseur.
 * <p>
 *
 * Une <i>mise en vente</i> met en relation un produit et un fournisseur qui le
 * vend et précise son prix unitaire.
 */
public class MiseEnVente {

    // ----- Attributs -----

    Produit produit;
    Fournisseur fournisseur;
    double pU;


    // ----- Constructeur -----

    MiseEnVente(Produit p, Fournisseur f, double pU)
    {
        this.produit = p;
        this.fournisseur = f;
        this.pU = pU;
    }

}
