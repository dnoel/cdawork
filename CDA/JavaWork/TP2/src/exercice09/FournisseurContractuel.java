package exercice09;

/**
 * Représente un fournisseur qui applique systématiquement une remise
 * contractuelle de taux fixe sur tous les produits qu'il met en vente.
 */
public class FournisseurContractuel extends Fournisseur {

	// ----- Attributs -----

	double remise;


	// ----- Constructeur -----

	FournisseurContractuel(String nom, double remise)
	{
		super(nom);
		this.remise = remise;
	}

	// ----- Méthodes -----

	@Override
	void addProduit(Produit p, double prix) {

		double newPrix = prix*(1-(this.remise/100));
		newPrix = (double)(int)Math.round((newPrix*100))/100;
		super.addProduit(p, newPrix);
	}

	@Override
	public String toString() {

		String result = super.toString();
		result += "(remise appliquée : " + this.remise + "%)";
		return result;
	}
}
