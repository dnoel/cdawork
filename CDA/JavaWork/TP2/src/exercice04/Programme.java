package exercice04;

/**
 * Classe de test.
 * <p>
 * 
 * Ne contient qu'une méthode main.
 */
class Programme {

	public static void affiche(Object... t) {
		for (Object o : t)
			System.out.println(o);
	}
	
	public static void main(String[] args) {

		System.out.println("\n------------------ Tests constructeurs");
		
		Produit p1 = new Produit("Dentifrice");
		Produit p2 = new Produit("Brocoli");
		Produit p3 = new Produit("Pneu");
		Produit p4 = new Produit("Banane");
		Fournisseur f1 = new Fournisseur("Régis");
		Fournisseur f2 = new Fournisseur("Bernadette");
		Fournisseur f3 = new FournisseurContractuel("Roger", 5);
		Fournisseur f4 = new Fournisseur("Mimi");
		affiche("** Produits :", p1, p2, p3, p4);
		affiche("** Fournisseurs :", f1, f2, f3, f4);
		

		System.out.println("\n------------------ Tests addFrounisseur");
		
		p1.addFournisseur(f1, 5.5);
		p1.addFournisseur(f2, 7.99);
		p1.addFournisseur(f1, 9);
		affiche("** Produits :", p1, p2, p3, p4);
		affiche("** Fournisseurs :", f1, f2, f3, f4);
		

		System.out.println("\n------------------ Tests addProduit");
		
		f3.addProduit(p2, 3.25);
		f3.addProduit(p3, 8);
		f3.addProduit(p2, 5.6);
		affiche("** Produits :", p1, p2, p3, p4);
		affiche("** Fournisseurs :", f1, f2, f3, f4);
		
	}

}

/*

------------------ Tests constructeurs
** Produits :
Dentifrice [ ]
Brocoli [ ]
Pneu [ ]
Banane [ ]
** Fournisseurs :
Régis [ ]
Bernadette [ ]
Roger [ ] (remise appliquée : 5.0%)
Mimi [ ]

------------------ Tests addFrounisseur
** Produits :
Dentifrice [ Régis (9.0 €) Bernadette (7.99 €) ]
Brocoli [ ]
Pneu [ ]
Banane [ ]
** Fournisseurs :
Régis [ Dentifrice (9.0 €) ]
Bernadette [ Dentifrice (7.99 €) ]
Roger [ ] (remise appliquée : 5.0%)
Mimi [ ]

------------------ Tests addProduit
** Produits :
Dentifrice [ Régis (9.0 €) Bernadette (7.99 €) ]
Brocoli [ Roger (5.32 €) ]
Pneu [ Roger (7.6 €) ]
Banane [ ]
** Fournisseurs :
Régis [ Dentifrice (9.0 €) ]
Bernadette [ Dentifrice (7.99 €) ]
Roger [ Brocoli (5.32 €) Pneu (7.6 €) ] (remise appliquée : 5.0%)
Mimi [ ]

 */
