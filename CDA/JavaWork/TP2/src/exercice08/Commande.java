package exercice08;

/**
 * Représente une commande de produits auprès de différents fournisseurs.
 */
public class Commande {

	// ----- Attributs -----

    LigneDeCommande[] ligneTable;
	int numero;
	static int compteur;

	
	// ----- Constructeur -----

	Commande()
    {
        this.ligneTable = new LigneDeCommande[0];
        this.numero = compteur;
        compteur++;
    }
	
}