package exercice08;

public class LigneDeCommande {

	// ----- Attributs -----

	MiseEnVente mev;
	int quantite;
	// ----- Constructeur -----

	LigneDeCommande(Fournisseur f, Produit p, int quantite)
	{
		for(MiseEnVente mev: f.mev)
		{
			if(mev.produit.equals(p))
			{
				this.mev = mev;
				this.quantite = quantite;
				break;
			}
		}

	}

	public double total()
	{
		if(this.mev == null) return 0;

		return (this.mev.pU * this.quantite);
	}

	@Override
	public String toString()
	{
		if(this.mev == null) return "Erreur";
		else return (this.quantite + " x " + this.mev.produit.designation + " (" + this.mev.fournisseur.nom + " - " +
				this.mev.pU + " euros) = " + this.quantite * this.mev.pU);
	}

}