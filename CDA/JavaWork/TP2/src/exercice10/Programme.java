package exercice10;

/**
 * Classe de test.
 * <p>
 * 
 * Ne contient qu'une méthode main.
 */
class Programme {

	public static void affiche(Object... t) {
		for (Object o : t)
			System.out.println(o);
	}
	
	public static void main(String[] args) {

		System.out.println("\n------------------ Tests constructeurs");

		Produit p1 = new Produit("Dentifrice");
		Produit p2 = new Produit("Brocoli");
		Produit p3 = new Produit("Pneu");
		Produit p4 = new Produit("Banane");
		Fournisseur f1 = new Fournisseur("Régis");
		Fournisseur f2 = new Fournisseur("Bernadette");
		Fournisseur f3 = new FournisseurContractuel("Roger", 5);
		Fournisseur f4 = new Fournisseur("Mimi");
		affiche("** Produits :", p1, p2, p3, p4);
		affiche("** Fournisseurs :", f1, f2, f3, f4);


		System.out.println("\n------------------ Tests addFrounisseur");

		p1.addFournisseur(f1, 5.5);
		p1.addFournisseur(f2, 7.99);
		p1.addFournisseur(f1, 9);
		affiche("** Produits :", p1, p2, p3, p4);
		affiche("** Fournisseurs :", f1, f2, f3, f4);


		System.out.println("\n------------------ Tests addProduit");

		f3.addProduit(p2, 3.25);
		f3.addProduit(p3, 8);
		f3.addProduit(p2, 5.6);
		affiche("** Produits :", p1, p2, p3, p4);
		affiche("** Fournisseurs :", f1, f2, f3, f4);


		System.out.println("\n------------------ Tests leMoinsCher");

		System.out.println(p1 + " -- Le moins cher : " + p1.leMoinsCher());
		System.out.println(p2 + " -- Le moins cher : " + p2.leMoinsCher());
		System.out.println(p3 + " -- Le moins cher : " + p3.leMoinsCher());
		System.out.println(p4 + " -- Le moins cher : " + p4.leMoinsCher());


		System.out.println("\n------------------ Tests Commande");

		Commande c = new Commande();
		System.out.println("** Commande vide :");
		System.out.println(c.toString());
		c.addLigne(p1, f1, 2);
		c.addLigne(p1, f2, 2);
		c.addLigne(p1, f1, 1);
		c.addLigne(p1, f3, 1);
		c.addLigne(p2, f3, 5);
		System.out.println("** Commande complète :");
		System.out.println(c.toString());
		Commande c2 = new Commande();
		System.out.println("** Nouvelle commande :");
		c2.addLigne(p1, f1, 8);
		c2.addLigne(p2, f3, 3);
		System.out.println(c2.toString());

	}

}

/*

------------------ Tests constructeurs
** Produits :
Dentifrice [ ]
Brocoli [ ]
Pneu [ ]
Banane [ ]
** Fournisseurs :
Régis [ ]
Bernadette [ ]
Roger [ ] (remise appliquée : 5.0%)
Mimi [ ]

------------------ Tests addFrounisseur
** Produits :
Dentifrice [ Régis (9.0 €) Bernadette (7.99 €) ]
Brocoli [ ]
Pneu [ ]
Banane [ ]
** Fournisseurs :
Régis [ Dentifrice (9.0 €) ]
Bernadette [ Dentifrice (7.99 €) ]
Roger [ ] (remise appliquée : 5.0%)
Mimi [ ]

------------------ Tests addProduit
** Produits :
Dentifrice [ Régis (9.0 €) Bernadette (7.99 €) ]
Brocoli [ Roger (5.32 €) ]
Pneu [ Roger (7.6 €) ]
Banane [ ]
** Fournisseurs :
Régis [ Dentifrice (9.0 €) ]
Bernadette [ Dentifrice (7.99 €) ]
Roger [ Brocoli (5.32 €) Pneu (7.6 €) ] (remise appliquée : 5.0%)
Mimi [ ]

------------------ Tests leMoinsCher
Dentifrice [ Régis (9.0 €) Bernadette (7.99 €) ] -- Le moins cher : Bernadette [ Dentifrice (7.99 €) ]
Brocoli [ Roger (5.32 €) ] -- Le moins cher : Roger [ Brocoli (5.32 €) Pneu (7.6 €) ] (remise appliquée : 5.0%)
Pneu [ Roger (7.6 €) ] -- Le moins cher : Roger [ Brocoli (5.32 €) Pneu (7.6 €) ] (remise appliquée : 5.0%)
Banane [ ] -- Le moins cher : null

------------------ Tests Commande
** Commande vide :
COMMANDE n°1
TOTAL : 0.0 €
** Commande complète :
COMMANDE n°1
- 1 x Dentifrice (Régis - 9.0 €) = 9.0 €
- 2 x Dentifrice (Bernadette - 7.99 €) = 15.98 €
- 5 x Brocoli (Roger - 5.32 €) = 26.6 €
TOTAL : 51.58 €
** Nouvelle commande :
COMMANDE n°2
- 8 x Dentifrice (Régis - 9.0 €) = 72.0 €
- 3 x Brocoli (Roger - 5.32 €) = 15.96 €
TOTAL : 87.96000000000001 €

 */
