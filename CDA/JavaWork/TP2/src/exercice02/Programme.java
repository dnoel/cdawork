package exercice02;

import java.util.Arrays;

/**
 * Classe de test.
 * <p>
 * 
 * Ne contient qu'une méthode main.
 */
class Programme {


	public static void affiche(Produit... t) {
		System.out.println("** Produits :");
		for (Produit p : t) {
			System.out.print(p.designation + " - ");
			for (MiseEnVente mev : p.mev)
				System.out.print(mev.fournisseur.nom + " (" + mev.pU + ") ; ");
			System.out.println();
		}		
	}
	
	public static void affiche(Fournisseur... t) {
		System.out.println("** Fournisseurs :");
		for (Fournisseur f : t) {
			System.out.print(f.nom + " - ");
			for (MiseEnVente mev : f.mev)
				System.out.print(mev.produit.designation + " (" + mev.pU + ") ; ");
			System.out.println();
		}		
	}

	
	public static void main(String[] args) {

		System.out.println("\n------------------ Tests constructeurs");
		
		Produit p1 = new Produit("Dentifrice");
		
		Produit p2 = new Produit("Brocoli");
		Produit p3 = new Produit("Pneu");
		Produit p4 = new Produit("Banane");
		
		Fournisseur f1 = new Fournisseur("Régis");


		
		
		
		Fournisseur f2 = new Fournisseur("Bernadette");
		Fournisseur f3 = new Fournisseur("Roger");
		Fournisseur f4 = new Fournisseur("Mimi");
		affiche(p1, p2, p3, p4);
		affiche(f1, f2, f3, f4);
		

		System.out.println("\n------------------ Tests addFrounisseur");
		
		p1.addFournisseur(f1, 5.5);
		p1.addFournisseur(f2, 7.99);
		p1.addFournisseur(f1, 9);
		affiche(p1, p2, p3, p4);
		affiche(f1, f2, f3, f4);	

		System.out.println("\n------------------ Tests addProduit");

		f3.addProduit(p2, 3.25);
		f3.addProduit(p3, 8);
		f3.addProduit(p2, 5.6);
		affiche(p1, p2, p3, p4);
		affiche(f1, f2, f3, f4);

	}

}

/*

------------------ Tests constructeurs
** Produits :
Dentifrice - 
Brocoli - 
Pneu - 
Banane - 
** Fournisseurs :
Régis - 
Bernadette - 
Roger - 
Mimi - 

------------------ Tests addFrounisseur
** Produits :
Dentifrice - Régis (9.0) ; Bernadette (7.99) ; 
Brocoli - 
Pneu - 
Banane - 
** Fournisseurs :
Régis - Dentifrice (9.0) ; 
Bernadette - Dentifrice (7.99) ; 
Roger - 
Mimi - 

------------------ Tests addProduit
** Produits :
Dentifrice - Régis (9.0) ; Bernadette (7.99) ; 
Brocoli - Roger (5.6) ; 
Pneu - Roger (8.0) ; 
Banane - 
** Fournisseurs :
Régis - Dentifrice (9.0) ; 
Bernadette - Dentifrice (7.99) ; 
Roger - Brocoli (5.6) ; Pneu (8.0) ; 
Mimi - 

 */
