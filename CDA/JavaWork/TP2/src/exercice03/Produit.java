package exercice03;

import java.util.Arrays;

/**
 * Représente un produit en vente.
 */
public class Produit {

	// ----- Attributs -----

	String designation;
	MiseEnVente[] mev;
	
	// ----- Constructeur -----

	Produit(String designation)
	{
		this.designation = designation;
		this.mev = new MiseEnVente[0];
	}

	// ----- Méthodes -----

	void addFournisseur(Fournisseur f, double prix) {
		for(MiseEnVente mev: this.mev)
		{
			if(mev.fournisseur.nom.equals(f.nom))
			{
				mev.pU = prix;
				
				for(MiseEnVente mevF : f.mev)
				{
					if(mevF.produit.designation.equals(this.designation))
					{
						mevF.pU = prix;
					}
				}
				return;
			}
		}
		
		MiseEnVente mev = new MiseEnVente(this, f, prix);
		this.mev = Arrays.copyOf(this.mev, this.mev.length +1);
		this.mev[this.mev.length-1] = mev;
		
		
		
		f.mev = Arrays.copyOf(f.mev, f.mev.length +1);
		f.mev[f.mev.length -1] = mev;
	}
	
	public String toString()
	{
		String result = '"' + this.designation + " [ ";
		for(MiseEnVente mev : this.mev)
		{
			result += mev.fournisseur.nom+ " (" + mev.pU + ") ";  
		}
		result += "]" + '"' + '\n';
		
		return result;
	}
	
}