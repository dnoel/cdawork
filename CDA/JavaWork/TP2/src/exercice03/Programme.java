package exercice03;



/**
 * Classe de test.
 * <p>
 * 
 * Ne contient qu'une méthode main.
 */
class Programme {



	
	public static void main(String[] args) {


		
		Produit p1 = new Produit("Dentifrice");
		
		Produit p2 = new Produit("Brocoli");
		Produit p3 = new Produit("Pneu");

		Fournisseur f1 = new Fournisseur("Régis");
		Fournisseur f2 = new Fournisseur("Bernadette");
		Fournisseur f3 = new Fournisseur("Roger");
		
		

		System.out.println("\n------------------ Tests addFrounisseur");
		
		p1.addFournisseur(f1, 5.5);
		p1.addFournisseur(f2, 7.99);
		p1.addFournisseur(f1, 9);


		System.out.println("\n------------------ Tests addProduit");

		f3.addProduit(p2, 3.25);
		f3.addProduit(p3, 8);
		f3.addProduit(p2, 5.6);

		System.out.println(p1.toString());
		System.out.println(p2.toString());
		System.out.println(p3.toString());
		
		System.out.println(f1.toString());
		System.out.println(f2.toString());
		System.out.println(f3.toString());

	}

}

/*

------------------ Tests constructeurs
** Produits :
Dentifrice [ ]
Brocoli [ ]
Pneu [ ]
Banane [ ]
** Fournisseurs :
Régis [ ]
Bernadette [ ]
Roger [ ]
Mimi [ ]

------------------ Tests addFrounisseur
** Produits :
Dentifrice [ Régis (9.0 €) Bernadette (7.99 €) ]
Brocoli [ ]
Pneu [ ]
Banane [ ]
** Fournisseurs :
Régis [ Dentifrice (9.0 €) ]
Bernadette [ Dentifrice (7.99 €) ]
Roger [ ]
Mimi [ ]

------------------ Tests addProduit
** Produits :
Dentifrice [ Régis (9.0 €) Bernadette (7.99 €) ]
Brocoli [ Roger (5.6 €) ]
Pneu [ Roger (8.0 €) ]
Banane [ ]
** Fournisseurs :
Régis [ Dentifrice (9.0 €) ]
Bernadette [ Dentifrice (7.99 €) ]
Roger [ Brocoli (5.6 €) Pneu (8.0 €) ]
Mimi [ ]

 */
