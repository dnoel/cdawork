package exercice03;

import java.util.Arrays;

/**
 * Représente un fournisseur.
 */
public class Fournisseur {

	// ----- Attributs -----

	String nom;
	MiseEnVente[] mev;
	
	
	// ----- Constructeur -----

	Fournisseur(String nom)
	{
		this.nom = nom;
		this.mev = new MiseEnVente[0];
	}

	// ----- Méthodes -----

	void addProduit(Produit p, double prix) {
		
		for(MiseEnVente mev: this.mev)
		{
			if(mev.produit.designation.equals(p.designation))
			{
				mev.pU = prix;
				
				for(MiseEnVente mevP : p.mev)
				{
					if(mevP.fournisseur.nom.equals(this.nom))
					{
						mevP.pU = prix;
					}
				}
				return;
			}
		}
		
		MiseEnVente mev = new MiseEnVente(p, this, prix);
		this.mev = Arrays.copyOf(this.mev, this.mev.length +1);
		this.mev[this.mev.length-1] = mev;
		
		
		
		p.mev = Arrays.copyOf(p.mev, p.mev.length +1);
		p.mev[p.mev.length -1] = mev;

	}
	
	public String toString()
	{
		String result = '"' + this.nom + " [ ";
		for(MiseEnVente mev : this.mev)
		{
			result += mev.produit.designation + " (" + mev.pU + " ) ";  
		}
		result += "]" + '"' + '\n';
		
		return result;
	}
	
}