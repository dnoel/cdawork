package exercice06;

/**
 * Représente une ligne de commande.
 */
public class LigneDeCommande {

	// ----- Attributs -----

	MiseEnVente mev;
    int quantite;
	// ----- Constructeur -----

	LigneDeCommande(Produit p, Fournisseur f, int quantite)
    {
        for(MiseEnVente mev: f.mev)
        {
            if(mev.produit.equals(p))
            {
                this.mev = mev;
                this.quantite = quantite;
                break;
            }
        }

    }

}