package exercice06;
import java.util.Arrays;
/**
 * Représente un produit en vente.
 */
public class Produit {

	// ----- Attributs -----

	String designation;
	MiseEnVente[] mev;

	// ----- Constructeur -----

	Produit(String designation)
	{
		this.designation = designation;
		this.mev = new MiseEnVente[0];
	}

	// ----- Méthodes -----

	void addFournisseur(Fournisseur f, double prix) {
		for(MiseEnVente mev: this.mev)
		{
			if(mev.fournisseur.nom.equals(f.nom))
			{
				mev.pU = prix;

				for(MiseEnVente mevF : f.mev)
				{
					if(mevF.produit.designation.equals(this.designation))
					{
						mevF.pU = prix;
					}
				}
				return;
			}
		}

		MiseEnVente mev = new MiseEnVente(this, f, prix);
		this.mev = Arrays.copyOf(this.mev, this.mev.length +1);
		this.mev[this.mev.length-1] = mev;



		f.mev = Arrays.copyOf(f.mev, f.mev.length +1);
		f.mev[f.mev.length -1] = mev;
	}

	public String toString()
	{
		String result = '"' + this.designation + " [ ";
		for(MiseEnVente mev : this.mev)
		{
			result += mev.fournisseur.nom+ " (" + mev.pU + ") ";
		}
		result += "]" + '"' + '\n';

		return result;
	}

	public Fournisseur leMoinsCher()
	{
		if(this.mev.length ==0) return null;
		Fournisseur f = this.mev[0].fournisseur;
		double p = this.mev[0].pU;

		if(this.mev.length <=1) return f;
		for(int i =1; i< this.mev.length; i++)
		{
			if(mev[i].pU< p)
			{
				p = mev[i].pU;
				f = mev[i].fournisseur;
			}
		}

		return f;
	}

}