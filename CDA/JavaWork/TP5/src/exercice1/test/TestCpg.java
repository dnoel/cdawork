package exercice1.test;

import java.util.Arrays;

import exercice1.Cpg;

public class TestCpg {

	public static void main(final String[] args) {
		Cpg cpg = new Cpg(Arrays.asList(0, 3, 2, 8, 7, 7, 9, 5, 2, 3, 0, 0));

		for (int i = 0; i < 10; i++)
			System.out.println(
					"code(" + i + ") = " + cpg.code(i) + " ; decode(" + cpg.code(i) + ") = " + cpg.decode(cpg.code(i)));
	}

}

/*
 
code(0) = 7 ; decode(7) = 0
code(1) = 1 ; decode(1) = 1
code(2) = 3 ; decode(3) = 2
code(3) = 2 ; decode(2) = 3
code(4) = 4 ; decode(4) = 4
code(5) = 5 ; decode(5) = 5
code(6) = 6 ; decode(6) = 6
code(7) = 0 ; decode(0) = 7
code(8) = 8 ; decode(8) = 8
code(9) = 9 ; decode(9) = 9

 */
