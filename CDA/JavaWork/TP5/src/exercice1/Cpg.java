package exercice1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Crypteur Pif Gadget.
 *
 * <p>
 * Un objet Cpg (<i>Crypteur Pif Gadget</i>) correspond à une liste d'octets
 * associée à une permutation quelconque de cette même liste d'octets.
 * 
 * <p>
 * Une liste de valeurs d'octets est fournie à la création. Cette liste est
 * mélangée aléatoirement. Un tableau associatif permet alors d'associer à
 * chaque élément de la liste d'origine un autre élément de la liste d'origine.
 */
@SuppressWarnings("unused") // à supprimer quand le code est complété
public class Cpg implements Serializable {

	/**
	 * Identifiant de sérialisation.
	 */
	private static final long serialVersionUID = -5014568058113026043L;

	/**
	 * Tableau associant des paires d'octets.
	 */
	private Map<Integer, Integer> code;

	/**
	 * Tableau réciproque de l'attribut code.
	 */
	private Map<Integer, Integer> decode;

	/**
	 * Constructeur de Cpg.
	 * 
	 * @param l valeurs à utiliser pour construire les tableaux associatifs.
	 */
	public Cpg(List<Integer> l) 
	{
		code= new HashMap<>();
		decode= new HashMap<>();
		
		HashSet<Integer> set=new HashSet<Integer>(l);
		ArrayList<Integer> values = new ArrayList<Integer>(set);
		ArrayList<Integer> shuffledValues = new ArrayList<Integer>(set);
		Collections.shuffle(shuffledValues);
		
		for(int i=0; i<values.size();i++)
		{
			code.put(values.get(i), shuffledValues.get(i));
			decode.put(shuffledValues.get(i), values.get(i));
		}
		return;
	}

	/**
	 * Codage d'octet.
	 * 
	 * @param c valeur d'octet à coder
	 * @return valeur codée
	 */
	public Integer code(Integer c) {
		if(code.get(c)==null) return c;
		return code.get(c);
			
//		code.forEach((k)->if(k==c) return code.get(k););
		
	}

	/**
	 * Décodage d'octet.
	 * 
	 * @param cc valeur d'octet à décoder
	 * @return valeur décodée
	 */
	public Integer decode(Integer c) {
		if(decode.get(c)==null) return c;
		return decode.get(c);
	}
}
