package exercice3.test;

import exercice3.Encrypteur;

public class TestEncrypteur {

	public static void main(final String[] args) throws InterruptedException {
		Thread t;
		final int attente = 8000; // temps d'attente avant annulation (en ms)
		final long startTime = System.currentTimeMillis();

		t = new Encrypteur("poeme.txt").start();
		// t.join(); // attente indéfinie
		t.join(attente);
		t.interrupt(); // attente maximale de "attente" s

		System.out.println();

		t = new Encrypteur("time.mp4").start();
		// t.join(); // attente indéfinie
		t.join(attente);
		t.interrupt(); // attente maximale de "attente" s

		System.out.println();

		long endTime = System.currentTimeMillis();
		System.out.println("Temps d'exécution : " + (endTime - startTime) + "ms");
	}

}

/*
 
1/3 - Chargement de poeme.txt... ok.
2/3 - Encryptage et sauvegarde... ok.
3/3 - Sauvegarde du crypteur dans poeme.txt.cpg... ok.

1/3 - Chargement de time.mp4... ok.
2/3 - Encryptage et sauvegarde... ok.
3/3 - Sauvegarde du crypteur dans time.mp4.cpg... ok.

Temps d'exécution : 1880ms
 
 */
