package exercice3.test;

import exercice3.Decrypteur;

public class TestDecrypteur {

	public static void main(final String[] args) throws InterruptedException {
		Thread t;
		final int attente = 8000; // temps d'attente avant annulation (en ms)
		final long startTime = System.currentTimeMillis();

		t = new Decrypteur("poeme.txt").start();
		// t.join(); // attente indéfinie
		t.join(attente);
		t.interrupt(); // attente maximale de "attente" s

		System.out.println();

		t = new Decrypteur("time.mp4").start();
		// t.join(); // attente indéfinie
		t.join(attente);
		t.interrupt(); // attente maximale de "attente" s

		System.out.println();

		long endTime = System.currentTimeMillis();
		System.out.println("Temps d'exécution : " + (endTime - startTime) + "ms");
	}

}

/*

1/4 - Chargement du crypteur depuis poeme.txt.cpg... ok.
2/4 - Chargement de poeme.txt... ok.
3/4 - décryptage et sauvegarde... ok.
4/4 - suppression du crypteur... ok.

1/4 - Chargement du crypteur depuis time.mp4.cpg... ok.
2/4 - Chargement de time.mp4... ok.
3/4 - décryptage et sauvegarde... ok.
4/4 - suppression du crypteur... ok.

Temps d'exécution : 1823ms

*/
