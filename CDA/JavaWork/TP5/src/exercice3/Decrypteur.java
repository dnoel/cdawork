package exercice3;

/**
 * Décryptage de fichier.
 * 
 * <p>
 * Un objet Decrypteur permet de charger un fichier crypté et un fichier
 * contenant le crypteur sérialisé correspondant, de le décrypter à l'aide de ce
 * crypteur, puis de le sauvegarder. Le crypteur utilisé est ensuite supprimé.
 */
public class Decrypteur extends Convertisseur {

	/**
	 * Constructeur.
	 * 
	 * @param fichier nom du fichier à décrypter
	 */
	public Decrypteur(String fichier) {
		super(fichier);
	}

	@Override
	public void run() {

		// TODO à compléter...

	}

}
