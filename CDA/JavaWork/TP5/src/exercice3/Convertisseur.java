package exercice3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Convertiseur de fichier.
 * 
 * <p>
 * Un convertisseur permet soit de crypter, soit de décrypter un fichier.
 */
abstract class Convertisseur {

	/**
	 * Nom du fichier à convertir.
	 */
	protected String fichier;

	/**
	 * Contenu du fichier à convertir.
	 */
	protected List<Integer> contenu;

	/**
	 * Crypteur utilisé pour crypter ou décrypter le fichier.
	 */
	protected Cpg crypteur;

	/**
	 * Indique si le fichier a déjà été converti.
	 */
	protected boolean converti;

	/**
	 * Constructeur.
	 * 
	 * @param fichier nom du fichier à convertir
	 */
	public Convertisseur(String fichier) {
		this.fichier  = fichier;
		this.contenu  = new ArrayList<>();
		this.crypteur = null;
		this.converti = false;
	}

	/**
	 * Démarre la conversion.
	 * 
	 * <p>
	 * La conversion démarre seulement si le fichier n'a pas déjà été converti.
	 */
	public abstract void start();

	/**
	 * Charge le fichier à convertir.
	 * 
	 * @throws IOException soulevée quand le fichier n'est pas accessible
	 */
	protected void load() throws IOException 
	{
		BufferedReader buffer= new BufferedReader(new FileReader(fichier));
		

		for(int c=buffer.read();c!=-1; c=buffer.read())
		{
			contenu.add(c);
		}
		
		buffer.close();	
	}

	/**
	 * Converti et sauvegarde le fichier à convertir.
	 * 
	 * @param crypte indique si la conversion correspond au cryptage du fichier
	 * @throws IOException soulevée quand le fichier n'est pas accessible
	 */
	protected void convertAndSave(boolean crypte) throws IOException 
	{
		
	}

}
