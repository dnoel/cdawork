package exercice3;

/**
 * Cryptage de fichier.
 * 
 * <p>
 * Un objet Encrypteur permet de charger un fichier quelconque et de le crypter
 * à l'aide d'un objet Cpg. L'objet Cpg utilisé est sérialisé afin qu'il puisse
 * être réutilisé lors du décryptage.
 */
public class Encrypteur extends Convertisseur {

	/**
	 * Constructeur.
	 * 
	 * @param fichier nom du fichier à crypter
	 */
	public Encrypteur(String fichier) {
		super(fichier);
	}

	@Override
	public void run() {

		// TODO à compléter...

	}

}
