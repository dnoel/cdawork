package exercice2.test;

import exercice2.Decrypteur;

public class TestDecrypteur {

	public static void main(final String[] args) throws InterruptedException {
		final long startTime = System.currentTimeMillis();

		new Decrypteur("poeme.txt").start();

		System.out.println();

		new Decrypteur("time.mp4").start();

		System.out.println();

		long endTime = System.currentTimeMillis();
		System.out.println("Temps d'exécution : " + (endTime - startTime) + "ms");
	}

}

/*

1/4 - Chargement du crypteur depuis poeme.txt.cpg... ok.
2/4 - Chargement de poeme.txt... ok.
3/4 - décryptage et sauvegarde... ok.
4/4 - suppression du crypteur... ok.

1/4 - Chargement du crypteur depuis time.mp4.cpg... ok.
2/4 - Chargement de time.mp4... ok.
3/4 - décryptage et sauvegarde... ok.
4/4 - suppression du crypteur... ok.

Temps d'exécution : 1823ms

*/
