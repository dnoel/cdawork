package exercice2.test;

import exercice2.Encrypteur;

public class TestEncrypteur {

	public static void main(final String[] args) throws InterruptedException {
		final long startTime = System.currentTimeMillis();

		new Encrypteur("poeme.txt").start();

		System.out.println();

		new Encrypteur("time.mp4").start();

		System.out.println();

		long endTime = System.currentTimeMillis();
		System.out.println("Temps d'exécution : " + (endTime - startTime) + "ms");
	}

}

/*
 
1/3 - Chargement de poeme.txt... ok.
2/3 - Encryptage et sauvegarde... ok.
3/3 - Sauvegarde du crypteur dans poeme.txt.cpg... ok.

1/3 - Chargement de time.mp4... ok.
2/3 - Encryptage et sauvegarde... ok.
3/3 - Sauvegarde du crypteur dans time.mp4.cpg... ok.

Temps d'exécution : 1880ms
 
 */
