package server;
import java.io.PrintStream;

public class CommandeBYE extends Commande {
	
	public CommandeBYE(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	public void execute(CommandExecutor ce) 
	{

			this.ps.println("0 Déconnexion du serveur");		
	}

}
