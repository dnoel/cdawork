package server;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class CommandeUSER extends Commande {
	
	public CommandeUSER(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	public void execute(CommandExecutor ce) 
	{
		ce.userOk = false;
		ce.pwOk = false;
		File f = new File(System.getProperty("user.dir").toString() + "/" + commandeArgs[0]);
		// Ce serveur accepte uniquement le user personne
		if(!f.exists())
		{
			System.out.println("Utilisateur " + commandeArgs[0] + " Introuvable");
			ps.println("2 Le user " + commandeArgs[0] + " n'existe pas");
			return;
		}
		
		ce.userOk = true;
		try 
		{
			ce.userPath = f.getCanonicalPath();
			ce.currentPath = ce.userPath.toString();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.ps.println("0 User valide");
		
	}

}
