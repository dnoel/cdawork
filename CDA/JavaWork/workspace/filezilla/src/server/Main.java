package server;
/*
 * TP JAVA RIP
 * Min Serveur FTP
 * */

import java.net.ServerSocket;
import java.net.Socket;

public class Main 
{
	
	public static void main(String[] args) throws Exception 
	{
		
		while(true)
		{
			ServerSocket serveurFTP = new ServerSocket(2121);
			System.out.println("Le Serveur FTP");
			
			System.out.println("Attente d'une connexion");
			Socket socket = serveurFTP.accept();
			System.out.println("Connexion établie");
			
			Thread t = new Thread(new ClientHandler(socket));
			t.start();
			serveurFTP.close();
			
		}
	}
}
