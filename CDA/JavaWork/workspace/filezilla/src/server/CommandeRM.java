package server;
import java.io.File;
import java.io.PrintStream;

public class CommandeRM extends Commande {
	
	public CommandeRM(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	public void execute(CommandExecutor ce) 
	{
		
		File f = new File(ce.currentPath+ "/" + this.commandeArgs[0]);
	      //Creating the directory	
	      if(!f.exists())
	      {
	          System.out.println("Le fichier � supprimer n'existe pas");
	          ps.println("2 Le fichier � supprimer n'existe pas");
	          return;
	      }
	      
	      
	      
	      if(f.delete()) {
	    	  System.out.println("fichier " + f.getName() + " supprim� avec succ�s");
		      ps.println("0 fichier" + f.getName() + "supprim�");
	       } else {
	          System.out.println("Probl�me de suppression");
	          ps.println("2 Echec de la suppression");
	       } 
	      
	}

}
