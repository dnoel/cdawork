package server;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class CommandeSTOR extends Commande 
{
	public CommandeSTOR(PrintStream ps, String commandeStr)
	{
		super(ps, commandeStr);
	}

	public void execute(CommandExecutor ce) throws IOException 
	{
		try 
		{
			ce.fileSharing = new ServerSocket(0);
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		System.out.println("Envoie du port du socket de transmission de fichiers : "+ ce.fileSharing.getLocalPort());
		ps.println("0 " + ce.fileSharing.getLocalPort());
		//Ouverture du socket pour le partage des fichiers
		Socket socket = ce.fileSharing.accept();
		
		byte[] buffer = new byte[packetSize]; 
		File f = new File(ce.currentPath +  "/" +  commandeArgs[0]);
		
		InputStream in = socket.getInputStream();
		FileOutputStream out = new FileOutputStream(f);
		
		
		int count=1;
		while ((count = in.read(buffer)) >0)
		{
		  out.write(buffer, 0, count);
		}
			
			System.out.println("Ecriture du contenu du fichier termin�e");
			in.close();
			out.close();
			ce.fileSharing.close();
		
		
		
	}

}
