package server;
import java.io.File;
import java.io.PrintStream;

public class CommandeRENAME extends Commande {
	
	public CommandeRENAME(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	public void execute(CommandExecutor ce) 
	{
		
		File f = new File(ce.currentPath+ "/" + this.commandeArgs[0]);
	      //Creating the directory	
	      if(!f.exists())
	      {
	          System.out.println("Le fichier � renommer n'existe pas");
	          ps.println("2 Le fichier � renommer n'existe pas");
	          return;
	      }
	      
	      File f2 = new File(ce.currentPath+ "/" +this.commandeArgs[1]);
	      
	      
	      if(f.renameTo(f2)) {
	    	  System.out.println("fichier " + f.getName() + "renomm� avec succ�s");
		      ps.println("0 fichier renomm� en " + f.getName());
	       } else {
	          System.out.println("Rename failed");
	          ps.println("2 Echec du renommage");
	       } 
	      
	}

}
