package server;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;

public class CommandExecutor {
	
	public  boolean userOk;
	public  boolean pwOk;
	public  ServerSocket fileSharing;
	public  final String originalPath = new File("").getAbsolutePath();
	public  String userPath = new String("");
	public  String currentPath = new String(originalPath.toString());
	public  int packetSize;
	
//	public ServerSocket fileSharing;
//	public final String originalPath = new File("").getAbsolutePath();
//	public String currentPath = new String(originalPath.toString());
//	public int packetSize = 100;
//	public PrintStream ps;
//	public String commandeNom = "";
//	public String [] commandeArgs ;
	
	public CommandExecutor()
	{
		this.userOk = false;
		this.pwOk = false;
		
	}
	
	public void executeCommande(PrintStream ps, String commande) throws IOException 
	{
		if(this.userOk && this.pwOk) 
		{
			// Changer de repertoire. Un (..) permet de revenir au repertoire superieur
			
			if(commande.split(" ")[0].equals("cd")) new CommandeCD(ps, commande).execute(this);
	
			// Telecharger un fichier
			if(commande.split(" ")[0].equals("get")) (new CommandeGET(ps, commande)).execute(this);
			
			// Afficher la liste des fichiers et des dossiers du repertoire courant
			if(commande.split(" ")[0].equals("ls")) (new CommandeLS(ps, commande)).execute(this);
		
			// Afficher le repertoire courant
			if(commande.split(" ")[0].equals("pwd")) (new CommandePWD(ps, commande)).execute(this);
			
			// Envoyer (uploader) un fichier
			if(commande.split(" ")[0].equals("store")) (new CommandeSTOR(ps, commande)).execute(this);
			
			if(commande.split(" ")[0].equals("bye")) (new CommandeBYE(ps, commande)).execute(this);
			
			if(commande.split(" ")[0].equals("mkdir")) new CommandeMKDIR(ps, commande).execute(this);
			if(commande.split(" ")[0].equals("touch")) new CommandeTOUCH(ps, commande).execute(this);
			if(commande.split(" ")[0].equals("rename")) new CommandeRENAME(ps, commande).execute(this);

		}
		else 
		{
			if(commande.split(" ")[0].equals("pass") || commande.split(" ")[0].equals("user")) 
			{
				// Le mot de passe pour l'authentification
				if(commande.split(" ")[0].equals("pass"))
				{
					new CommandePASS(ps, commande).execute(this);
				}
	
				// Le login pour l'authentification
				if(commande.split(" ")[0].equals("user")) 
				{
					new CommandeUSER(ps, commande).execute(this);
				}
				
			}
			else
			{
				ps.println("2 Vous n'êtes pas connecté !");
			}
		}
	}

}
