package application;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class TableFile {
	
	private SimpleStringProperty name;
	private SimpleIntegerProperty size;
	private SimpleBooleanProperty folder;
	private SimpleStringProperty lastModified;
	
	//file to go to previous folder
	TableFile()
	{
		this.name = new SimpleStringProperty("..");
		this.size = new SimpleIntegerProperty(0);
		this.folder = new SimpleBooleanProperty(true);
		this.lastModified = new SimpleStringProperty("");
	}
	
	//Constructor to create all files
	TableFile(String name, int size, Boolean folder, String lastModified)
	{
		this.name = new SimpleStringProperty(name);
		this.size = new SimpleIntegerProperty(size);
		this.folder = new SimpleBooleanProperty(folder);
		this.lastModified = new SimpleStringProperty(lastModified);
	}
	
	public SimpleStringProperty getName()
	{
		return this.name;
	}
	
	public SimpleIntegerProperty getsize()
	{
		return this.size;
	}
	
	public SimpleBooleanProperty getFolder()
	{
		return this.folder;
	}
	
	public SimpleStringProperty getlastModified()
	{
		return this.lastModified;
	}
	
	

}
