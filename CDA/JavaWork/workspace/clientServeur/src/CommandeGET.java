import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;


public class CommandeGET extends Commande 
{
	
	public CommandeGET(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
		packetSize = 100;
	}

	public void execute(CommandExecutor ce) throws IOException
	{
		File f = new File(ce.currentPath + "/" + commandeArgs[0]);
		if(!f.exists())
		{
			System.out.println("Le fichier demandé n'existe pas");
			ps.println("2 Le fichier demandé n'existe pas");
			return;
		}
		if(f.isDirectory())
		{
			System.out.println("Le fichier demandé est un répertoire -> REFUS");
			ps.println("2 Demande de copie de répertoire -> REFUS");
			return;
		}
		
		System.out.println("Le fichier existe et n'est pas un répertoire");
		ce.fileSharing = new ServerSocket(0);
		System.out.println("Envoie du port du socket de transmission de fichiers : "+ ce.fileSharing.getLocalPort());
		ps.println("0 " + ce.fileSharing.getLocalPort());
		//Ouverture du socket pour le partage des fichiers
		Socket socket= ce.fileSharing.accept();
		
		//Envoie des informations au client	
		
		byte[] buffer = new byte[packetSize]; 
		FileInputStream in = new FileInputStream(f);
		
		OutputStream out = socket.getOutputStream();;
		int count;
		while ((count = in.read(buffer)) > 0)
		{
		  out.write(buffer, 0, count);
		}
		
		System.out.println("Fin d'envoie du contenu des fichiers");
		in.close();
		out.close();
	}
}
