import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class CommandeCD extends Commande 
{
	
	public CommandeCD(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	public void execute(CommandExecutor ce)
	{
		
		File file = new File(ce.currentPath + "/" + this.commandeArgs[0]);
		if(!file.exists()) 
		{
			System.out.println("Le répertoir n'existe pas");
			ps.println("2 Le répertoire n'existe pas");
			return;

		}
		
		String filePath = "";
		try 
		{
			filePath = file.getCanonicalPath().toString();
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("filePath : " + filePath);
		
		//Si la nouvelle path est au moins de la taille de la path originale 
		if((filePath.length() >=ce.userPath.length()))
		{
			//Si celle-ci commence bine par la path originale 
			if(filePath.substring(0, ce.userPath.length()).equals(ce.userPath))
			{
				//On change la currentPath et on la renvoie au client
				try {
					ce.currentPath = file.getCanonicalPath().toString();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				ps.println("0 " + ce.currentPath);
				return;
			}
		}
		
		//Si le répertoire demandé n'est pas accessible
		System.out.println("Tentative de dépasser le répertoire original");
		ps.println("2 Vous ne pouvez dépasser le répertoir original du serveur");
		return;

	}
}
