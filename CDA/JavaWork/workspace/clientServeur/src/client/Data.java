package client;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Data 
{

	public String serverPATH = new String("");
	public String clientPATH = "C:\\Damien\\workspace\\clientServeur\\clientPATH";
	public Socket sock;
	public Socket fileSharing;
	public int packetSize = 100;
	public InetAddress addr;
	public Scanner scanner;
	public PrintStream ps;
	public BufferedReader br;
	public String reponse;
	public String requete;
	public String[] serverDirectory;
	public String[] clientDirectory;
	
	public Data()
	{
		this.serverPATH = new String("");
		this.clientPATH = "C:\\Damien\\workspace\\clientServeur\\clientPATH";
		this.sock = null;
		this.fileSharing = null;
		this.packetSize = 100;
		this.addr = null;
		this.scanner = null;
		this.ps = null;
		this.br = null;
		this.reponse = null;
		this.requete = null;
		this.serverDirectory = null;
		this.clientDirectory = null;
		
	}
}
