import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class CommandeTOUCH extends Commande {
	
	public CommandeTOUCH(PrintStream ps, String commandeStr) 
	{
		super(ps, commandeStr);
	}

	public void execute(CommandExecutor ce) 
	{
		
		File f = new File(ce.userPath+ "/" + this.commandeArgs[0]);
	      //Creating the directory	
	      if(f.exists())
	      {
	          System.out.println("Le fichier existe d�j�");
	          ps.println("2 Le fichier existe d�j�");
	          return;
	      }
	      
	      try 
	      {
			f.createNewFile();
	      } 
		  catch (IOException e) 
		  {
			// TODO Auto-generated catch block
			ps.println("2 " + e.toString());
		  }
	      System.out.println("fichier " + f.getName() + "cr�e avec succ�s");
	      ps.println("fichier " + f.getName() + "cr�e avec succ�s");
	}

}
