package exercice6;

public class ComparateurInteger implements Comparateur{
	
	@Override
	public Integer compare(Object o1, Object o2)
	{
		if(!(o1 instanceof Integer) || !(o2 instanceof Integer)) return null;
		
		return (Integer)o1-(Integer)o2;
		
	}
	
}