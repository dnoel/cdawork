package exercice9;

/**
 * Représente un ensemble d'objets quelconques grâce à une chaîne d'objets.
 * <p>
 * 
 * Les éléments de l'ensemble sont stockés dans des chaînons reliés entre eux
 * par un simple chaînage.
 * <p>
 * 
 * La classe stocke la référence du chaînon de début de chaîne ainsi que la
 * taille de la chaîne (<i>i.e.</i> nombre d'éléments dans l'ensemble).
 */
public class ChaineEnsemble implements Ensemble 
{

	/**
	 * Représente un chaînon de la chaîne avec une valeur stockée et la référence du
	 * chaînon suivant dans la chaîne.
	 */
	@SuppressWarnings("unused")
	private static class Chainon implements Cloneable
	{

		/**
		 * Référence de l'objet stocké.
		 */
		Object val;

		/**
		 * Référence du chaînon suivant dans la chaîne.
		 */
		Chainon next;

		/**
		 * Initialise un chaînon à partir d'un objet à stocker et de la référence du
		 * chaînon suivant.
		 * 
		 * @param val  référence de l'objet à stocker
		 * @param next référence du chaînon suivant (peut être null)
		 */
		Chainon(Object val, Chainon next) 
		{
			this.val = val;
			this.next = next;
			
		}	
		
		public Chainon clone()
		{
			try 
			{
				return (Chainon)super.clone();
				
			}
			catch (CloneNotSupportedException e) 
			{
				return null;
			}
			
		}
	}

	/**
	 * Représente un itérateur sur un ensemble de type ChaineEnsemble.
	 * <p>
	 * 
	 * L'itérateur est matérialisé par référence du prochain chaînon à lire dans la
	 * chaîne.
	 */
	@SuppressWarnings("unused")
	private class ChaineEnsembleIterateur implements Iterateur 
	{

		// ----- Attributs -----

		Chainon next;
		
		// ----- Constructeur -----

		ChaineEnsembleIterateur()
		{
			this.next = debut;
		}

		// ----- Méthodes -----

		@Override
		public boolean hasNext() 
		{
			
			if (this.next!=null) return true;
			return false; 
		}

		@Override
		public Object next() 
		{
			if(this.hasNext())
			{
				Object o = this.next.val;
				this.next = this.next.next;
				return o;
			}
			return null;
		}

	}

	// ----- Attributs -----

	private Chainon debut;
	private int size;
	
	// ----- Constructeur -----
	public ChaineEnsemble()
	{
		this.debut = null;
		this.size=0;
	}

	// ----- Méthodes -----

	@Override
	public void add(Object o) 
	{
		if(o == null || this.contains(o)) return;
		
		Chainon c = new Chainon(o, this.debut);
		this.debut = c;
		this.size++;
	}

	@Override
	public void remove(Object o) 
	{
		if(o == null || this.size == 0) return;
		
		if(this.debut.val == o) 
		{
			this.debut = this.debut.next;
			this.size--;
			return;
			
		}
		
		Chainon previousC= this.debut;
		Chainon currentC = this.debut.next;	
		
		while(currentC!=null)
		{
			if(currentC.val == o)
			{
				previousC.next = currentC.next;
				this.size--;
				break;
			}
			previousC=currentC;
			currentC=currentC.next;
		}	
		
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean contains(Object o) 
	{
		Chainon c = this.debut;
		
		while(c!=null)
		{
			if(c.val == o) return true;
			c=c.next;
		}
		return false ;	
	}

	@Override
	public ChaineEnsembleIterateur getIterateur() {
		ChaineEnsembleIterateur it = new ChaineEnsembleIterateur();
		return it;
	}

	@Override
	public Object min(Comparateur c) 
	{
		ChaineEnsembleIterateur it = this.getIterateur();
	
		if(it.next==null) return this.debut;
		
		Object res = this.debut.val;
		Object o = it.next.val;
		
		while(o!=null)
		{
			Integer in = c.compare(res, o);
			if(in == null) return null;
			
			if(in>0)
			{
				res = o;
			}
			
			o = it.next();
			
		}
		return res;
	}

	@Override
	public Object max(Comparateur c) 
	{
		
		ChaineEnsembleIterateur it = this.getIterateur();
		
		if(it.next==null) return this.debut;
		
		Object res = this.debut.val;
		Object o = it.next.val;
		
		while(o!=null)
		{
			Integer in = c.compare(res, o);
			
			if(in == null) return null;			
			if(in<0)
			{
				res = o;
			}
			
			o = it.next();
			
		}
		
		return res;
	}

	@Override
	public String toString() 
	{
		String res = "(";
		Chainon c = this.debut;
		
		while(c!=null)
		{
			res +=" " + c.val;
			c=c.next;
		}
		
		res+=" )";
		return res;
		
	}

	@Override
	public ChaineEnsemble clone() 
	{
		ChaineEnsemble clone = null;
		try 
		{
			clone = (ChaineEnsemble)super.clone();
		}
		catch (CloneNotSupportedException e) 
		{
			e.printStackTrace();
		}

		if(this.debut==null) return clone;
		clone.debut = this.debut.clone();
		
		for(Chainon c=clone.debut; c.next!=null; c = c.next)
		{
			c.next = c.next.clone();
		}
		return clone;
	}

}
