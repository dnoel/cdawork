package exercice9;

import java.util.Arrays;



/**
 * Représente un ensemble d'objets quelconques grâce à un tableau.
 * <p>
 * 
 * Les éléments de l'ensemble sont stockés dans un tableau réalloué par blocs.
 * Quand le tableau est plein, il est réalloué avec n cases en plus. Ces n cases
 * constituent un bloc.
 * <P>
 * 
 * De même, quand les cases libres du tableau constituent un bloc entier, le
 * tableau est réalloué avec un bloc de cases en moins.
 */
public class TabEnsemble implements Ensemble {

	// ----- Attributs -----

	 private final int TAILLEBLOC = 5;
	 private Object[] tab;
	 private int prochainIndice;
	 
	 
	 //Classe Interne 
	 
	 private class TabEnsembleIterateur implements Iterateur{
		 
		// Attributs
		 
		private int next; 
		
		
		// Constructeurs
		
		TabEnsembleIterateur()
		{
			this.next = 0;
		}
		
		//Méthodes
		
		@Override
		public boolean hasNext()
		{
			if(this.next < TabEnsemble.this.prochainIndice) return true;		
			return false;
		}
		
		@Override
		public Object next()
		{
			if(this.hasNext()) return TabEnsemble.this.tab[this.next++];
			return null;
		}
		
	}
	 
	 // Classe interne correspondant au comparateur
	 
	

	// ----- Constructeur -----
	 
	 TabEnsemble()
	 {
		 this.tab = new Object[this.TAILLEBLOC];
		 this.prochainIndice = 0;
	 }
	// ----- Méthodes -----

	@Override
	public void add(Object o) {
		if(o == null || this.contains(o)) return;
		
		if(this.prochainIndice == this.tab.length)
		{
			this.tab = Arrays.copyOf(this.tab, this.size()+ this.TAILLEBLOC);
		}
		
		this.tab[this.prochainIndice] = o;
		this.prochainIndice++;
	}

	@Override
	public void remove(Object o) {
		if(o == null || !this.contains(o)) return;
		
		for(int i=0; i<this.prochainIndice; i++)
		{
			if(o.equals(this.tab[i]))
			{
				this.tab[i] = this.tab[this.prochainIndice-1];
				this.tab[this.prochainIndice-1] = null;
				this.prochainIndice--;
				
				if(this.tab.length - this.prochainIndice == this.TAILLEBLOC)
				{
					this.tab = Arrays.copyOf(this.tab, this.tab.length-this.TAILLEBLOC);
					return;
				}
				
			}
		}
	}
	

	@Override
	public int size() {
		
		return this.prochainIndice;
	}

	@Override
	public boolean contains(Object o) {
		for(Object obj: this.tab)
		{
			if( obj != null && obj.equals(o)) return true;
		}
		return false; 
	}
	
	@Override
	public TabEnsembleIterateur getIterateur() {
		
		TabEnsembleIterateur it = new TabEnsembleIterateur();
		return it; 
	}

	@Override
	public String toString() {
		String res = "[ ";
		
		for(int i=0; i<this.prochainIndice; i++)
		{
			res+= this.tab[i] + " ";
		}
		res += " ]";
		return res;
	}
	
	public Object min(Comparateur c)
	{
		if( c == null || this.size() == 0) return null;
		
		TabEnsembleIterateur it = this.getIterateur();
		
		Object res = it.next();
		
		Object o;
		
		boolean x = true;
		while(x && it.hasNext())
		{
			o = it.next();
			
			if(o != null)
			{
				
				if(c.compare(res, o) == null) return null;
				
				if(c.compare(res, o) >0)
				{
					res = o;
				}
			}
			else
			{
				x = false;	
			}
			
		}
		
		return res;
		
	}

	/**
	 * Renvoie la plus grande valeur de l'ensemble conformément à une relation
	 * d'ordre définie par un comparateur.
	 * <p>
	 * 
	 * Tous les éléments de l'ensemble doivent être comparables selon le comparateur
	 * fourni, faute de quoi le résultat est null.
	 * 
	 * @param c comparateur à utiliser pour déterminer le max
	 * @return plus grande valeur selon c si elle existe, null sinon
	 */
	public Object max(Comparateur c)
	{
		if( c == null || this.size() == 0) return null;
		
		TabEnsembleIterateur it = this.getIterateur();
		
		Object res = it.next();
		
		Object o;
		
		boolean x = true;
		while(x && it.hasNext())
		{
			o = it.next();
			
			if(o == null) x = false;
			
			if(c.compare(res, o) == null) return null;
			
			if(c.compare(res, o) <0)
			{
				res = o;
			}
		}
		
		return res;
	}
	
	@Override
	
	public TabEnsemble clone()
	{
		TabEnsemble c = null;
		try 
		{
			c = (TabEnsemble)super.clone();
			
		}
		catch (CloneNotSupportedException e) 
		{
			e.printStackTrace();
		}
		
		c.tab = this.tab.clone();
		return c;

	}

	
	
}
