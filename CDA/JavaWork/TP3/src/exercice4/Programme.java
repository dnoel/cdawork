package exercice4;

/**
 * Classe de test.
 * <p>
 * 
 * Contient une méthode main et des fonctions de test.
 */
class Programme {

	static void affiche(Ensemble e) {
		affiche(e, null);
	}

	static void affiche(Ensemble e, Object o) {
		String contient = "";
		if (o != null)
			contient = (e.contains(o) ? ", contient " : ", ne contient pas ") + o;
		System.out.println(e.toString() + " " + e.size() + " élément" + (e.size() == 0 ? "" : "s" + contient));
	}

	static void test1(Ensemble e) {
		System.out.println("Ensemble initial :");
		affiche(e);		
		System.out.println("Ajout des entiers 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...");
		for (int i = 0; i < 10; i++)
			e.add(i);
		System.out.println("Deuxième ajout de 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...");
		for (int i = 0; i < 10; i++)
			e.add(i);
		System.out.println("Ensemble mis à jour :");
		affiche(e);
	}

	static void test2(Ensemble e) {
		System.out.println("Ensemble initial :");
		affiche(e, 9);
		System.out.println("Ajout des entiers 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...");
		for (int i = 0; i < 10; i++)
			e.add(i);
		System.out.println("Ensemble mis à jour :");
		affiche(e, 9);
		System.out.println("Suppression de 9...");
		e.remove(9);
		System.out.println("Deuxième suppression de 9...");
		e.remove(9);
		System.out.println("Ensemble mis à jour :");
		affiche(e, 9);
		affiche(e, 0);
		System.out.println("Suppression de 0...");
		e.remove(0);
		System.out.println("Deuxième suppression de 0...");
		e.remove(0);
		System.out.println("Ensemble mis à jour :");
		affiche(e, 0);
		affiche(e, 4);
		System.out.println("Suppression de 4...");
		e.remove(4);
		System.out.println("Deuxième suppression de 4...");
		e.remove(4);
		System.out.println("Ensemble mis à jour :");
		affiche(e, 4);
		System.out.println("Suppression des entiers 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...");
		for (int i = 0; i < 10; i++)
			e.remove(i);
		System.out.println("Ensemble mis à jour :");
		affiche(e, 4);
	}

	public static void main(String[] args) {

		System.out.println("\n-------------------------------------------------------------");
		System.out.println("------- Tests de constructeurs, toString, add et size -------");
		System.out.println("-------------------------------------------------------------");
		test1(new TabEnsemble());

		System.out.println("\n-------------------------------------------------------------");
		System.out.println("------- Tests de remove et contains -------------------------");
		System.out.println("-------------------------------------------------------------");
		test2(new TabEnsemble());

	}

}

/*

-------------------------------------------------------------
------- Tests de constructeurs, toString, add et size -------
-------------------------------------------------------------
Ensemble initial :
[ ] 0 élément
Ajout des entiers 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...
Deuxième ajout de 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...
Ensemble mis à jour :
[ 0 1 2 3 4 5 6 7 8 9 ] 10 éléments

-------------------------------------------------------------
------- Tests de remove et contains -------------------------
-------------------------------------------------------------
Ensemble initial :
[ ] 0 élément
Ajout des entiers 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...
Ensemble mis à jour :
[ 0 1 2 3 4 5 6 7 8 9 ] 10 éléments, contient 9
Suppression de 9...
Deuxième suppression de 9...
Ensemble mis à jour :
[ 0 1 2 3 4 5 6 7 8 ] 9 éléments, ne contient pas 9
[ 0 1 2 3 4 5 6 7 8 ] 9 éléments, contient 0
Suppression de 0...
Deuxième suppression de 0...
Ensemble mis à jour :
[ 8 1 2 3 4 5 6 7 ] 8 éléments, ne contient pas 0
[ 8 1 2 3 4 5 6 7 ] 8 éléments, contient 4
Suppression de 4...
Deuxième suppression de 4...
Ensemble mis à jour :
[ 8 1 2 3 7 5 6 ] 7 éléments, ne contient pas 4
Suppression des entiers 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...
Ensemble mis à jour :
[ ] 0 élément

*/
