package exercice2;



/**
 * Représente un ensemble d'objets quelconques grâce à un tableau.
 * <p>
 * 
 * Les éléments de l'ensemble sont stockés dans un tableau réalloué par blocs.
 * Quand le tableau est plein, il est réalloué avec n cases en plus. Ces n cases
 * constituent un bloc.
 * <P>
 * 
 * De même, quand les cases libres du tableau constituent un bloc entier, le
 * tableau est réalloué avec un bloc de cases en moins.
 */
public class TabEnsemble implements Ensemble {

	// ----- Attributs -----

	 private final int TAILLEBLOC = 5;
	 private Object[] tab;
	 private int prochainIndice;
	 
	// ----- Constructeur -----

	TabEnsemble()
	{
		this.tab = new Object[this.TAILLEBLOC];
		this.prochainIndice = 0;
	}

	// ----- Méthodes -----

	@Override
	public void add(Object o) {
		// TODO à compléter
	}

	@Override
	public void remove(Object o) {
		// TODO à compléter
	}

	@Override
	public int size() {
		
		return this.prochainIndice;
	}

	@Override
	public boolean contains(Object o) {
		for(Object obj: this.tab)
		{
			if(obj.equals(o)) return true;
		}
		return false; 
	}

	@Override
	public String toString() {
		String res = "[ ";
		
		for(int i=0; i<this.prochainIndice; i++)
		{
			res+= this.tab[i] + " ";
		}
		res += " ]";
		return res;
	}
}
