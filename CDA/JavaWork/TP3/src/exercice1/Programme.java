package exercice1;

/**
 * Classe de test.
 * <p>
 * 
 * Contient une méthode main et des fonctions de test.
 */
class Programme {

	static void affiche(Ensemble e) {
		affiche(e, null);
	}

	static void affiche(Ensemble e, Object o) {
		String contient = "";
		if (o != null)
			contient = (e.contains(o) ? ", contient " : ", ne contient pas ") + o;
		System.out.println(e.toString() + " " + e.size() + " élément" + (e.size() == 0 ? "" : "s" + contient));
	}

	static void test0(Ensemble e) {
		System.out.println("Ensemble initial :");
		affiche(e);		
	}

	public static void main(String[] args) {

		System.out.println("\n-------------------------------------------------------------");
		System.out.println("------- Tests de constructeurs, toString --------------------");
		System.out.println("-------------------------------------------------------------");
		test0(new TabEnsemble());

	}

}

/*

-------------------------------------------------------------
------- Tests de constructeurs, toString --------------------
-------------------------------------------------------------
Ensemble initial :
[ ] 0 élément

*/
