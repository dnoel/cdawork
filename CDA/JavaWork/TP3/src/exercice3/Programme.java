package exercice3;

/**
 * Classe de test.
 * <p>
 * 
 * Contient une méthode main et des fonctions de test.
 */
class Programme {

	static void affiche(Ensemble e) {
		affiche(e, null);
	}

	static void affiche(Ensemble e, Object o) {
		String contient = "";
		if (o != null)
			contient = (e.contains(o) ? ", contient " : ", ne contient pas ") + o;
		System.out.println(e.toString() + " " + e.size() + " élément" + (e.size() == 0 ? "" : "s" + contient));
	}

	static void test1(Ensemble e) {
		System.out.println("Ensemble initial :");
		affiche(e);		
		System.out.println("Ajout des entiers 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...");
		for (int i = 0; i < 10; i++)
			e.add(i);
		System.out.println("Deuxième ajout de 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...");
		for (int i = 0; i < 10; i++)
			e.add(i);
		System.out.println("Ensemble mis à jour :");
		affiche(e);
	}

	public static void main(String[] args) {

		System.out.println("\n-------------------------------------------------------------");
		System.out.println("------- Tests de constructeurs, toString, add et size -------");
		System.out.println("-------------------------------------------------------------");
		test1(new TabEnsemble());

	}

}

/*

-------------------------------------------------------------
------- Tests de constructeurs, toString, add et size -------
-------------------------------------------------------------
Ensemble initial :
[ ] 0 élément
Ajout des entiers 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...
Deuxième ajout de 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9...
Ensemble mis à jour :
[ 0 1 2 3 4 5 6 7 8 9 ] 10 éléments

*/
